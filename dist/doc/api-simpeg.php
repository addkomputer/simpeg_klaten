<?php
class pegawai_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function list_pegawai_all_simpeg($unitkerja_id='') {
        $api_url    = 'http://simpeg.kebumenkab.go.id/ws/index.php/api/data_pns/pns/unit_kerja/'.$unitkerja_id;
        $api_source = file_get_contents($api_url);
        $api_result = json_decode($api_source,true);
        //
        $no=1;
        foreach($api_result as $key => $val) {
            $api_result[$key]['no'] = $no;
            $api_result[$key]['pegawai_nm'] = $api_result[$key]['nama'];
            $api_result[$key]['tempat_lhr'] = $api_result[$key]['tempat_lahir'];
            $api_result[$key]['tgl_lhr'] = $api_result[$key]['tanggal_lahir'];
            $api_result[$key]['pegawai_nm_lengkap'] = $this->_create_nama_lengkap($api_result[$key]['gelar_depan'], $api_result[$key]['nama'], $api_result[$key]['gelar_belakang']);
            $api_result[$key]['unitkerja_nm'] = $api_result[$key]['unit_kerja'];
            $api_result[$key]['golongan_nm'] = $api_result[$key]['golongan'];
            $api_result[$key]['jabatan_nm'] = $api_result[$key]['jabatan'];
            //
            $api_result[$key]['unitkerja_id'] = $this->_get_unitkerja_by_nm($api_result[$key]['unit_kerja']);
            $api_result[$key]['golongan_id'] = $this->_get_golongan_by_nm($api_result[$key]['golongan']);
            $api_result[$key]['jabatan_id'] = $this->_get_jabatan_by_nm($api_result[$key]['jabatan']);
            $no++;
        }
        return $api_result;
    }

    function get_pegawai_simpeg($nip=null) {
        $api_url    = 'http://simpeg.kebumenkab.go.id/ws/index.php/api/data_pns/pns/id/'.$nip;
        $api_source = file_get_contents($api_url);
        $api_result = json_decode($api_source,true);
        $api_result['pegawai_nm'] = $api_result['nama'];
        $api_result['tempat_lhr'] = $api_result['tempat_lahir'];
        $api_result['tgl_lhr'] = $api_result['tanggal_lahir'];
        $api_result['unitkerja_id'] = $this->_get_unitkerja_by_nm($api_result['unit_kerja']);
        $api_result['golongan_id'] = $this->_get_golongan_by_nm($api_result['golongan']);
        $api_result['jabatan_id'] = $this->_get_jabatan_by_nm($api_result['jabatan']);
        return $api_result;
    }
    

}
