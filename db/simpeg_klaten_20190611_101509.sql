-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.37-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table simpeg_klaten.ap_aktivitas
CREATE TABLE IF NOT EXISTS `ap_aktivitas` (
  `id_aktivitas` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipe` varchar(50) NOT NULL DEFAULT '0',
  `id_pengguna` int(11) NOT NULL DEFAULT '0',
  `nama` varchar(50) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `platform` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_aktivitas`)
) ENGINE=InnoDB AUTO_INCREMENT=613 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ap_grup
CREATE TABLE IF NOT EXISTS `ap_grup` (
  `id_grup` int(11) NOT NULL AUTO_INCREMENT,
  `grup` varchar(50) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) NOT NULL DEFAULT 'System',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_grup`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ap_hak_akses
CREATE TABLE IF NOT EXISTS `ap_hak_akses` (
  `id_hak_akses` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` varchar(50) NOT NULL,
  `id_grup` int(11) NOT NULL,
  `_create` tinyint(1) NOT NULL,
  `_read` tinyint(1) NOT NULL,
  `_update` tinyint(1) NOT NULL,
  `_delete` tinyint(1) NOT NULL,
  `created` timestamp(1) NOT NULL DEFAULT CURRENT_TIMESTAMP(1),
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(50) NOT NULL DEFAULT 'System',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_hak_akses`)
) ENGINE=InnoDB AUTO_INCREMENT=3553 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ap_id
CREATE TABLE IF NOT EXISTS `ap_id` (
  `id_cpns` int(100) DEFAULT NULL,
  `id_pns` int(100) DEFAULT NULL,
  `id_blud` int(100) DEFAULT NULL,
  `id_p3k` int(100) DEFAULT NULL,
  `id_thl` int(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ap_menu
CREATE TABLE IF NOT EXISTS `ap_menu` (
  `id_menu` varchar(50) NOT NULL,
  `induk` varchar(50) DEFAULT NULL,
  `tipe` tinyint(1) NOT NULL DEFAULT '1',
  `menu` varchar(50) NOT NULL,
  `controller` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `url` varchar(50) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ap_pengguna
CREATE TABLE IF NOT EXISTS `ap_pengguna` (
  `id_pengguna` int(11) NOT NULL AUTO_INCREMENT,
  `id_grup` int(11) NOT NULL DEFAULT '0',
  `id_instansi` int(11) NOT NULL DEFAULT '0',
  `user_name` varchar(255) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_pengguna`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ap_profil
CREATE TABLE IF NOT EXISTS `ap_profil` (
  `id_profil` int(11) NOT NULL AUTO_INCREMENT,
  `nama_aplikasi` varchar(255) NOT NULL,
  `singkatan` varchar(255) NOT NULL,
  `pemda` varchar(255) NOT NULL,
  `nama_instansi` varchar(255) NOT NULL,
  `alamat_1` text NOT NULL,
  `alamat_2` text NOT NULL,
  `logo` text NOT NULL,
  `versi` varchar(20) NOT NULL,
  `id_kepala` varchar(255) DEFAULT NULL,
  `kop_ttd_kepala` text NOT NULL,
  `tahun` int(4) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_profil`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ap_service
CREATE TABLE IF NOT EXISTS `ap_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `time_start` time NOT NULL,
  `time_end` time NOT NULL,
  `loop_second` int(11) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_anak
CREATE TABLE IF NOT EXISTS `dt_anak` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `id_pegawai` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tgl_lahir` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_atasan
CREATE TABLE IF NOT EXISTS `dt_atasan` (
  `id_pegawai` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `id_atasan` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `id_periode` int(11) DEFAULT '0',
  `nip_atasan` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nip_app` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nip_ttd_tgs_tambahan` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plh_plt_pp` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_jab_plh_plt_pp` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plh_plt_app` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_jab_plh_plt_app` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_pegawai`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_file_riwayat_jabatan
CREATE TABLE IF NOT EXISTS `dt_file_riwayat_jabatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pegawai` varchar(50) NOT NULL DEFAULT '0',
  `id_riwayat` varchar(50) NOT NULL DEFAULT '0',
  `file` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_file_riwayat_pangkat
CREATE TABLE IF NOT EXISTS `dt_file_riwayat_pangkat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pegawai` varchar(50) NOT NULL DEFAULT '0',
  `id_riwayat` varchar(50) NOT NULL DEFAULT '0',
  `file` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_file_riwayat_pendidikan
CREATE TABLE IF NOT EXISTS `dt_file_riwayat_pendidikan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pegawai` varchar(50) NOT NULL DEFAULT '0',
  `id_riwayat` varchar(50) NOT NULL DEFAULT '0',
  `file` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_file_upload
CREATE TABLE IF NOT EXISTS `dt_file_upload` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipe` tinyint(1) NOT NULL DEFAULT '0',
  `id_pegawai` varchar(50) NOT NULL DEFAULT '0',
  `judul` varchar(50) NOT NULL DEFAULT '0',
  `file` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_gaji
CREATE TABLE IF NOT EXISTS `dt_gaji` (
  `id_gaji` int(11) NOT NULL AUTO_INCREMENT,
  `tahun` char(4) DEFAULT NULL,
  `bulan` char(2) DEFAULT NULL,
  `id_pegawai` varchar(20) NOT NULL,
  `jenjang_pend` varchar(20) NOT NULL,
  `tahun_kerja` char(4) NOT NULL,
  `bulan_kerja` char(2) NOT NULL,
  `kelompok_grade` varchar(20) DEFAULT NULL,
  `id_jabatan` varchar(20) DEFAULT NULL,
  `kd_jabatan_grade` varchar(20) DEFAULT NULL,
  `nilai_jabatan_grade` double DEFAULT NULL,
  `pir` double DEFAULT NULL,
  `total_remun` double DEFAULT NULL,
  `nilai_p1` double DEFAULT NULL,
  `nilai_p2` double DEFAULT NULL,
  `jumlah_p1` double DEFAULT NULL,
  `jumlah_p2` double DEFAULT NULL,
  `jumlah_p3` double DEFAULT NULL,
  `jumlah_total` double DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_gaji`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_gaji_dokter
CREATE TABLE IF NOT EXISTS `dt_gaji_dokter` (
  `id_gaji_dokter` int(11) NOT NULL AUTO_INCREMENT,
  `tahun` char(4) DEFAULT NULL,
  `bulan` char(2) DEFAULT NULL,
  `id_pegawai` varchar(20) DEFAULT NULL,
  `id_golongan` int(11) NOT NULL,
  `tahun_kerja` char(4) NOT NULL,
  `bulan_kerja` char(2) NOT NULL,
  `grade` double DEFAULT NULL,
  `nilai` double DEFAULT NULL,
  `jumlah_p1` double DEFAULT NULL,
  `jumlah_p2` double DEFAULT NULL,
  `jumlah_p3` double DEFAULT NULL,
  `jumlah_total` double DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_gaji_dokter`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_gaji_dokter_resume
CREATE TABLE IF NOT EXISTS `dt_gaji_dokter_resume` (
  `id_gaji_resume` int(11) NOT NULL AUTO_INCREMENT,
  `tahun` char(4) NOT NULL,
  `bulan` char(2) NOT NULL,
  `total_nilai` double DEFAULT NULL,
  `total_p1_sum` double DEFAULT NULL,
  `jasa_dibagi` double DEFAULT NULL,
  `jasa_dibagi_f1` double DEFAULT NULL,
  `total_jumlah_p1` double DEFAULT NULL,
  `nilai_pir` double DEFAULT NULL,
  `kesesuaian_p1` varchar(10) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_gaji_resume`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_gaji_resume
CREATE TABLE IF NOT EXISTS `dt_gaji_resume` (
  `id_gaji_resume` int(11) NOT NULL AUTO_INCREMENT,
  `tahun` char(4) NOT NULL,
  `bulan` char(2) NOT NULL,
  `total_nilai_jabatan_grade` double DEFAULT NULL,
  `jasa_dibagi` double DEFAULT NULL,
  `jasa_dibagi_f1` double DEFAULT NULL,
  `jasa_dibagi_f2` double DEFAULT NULL,
  `nominal_pir` double DEFAULT NULL,
  `nilai_pir` double DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_gaji_resume`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_keluarga
CREATE TABLE IF NOT EXISTS `dt_keluarga` (
  `id_pegawai` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nama_pasangan` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_lahir_pasangan` date NOT NULL,
  `tgl_perkawinan` date NOT NULL,
  `nip_pasangan` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pekerjaan_pasangan` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gaji_pasangan` float DEFAULT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `seri_karis` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_karis` date DEFAULT NULL,
  `jml_anak` int(11) DEFAULT NULL,
  `file_ktp` text COLLATE utf8_unicode_ci,
  `file_kk` text COLLATE utf8_unicode_ci,
  `file_karsu` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id_pegawai`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_keluarga_master
CREATE TABLE IF NOT EXISTS `dt_keluarga_master` (
  `id_pegawai` int(100) NOT NULL,
  `nama_pasangan` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nip_pasangan` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pekerjaan_pasangan` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seri_karis` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_karis` date DEFAULT NULL,
  `jml_anak` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_pegawai`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_pegawai
CREATE TABLE IF NOT EXISTS `dt_pegawai` (
  `id_pegawai` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `id_grup` tinyint(1) DEFAULT '3',
  `id_instansi` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'no-photo.jpg',
  `nomor_induk` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passwd` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nip_lama` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nik` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_ktp` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gelar_depan` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gelar_belakang` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tempat_lhr` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_lhr` date DEFAULT NULL,
  `agama` int(11) DEFAULT NULL,
  `jenis_kelamin` int(11) DEFAULT NULL,
  `alamat` text COLLATE utf8_unicode_ci,
  `warganegara` tinyint(1) DEFAULT '1',
  `id_kedudukan_pegawai` int(11) DEFAULT NULL,
  `id_status_pegawai` int(11) DEFAULT NULL,
  `tmt_cpns` date DEFAULT NULL,
  `tmt_pns` date DEFAULT NULL,
  `tmt_pegawai` date DEFAULT NULL,
  `seri_karpeg` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_karpeg` date DEFAULT NULL,
  `tingkat_pend_cpns` int(11) DEFAULT NULL,
  `pend_cpns` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tingkat_pend_akhir` int(11) DEFAULT NULL,
  `pend_terakhir` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thn_lulus_pend` int(4) DEFAULT NULL,
  `id_golongan` int(11) DEFAULT NULL,
  `tmt_gol` date DEFAULT NULL,
  `masakerja_gol_thn` int(11) DEFAULT NULL,
  `masakerja_gol_bln` int(11) DEFAULT NULL,
  `no_sk_gol` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_sk_gol` date DEFAULT NULL,
  `no_bkn_gol` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_bkn_gol` date DEFAULT NULL,
  `id_unit_kerja` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_jabatan` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_dokter` tinyint(1) DEFAULT '0',
  `tmt_jabatan` date DEFAULT NULL,
  `gaji_pokok` float DEFAULT NULL,
  `gaji_menurut` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jabatan_samping` text COLLATE utf8_unicode_ci,
  `gaji_samping` float DEFAULT NULL,
  `pensiun` float DEFAULT NULL,
  `usulan_pak_berikutnya` date DEFAULT NULL,
  `pak_berikutnya` date DEFAULT NULL,
  `pembebasan` date DEFAULT NULL,
  `pangkat_berikutnya` date DEFAULT NULL,
  `tmt_pangkat` date DEFAULT NULL,
  `sip` text COLLATE utf8_unicode_ci,
  `tgl_sip` date DEFAULT NULL,
  `tgl_sip_berlaku` date DEFAULT NULL,
  `str` text COLLATE utf8_unicode_ci,
  `tgl_str` date DEFAULT NULL,
  `tgl_str_berlaku` date DEFAULT NULL,
  `status_perkawinan` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kode_pos_rumah` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npwp` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `status_pupns_2015` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `no_registrasi_pupns` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kode_train` tinyint(4) DEFAULT NULL,
  `kode_lemari` tinyint(4) DEFAULT NULL,
  `kode_rak` tinyint(11) DEFAULT NULL,
  `status_kpe` int(11) DEFAULT NULL,
  `mutasi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tmt_mutasi` date DEFAULT NULL,
  `datauser` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `skp` int(11) DEFAULT NULL,
  `skp_locked` int(11) DEFAULT '0',
  `skp_locked_date` date DEFAULT NULL,
  `id_session` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_sertifikasi` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_hp` text COLLATE utf8_unicode_ci,
  `kd_tj` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_pak` text COLLATE utf8_unicode_ci,
  `file_str` text COLLATE utf8_unicode_ci,
  `file_sip` text COLLATE utf8_unicode_ci,
  `file_rkk` text COLLATE utf8_unicode_ci,
  `file_karpeg` text COLLATE utf8_unicode_ci,
  `is_regular` tinyint(1) DEFAULT '1',
  `is_pegawai` tinyint(1) DEFAULT '1',
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'System',
  `updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_pegawai`),
  KEY `pin` (`pin`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_perilaku_kerja
CREATE TABLE IF NOT EXISTS `dt_perilaku_kerja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pegawai` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nilai_op` float DEFAULT NULL,
  `nilai_integritas` float DEFAULT NULL,
  `nilai_komitmen` float DEFAULT NULL,
  `nilai_disiplin` float DEFAULT NULL,
  `nilai_kerjasama` float DEFAULT NULL,
  `nilai_kepemimpinan` float DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=FIXED;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_perilaku_kerja_manual
CREATE TABLE IF NOT EXISTS `dt_perilaku_kerja_manual` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pegawai` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nilai_op` float DEFAULT NULL,
  `nilai_integritas` float DEFAULT NULL,
  `nilai_komitmen` float DEFAULT NULL,
  `nilai_disiplin` float DEFAULT NULL,
  `nilai_kerjasama` float DEFAULT NULL,
  `nilai_kepemimpinan` float DEFAULT NULL,
  `ipk` float DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=FIXED;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_perilaku_kerja_tubel
CREATE TABLE IF NOT EXISTS `dt_perilaku_kerja_tubel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pegawai` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nilai_op` float DEFAULT NULL,
  `nilai_integritas` float DEFAULT NULL,
  `nilai_komitmen` float DEFAULT NULL,
  `nilai_disiplin` float DEFAULT NULL,
  `nilai_kerjasama` float DEFAULT NULL,
  `nilai_kepemimpinan` float DEFAULT NULL,
  `ipk` float DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=FIXED;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_proses_penilaian
CREATE TABLE IF NOT EXISTS `dt_proses_penilaian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pegawai` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tgl_keberatan` date DEFAULT NULL,
  `alasan_keberatan` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_tanggapan` date DEFAULT NULL,
  `alasan_tanggapan` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_kep_penilaian` date DEFAULT NULL,
  `alasan_kep_penilaian` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_rekomendasi` date DEFAULT NULL,
  `alasan_rekomendasi` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_riwayat_diklat
CREATE TABLE IF NOT EXISTS `dt_riwayat_diklat` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `id_pegawai` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `penyelenggara` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_sertifikat` char(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_sertifikat` date DEFAULT NULL,
  `nama_diklat` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `thn_diklat` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_unit_kerja` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lama_bln` int(11) NOT NULL,
  `lama_hari` int(11) NOT NULL,
  `lama_jam` int(11) NOT NULL,
  `file_sertifikat` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_riwayat_diklat_baru
CREATE TABLE IF NOT EXISTS `dt_riwayat_diklat_baru` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `id_pegawai` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `penyelenggara` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_sertifikat` char(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_sertifikat` date DEFAULT NULL,
  `nama_diklat` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `thn_diklat` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_unit_kerja` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lama_bln` int(11) NOT NULL,
  `lama_hari` int(11) NOT NULL,
  `lama_jam` int(11) NOT NULL,
  `file_sertifikat` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_riwayat_hukuman
CREATE TABLE IF NOT EXISTS `dt_riwayat_hukuman` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `id_pegawai` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `id_hukuman` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `tmt_hukuman` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_riwayat_jabatan
CREATE TABLE IF NOT EXISTS `dt_riwayat_jabatan` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `id_pegawai` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `idjabatan` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `eselon` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tmt_jabatan` date NOT NULL,
  `no_sk` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tgl_sk` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_riwayat_pangkat
CREATE TABLE IF NOT EXISTS `dt_riwayat_pangkat` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `id_pegawai` int(100) NOT NULL,
  `id_gol` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `tmt_riwayat_gol` date NOT NULL,
  `no_sk` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tgl_sk` date NOT NULL,
  `masakerja_thn` int(11) NOT NULL,
  `masakerja_bln` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_riwayat_pendidikan
CREATE TABLE IF NOT EXISTS `dt_riwayat_pendidikan` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `id_pegawai` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tingkat_pendidikan` int(11) NOT NULL,
  `nama_sekolah` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `jurusan` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `tgl_lulus` date NOT NULL,
  `no_ijazah` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_riwayat_penghargaan
CREATE TABLE IF NOT EXISTS `dt_riwayat_penghargaan` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `id_pegawai` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `penghargaan` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `thn_penghargaan` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_riwayat_penilaian
CREATE TABLE IF NOT EXISTS `dt_riwayat_penilaian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pegawai` varchar(50) NOT NULL DEFAULT '0',
  `judul` varchar(50) NOT NULL DEFAULT '0',
  `file` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_riwayat_ppk
CREATE TABLE IF NOT EXISTS `dt_riwayat_ppk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pegawai` varchar(50) DEFAULT NULL,
  `tahun` int(4) DEFAULT NULL,
  `nilai_capaian_skp` float DEFAULT NULL,
  `op` float DEFAULT NULL,
  `integritas` float DEFAULT NULL,
  `kom` float DEFAULT NULL,
  `dis` float DEFAULT NULL,
  `kerjasama` float DEFAULT NULL,
  `kepemimpinan` float DEFAULT NULL,
  `ratarata_perilaku` float DEFAULT NULL,
  `nilai_ppk` float DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_riwayat_usulan_diklat
CREATE TABLE IF NOT EXISTS `dt_riwayat_usulan_diklat` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `id_pegawai` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `id_nama_diklat` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thn` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_sertifikat` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_skum_anak1
CREATE TABLE IF NOT EXISTS `dt_skum_anak1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(50) NOT NULL DEFAULT '0',
  `id_pegawai` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `is_kawin` tinyint(1) NOT NULL,
  `sekolah` text NOT NULL,
  `beasiswa` tinyint(1) NOT NULL,
  `ayah` text NOT NULL,
  `ibu` text NOT NULL,
  `tgl` date NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_skum_anak2
CREATE TABLE IF NOT EXISTS `dt_skum_anak2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(50) NOT NULL DEFAULT '0',
  `id_pegawai` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `is_kawin` tinyint(1) NOT NULL,
  `sekolah` text NOT NULL,
  `beasiswa` tinyint(1) NOT NULL,
  `is_kerja` tinyint(1) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_tgs_kreatifitas
CREATE TABLE IF NOT EXISTS `dt_tgs_kreatifitas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pegawai` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tgs_kreatifitas` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nilai_tgs_kreatifitas` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_tgs_pokok
CREATE TABLE IF NOT EXISTS `dt_tgs_pokok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pegawai` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tgs` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `s_ak` float DEFAULT NULL,
  `s_kuant` int(11) DEFAULT NULL,
  `satuan_output` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `s_kual` int(11) DEFAULT NULL,
  `s_waktu` int(11) DEFAULT NULL,
  `satuan_waktu` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `t_ak` float DEFAULT NULL,
  `t_kuant` int(11) DEFAULT NULL,
  `t_waktu` int(11) DEFAULT NULL,
  `t_kual` int(11) DEFAULT NULL,
  `s_biaya` bigint(30) DEFAULT NULL,
  `t_biaya` bigint(30) DEFAULT NULL,
  `penghitungan` float DEFAULT NULL,
  `nilai_capaian_skp` float DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_tgs_tambahan
CREATE TABLE IF NOT EXISTS `dt_tgs_tambahan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pegawai` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tgs_tambahan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.dt_tingkat
CREATE TABLE IF NOT EXISTS `dt_tingkat` (
  `id_pegawai` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `id_tingkat` int(11) NOT NULL,
  PRIMARY KEY (`id_pegawai`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=FIXED;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.keys
CREATE TABLE IF NOT EXISTS `keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.master_mapping_shift
CREATE TABLE IF NOT EXISTS `master_mapping_shift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomor_induk` varchar(50) NOT NULL DEFAULT '0',
  `id_shift` int(11) NOT NULL DEFAULT '0',
  `kode` int(11) NOT NULL DEFAULT '0',
  `jam_masuk` time NOT NULL DEFAULT '00:00:00',
  `jam_batas_masuk` time NOT NULL DEFAULT '00:00:00',
  `jam_pulang` time NOT NULL DEFAULT '00:00:00',
  `is_over24` tinyint(1) NOT NULL DEFAULT '0',
  `jam_batas_pulang` time NOT NULL DEFAULT '00:00:00',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ms_eselon
CREATE TABLE IF NOT EXISTS `ms_eselon` (
  `id_eselon` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `eselon` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `id_duk` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_eselon`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ms_golongan
CREATE TABLE IF NOT EXISTS `ms_golongan` (
  `id_golongan` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `pangkat` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `golongan` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `gol_Tingkat` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_golongan`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ms_grade
CREATE TABLE IF NOT EXISTS `ms_grade` (
  `id_grade` int(11) NOT NULL AUTO_INCREMENT,
  `kelompok` varchar(60) NOT NULL,
  `jenjang_pend` varchar(20) NOT NULL,
  `gr_00` varchar(5) DEFAULT NULL,
  `gr_01` varchar(5) DEFAULT NULL,
  `gr_02` varchar(5) DEFAULT NULL,
  `gr_03` varchar(5) DEFAULT NULL,
  `gr_04` varchar(5) DEFAULT NULL,
  `gr_05` varchar(5) DEFAULT NULL,
  `gr_06` varchar(5) DEFAULT NULL,
  `gr_07` varchar(5) DEFAULT NULL,
  `gr_08` varchar(5) DEFAULT NULL,
  `gr_09` varchar(5) DEFAULT NULL,
  `gr_10` varchar(5) DEFAULT NULL,
  `gr_11` varchar(5) DEFAULT NULL,
  `gr_12` varchar(5) DEFAULT NULL,
  `gr_13` varchar(5) DEFAULT NULL,
  `gr_14` varchar(5) DEFAULT NULL,
  `gr_15` varchar(5) DEFAULT NULL,
  `gr_16` varchar(5) DEFAULT NULL,
  `gr_17` varchar(5) DEFAULT NULL,
  `gr_18` varchar(5) DEFAULT NULL,
  `gr_19` varchar(5) DEFAULT NULL,
  `gr_20` varchar(5) DEFAULT NULL,
  `gr_21` varchar(5) DEFAULT NULL,
  `gr_22` varchar(5) DEFAULT NULL,
  `gr_23` varchar(5) DEFAULT NULL,
  `gr_24` varchar(5) DEFAULT NULL,
  `gr_25` varchar(5) DEFAULT NULL,
  `gr_26` varchar(5) DEFAULT NULL,
  `gr_27` varchar(5) DEFAULT NULL,
  `gr_28` varchar(5) DEFAULT NULL,
  `gr_29` varchar(5) DEFAULT NULL,
  `gr_30` varchar(5) DEFAULT NULL,
  `gr_31` varchar(5) DEFAULT NULL,
  `gr_32` varchar(5) DEFAULT NULL,
  `gr_33` varchar(5) DEFAULT NULL,
  `gr_34` varchar(5) DEFAULT NULL,
  `gr_35` varchar(5) DEFAULT NULL,
  `gr_36` varchar(5) DEFAULT NULL,
  `gr_37` varchar(5) DEFAULT NULL,
  `gr_38` varchar(5) DEFAULT NULL,
  `gr_39` varchar(5) DEFAULT NULL,
  `gr_40` varchar(5) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_grade`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ms_grade_dokter
CREATE TABLE IF NOT EXISTS `ms_grade_dokter` (
  `id_grade_dokter` int(11) NOT NULL AUTO_INCREMENT,
  `grade` varchar(10) NOT NULL,
  `nilai` double NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_grade_dokter`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ms_hukuman_disiplin
CREATE TABLE IF NOT EXISTS `ms_hukuman_disiplin` (
  `id_hukuman_disiplin` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `hukuman_disiplin` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_hukuman_disiplin`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ms_instansi
CREATE TABLE IF NOT EXISTS `ms_instansi` (
  `id_instansi` int(11) NOT NULL AUTO_INCREMENT,
  `instansi` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat` text COLLATE utf8_unicode_ci,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_instansi`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ms_jabatan
CREATE TABLE IF NOT EXISTS `ms_jabatan` (
  `id_jabatan` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `jabatan` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `delegasi_kgb` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kepala_skpd` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rumpun` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tj` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'System',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_jabatan`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ms_jasa
CREATE TABLE IF NOT EXISTS `ms_jasa` (
  `id_jasa` int(11) NOT NULL AUTO_INCREMENT,
  `tahun` char(4) NOT NULL,
  `bulan` char(2) NOT NULL,
  `total_jasa` double NOT NULL,
  `total_jasa_dokter` double DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_jasa`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ms_jenis_diklat
CREATE TABLE IF NOT EXISTS `ms_jenis_diklat` (
  `id_diklat` int(11) NOT NULL AUTO_INCREMENT,
  `no_jenis_diklat` varchar(5) NOT NULL,
  `nama_diklat` varchar(200) NOT NULL,
  `rumpun_diklat` varchar(50) NOT NULL,
  PRIMARY KEY (`id_diklat`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=173 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ms_kecamatan
CREATE TABLE IF NOT EXISTS `ms_kecamatan` (
  `id_kecamatan` int(11) NOT NULL AUTO_INCREMENT,
  `kecamatan` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_kecamatan`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ms_libur
CREATE TABLE IF NOT EXISTS `ms_libur` (
  `id_libur` int(11) NOT NULL AUTO_INCREMENT,
  `libur` varchar(255) NOT NULL,
  `tanggal` date NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_libur`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ms_nilai_grade
CREATE TABLE IF NOT EXISTS `ms_nilai_grade` (
  `id_nilai_grade` int(11) NOT NULL AUTO_INCREMENT,
  `kelompok` varchar(60) NOT NULL,
  `jenjang_pend_str` varchar(255) DEFAULT NULL,
  `min` varchar(10) NOT NULL,
  `max` varchar(10) NOT NULL,
  `kd_jabatan_grade` varchar(10) NOT NULL,
  `gr_00` varchar(10) DEFAULT NULL,
  `gr_01` varchar(10) DEFAULT NULL,
  `gr_02` varchar(10) DEFAULT NULL,
  `gr_03` varchar(10) DEFAULT NULL,
  `gr_04` varchar(10) DEFAULT NULL,
  `gr_05` varchar(10) DEFAULT NULL,
  `gr_06` varchar(10) DEFAULT NULL,
  `gr_07` varchar(10) DEFAULT NULL,
  `gr_08` varchar(10) DEFAULT NULL,
  `gr_09` varchar(10) DEFAULT NULL,
  `gr_10` varchar(10) DEFAULT NULL,
  `gr_11` varchar(10) DEFAULT NULL,
  `gr_12` varchar(10) DEFAULT NULL,
  `gr_13` varchar(10) DEFAULT NULL,
  `gr_14` varchar(10) DEFAULT NULL,
  `gr_15` varchar(10) DEFAULT NULL,
  `gr_16` varchar(10) DEFAULT NULL,
  `gr_17` varchar(10) DEFAULT NULL,
  `gr_18` varchar(10) DEFAULT NULL,
  `gr_19` varchar(10) DEFAULT NULL,
  `gr_20` varchar(10) DEFAULT NULL,
  `gr_21` varchar(10) DEFAULT NULL,
  `gr_22` varchar(10) DEFAULT NULL,
  `gr_23` varchar(10) DEFAULT NULL,
  `gr_24` varchar(10) DEFAULT NULL,
  `gr_25` varchar(10) DEFAULT NULL,
  `gr_26` varchar(10) DEFAULT NULL,
  `gr_27` varchar(10) DEFAULT NULL,
  `gr_28` varchar(10) DEFAULT NULL,
  `gr_29` varchar(10) DEFAULT NULL,
  `gr_30` varchar(10) DEFAULT NULL,
  `gr_31` varchar(10) DEFAULT NULL,
  `gr_32` varchar(10) DEFAULT NULL,
  `gr_33` varchar(10) DEFAULT NULL,
  `gr_34` varchar(10) DEFAULT NULL,
  `gr_35` varchar(10) DEFAULT NULL,
  `gr_36` varchar(10) DEFAULT NULL,
  `gr_37` varchar(10) DEFAULT NULL,
  `gr_38` varchar(10) DEFAULT NULL,
  `gr_39` varchar(10) DEFAULT NULL,
  `gr_40` varchar(10) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_nilai_grade`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ms_periode_penilaian
CREATE TABLE IF NOT EXISTS `ms_periode_penilaian` (
  `id` int(11) NOT NULL,
  `tgl_cetak_skp` date NOT NULL,
  `awal_penilaian` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `akhir_penilaian` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `periode_penilaian` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tgl_penilaian_target` date DEFAULT NULL,
  `tgl_diterima_ybs` date DEFAULT NULL,
  `tgl_nilai_dibuat` date DEFAULT NULL,
  `tgl_diterima_app` date DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ms_potongan
CREATE TABLE IF NOT EXISTS `ms_potongan` (
  `id_potongan` int(11) NOT NULL AUTO_INCREMENT,
  `potongan` varchar(50) NOT NULL,
  `batas_menit_atas` int(11) NOT NULL,
  `batas_menit_bawah` int(11) NOT NULL,
  `periode` varchar(50) NOT NULL DEFAULT '0',
  `is_cuti` tinyint(1) NOT NULL DEFAULT '0',
  `is_tmktk` tinyint(1) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(50) NOT NULL DEFAULT 'System',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_potongan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ms_ruang
CREATE TABLE IF NOT EXISTS `ms_ruang` (
  `id_ruang` int(11) NOT NULL AUTO_INCREMENT,
  `ruang` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ms_rumpun_jabatan
CREATE TABLE IF NOT EXISTS `ms_rumpun_jabatan` (
  `id_rumpun_jabatan` int(11) NOT NULL AUTO_INCREMENT,
  `rumpun_jabatan` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_rumpun_jabatan`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ms_shift
CREATE TABLE IF NOT EXISTS `ms_shift` (
  `id_shift` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(50) NOT NULL DEFAULT '0',
  `shift` varchar(50) NOT NULL,
  `jam_datang` time NOT NULL,
  `jam_batas_datang` time NOT NULL,
  `jam_pulang` time NOT NULL,
  `jam_batas_pulang` time NOT NULL,
  `is_over24` tinyint(1) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(50) NOT NULL DEFAULT 'System',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_shift`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ms_status_pegawai
CREATE TABLE IF NOT EXISTS `ms_status_pegawai` (
  `id_status_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `status_pegawai` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(50) NOT NULL DEFAULT 'System',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_status_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.ms_tingkat
CREATE TABLE IF NOT EXISTS `ms_tingkat` (
  `id_tingkat` int(11) NOT NULL AUTO_INCREMENT,
  `tingkat` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `id_jenjang` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_tingkat`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.tb_device
CREATE TABLE IF NOT EXISTS `tb_device` (
  `id_device` int(11) NOT NULL AUTO_INCREMENT,
  `server_IP` varchar(50) NOT NULL DEFAULT '192.168.8.100',
  `server_port` varchar(50) NOT NULL DEFAULT '8080',
  `device_sn` varchar(50) NOT NULL,
  `device_no` int(11) NOT NULL,
  `device_IP` varchar(50) NOT NULL,
  `device_key` text NOT NULL,
  `jadwal1` time NOT NULL,
  `jadwal2` time NOT NULL,
  `jadwal3` time NOT NULL,
  `lokasi` text NOT NULL,
  `all_data` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_device`),
  KEY `device_sn` (`device_sn`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.tb_device_bu
CREATE TABLE IF NOT EXISTS `tb_device_bu` (
  `id_device` int(11) NOT NULL AUTO_INCREMENT,
  `server_IP` text NOT NULL,
  `server_port` text NOT NULL,
  `device_sn` text NOT NULL,
  `device_no` int(11) NOT NULL,
  `lokasi` text NOT NULL,
  `jadwal1` time NOT NULL,
  `jadwal2` time NOT NULL,
  `jadwal3` time NOT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_device`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.tb_log_jaldin_2019
CREATE TABLE IF NOT EXISTS `tb_log_jaldin_2019` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bulan` varchar(2) NOT NULL,
  `id_pegawai` varchar(50) NOT NULL DEFAULT '0',
  `nomor_induk` varchar(50) DEFAULT '0',
  `pin` varchar(50) DEFAULT '0',
  `id_instansi` int(11) DEFAULT '0',
  `tanggal` date NOT NULL,
  `id_tipe` char(2) DEFAULT NULL,
  `file_bukti` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.tb_log_kehadiran_2019
CREATE TABLE IF NOT EXISTS `tb_log_kehadiran_2019` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bulan` varchar(2) NOT NULL,
  `id_pegawai` varchar(50) DEFAULT '0',
  `nomor_induk` varchar(50) DEFAULT '0',
  `pin` varchar(50) DEFAULT '0',
  `id_instansi` int(11) DEFAULT '0',
  `id_tipe` int(1) DEFAULT '-1',
  `id_shift` int(11) DEFAULT '7',
  `is_tukar` tinyint(1) DEFAULT '0',
  `kode` varchar(50) DEFAULT 'P3',
  `tanggal` date DEFAULT NULL,
  `jam_datang` time DEFAULT NULL,
  `jam_batas_datang` time DEFAULT NULL,
  `is_datang` tinyint(1) NOT NULL DEFAULT '0',
  `jam_datang_pegawai` time DEFAULT NULL,
  `terlambat_datang` int(11) DEFAULT '0',
  `ket_datang` text,
  `is_pulang` tinyint(1) NOT NULL DEFAULT '0',
  `jam_pulang` time DEFAULT NULL,
  `jam_batas_pulang` time DEFAULT NULL,
  `jam_pulang_pegawai` time DEFAULT NULL,
  `mendahului_pulang` int(11) DEFAULT '0',
  `ket_pulang` text,
  `file_bukti` varchar(100) DEFAULT NULL,
  `keterangan` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.tb_log_tipe
CREATE TABLE IF NOT EXISTS `tb_log_tipe` (
  `id_tipe` varchar(50) NOT NULL,
  `tipe` text NOT NULL,
  `table` varchar(50) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_tipe`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.tb_template
CREATE TABLE IF NOT EXISTS `tb_template` (
  `pin` varchar(32) NOT NULL,
  `finger_idx` tinyint(4) NOT NULL,
  `alg_ver` tinyint(4) NOT NULL COMMENT 'ZK : 9&10, Realand : 19, Ebio : 29, HY : 39',
  `template` text NOT NULL,
  PRIMARY KEY (`pin`,`finger_idx`,`alg_ver`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten.tb_user
CREATE TABLE IF NOT EXISTS `tb_user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `pin` text NOT NULL,
  `nama` text NOT NULL,
  `pwd` text NOT NULL,
  `rfid` text NOT NULL,
  `privilege` int(11) NOT NULL,
  `device_sn` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
-- Dumping structure for table simpeg_klaten._2019_01
CREATE TABLE IF NOT EXISTS `_2019_01` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_pegawai` varchar(50) NOT NULL DEFAULT '0',
  `nomor_induk` varchar(50) DEFAULT '0',
  `pin` varchar(50) DEFAULT '0',
  `id_instansi` int(11) DEFAULT '0',
  `tanggal` date NOT NULL,
  `id_tipe` char(2) DEFAULT NULL,
  `file_bukti` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL DEFAULT 'System',
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
