<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-<?=$access->icon?>"></i> <?=$access->menu?>
      <small><?=$subtitle?></small>
      <div class="pull-right">
        <div class="badge bg-green">Total Keseluruhan : <?=$num_rows_all  ?></div>
      </div>
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box box-default">
      <div class="box-body table-responsive">
        <a class="btn btn-sm btn-primary pull-left" href="<?=base_url()?>ms_lokasi/form"><i class="fa fa-plus"></i> Tambah</a>
        <div>
          <form action="<?=base_url()?>ms_lokasi/index" method="post">
            <div class="input-group input-group-sm pull-right" style="width: 250px;">
              <input type="text" name="role_name" class="form-control pull-right" placeholder="Pencarian" value="">
              <div class="input-group-btn">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                <a class="btn btn-default" href="<?=base_url()?>ms_lokasi/reset_search"><i class="fa fa-refresh"></i></a>
              </div>
            </div>
          </form>
        </div>
        <br><br>
        <?php echo $this->session->flashdata('status'); ?>
        <?php if ($this->session->userdata('search')): ?>
          <i class="search_result">Hasil pencarian dengan kata kunci: <b><?=$search['role_name'];?></b></i><br><br>
        <?php endif; ?>
        <table class="table table-striped table-bordered table-condensed">
          <thead>
            <tr>
              <th class="text-center" width="30">No.</th>
              <th class="text-center" width="60">Aksi</th>
              <th class="text-center">Nama</th>
              <th class="text-center" width="150">Dibuat</th>
              <th class="text-center" width="150">Diperbarui</th>
              <th class="text-center" width="30">Status</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($lokasi != null): ?>
              <?php $i=1;foreach ($lokasi as $row): ?>
                <tr>
                  <td class="text-center"><?=$this->uri->segment('3')+$i++?></td>
                  <td class="text-center">
                    <?php if($row->id_lokasi != null): ?>
                      <a class="text-orange" href="<?=base_url()?>ms_lokasi/form/<?=$row->id_lokasi?>" style="padding-right:0.40em;"><i class="fa fa-pencil"></i></a> 
                      <a class="text-red" href="#" onclick="del(<?=$row->id_lokasi?>)"><i class="fa fa-trash"></i></a>
                    <?php endif;?>
                  </td>
                  <td><?=$row->lokasi?></td>
                  <td class="text-center"><?=$row->created?></td>
                  <td class="text-center"><?=$row->updated?></td>
                  <td class="text-center">
                    <?php if ($row->is_active == 1): ?>
                      <i class="fa fa-check text-green"></i>
                    <?php else: ?>
                      <i class="fa fa-close text-red"></i>
                    <?php endif; ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td class="text-center" colspan="6">Tidak ada data!</td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <span class="badge bg-orange" style="margin-top:6px;">Jumlah Data : <?=$num_rows?></span>
        <?php echo $this->pagination->create_links(); ?>
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- Modal Delete -->
<div id="modal_delete" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>ms_lokasi/delete" method="post">
        <input type="hidden" name="id_lokasi" id="id_lokasi">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
  function del(id) {
    $("#modal_delete").modal('show');
    $("#id_lokasi").val(id);
  }
</script>