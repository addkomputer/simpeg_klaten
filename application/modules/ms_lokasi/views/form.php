<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?>"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-primary">
        <form id="form" class="form-horizontal" action="<?=base_url()?>ms_lokasi/<?=$action?>" method="post" autocomplete="off"> 
          <div class="box-header with-border">
            <h3 class="box-title">Form <?=$access->menu?></h3>
          </div>
          <div class="box-body">
            <input type="hidden" class="form-control input-sm" name="id_lokasi" id="id_lokasi" value="<?php if($lokasi != null){echo $lokasi->id_lokasi;};?>">
            <div class="form-group">
              <label class="col-sm-2 control-label">Nama</label>
              <div class="col-sm-3">
                <input type="text" class="form-control input-sm" name="lokasi" id="lokasi" value="<?php if($lokasi != null){echo $lokasi->lokasi;};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Tipe</label>
              <div class="col-sm-2">
                <select class="form-control select2" name="tipe" id="tipe">
                  <option value="0">-- Pilih --</option>
                  <option value="1">Ruang</option>
                  <option value="2">Instalasi</option>
                  <option value="3">Unit</option>
                  <option value="4">Seksi</option>
                  <option value="5">Subbag</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Aktif</label>
              <div class="col-sm-5">
                <input class="iCheckbox" type="checkbox" name="is_active" value="1" <?php if($lokasi != null){if($lokasi->is_active == 1){echo 'checked';}}else{echo 'checked';}?>>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-offset-2 col-sm-8">
                <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
              </div>
            </div>
          </div>
        </form>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<script>
  $(document).ready(function () {
    // form validator
    $('#form').validate({
      rules : {
        tipe : {
          valueNotEquals : '0'
        }
      }
    });
  })
</script>