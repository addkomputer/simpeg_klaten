<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ms_hukuman_disiplin extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_hukuman_disiplin')
      ->get('ms_hukuman_disiplin',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_hukuman_disiplin')
      ->get('ms_hukuman_disiplin')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->get('ms_hukuman_disiplin')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id_hukuman_disiplin',$id)->get('ms_hukuman_disiplin')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_hukuman_disiplin','asc')->get('ms_hukuman_disiplin')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_hukuman_disiplin','desc')->get('ms_hukuman_disiplin')->row();
  }

  public function insert($data)
  {
    $this->db->insert('ms_hukuman_disiplin',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_hukuman_disiplin',$id)->update('ms_hukuman_disiplin',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id_hukuman_disiplin',$id)->update('ms_hukuman_disiplin',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_hukuman_disiplin',$id)->delete('ms_hukuman_disiplin');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ms_hukuman_disiplin");
  }

}
