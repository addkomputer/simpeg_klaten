<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dt_cpns extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
    $profil = $this->db->get('ap_profil')->row();
		if ($search != null) {
      if ($search['term'] != '') {
        $this->db->like('a.nama',$search['term']);
      }
      if ($search['id_instansi'] != '') {
        $this->db->where('a.id_instansi',$search['id_instansi']);
      }
      if ($search['id_jabatan'] != '') {
        $this->db->where('a.id_jabatan',$search['id_jabatan']);
      }
    }
    
    return $this->db
      ->join('ms_jabatan b', 'a.id_jabatan = b.id_jabatan','left')
      ->join('ms_eselon c', 'c.id_eselon = right(a.id_jabatan,2)','left')
      ->join('ms_golongan d', 'a.id_golongan = d.id_golongan','left')
      ->join('ms_status_pegawai e', 'a.id_status_pegawai = e.id_status_pegawai')
      ->join('ms_instansi f', 'a.id_instansi = f.id_instansi','left')
      ->where('e.id_status_pegawai', 1)
      ->where('a.is_deleted', 0)
      ->where('a.is_pegawai',1)
      ->order_by('a.id_golongan','desc')
      ->order_by('a.id_pegawai')
      ->get('dt_pegawai a',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		$profil = $this->db->get('ap_profil')->row();

		if ($search != null) {
      if ($search['term'] != '') {
        $this->db->like('a.nama',$search['term']);
      }
      if ($search['id_instansi'] != '') {
        $this->db->where('a.id_instansi',$search['id_instansi']);
      }
      if ($search['id_jabatan'] != '') {
        $this->db->where('a.id_jabatan',$search['id_jabatan']);
      }
    }
    
    return $this->db
      ->join('ms_jabatan b', 'a.id_jabatan = b.id_jabatan','left')
      ->join('ms_eselon c', 'c.id_eselon = right(a.id_jabatan,2)','left')
      ->join('ms_golongan d', 'a.id_golongan = d.id_golongan','left')
      ->join('ms_status_pegawai e', 'a.id_status_pegawai = e.id_status_pegawai')
      ->join('ms_instansi f', 'a.id_instansi = f.id_instansi','left')
      ->where('e.id_status_pegawai', 1)
      ->where('a.is_deleted', 0)
      ->where('a.is_pegawai',1)
      ->order_by('a.id_golongan','desc')
      ->order_by('a.id_pegawai')
      ->get('dt_pegawai a')
      ->num_rows();
  }
  
  function num_rows_all(){
    $profil = $this->db->get('ap_profil')->row();

    return $this->db
      ->join('ms_jabatan b', 'a.id_jabatan = b.id_jabatan','left')
      ->join('ms_eselon c', 'c.id_eselon = right(a.id_jabatan,2)','left')
      ->join('ms_golongan d', 'a.id_golongan = d.id_golongan','left')
      ->join('ms_status_pegawai e', 'a.id_status_pegawai = e.id_status_pegawai')
      ->join('ms_instansi f', 'a.id_instansi = f.id_instansi','left')
      ->where('e.id_status_pegawai', 1)
      ->where('a.is_deleted', 0)
      ->where('a.is_pegawai',1)
      ->order_by('a.id_golongan','desc')
      ->order_by('a.id_pegawai')
      ->get('dt_pegawai a')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('dt_pegawai a')->result();
  }
  
  public function get_by_id_pegawai($id)
  {
    return $this->db
      ->where('a.id_pegawai',$id)
      ->order_by('a.id','desc')
      ->get('dt_pegawai a a')
      ->result();
  }

  public function get_by_id($id)
  {
    return $this->db
      ->select('a.*,b.jabatan,c.eselon,d.golongan,d.pangkat,e.status_pegawai,f.instansi')
      ->join('ms_jabatan b', 'a.id_jabatan = b.id_jabatan','left')
      ->join('ms_eselon c', 'c.id_eselon = right(a.id_jabatan,2)','left')
      ->join('ms_golongan d', 'a.id_golongan = d.id_golongan','left')
      ->join('ms_status_pegawai e', 'a.id_status_pegawai = e.id_status_pegawai','left')
      ->join('ms_instansi f', 'a.id_instansi = f.id_instansi','left')
      ->where('id_pegawai',$id)
      ->get('dt_pegawai a')->row();
  }

  public function pin_max()
  {
    $query = $this->db->query("SELECT MAX(pin) as pin_max FROM dt_pegawai WHERE pin LIKE '2%'")->row();
    if ($query->pin_max == NULL) {
      return "20001";
    }else{
      return intval($query->pin_max)+1;
    }
  }

  public function get_jabatan_by_id($id)
  {
    return $this->db
      ->select('a.*,substr(a.id_jabatan,16,2) AS id_rumpun_jabatan')
      ->where('a.id_pegawai',$id)
      ->get('dt_pegawai a')->row();
  }

  public function get_tingkat_by_id($id)
  {
    return $this->db
      ->where('id_pegawai',$id)
      ->get('dt_tingkat')
      ->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_pegawai','asc')->get('dt_pegawai a')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_pegawai','desc')->get('dt_pegawai a')->row();
  }

  public function insert($data)
  {
    $this->db->insert('dt_pegawai',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_pegawai',$id)->update('dt_pegawai a',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id_pegawai',$id)->update('dt_pegawai a',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_pegawai',$id)->delete('dt_pegawai a');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE dt_pegawai a");
  }

}
