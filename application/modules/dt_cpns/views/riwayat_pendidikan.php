<?php 
  function get_pendidikan($id)
  {
    switch ($id) {
      case 1:
        return 'SD / PAKET A';
        break;
      case 2: 
        return 'SMP / PAKET B';
        break;
      case 3: 
        return 'SMA / SMK / PAKET C';
        break;
      case 4: 
        return 'D-1 ';
        break;
      case 5: 
        return 'D-2 ';
        break;
      case 6: 
        return 'D-3 ';
        break;
      case 7: 
        return 'D-4 ';
        break;
      case 8: 
        return 'S-1 ';
        break;
      case 9: 
        return 'S-2 ';
        break;
      case 10: 
        return 'S-3 ';
        break;
    }
  }
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <?php $this->load->view('dt_pns/sidebar'); ?>
        </div>
        <div class="col-md-9">
          <?php $this->load->view('dt_pns/header'); ?>
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title text-orange">Riwayat Pendidikan</h3>
            </div>
            <div class="box-body">
              <?php echo $this->session->flashdata('status'); ?>
              <form id="form" class="form-horizontal" enctype="multipart/form-data" action="<?=base_url().$access->controller?>/riwayat_pendidikan_action/<?=$action?>" method="post" autocomplete="off"> 
                <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
                <input type="hidden" name="id" id="id" value="<?php if($pendidikan){echo $pendidikan->id;}?>">
                <div class="form-group">
                  <label class="col-md-2 control-label">Pendidikan</label>
                  <div class="col-md-3">
                    <select class="form-control select2" name="tingkat_pendidikan" id="tingkat_pendidikan">
                      <option value="0" <?php if($pendidikan){if($pendidikan->tingkat_pendidikan == 0){echo 'selected';}}?>> -- Pilih -- </option>
                      <option value="1" <?php if($pendidikan){if($pendidikan->tingkat_pendidikan == 1){echo 'selected';}}?>> SD / PAKET A</option>
                      <option value="2" <?php if($pendidikan){if($pendidikan->tingkat_pendidikan == 2){echo 'selected';}}?>> SMP / PAKET B</option>
                      <option value="3" <?php if($pendidikan){if($pendidikan->tingkat_pendidikan == 3){echo 'selected';}}?>> SMA / SMK / PAKET C</option>
                      <option value="4" <?php if($pendidikan){if($pendidikan->tingkat_pendidikan == 4){echo 'selected';}}?>> D-1 </option>
                      <option value="5" <?php if($pendidikan){if($pendidikan->tingkat_pendidikan == 5){echo 'selected';}}?>> D-2 </option>
                      <option value="6" <?php if($pendidikan){if($pendidikan->tingkat_pendidikan == 6){echo 'selected';}}?>> D-3 </option>
                      <option value="7" <?php if($pendidikan){if($pendidikan->tingkat_pendidikan == 7){echo 'selected';}}?>> D-4 </option>
                      <option value="8" <?php if($pendidikan){if($pendidikan->tingkat_pendidikan == 8){echo 'selected';}}?>> S-1 </option>
                      <option value="9" <?php if($pendidikan){if($pendidikan->tingkat_pendidikan == 9){echo 'selected';}}?>> S-2 </option>
                      <option value="10" <?php if($pendidikan){if($pendidikan->tingkat_pendidikan == 10){echo 'selected';}}?>> S-3 </option>
                    </select>
                  </div>
                  <label class="col-md-2 control-label" style="width:max-content !important;">Jurusan</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="jurusan" id="jurusan" value="<?php if($pendidikan){echo $pendidikan->jurusan;}?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Nama Sekolah</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="nama_sekolah" id="nama_sekolah" value="<?php if($pendidikan){echo $pendidikan->nama_sekolah;}?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">No. Ijazah</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control" name="no_ijazah" id="no_ijazah" value="<?php if($pendidikan){echo $pendidikan->no_ijazah;}?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Tanggal Lulus</label>
                  <div class="col-md-2">
                    <input type="text" class="form-control datepicker" name="tgl_lulus" id="tgl_lulus" value="<?php if($pendidikan){echo date_to_id($pendidikan->tgl_lulus);}?>" placeholder="dd-mm-yyyy" required>
                  </div>
                </div>
                <?php if($pendidikan): ?>
                  <?php foreach($pendidikan->file as $row): ?>
                    <div class="form-group">
                      <div class="col-md-5 col-md-offset-2">
                        <?=$row->file?>
                      </div>
                      <div class="col-md-2">
                        <button class="btn btn-xs btn-danger del-file" onclick="del_file('<?=$row->id?>','<?=$pendidikan->id?>')" type="button"><i class="fa fa-minus"></i> Hapus File</a>
                      </div>
                    </div>
                  <?php endforeach;?>
                <?php endif;?> 
                <div id="files">
                
                </div>
                <div class="form-group">
                  <div class="col-md-2 col-md-offset-2">
                    <button class="btn btn-xs btn-default" type="button" onclick="add_file()"><i class="fa fa-plus"></i> Tambah File</button>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-8 col-md-offset-2">
                    <em>Sebelum mengunggah file, pastikan nama file yang akan diunggah sudah benar sesuai dengan isi file. Jenis file yang diterima adalah *.jpg,*.png,*.jpeg dan *.pdf</em>
                  </div>
                </div>
                <hr>
                <div class="form-group">
                  <div class="col-md-offset-2 col-sm-8">
                    <button class="btn btn-success btn-flat" type="submit" value='Upload' name='upload'><i class="fa fa-save"></i> Simpan</button>
                    <a class="btn btn-default btn-flat" href="<?=base_url()?>dt_pns/riwayat_pendidikan/<?=$pegawai->id_pegawai?>"><i class="fa fa-close"></i> Batal</a>
                  </div>
                </div>
                <hr>
              </form>
              <table class="table table-striped table-bordered table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" width="30">No.</th>
                    <th class="text-center" width="60">Aksi</th>
                    <th class="text-center" width="200">Pendidikan</th>
                    <th class="text-center">Nama Sekolah</th>
                    <th class="text-center" width="100">Tanggal Lulus</th>
                    <th class="text-center" width="200">No. Ijazah</th>
                    <th class="text-center" width="80">File</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if ($riwayat != null): ?>
                    <?php $i=1;foreach ($riwayat as $row): ?>
                      <tr>
                        <td class="text-center"><?=$i++?></td>
                        <td class="text-center">
                          <?php if($row->id != 0): ?>
                            <a class="btn btn-flat btn-xs btn-warning" href="<?=base_url()?>dt_pns/riwayat_pendidikan/<?=$pegawai->id_pegawai?>/<?=$row->id?>" style="padding-right:0.250em;"><i class="fa fa-pencil"></i></a> 
                            <a class="btn btn-flat btn-xs btn-danger" href="#" onclick="del(<?=$row->id?>)"><i class="fa fa-trash"></i></a>
                          <?php endif;?>
                        </td>
                        <td class="text-center"><?=get_pendidikan($row->tingkat_pendidikan).' - '.$row->jurusan?></td>
                        <td><?=$row->nama_sekolah?></td>
                        <td class="text-center"><?=date_to_id($row->tgl_lulus)?></td>
                        <td><?=$row->no_ijazah?></td>
                        <td class="text-center"><a class="text-green">
                          <?php foreach($row->file as $row2): ?>
                            <a target="_blank" href="<?=base_url()?>berkas/riwayat_pendidikan/<?=$row2->file?>" title="<?=$row2->file?>"><i class="fa fa-download"></i></a> 
                          <?php endforeach;?>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <tr>
                      <td class="text-center" colspan="99">Data tidak ada!</td>
                    </tr>
                  <?php endif; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
<!-- /.content-wrapper -->
<!-- Modal Delete -->
<div id="modal_delete" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>dt_pns/riwayat_pendidikan_action/delete" method="post">
        <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
        <input type="hidden" name="id" id="id_delete">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn  btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn  btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div id="modal_delete_file" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>dt_pns/file_riwayat_pendidikan_action/delete" method="post">
        <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
        <input type="hidden" name="id" id="id_delete_file">
        <input type="hidden" name="id_riwayat" id="id_riwayat">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus File</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin file ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Tindakan ini tidak bisa dibatalkan.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn  btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn  btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
  $(document).ready(function () {
    $('#form').validate({
      rules : {
        tingkat_pendidikan : {
          valueNotEquals : '0'
        }
      }
    })
  })
  function del(id) {
    $("#modal_delete").modal('show');
    $("#id_delete").val(id);
  }
  function del_file(id,id_riwayat) {
    $("#modal_delete_file").modal('show');
    $("#id_delete_file").val(id);
    $("#id_riwayat").val(id_riwayat);
  }
  function add_file() {
    html =  '<div class="form-group row-file">'+
              '<label class="col-md-2 control-label">File Scan</label>'+
              '<div class="col-md-5">'+
                '<input type="file" class="" name="files[]" value="">'+
              '</div>'+
              '<div class="col-md-2">'+
                '<button class="btn btn-xs btn-danger del-file" type="button"><i class="fa fa-minus"></i> Hapus File</button>'+
              '</div>'+
            '</div>';
    $('#files').append(html);
  }
</script>