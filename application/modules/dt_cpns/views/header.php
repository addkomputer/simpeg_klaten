<div class="box box-primary">
  <div class="box-body">
    <a class="btn btn-app btn-default btn-flat" href="<?= base_url().$access->controller;?>/data_pokok/<?=@$pegawai->id_pegawai?>">
      <i class="fa fa-database"></i> Data Utama
    </a>
    <a class="btn btn-app btn-default btn-flat update" href="<?= base_url().$access->controller;?>/riwayat_pangkat/<?=@$pegawai->id_pegawai?>">
      <i class="fa fa-user"></i> Pangkat / Gol
    </a>
    <a class="btn btn-app btn-default btn-flat update" href="<?= base_url().$access->controller;?>/riwayat_pendidikan/<?=@$pegawai->id_pegawai?>">
      <i class="fa fa-graduation-cap"></i> Pendidikan
    </a>
    <a class="btn btn-app btn-default btn-flat update" href="<?= base_url().$access->controller;?>/riwayat_jabatan/<?=@$pegawai->id_pegawai?>">
      <i class="fa fa-id-card-o"></i> Jabatan
    </a>
    <a class="btn btn-app btn-default btn-flat update" href="<?= base_url().$access->controller;?>/riwayat_diklat/<?=@$pegawai->id_pegawai?>">
      <i class="fa fa-book"></i> Diklat
    </a>
    <a class="btn btn-app btn-default btn-flat update" href="<?= base_url().$access->controller;?>/riwayat_penghargaan/<?=@$pegawai->id_pegawai?>">
      <i class="fa fa-trophy"></i> Penghargaan
    </a>
    <a class="btn btn-app btn-default btn-flat update" href="<?= base_url().$access->controller;?>/riwayat_hukuman/<?=@$pegawai->id_pegawai?>">
      <i class="fa fa-thumbs-down"></i> Huk. Disiplin
    </a>
    <a class="btn btn-app btn-default btn-flat update" href="<?= base_url().$access->controller;?>/riwayat_ppk/<?=@$pegawai->id_pegawai?>">
      <i class="fa fa-address-book-o"></i> PPK
    </a>
    <a class="btn btn-app btn-default btn-flat update" href="<?= base_url().$access->controller;?>/file_upload/<?=@$pegawai->id_pegawai?>">
      <i class="fa fa-upload"></i> File Upload
    </a>
    <a class="btn btn-app btn-default btn-flat update" href="<?= base_url().$access->controller;?>/riwayat_penilaian/<?=@$pegawai->id_pegawai?>">
      <i class="fa fa-flag-checkered"></i> Penilaian Klinis
    </a>
    <a class="btn btn-app btn-default btn-flat update" href="<?= base_url().$access->controller;?>/skum/<?=@$pegawai->id_pegawai?>">
      <i class="fa fa-file-o"></i> SKUM
    </a>
    <a class="btn btn-app btn-default btn-flat update" href="<?= base_url().$access->controller;?>/file_manager/<?=@$pegawai->id_pegawai?>">
      <i class="fa fa-folder-open-o"></i> File Manager
    </a>
  </div>
</div>
