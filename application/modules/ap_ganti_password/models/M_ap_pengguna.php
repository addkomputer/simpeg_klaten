<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ap_pengguna extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
    $this->db->select('a.*, b.grup');
    if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('a.is_deleted','0')
      ->join('ap_grup b','a.id_grup = b.id_grup')
      ->order_by('a.id_grup')
      ->order_by('id_pegawai','desc')
      ->get('dt_pegawai a',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('dt_pegawai.is_deleted','0')
      ->join('ap_grup','dt_pegawai.id_grup = ap_grup.id_grup')
      ->order_by('id_pegawai')
      ->get('dt_pegawai')
      ->num_rows();
  }
  
  function num_rows_total(){
    return $this->db
      ->where('dt_pegawai.is_deleted','0')
      ->join('ap_grup','dt_pegawai.id_grup = ap_grup.id_grup')
      ->order_by('id_pegawai')
      ->get('dt_pegawai')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('dt_pegawai')->result();
	}

  public function get_by_id($id)
  {
    return $this->db
      ->where('id_pegawai',$id)
      ->get('dt_pegawai')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_pegawai','asc')->get('dt_pegawai')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_pegawai','desc')->get('dt_pegawai')->row();
  }

  public function insert($data)
  {
    $this->db->insert('dt_pegawai',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_pegawai',$id)->update('dt_pegawai',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id_pegawai',$id)->update('dt_pegawai',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_pegawai',$id)->delete('dt_pegawai');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE dt_pegawai");
  }

}
