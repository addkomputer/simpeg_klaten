
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ap_ganti_password extends MY_Controller {

	var $access, $id_grup;

  function __construct(){
		parent::__construct();
		if (!$this->session->userdata('logged')) {
			redirect(base_url().'ap_error/error_403');
		}else{
			$this->load->model('m_ap_pengguna');
			$this->load->model('ap_grup/m_ap_grup');
			;
		}
	}

	
	public function form()
	{
		$data['subtitle'] = 'Ubah Data';
		$data['action'] = 'update';
		$id = $this->session->userdata('id_pegawai');
		$data['pegawai'] = $this->m_ap_pengguna->get_by_id($id);
		$this->view('form', $data);
	}

	public function update_password()
	{
		$data = $_POST;
		$data['updated_by'] = $this->session->userdata('menu');
		$data['passwd'] = md5($data['passwd']);
		$id = $data['id_pegawai'];
		$pegawai = $this->m_ap_pengguna->get_by_id($id);
		if (md5($data['passwd_old']) != $pegawai->passwd) {
			$this->session->set_flashdata('status', '<div class="callout callout-danger callout-dismissable fade in">Kata sandi lama salah!</div>');
		}else{
			unset($data['passwd_again'],$data['passwd_old']);
			$this->m_ap_pengguna->update($id, $data);
			insert_log('update',$this->access->menu);
			$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
		}
		redirect(base_url().'ap_ganti_password/form');
	}

}