<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-key text-red"></i> Ganti Kata Sandi
        <small><?=$subtitle?></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">Pengaturan</li>
        <li class="active text-orange">Ganti Kata Sandi</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php if($action == 'update'): ?>
        <div class="box box-danger">
          <form id="form_password" class="form-horizontal" action="<?=base_url()?>ap_ganti_password/update_password" method="post">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-key text-maroon"></i> Ganti Kata Sandi</h3>
            </div>
            <div class="box-body">
              <?php echo $this->session->flashdata('status'); ?>
              <input type="hidden" class="form-control" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai != null){echo $pegawai->id_pegawai;};?>">
              <div class="form-group">
                <label class="col-sm-2 control-label">Kata Sandi Lama</label>
                <div class="col-sm-3">
                  <input type="password" class="form-control" name="passwd_old" id="passwd_old" value="" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Kata Sandi</label>
                <div class="col-sm-3">
                  <input type="password" class="form-control" name="passwd" id="passwd" value="" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Ulang Kata Sandi</label>
                <div class="col-sm-3">
                  <input type="password" class="form-control" name="passwd_again" id="passwd_again" data-match="#passwd" data-match-error="Kata Sandi tidak sama!" value="" required>
                  <div class="help-block with-errors"></div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-offset-2 col-sm-8">
                  <button class="btn btn-flat btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button>
                  <a class="btn btn-flat btn-default" href="<?=base_url()?>ap_ganti_password/form"><i class="fa fa-close"></i> Batal</a>
                </div>
              </div>
            </div>
          </form>
        </div>
      <?php endif;?>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<script>
  $(document).ready(function () {
    $('#form_password').validate({
      rules: {
        passwd: "required",
        passwd_again: {
          equalTo: "#passwd"
        }
      }
    });
  })
</script>