<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ms_rumpun_jabatan extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_rumpun_jabatan')
      ->get('ms_rumpun_jabatan',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_rumpun_jabatan')
      ->get('ms_rumpun_jabatan')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('ms_rumpun_jabatan')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id_rumpun_jabatan',$id)->get('ms_rumpun_jabatan')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_rumpun_jabatan','asc')->get('ms_rumpun_jabatan')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_rumpun_jabatan','desc')->get('ms_rumpun_jabatan')->row();
  }

  public function insert($data)
  {
    $this->db->insert('ms_rumpun_jabatan',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_rumpun_jabatan',$id)->update('ms_rumpun_jabatan',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id_rumpun_jabatan',$id)->update('ms_rumpun_jabatan',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_rumpun_jabatan',$id)->delete('ms_rumpun_jabatan');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ms_rumpun_jabatan");
  }

}
