<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dt_gaji_dokter extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_gaji_resume')
      ->get('dt_gaji_dokter_resume',$number,$offset)
      ->result_array();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_gaji_resume')
      ->get('dt_gaji_dokter_resume')
      ->num_rows();
  }
  
  function num_rows_total(){  
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_gaji_resume')
      ->get('dt_gaji_dokter_resume')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('dt_gaji_dokter')->result();
	}

  public function get_all_golongan()
  {
    return $this->db
      ->order_by('id_golongan','asc')
      ->where('is_deleted','0')
      ->where('is_active','1')
      ->get('ms_golongan')->result_array();
  }

  public function get_by_id($id)
  {
    return $this->db->where('id_gaji_dokter',$id)->get('dt_gaji_dokter')->row_array();
  }

  public function get_resume_by_id($id)
  {
    return $this->db->where('id_gaji_resume',$id)->get('dt_gaji_dokter_resume')->row_array();
  }

  public function get_first()
  {
    return $this->db->order_by('id_gaji','asc')->get('dt_gaji_dokter')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_gaji','desc')->get('dt_gaji_dokter')->row();
  }

  public function get_all_pegawai($bulan=null, $tahun=null)
  {
      $sql = "SELECT * FROM 
              (
                SELECT 
                  a.id_pegawai, 
                  a.nama, 
                  a.gelar_depan, 
                  a.gelar_belakang, 
                  a.pend_terakhir, 
                  a.nomor_induk, 
                  IF(b.id_golongan IS NULL,a.id_golongan,b.id_golongan) as id_golongan,                   
                  b.id_gaji_dokter,                  
                  IF(b.tahun_kerja IS NULL,a.masakerja_gol_thn,b.tahun_kerja) as tahun_kerja,
                  IF(b.bulan_kerja IS NULL,a.masakerja_gol_bln,b.bulan_kerja) as bulan_kerja,
                  b.grade,
                  b.nilai,
                  b.jumlah_p1,
                  b.jumlah_p2,
                  b.jumlah_p3,
                  b.jumlah_total,
                  IF(c.id_direktur IS NOT NULL,'6423-3100-1000-0131',a.id_jabatan) as id_jabatan,
                  d.golongan   
                FROM dt_pegawai a 
                LEFT JOIN 
                ( 
                  SELECT * FROM dt_gaji_dokter WHERE bulan='$bulan' AND tahun='$tahun' GROUP BY id_pegawai
                ) b ON a.id_pegawai=b.id_pegawai 
                LEFT JOIN 
                (
                  SELECT id_direktur FROM ap_profil
                ) c ON a.id_pegawai=c.id_direktur 
                LEFT JOIN ms_golongan d ON a.id_golongan=d.id_golongan 
                WHERE a.is_deleted=0 AND a.is_active=1 AND a.is_dokter IN ('1','2')
              ) x 
              ORDER BY LEFT(x.id_jabatan,14) ASC, x.nama ASC ";
      return $this->db->query($sql)->result_array();
  }

  public function get_grade_dokter($grade=null)
  {
    $result =  $this->db
                  ->where('grade',$grade)
                  ->get('ms_grade_dokter')
                  ->row_array();
    return @$result['nilai'];                  
  }

  public function insert($data)
  {
    // gaji_resume
    $data_gaji_resume['tahun'] = $data['tahun'];
    $data_gaji_resume['bulan'] = $data['bulan'];
    $data_gaji_resume['total_nilai'] = str_replace('.','',$data['total_nilai']);
    $data_gaji_resume['total_p1_sum'] = str_replace('.','',$data['total_p1_sum']);
    $data_gaji_resume['jasa_dibagi'] = str_replace('.','',$data['jasa_dibagi']);
    $data_gaji_resume['jasa_dibagi_f1'] = str_replace('.','',$data['jasa_dibagi_f1']);
    $data_gaji_resume['total_jumlah_p1'] = str_replace('.','',$data['total_jumlah_p1']);
    $data_gaji_resume['nilai_pir'] = str_replace('.','',$data['nilai_pir']);
    $data_gaji_resume['kesesuaian_p1'] = $data['kesesuaian_p1'];
    $data_gaji_resume['created_by'] = $data['created_by'];
    $data_gaji_resume['is_active'] = $data['is_active'];
    $this->db->insert('dt_gaji_dokter_resume',$data_gaji_resume);
    //
    // gaji
    $data_gaji['tahun'] = $data['tahun'];
    $data_gaji['bulan'] = $data['bulan'];
    foreach($data['id_pegawai'] as $key => $val) {
      $data_gaji['tahun_kerja'] = @$data['tahun_kerja'][$key];
      $data_gaji['bulan_kerja'] = @$data['bulan_kerja'][$key];
      $data_gaji['id_pegawai'] = @$data['id_pegawai'][$key];
      $data_gaji['id_golongan'] = @$data['id_golongan'][$key];
      $data_gaji['grade'] = @$data['grade'][$key];
      $data_gaji['nilai'] = @$data['nilai'][$key];
      $data_gaji['jumlah_p1'] = str_replace('.','',@$data['jumlah_p1'][$key]);
      $data_gaji['jumlah_total'] = str_replace('.','',@$data['jumlah_p1'][$key]);
      $data_gaji['created_by'] = $data['created_by'];
      $data_gaji['is_active'] = $data['is_active'];
      $this->db->insert('dt_gaji_dokter',$data_gaji);
    }
  }

  public function update($id,$data)
  {
    // gaji_resume
    $data_gaji_resume['tahun'] = $data['tahun'];
    $data_gaji_resume['bulan'] = $data['bulan'];
    $data_gaji_resume['total_nilai'] = str_replace('.','',$data['total_nilai']);
    $data_gaji_resume['total_p1_sum'] = str_replace('.','',$data['total_p1_sum']);
    $data_gaji_resume['jasa_dibagi'] = str_replace('.','',$data['jasa_dibagi']);
    $data_gaji_resume['jasa_dibagi_f1'] = str_replace('.','',$data['jasa_dibagi_f1']);
    $data_gaji_resume['total_jumlah_p1'] = str_replace('.','',$data['total_jumlah_p1']);
    $data_gaji_resume['nilai_pir'] = str_replace('.','',$data['nilai_pir']);
    $data_gaji_resume['kesesuaian_p1'] = $data['kesesuaian_p1'];
    $data_gaji_resume['updated_by'] = $data['updated_by'];
    $data_gaji_resume['is_active'] = $data['is_active'];
    $this->db->where('id_gaji_resume', $id);
    $this->db->update('dt_gaji_dokter_resume',$data_gaji_resume);
    //
    // gaji
    $data_gaji['tahun'] = $data['tahun'];
    $data_gaji['bulan'] = $data['bulan'];
    foreach($data['id_pegawai'] as $key => $val) {
      $id_gaji_dokter = @$data['id_gaji_dokter'][$key];
      //
      $data_gaji['tahun_kerja'] = @$data['tahun_kerja'][$key];
      $data_gaji['bulan_kerja'] = @$data['bulan_kerja'][$key];
      $data_gaji['id_pegawai'] = @$data['id_pegawai'][$key];
      $data_gaji['id_golongan'] = @$data['id_golongan'][$key];
      $data_gaji['grade'] = @$data['grade'][$key];
      $data_gaji['nilai'] = @$data['nilai'][$key];
      $data_gaji['jumlah_p1'] = str_replace('.','',@$data['jumlah_p1'][$key]);
      $data_gaji['jumlah_total'] = str_replace('.','',@$data['jumlah_p1'][$key]);
      $data_gaji['updated_by'] = $data['updated_by'];
      $data_gaji['is_active'] = $data['is_active'];
      //
      if($id_gaji_dokter == '') {
        $data_gaji['created_by'] = $data['updated_by'];
        $this->db->insert('dt_gaji_dokter',$data_gaji);
      } else {
        $data_gaji['updated_by'] = $data['updated_by'];
        $this->db->where('id_gaji_dokter', $id_gaji_dokter);
        $this->db->update('dt_gaji_dokter',$data_gaji);
      }      
    }
  }

  public function delete_temp($id)
  {
    $data = $this->get_by_id($id);
    //
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id_gaji_resume',$id)->update('dt_gaji_resume',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
    //
    $this->db->where(
      array(
        'tahun'=> $data['tahun'],
        'bulan'=> $data['bulan'],
      )
    )->update('dt_gaji_dokter',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_gaji',$id)->delete('dt_gaji_dokter');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE dt_gaji_dokter");
  }

}
