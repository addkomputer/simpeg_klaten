<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dt_gaji_dokter extends MY_Controller {

	var $access, $id_grup;

  	function __construct(){
		parent::__construct();

		$controller = 'dt_gaji_dokter';

	    if($this->session->userdata('menu') != $controller){
	      $this->session->unset_userdata('search');
	      $this->session->set_userdata(array('menu' => 'dt_gaji_dokter'));
	    }
	    $this->load->model('ap_konfigurasi/m_ap_konfigurasi');
		$this->id_grup = $this->session->userdata('id_grup');
		$this->access = $this->m_ap_konfigurasi->get_permission($this->id_grup, $controller);
		if ($this->access == null) {
			redirect(base_url().'ap_error/error_403');
		}
	    $this->load->model('dt_gaji_dokter/m_dt_gaji_dokter');
	    $this->load->model('ms_nilai_grade/m_ms_nilai_grade');
	    $this->load->model('ms_grade_dokter/m_ms_grade_dokter');
	    $this->load->model('ms_jasa/m_ms_jasa');
	    $this->load->model('ap_profil/m_ap_profil');
	}
	
	public function index()
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Daftar';

		if ($this->access->_read) {
			$search = null;
			if($search = $_POST){
				$this->session->set_userdata(array('search' => $search));
			}else{
				if($this->session->userdata('search') != null){
					$search = $this->session->userdata('search');
				}
			}
			$data['search'] = $search;
	
			$config['base_url'] = base_url().'dt_gaji_dokter/index/';
			$config['per_page'] = 10;
	
			$from = $this->uri->segment(3);
	
			if($search == null){
				$num_rows = $this->m_dt_gaji_dokter->num_rows();
	
				$config['total_rows'] = $num_rows;
				$this->pagination->initialize($config);
	
				$data['gaji_dokter'] = $this->m_dt_gaji_dokter->get_list($config['per_page'],$from,$search = null);
			}else{
				$search = $this->session->userdata('search');
				$num_rows = $this->m_dt_gaji_dokter->num_rows($search);
				$config['total_rows'] = $num_rows;
				$this->pagination->initialize($config);
	
				$data['gaji_dokter'] = $this->m_dt_gaji_dokter->get_list($config['per_page'],$from,$search);
			}
			$data['num_rows'] = $num_rows;
			$data['num_rows_total'] = $this->m_dt_gaji_dokter->num_rows_total();
			insert_log('read',$this->access->menu);
			$this->view('index',$data);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}
	
	public function form($id = null)
	{
		$data['access'] = $this->access;
		$data['list_month'] = list_month();
		$data['start_year'] = '2018';
		$data['end_year'] = date('Y');
		//
		$data['bulan'] = $this->input->post('bulan');
		$data['tahun'] = $this->input->post('tahun');
		$data['is_process'] = $this->input->post('is_process');
		$data['all_golongan'] = $this->m_dt_gaji_dokter->get_all_golongan();
		$data['all_grade_dokter'] = $this->m_ms_grade_dokter->get_all_grade_dokter();
		//
		if($data['is_process'] == 'TRUE' || $id != '') {
			$data['is_process'] = 'TRUE';			
		}
		//
		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Tambah Data';
				$data['action'] = 'insert';
				$data['action_filter'] = 'form';
				$data['all_pegawai'] = $this->m_dt_gaji_dokter->get_all_pegawai();
				$data['gaji_dokter_resume'] = null;
				$data['jasa'] = $this->m_ms_jasa->get_by_filter($data['tahun'], $data['bulan']);
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Ubah Data';
				$data['action'] = 'update';
				$data['action_filter'] = 'form/'.$id;
				$data['gaji_dokter_resume'] = $this->m_dt_gaji_dokter->get_resume_by_id($id);
				$data['bulan'] = ($data['bulan'] != '' ? $data['bulan'] : $data['gaji_dokter_resume']['bulan']);
				$data['tahun'] = ($data['tahun'] != '' ? $data['tahun'] : $data['gaji_dokter_resume']['tahun']);
				$data['all_pegawai'] = $this->m_dt_gaji_dokter->get_all_pegawai($data['bulan'], $data['tahun']);
				$data['jasa'] = $this->m_ms_jasa->get_by_filter($data['tahun'], $data['bulan']);				
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		}
	}

	public function reset_search()
  	{
    $this->session->unset_userdata('search');
    redirect(base_url().'dt_gaji_dokter/index');
  	}

	public function insert()
	{
		$data = $_POST;
		if ($data != null) {
			$data['created_by'] = $this->session->userdata('nama');
			if(!isset($data['is_active'])){$data['is_active'] = 0;}
			$this->m_dt_gaji_dokter->insert($data);
			insert_log('insert',$this->access->menu);
			$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
			redirect(base_url().'dt_gaji_dokter/index');
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function update()
	{
		$data = $_POST;
		if ($data != null) {
			if($this->access->_update){
				$data['updated_by'] = $this->session->userdata('nama');
				if(!isset($data['is_active'])){$data['is_active'] = 0;}
				$id = $data['id_gaji_resume'];
				$this->m_dt_gaji_dokter->update($id, $data);
				insert_log('update',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				redirect(base_url().'dt_gaji_dokter/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function delete()
	{
		$data = $_POST;
		if($data != null){
			if ($this->access->_delete) {
				$this->m_dt_gaji_dokter->delete_temp($data['id_gaji_dokter']);
				insert_log('delete',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				redirect(base_url().'dt_gaji_dokter/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function ajax($id=null)
	{
		if($id == 'get_grade_dokter') 
		{	
			$grade = $this->input->get('grade');
			$nilai = $this->m_dt_gaji_dokter->get_grade_dokter($grade);
			echo json_encode(array(
				'nilai' => $nilai
			));
		}	
	}

	function cetak_pdf($id=null)
	{
		ini_set("memory_limit","-1");
		//
		$data['title'] = 'Daftar Gaji Dokter';
		$data['profil'] = $this->m_ap_profil->get_first();
		$data['gaji_dokter_resume'] = $this->m_dt_gaji_dokter->get_resume_by_id($id);
		$data['list_gaji'] = $this->m_dt_gaji_dokter->get_all_pegawai($data['gaji_dokter_resume']['bulan'], $data['gaji_dokter_resume']['tahun']);
		//
        $pdfFilePath = str_replace(' ', '_', $data['title']).'.pdf';
        $this->load->file(APPPATH . 'libraries/mpdf/mpdf.php');
        $pdf = new mPDF("en-GB-x",array(210,330),"","",10,10,10,10,7,7,"P");
        //
        $pdf->cacheTables = true;
        $pdf->simpleTables = true;
        $pdf->packTableData = true;
        //
        $html = $this->load->view('cetak_pdf',$data,true);
        $pdf->WriteHTML($html);
        $pdf->Output($pdfFilePath, "I");
	}

}