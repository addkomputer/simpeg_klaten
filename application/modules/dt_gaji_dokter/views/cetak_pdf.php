<!DOCTYPE html>
<html>
    <head>
        <title><?=$title?></title>
        <style type="text/css">
        h1{font-size:20px; font-family: "arial,helvetica,san-serif"; font-weight: bold; text-align: center;}
        h2{font-size:16px; font-family: "arial,helvetica,san-serif"; font-weight: bold;}
        h3{font-size:14px; font-family: "arial,helvetica,san-serif"; font-weight: normal;}
        table { border-collapse: collapse; font-family: "arial,helvetica,san-serif"; font-size: 10px;}
        table td { padding: 3px; }
        .content_title { font-weight: bold; font-size: 16px; font-family: arial;}
        .td-border-all { border: 1px solid #000; }
        .td-border-left { border-left: 1px solid #000; border-bottom: 1px solid #000; border-top: 1px solid #000;}
        .td-border-right { border-right: 1px solid #000; border-bottom: 1px solid #000; border-top: 1px solid #000;}
        .td-border-bottom { border-bottom: 1px solid #000;}
        .td-border-top { border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;}
        .valign-top{ vertical-align: top!important; }
        .text-center { text-align: center; }
        .text-right { text-align: right; }
        .bold { font-weight: bold; }
        </style>
    </head>
    <body>

    <table width="100%">
    <tr>
        <td width="10%" align="left"><img src="<?=base_url().'img/kebumen.png'?>" height="60px"></td>
        <td width="80%" class="text-center">
            <h2>PEMERINTAH KABUPATEN KEBUMEN</h2>
            <h1>RUMAH SAKIT UMUM DAERAH dr. SOEDIRMAN</h1>
            Jalan Lingkar Selatan, Muktisari, Kebumen, Kabupaten Kebumen, Jawa Tengah 54351<br>
            Telp/Fax : 0287-3873318, 0287-381101, 0287-385274; WA : 0877-3427-026; Email : rsud@kebumenkab.go.id<br>
        </td>
        <td width="10%" align="right"><img src="<?=base_url().'img/logo.png'?>" height="60px"></td>
    </tr>
    <tr>
        <td colspan="3" class="td-border-bottom"></td>
    </tr>
    </table>
    <h2 class="text-center">DAFTAR GAJI DOKTER</h2>
    <table width="100%">
    <thead>
        <tr>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 10px">No</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 220px">Nama</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 60px">Gol</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 40px">Tahun</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 40px">Bulan</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 40px">Grade</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 70px">Nilai</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 80px">P1</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $no=1; 
        $total_nilai = 0;
        $total_jumlah_p1 = 0;
        foreach($list_gaji as $row):?>
        <tr>
            <td class="td-border-all text-center"><?=$no?></td>
            <td class="td-border-all"><?=($row['gelar_depan'] != '' ? $row['gelar_depan'].' ' : '')?><?=$row['nama']?><?=($row['gelar_belakang'] != '' ? ' '.$row['gelar_belakang'] : '')?></td>
            <td class="td-border-all text-center"><?=$row['golongan']?></td>
            <td class="td-border-all text-center"><?=$row['tahun_kerja']?></td>
            <td class="td-border-all text-center"><?=$row['bulan_kerja']?></td>
            <td class="td-border-all text-center"><?=$row['grade']?></td>
            <td class="td-border-all text-right"><?=digit($row['nilai'])?></td>
            <td class="td-border-all text-right"><?=digit($row['jumlah_p1'])?></td>
        </tr>
        <?php 
        $no++; 
        $total_nilai += $row['nilai'];
        $total_jumlah_p1 += $row['jumlah_p1'];
        endforeach;?>
        <tr>
            <td class="td-border-all bold text-center" colspan="6">JUMLAH</td>
            <td class="td-border-all bold text-right"><?=digit($total_nilai)?></td>
            <td class="td-border-all bold text-right"><?=digit($total_jumlah_p1)?></td>
        </tr>
    </tbody>
    </table>
    <br>
    <table width="100%">
    <tr>
        <td width="15%">TOTAL JASA</td>
        <td width="40%">: <?=digit($gaji_dokter_resume['jasa_dibagi'])?></td>
        <td width="45%"></td>
    </tr>
    <tr>
        <td>JASA DOKTER</td>
        <td>: <?=digit($gaji_dokter_resume['jasa_dibagi_f1'])?></td>
        <td></td>
    </tr>
    <tr>
        <td>P1</td>
        <td>: <?=digit($gaji_dokter_resume['total_jumlah_p1'])?></td>
        <td></td>
    </tr>
    <tr>
        <td>P2</td>
        <td>: </td>
        <td></td>
    </tr>
    <tr>
        <td>PIR</td>
        <td>: <?=($gaji_dokter_resume['nilai_pir'])?></td>
        <td></td>
    </tr>
    </table>

    </body>
</html>