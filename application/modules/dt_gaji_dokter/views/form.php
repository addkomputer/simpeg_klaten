<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-primary">
        <form id="form-filter" class="form-horizontal" action="<?=base_url()?>dt_gaji_dokter/<?=$action_filter?>" method="post" autocomplete="off"> 
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-2 control-label">Bulan - Tahun</label>
              <div class="col-sm-3">
                <select name="bulan" class="form-control select2 input-sm">
                  <?php foreach($list_month as $key => $val):?>
                    <option value="<?=$key?>" <?php if(@$bulan == $key) echo 'selected'?>><?=$val?></option>
                  <?php endforeach;?>
                </select>
              </div>
              <div class="col-sm-2">
                <select name="tahun" class="form-control select2 input-sm">
                  <?php for($t=$start_year; $t<=$end_year; $t++):?>
                    <option value="<?=$t?>" <?php if(@$tahun == $t) echo 'selected'?>><?=$t?></option>
                  <?php endfor;?>
                </select>
              </div>
              <div class="col-sm-8">
              </div>
            </div>
            <div class="form-group" style="margin-top: -10px">
              <div class="col-md-offset-2 col-sm-8">
                <input type="hidden" name="is_process" value="TRUE">
                <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-check-square-o"></i> Proses</button>
                <a class="btn btn-sm btn-default" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Batal</a>
              </div>
            </div>
          </div>
        </form>

        <?php if($is_process == 'TRUE'):?>
        <form id="form" class="form-horizontal" action="<?=base_url()?>dt_gaji_dokter/<?=$action?>" method="post" autocomplete="off"> 
          <input type="hidden" name="bulan" value="<?=$bulan?>">
          <input type="hidden" name="tahun" value="<?=$tahun?>">
          <hr style="margin-top: -10px">
          <div class="box-body" style="margin: -15px 0px">
            <input type="hidden" class="form-control input-sm" name="id_gaji_resume" id="id_gaji_resume" value="<?php if($gaji_dokter_resume != null){echo $gaji_dokter_resume['id_gaji_resume'];};?>">
            <div class="form-group">
              <label class="col-sm-2 control-label">Jasa Yang Dibagi</label>
              <div class="col-sm-3">
                <div class="input-group">
                  <span class="input-group-addon">Rp.</span>
                  <input type="text" name="jasa_dibagi_set" class="form-control input-sm" value="<?=digit($jasa['total_jasa_dokter'])?>" readonly="1">
                </div>
              </div>
              <?php if(@$jasa['id_jasa'] == ''):?>
              <div class="col-sm-7">
                <em style="color: red">Perhatian : Jasa belum diseting, silahkan klik <a href="<?=site_url('ms_jasa/form')?>">Tambah Jasa Rumah Sakit</a></em>
              </div>
              <?php else:?>
              <div class="col-sm-7 text-right">
                <b>P1 : PAY FOR POSITION</b>
              </div>
              <?php endif;?>
            </div>
          </div>

          <div style="width: 100%; height: 300px; overflow: auto;">
            <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
                <th style="vertical-align: middle; width: 50px" class="text-center">No.</th>
                <th style="vertical-align: middle; width: 250px" class="text-center">Nama</th>
                <th style="vertical-align: middle; width: 100px" class="text-center">Gol</th>
                <th style="vertical-align: middle; width: 50px" class="text-center">Tahun</th>
                <th style="vertical-align: middle; width: 50px" class="text-center">Bulan</th>                
                <th style="vertical-align: middle; width: 100px" class="text-center">Grade</th>
                <th style="vertical-align: middle; width: 100px" class="text-center">Nilai</th>
                <th style="vertical-align: middle; width: 100px" class="text-center">P1</th>
              </tr>
            </thead>
            <tbody>
              <?php $i=1; foreach($all_pegawai as $row):?>
                <tr class="data-pegawai" data-id="<?=$row['id_pegawai']?>">
                  <td class="text-center"><?=($i++)?></td>                  
                  <td class="text-left">
                    <input type="hidden" name="id_gaji_dokter[]" value="<?=$row['id_gaji_dokter']?>">
                    <input type="hidden" name="id_pegawai[]" value="<?=$row['id_pegawai']?>">
                    <input type="hidden" name="id_golongan[]" value="<?=$row['id_golongan']?>">
                    <?=$row['gelar_depan']?> <?=$row['nama']?> <?=$row['gelar_belakang']?><br>
                    <em style="color: blue; font-size: 10px">NIP : <?=$row['nomor_induk']?></em>
                  </td>                  
                  <td class="text-center">
                    <select name="id_golongan[]" class="form-control input-sm id_golongan" id="id_golongan_<?=$row['id_pegawai']?>">
                      <option value="">-----</option>
                      <?php foreach($all_golongan as $gl):?>
                        <option value="<?=$gl['id_golongan']?>" <?php if(@$row['id_golongan'] == $gl['id_golongan']) echo 'selected'?>><?=$gl['golongan']?></option>
                      <?php endforeach;?>
                    </select>
                  </td>          
                  <td class="text-center"><input type="text" class="form-control input-sm tahun_kerja" name="tahun_kerja[]" id="tahun_kerja_<?=$row['id_pegawai']?>" value="<?=@$row['tahun_kerja']?>"></td>                            
                  <td class="text-center"><input type="text" class="form-control input-sm bulan_kerja" name="bulan_kerja[]" id="bulan_kerja_<?=$row['id_pegawai']?>" value="<?=@$row['bulan_kerja']?>"></td>                  
                  <td class="text-center">
                    <!-- <input type="text" class="form-control input-sm grade" name="grade[]" id="grade_<?=$row['id_pegawai']?>" value="<?=@$row['grade']?>"> -->
                    <select name="grade[]" class="form-control input-sm grade" id="grade_<?=$row['id_pegawai']?>">
                      <option value="">-----</option>
                      <?php foreach($all_grade_dokter as $gr):?>
                        <option value="<?=$gr['grade']?>" <?php if(@$row['grade'] == $gr['grade']) echo 'selected'?>><?=$gr['grade']?></option>
                      <?php endforeach;?>
                    </select>
                  </td>                                    
                  <td class="text-center"><input type="text" class="form-control input-sm nilai" name="nilai[]" id="nilai_<?=$row['id_pegawai']?>" value="<?=@$row['nilai']?>"></td>                                    
                  <td class="text-center"><input type="text" class="form-control input-sm jumlah_p1" name="jumlah_p1[]" id="jumlah_p1_<?=$row['id_pegawai']?>" value="<?=(@$row['jumlah_p1'] !='' ? digit(@$row['jumlah_p1']) : '')?>"></td>                  
                </tr>
              <?php endforeach;?>              
            </tbody>
            </table>
          </div>

          <table class="table table-striped table-bordered table-condensed">
            <tbody>
            <tr>
              <td colspan="7" style="background-color: #eee"></td>
            </tr>
            <tr>
                <td width="10%" class="text-left"></td>    
                <td width="12%" class="text-left"></td>    
                <td width="8%" class="text-left"><b>JUMLAH NILAI</b></td>    
                <td width="10%"><input type="text" class="form-control input-sm" name="total_nilai" id="total_nilai" readonly="1"></td>
                <td width="8%" class="text-left"><b>JUMLAH P1</b></td>    
                <td width="10%">
                  <div class="input-group">
                    <span class="input-group-addon">Rp.</span>
                    <input type="text" class="form-control input-sm" name="total_p1_sum" id="total_p1_sum" readonly="1" value="<?=@$gaji_dokter_resume['total_p1_sum']?>">
                  </div>
                </td>
                <td width="10%"><em style="color: red; font-size: 9px">* Mengambil dari akumulasi P1</em></td>
            </tr>
            <tr>
                <td class="text-left"><b>TOTAL JASA</b></td>    
                <td class="text-center">
                  <div class="input-group">
                    <span class="input-group-addon">Rp.</span>
                    <input type="text" class="form-control input-sm" name="jasa_dibagi" id="jasa_dibagi" value="<?=digit($jasa['total_jasa_dokter'])?>" readonly="1">
                  </div>
                </td>   
                <td colspan="5"></td>
            </tr>
            <tr>
                <td class="text-left"><b>JASA DOKTER</b></td>           
                <td class="text-center">
                  <div class="input-group">
                    <span class="input-group-addon">Rp.</span>
                    <input type="text" class="form-control input-sm" name="jasa_dibagi_f1" id="jasa_dibagi_f1" value="<?=@$gaji_dokter_resume['jasa_dibagi_f1']?>" readonly="1">
                  </div>
                <td colspan="5"></td>
            </tr>
            <tr>
                <td class="text-left"><b>P1</b></td>    
                <td>
                  <div class="input-group">
                    <span class="input-group-addon">Rp.</span>
                    <input type="text" class="form-control input-sm" name="total_jumlah_p1" id="total_jumlah_p1" readonly="1" value="<?=@$gaji_dokter_resume['total_jumlah_p1']?>">
                  </div>
                </td>
                <td colspan="5"><em style="color: red; font-size: 9px">* Mengambil dari Jasa Dokter X 15%</em></td>
            </tr>
            <tr>
                <td class="text-left"><b>PIR</b></td>    
                <td><input type="text" class="form-control input-sm" name="nilai_pir" id="nilai_pir" value="<?=(@$gaji_dokter_resume['nilai_pir'] != '' ? digit($gaji_dokter_resume['nilai_pir']) : '')?>" readonly="1"></td>          
                <td><b>KESESUAIAN P1</b></td>          
                <td><input type="text" class="form-control input-sm" name="kesesuaian_p1" id="kesesuaian_p1" readonly="1" <?=@$gaji_dokter_resume['kesesuaian_p1']?>></td>          
                <td></td>          
            </tr>
            </tbody>
          </table>

          <hr>
          <div class="form-group">
            <div class="col-md-offset-2 col-sm-8">
              <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
              <a class="btn btn-sm btn-default" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Batal</a>
            </div>
          </div>
          <br>
        </div>
        </form>
        <?php endif;?>

      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<script>
  $(document).ready(function () {
    // form validator
    $('#form').validate();
    //
    $('.data-pegawai').each(function() {
        var i = $(this).attr('data-id');
        //
        $('#tahun_kerja_'+i+', #bulan_kerja_'+i+', #grade_'+i).bind('change',function() {
            var grade = ($('#grade_'+i).val() != '' ? $('#grade_'+i).val() : '');
            //
            $.get('<?=site_url("dt_gaji_dokter/ajax/get_grade_dokter")?>?grade='+grade,null,function(data) {
              $('#nilai_'+i).val(data.nilai);
              __get_total_nilai();
              __get_nilai_pir();
              __get_jumlah_p1(i);
              __get_all_jumlah_p1();
              __cek_kesesuaian();
            },'json');          
        });  
        //
        __get_total_nilai();
        __get_nilai_pir();
        __get_jumlah_p1(i);
        __get_all_jumlah_p1();
        __cek_kesesuaian();
    });
    //
    __cek_kesesuaian();
    function __cek_kesesuaian() {
      var total_p1_sum = $('#total_p1_sum').val();
      var total_jumlah_p1 = $('#total_jumlah_p1').val();
      if(total_p1_sum == total_jumlah_p1) {
        var result = 'TRUE';
        $('#kesesuaian_p1').val(result).css('border-color','yellow');
      } else {
        var result = 'FALSE';
        $('#kesesuaian_p1').val(result).css('border-color','red');
      }      
    }
    //
    __get_all_jumlah_p1();
    function __get_all_jumlah_p1() {
      var t = 0;
      $('.data-pegawai').each(function() {
        var i = $(this).attr('data-id');
        __get_jumlah_p1(i);
        //
        var j = ($('#jumlah_p1_'+i).val() != "" ? $('#jumlah_p1_'+i).val().replace(/[.]/g,"") : 0);
            t += parseFloat(j);
      });
      //
      var result = Math.round(t);
      var result = number_format(result);
      $('#total_p1_sum').val(result);
    }
    //
    function __get_jumlah_p1(i) {
      var nilai_pir_set = '<?=@$gaji_dokter_resume["nilai_pir"]?>';
      var nilai = ($('#nilai_'+i).val() != "" ? $('#nilai_'+i).val().replace(/[.]/g,"") : 0);
      var nilai_pir = ($('#nilai_pir').val() != "" ? $('#nilai_pir').val().replace(/[.]/g,"") : 0);
      if(nilai_pir == '0' && nilai_pir_set != '') {
        var nilai_pir = nilai_pir_set;
      }
      var result = parseFloat(nilai) * parseFloat(nilai_pir);
      var result = Math.round(result);
      var result = number_format(result);
      $('#jumlah_p1_'+i).val(result);
    }
    //
    __get_total_nilai();
    function __get_total_nilai() {
      var t = 0;
      $('.nilai').each(function() {
        var i = ($(this).val() != '' ? $(this).val() : 0);
            t += parseFloat(i);
      });
      $('#total_nilai').val(t);
    }
    //
    __get_jasa_dibagi_f1();
    function __get_jasa_dibagi_f1() {
      var prosen = '35'; // 35%
      var jasa_dibagi = ($('#jasa_dibagi').val() != '' ? $('#jasa_dibagi').val().replace(/[.]/g,"") : '0');
      var result = parseFloat(prosen) / 100 * parseFloat(jasa_dibagi);
      var result = Math.round(result);
      var result = number_format(result);
      $('#jasa_dibagi_f1').val(result);
      __get_total_jumlah_p1();
    }
    //
    function __get_total_jumlah_p1() {
      var prosen = '15'; // 15%
      var jasa_dibagi_f1 = ($('#jasa_dibagi_f1').val() != '' ? $('#jasa_dibagi_f1').val().replace(/[.]/g,"") : '0');
      var result = parseFloat(prosen) / 100 * parseFloat(jasa_dibagi_f1);
      var result = Math.round(result);
      var result = number_format(result);
      $('#total_jumlah_p1').val(result);
      __get_nilai_pir();
    }
    //
    function __get_nilai_pir() {
      var total_jumlah_p1 = ($('#total_jumlah_p1').val() != '' ? $('#total_jumlah_p1').val().replace(/[.]/g,"") : '0');
      var total_nilai = ($('#total_nilai').val() != '' ? $('#total_nilai').val().replace(/[.]/g,"") : '0');
      if(total_nilai == 0) {
        var result = 0;
      } else {
        var result = parseFloat(total_jumlah_p1) / parseFloat(total_nilai);
      }
      //
      var result = Math.round(result);
      $('#nilai_pir').val(result);
    }
    //
    function number_format(nStr) {
        nStr += '';
        x = nStr.split(',');
        x1 = x[0];
        x2 = x.length > 1 ? ',' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }
  })
</script>