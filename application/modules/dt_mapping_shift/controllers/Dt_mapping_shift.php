<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Dt_mapping_shift extends MY_Controller {

	var $access, $id_mapping_shift;

  function __construct(){
		parent::__construct();

		$controller = 'dt_mapping_shift';

    if($this->session->userdata('menu') != $controller){
      $this->session->unset_userdata('search');
      $this->session->set_userdata(array('menu' => 'dt_mapping_shift'));
    }
    $this->load->model('ap_konfigurasi/m_ap_konfigurasi');
		$this->id_grup = $this->session->userdata('id_grup');
		$this->access = $this->m_ap_konfigurasi->get_permission($this->id_grup, $controller);
		if ($this->access == null) {
			redirect(base_url().'ap_error/error_403');
		}
    $this->load->model('m_dt_mapping_shift');
    $this->load->model('ms_shift/m_ms_shift');
    $this->load->model('ms_ruang/m_ms_ruang');
    $this->load->model('dt_pegawai/m_dt_pegawai');
	}
	
	public function index()
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Daftar';
		$data['ruang'] = $this->m_ms_ruang->get_all();

		if ($this->access->_read) {

			$data['search'] = $this->session->userdata('search');

			$from = $this->uri->segment(3);
			if ($from == '') {
				$from = 0;
			}

			$num_rows = $this->m_dt_mapping_shift->num_rows($data['search']);
			$config['total_rows'] = $num_rows;
			$config['per_page'] = 10;
			$config['base_url'] = base_url().$this->access->controller.'/index';
			$this->pagination->initialize($config);

			$data['main'] = $this->m_dt_mapping_shift->get_list($config['per_page'],$from,$data['search']);

			$data['num_rows'] = $num_rows;
			$data['num_rows_total'] = $this->m_dt_mapping_shift->num_rows_total();
			insert_log('read',$this->access->menu);
			$this->view('index',$data);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function search()
	{
		$search = $_POST;
		$this->session->set_userdata(array('search' => $search));
		redirect(base_url().$this->access->controller);
	}
	
	public function form($id = null)
	{
		$data['access'] = $this->access;
		$data['ruang'] = $this->m_ms_ruang->get_all();
		$data['search'] = $this->session->userdata('search');
		$this->m_dt_mapping_shift->create_table($data['search']['tahun']."_".$data['search']['bulan']);
		// var_dump($data['search']);
		// exit();
		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Tambah Data';
				$data['action'] = 'insert';
				$data['mapping_shift'] = null;
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Ubah Data';
				$data['action'] = 'update';
				$data['mapping_shift'] = $this->m_dt_mapping_shift->get_by_id($id);
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		}
	}

	public function form_detail($id = null)
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Edit Data';
		$data['action'] = 'update_detail';
		$data['pegawai'] = $this->m_dt_pegawai->get_by_id($id);
		$data['shift'] = $this->m_ms_shift->get_all();
		$data['mapping'] = $this->m_dt_mapping_shift->get_by_id_pegawai($id);
		// echo json_encode($data['mapping']);exit();
		$data['search'] = $this->session->userdata('search');
		$this->view('form_detail', $data);
	}

	public function update_detail()
	{
		$data = $_POST;
		$suffix = $data['tahun']."_".$data['bulan'];
		foreach ($data['tanggal'] as $key => $val) {
			if($data['kode'][$key] != ''){
				$shift = $this->m_ms_shift->get_by_kode($data['kode'][$key]);
				$d = array(
					'id_pegawai' => $data['id_pegawai'],
					'nomor_induk' => $data['nomor_induk'],
					'pin' => $data['pin'],
					'id_shift' => $shift->id_shift,
					'kode' => $shift->kode,
					'tanggal' => $val, 
					'jam_datang' => $shift->jam_datang,
					'jam_batas_datang' => $shift->jam_batas_datang,
					'jam_pulang' => $shift->jam_pulang,
					'jam_batas_pulang' => $shift->jam_batas_pulang,
					'is_over24' => $shift->is_over24,
					'updated_by' => $this->session->userdata('nama')
				);
			}
			$this->m_dt_mapping_shift->update_detail($suffix,$d);
		}
		insert_log('update',$this->access->menu);
		$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
		redirect(base_url().'dt_mapping_shift/index');
	}

	public function reset_search()
  {
    $this->session->unset_userdata('search');
    redirect(base_url().'dt_mapping_shift/index');
	}

	public function create_table($tahun,$bulan)
	{
		$suffix = $tahun."_".$bulan;
		$this->m_dt_mapping_shift->create_table($suffix);
		$search = array(
			'term' => '',
			'bulan' => $bulan,
			'tahun' => $tahun,
			'id_ruang' => ''
		);
		$this->session->set_userdata(array('search' => $search));
		redirect(base_url().$this->access->controller.'/form');
	}

	public function insert()
	{
		$data = $_POST;
		$search = array(
			'term' => '',
			'bulan' => $data['bulan'],
			'tahun' => $data['tahun'],
			'id_ruang' => $data['id_ruang']
		);
		$this->session->set_userdata(array('search' => $search));
		if ($data != null) {
			$config['file_name'] = date('YmdHis');
			$config['upload_path'] = './mapping_shift/';
			$config['allowed_types'] = '*';
	
			$this->load->library('upload', $config);
	
			if (!$this->upload->do_upload('file_template')){
				$error = array('error' => $this->upload->display_errors());
				var_dump($error);
			}else{
				$file = $this->upload->data();
				// var_dump($file);
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
				$reader->setReadDataOnly(true);
				$spreadsheet = $reader->load('./mapping_shift/'.$file['file_name']);
				$totalRow = $spreadsheet->getActiveSheet()->getHighestRow();
				$totalColumn = $spreadsheet->getActiveSheet()->getHighestColumn();
				++$totalColumn;
				$bulan = str_pad($spreadsheet->getActiveSheet()->getCell('B2')->getValue(), 2, '0', STR_PAD_LEFT);
				$tahun = $spreadsheet->getActiveSheet()->getCell('B3')->getValue();
				$suffix = $tahun."_".$bulan;
				$result = array();
				if (intval($bulan) == intval($data['bulan']) && intval($tahun) == intval($data['tahun'])) {
					for ($i=12; $i <= $totalRow; $i++) { 
						$nomor_induk = $spreadsheet->getActiveSheet()->getCell('B'.$i)->getValue();
						//update status pegawai bahwa dia pegawai shift
						if ($nomor_induk != '') {
							$this->m_dt_pegawai->update_by_nomor_induk($nomor_induk,array('is_regular' => 0));
							$peg = $this->m_dt_mapping_shift->get_pegawai_by_nomor_induk($nomor_induk);
							//jika pegawai tidak ada 
							if($peg == null){
								array_push($result,'<div class="badge bg-red"><i class="fa fa-close"></i> Nomor induk '.$nomor_induk.' tidak ditemukan.</div><br>');
							}else{
								if ($peg->id_ruang == $data['id_ruang']) {
									for ($j='C'; $j != $totalColumn; $j++) {
										$tanggal = str_pad($spreadsheet->getActiveSheet()->getCell($j.'11')->getValue(), 2, '0', STR_PAD_LEFT);
										if (checkdate(intval($bulan),intval($tanggal),intval($tahun))) {
											$date = $tahun."-".$bulan."-".$tanggal;
											$kode = $spreadsheet->getActiveSheet()->getCell($j.$i)->getValue();
											if($kode != ''){
												$err_num = 0;
												$shift = $this->m_ms_shift->get_by_kode($kode);
												if ($shift == 'null') {
													$err_num++;
													array_push($result,'<div class="badge bg-red"><i class="fa fa-close"></i> Shift nomor induk '.$nomor_induk.' pada '.$date.'-'.'Kode '.$kode.' tidak ditemukan.</div><br>');
												}else{
													$data_shift = array(
														'id_pegawai' => $peg->id_pegawai,
														'nomor_induk' => $peg->nomor_induk,
														'pin' => $peg->pin,
														'id_ruang' => $peg->id_ruang,
														'tanggal' => $date,
														'id_shift' => $shift->id_shift,
														'kode' => $shift->kode,
														'jam_datang' => $shift->jam_datang,
														'jam_batas_datang' => $shift->jam_batas_datang,
														'jam_pulang' => $shift->jam_pulang,
														'jam_batas_pulang' => $shift->jam_batas_pulang,
														'is_over24' => $shift->is_over24
													);
													//cek mapping shift 
													$cek = $this->m_dt_mapping_shift->cek($nomor_induk,$date);
													if ($cek == null) {
														$data_shift['created_by'] = $this->session->userdata('nama');
														$this->m_dt_mapping_shift->insert($suffix,$data_shift);
													}else{
														$data_shift['updated_by'] = $this->session->userdata('nama');
														$this->m_dt_mapping_shift->update($suffix,$nomor_induk,$date,$data_shift);
													}
												}
											}
										}
									}
									if ($err_num == 0) {
										array_push($result,'<div class="badge bg-green"><i class="fa fa-check"></i> Nomor induk '.$nomor_induk.' berhasil</div><br>');
									}
								}else{
									array_push($result,'<div class="badge bg-red"><i class="fa fa-close"></i> Nomor induk '.$nomor_induk.' memiliki ruang yang tidak sesuai dengan masukkan.</div><br>');
								}
							}
						}
					}
				}else{
					array_push($result,'<div class="badge bg-red"><i class="fa fa-close"></i> Tahun dan/atau tanggal input dan file excel tidak sama<br>');
					$this->session->set_flashdata('result', $result);
					redirect(base_url().'dt_mapping_shift/status');
				}
			}
			insert_log('insert',$this->access->menu);
			$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
			$this->session->set_flashdata('result', $result);
			redirect(base_url().'dt_mapping_shift/status');
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function status()
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Status Upload';
		$this->view('status',$data);
	}

	public function update()
	{
		$data = $_POST;
		if ($data != null) {
			if($this->access->_update){
				$data['updated_by'] = $this->session->userdata('nama');
				if(!isset($data['is_active'])){$data['is_active'] = 0;}
				$id = $data['id_mapping_shift'];
				$this->m_dt_mapping_shift->update($id, $data);
				insert_log('update',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				redirect(base_url().'dt_mapping_shift/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function delete()
	{
		$data = $_POST;
		if($data != null){
			if ($this->access->_delete) {
				$this->m_dt_mapping_shift->delete_permanent($data['id_pegawai']);
				insert_log('delete',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				redirect(base_url().'dt_mapping_shift/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}

}