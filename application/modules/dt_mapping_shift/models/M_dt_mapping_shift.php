<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dt_mapping_shift extends CI_Model {
  
  private $table,$suffix,$bulan,$tahun,$id_ruang;

  public function __construct() {
    parent::__construct();
    if ($this->session->userdata('search')) {
      $this->bulan = $this->session->userdata('search')['bulan'];
      $this->tahun = $this->session->userdata('search')['tahun'];
      $this->id_ruang = $this->session->userdata('search')['id_ruang'];
      $this->suffix = $this->tahun.'_'.$this->bulan;
      $this->term = $this->session->userdata('search')['term'];
      if ($this->session->userdata('id_grup') == 4) {
        $this->id_ruang = $this->session->userdata('id_ruang');
      }
    }else{ 
      $this->bulan = date('m');
      $this->tahun = date('Y');
      $this->suffix = date('Y_m');
      $this->id_ruang = '';
      $this->term = '';
      if ($this->session->userdata('id_grup') == 4) {
        $this->id_ruang = $this->session->userdata('id_ruang');
      }
      $search = array(
        'id_ruang' => $this->id_ruang,
        'bulan' => $this->bulan,
        'tahun' => $this->tahun,
        'suffix' => $this->suffix,
        'term' => $this->term
      );
      $this->session->set_userdata(array('search' => $search));
    }
    $this->create_table($this->suffix);
  }

  public function create_table($suffix)
  {
    $this->db->query(
      "CREATE TABLE IF NOT EXISTS `dt_mapping_shift_$this->suffix` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `id_pegawai` varchar(50) DEFAULT '0',
        `nomor_induk` varchar(50) DEFAULT '0',
        `pin` varchar(50) DEFAULT '0',
        `id_ruang` int(11) DEFAULT '0',
        `id_shift` int(11) DEFAULT '8',
        `kode_awal` varchar(50) DEFAULT NULL,
        `kode` varchar(50) DEFAULT 'P3',
        `is_tukar` tinyint(1) NOT NULL DEFAULT '0',
        `tanggal` date DEFAULT '0000-00-00',
        `jam_datang` time DEFAULT '00:00:00',
        `jam_batas_datang` time DEFAULT '00:00:00',
        `jam_pulang` time DEFAULT '00:00:00',
        `jam_batas_pulang` time DEFAULT '00:00:00',
        `is_over24` tinyint(1) DEFAULT '0',
        `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
        `created_by` varchar(50) DEFAULT 'System',
        `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
        `updated_by` varchar(50) DEFAULT NULL,
        `is_active` tinyint(1) DEFAULT '1',
        `is_deleted` tinyint(1) DEFAULT '0',
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1;"
    );
  }

	public function get_list($number,$offset,$search = null)
  { 
    $where = '';
    if ($search != null) {
      $where = "WHERE 1 = 1 ";
      if($search['id_ruang'] != ''){
        $where .= "AND a.id_ruang =".$search['id_ruang']." ";
      }
      if($search['term'] != ''){
        $where .= "AND b.nama LIKE '%".$search['term']."%' ";
      }
    }

    $return = array();

    $query = $this->db->query(
      "SELECT 
        a.*,
        b.gelar_depan,b.nama,b.gelar_belakang,
        c.status_pegawai,
        d.jabatan,
        e.ruang 
      FROM dt_mapping_shift_$this->suffix a
      JOIN dt_pegawai b ON a.pin = b.pin
      LEFT JOIN ms_status_pegawai c ON b.id_status_pegawai = c.id_status_pegawai
      LEFT JOIN ms_jabatan d ON b.id_jabatan = d.id_jabatan
      LEFT JOIN ms_ruang e ON b.id_ruang = e.id_ruang
        $where
      GROUP BY 
        a.pin
      LIMIT $offset,$number
      "
    );

    foreach ($query->result() as $row) {
      $return[$row->pin] = $row;
      $return[$row->pin]->detail = array();
      
      $detail = $this->db
        ->query("SELECT * FROM dt_mapping_shift_$this->suffix a
          JOIN ms_shift b ON a.id_shift = b.id_shift
          WHERE pin = '$row->pin'
          AND tanggal != '0000-00-00'
          AND tanggal LIKE '%$this->tahun-$this->bulan-%'
          GROUP BY tanggal")
        ->result_array();

      if ($detail != null) {
        foreach ($detail as $row2) {
          $return[$row->pin]->detail[$row2['tanggal']] = $row2;
        }
      }
    }

    return $query->result();
  }

  function num_rows($search = null){
    $where = '';
    if ($search != null) {
      $where = "WHERE 1 = 1 ";
      if($search['id_ruang'] != ''){
        $where .= "AND a.id_ruang =".$search['id_ruang']." ";
      }
      if($search['term'] != ''){
        $where .= "AND b.nama LIKE '%".$search['term']."%' ";
      }
    }

    $query = $this->db->query(
      "SELECT a.*,b.gelar_depan,b.nama,b.gelar_belakang 
      FROM dt_mapping_shift_$this->suffix a
      JOIN dt_pegawai b ON a.pin = b.pin
        $where
      GROUP BY 
        a.nomor_induk
      "
    );

    return $query->num_rows();
  }

  public function cek($nomor_induk,$tanggal)
  {
    return $this->db
      ->where('nomor_induk',$nomor_induk)
      ->where('tanggal',$tanggal)
			->get('dt_mapping_shift_'.$this->suffix)->row();
  }
  
  function num_rows_total(){
    return $query = $this->db->query(
      "SELECT * FROM dt_mapping_shift_$this->suffix"
    )->num_rows();
	}

  public function get_pegawai_by_nomor_induk($nomor_induk)
  {
    return $this->db
      ->select('id_pegawai,pin,nomor_induk,nama,id_ruang')
      ->where('nomor_induk',$nomor_induk)->get('dt_pegawai')->row();
  }

	public function get_all()
	{
    $profil = $this->db->get('ap_profil')->row();

    return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('dt_mapping_shift_'.$this->suffix)->result();
  }
  
  public function get_by_id_pegawai($id_pegawai)
  {
    $query = $this->db
      ->where('id_pegawai',$id_pegawai)
      ->where('tanggal !=','0000-00-00')
      ->group_by('tanggal')
      ->get('dt_mapping_shift_'.$this->suffix);

    $res = array();
    foreach ($query->result_array() as $row) {
      $res[$row['tanggal']] = $row;
    }

    return $res;
  }

  public function get_by_id($id)
  {
    return $this->db->where('id',$id)->get('dt_mapping_shift_'.$this->suffix)->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id','asc')->get('dt_mapping_shift_'.$this->suffix)->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id','desc')->get('dt_mapping_shift_'.$this->suffix)->row();
  }

  public function insert($suffix,$data)
  {
    $this->db->insert('dt_mapping_shift_'.$suffix,$data);
  }

  public function update($suffix,$nomor_induk,$tanggal,$data)
  {
    $this->db
      ->where('nomor_induk',$nomor_induk)
      ->where('tanggal',$tanggal)
      ->update('dt_mapping_shift_'.$suffix,$data);
  }

  public function update_detail($suffix,$data)
  {
    //cek data 
    $cek = $this->db
      ->where('id_pegawai',$data['id_pegawai'])
      ->where('tanggal',$data['tanggal'])
      ->get('dt_mapping_shift_'.$this->suffix)
      ->row();

    if($cek == null){
      $this->db
        ->insert('dt_mapping_shift_'.$this->suffix,$data);
    }else{
      $this->db
        ->where('id_pegawai',$data['id_pegawai'])
        ->where('tanggal',$data['tanggal'])
        ->update('dt_mapping_shift_'.$this->suffix,$data);
    }
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id',$id)->update('dt_mapping_shift_'.$this->suffix,array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_pegawai',$id)->delete('dt_mapping_shift_'.$this->suffix);
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE 'dt_mapping_shift_'.$this->suffix");
  }

}
