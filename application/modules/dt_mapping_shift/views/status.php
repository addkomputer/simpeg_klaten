<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
      <small><?=$subtitle?></small>
    </h1>  
    <ol class="breadcrumb">
      <li class="active">Administrasi Kehadiran</li>
      <li class="active text-orange"><?=$access->menu?></li>
    </ol>    
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box box-primary">
      <div class="box-header with-border">
        <i class="fa fa-table text-maroon"></i>
        <h3 class="box-title">Status Upload <?=$access->menu?></h3>
      </div> 
      <div class="box-body">
        <?php $stat = ($this->session->flashdata('result')) ?>
        <?php if($stat != null):?>
          <?php foreach($stat as $row):?>
            <?=$row?>
          <?php endforeach; ?>
        <?php endif; ?>
      </div>
      <div class="box-footer">
        <a class="btn btn-success" href="<?=base_url().$this->access->controller?>"><i class="fa fa-check"></i> Selesai</a>
      </div>
    </div>
  </section>
</div>
<!-- /.content-wrapper -->