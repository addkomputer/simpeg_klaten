<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">Administrasi Kehadiran</li>
        <li class="active text-orange"><?=$access->menu?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-primary">
        <div class="box-header with-border">
          <i class="fa fa-table text-maroon"></i>
          <h3 class="box-title">Form <?=$access->menu?></h3>
        </div>
        <form id="form" class="form-horizontal" action="<?=base_url()?>dt_mapping_shift/<?=$action?>" method="post" enctype="multipart/form-data" autocomplete="off"> 
          <div class="box-body">
            <div class="alert alert-info">
              <i class="fa fa-info"></i> 
              Contoh template mapping shift dapat diunduh
              <a href="<?=base_url()?>assets/template_shift.xlsx"><i class="fa fa-download"></i> disini</a>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Bulan</label>
              <div class="col-sm-2">
                <select class="form-control select2" name="bulan" id="bulan">
                  <option value="01" <?php if(@$search['bulan'] == "01"){echo 'selected';} ?>>Januari</option>
                  <option value="02" <?php if(@$search['bulan'] == "02"){echo 'selected';} ?>>Februari</option>
                  <option value="03" <?php if(@$search['bulan'] == "03"){echo 'selected';} ?>>Maret</option>
                  <option value="04" <?php if(@$search['bulan'] == "04"){echo 'selected';} ?>>April</option>
                  <option value="05" <?php if(@$search['bulan'] == "05"){echo 'selected';} ?>>Mei</option>
                  <option value="06" <?php if(@$search['bulan'] == "06"){echo 'selected';} ?>>Juni</option>
                  <option value="07" <?php if(@$search['bulan'] == "07"){echo 'selected';} ?>>Juli</option>
                  <option value="08" <?php if(@$search['bulan'] == "08"){echo 'selected';} ?>>Agustus</option>
                  <option value="09" <?php if(@$search['bulan'] == "09"){echo 'selected';} ?>>September</option>
                  <option value="10" <?php if(@$search['bulan'] == "10"){echo 'selected';} ?>>Oktober</option>
                  <option value="11" <?php if(@$search['bulan'] == "11"){echo 'selected';} ?>>November</option>
                  <option value="12" <?php if(@$search['bulan'] == "12"){echo 'selected';} ?>>Desember</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Tahun</label>
              <div class="col-sm-2">
                <select class="form-control select2" name="tahun" id="tahun">
                  <?php for ($i=2018; $i <= date('Y')+1; $i++): ?>
                    <option value="<?=$i?>" <?php if(date('Y')==$i){echo 'selected';}?>><?=$i?></option>
                  <?php endfor; ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Ruang</label>
              <div class="col-sm-4">
                <select class="form-control select2" name="id_ruang" id="id_ruang">
                  <?php foreach($ruang as $row):?>
                    <?php if($this->session->userdata('id_grup') == 4): ?>
                      <?php if($this->session->userdata('id_ruang') == $row->id_ruang):?>
                        <option value="<?=$row->id_ruang?>"><?=$row->ruang?></option>
                      <?php endif; ?> 
                    <?php else: ?>
                      <option value="<?=$row->id_ruang?>"><?=$row->ruang?></option>
                    <?php endif; ?>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">File Excel</label>
              <div class="col-sm-1">
                <input type="file" name="file_template" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label"></label>
              <div class="col-sm-5">
                <span class="text-green">Pastikan format excel beserta kolomnya sudah sesuai dengan template mapping shift. Extensi file yang dapat diupload adalah .xlsx</span>
              </div>
            </div>
            <hr>
            <div class="form-group">
              <div class="col-md-offset-2 col-sm-8">
                <button class="btn btn-flat btn-success btn-action" type="submit" name="submit"><i class="fa fa-save"></i> Simpan</button>
                <a class="btn btn-flat btn-default btn-action" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Batal</a>
                <a id="btn-progress" class="btn btn-flat btn-success disabled"><i class="fa fa-spinner fa-spin"></i> Proses...</a>
              </div>
            </div>
          </div>
        </form>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<script>
  $(document).ready(function () {
    $('#btn-progress').hide();
    // form validator
    $('#form').validate({
      submitHandler: function (form) {
        $('#btn-progress').show();
        $('.btn-action').hide();
        form.submit();
      }
    });
    $('#bulan').on('select2:select', function (e) {
      var bulan = $("#bulan option:selected").val();
      var tahun = $("#tahun option:selected").val();
      window.location.replace("<?=base_url().$access->controller?>/create_table/"+tahun+"/"+bulan);
    });
    $('#tahun').on('select2:select', function (e) {
      var bulan = $("#bulan option:selected").val();
      var tahun = $("#tahun option:selected").val();
      window.location.replace("<?=base_url().$access->controller?>/create_table/"+tahun+"/"+bulan);
    });
  })
</script>