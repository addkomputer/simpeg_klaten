<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">Administrasi Kehadiran</li>
        <li class="active text-orange"><?=$access->menu?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-primary">
        <div class="box-header with-border">
          <i class="fa fa-table text-maroon"></i>
          <h3 class="box-title">Form <?=$access->menu?></h3>
        </div>
        <form id="form" class="form-horizontal" action="<?=base_url()?>dt_mapping_shift/<?=$action?>" method="post" enctype="multipart/form-data" autocomplete="off"> 
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                <table class="table table-bordered table-condensed table-striped">
                  <tbody>
                    <tr>
                      <td width="200">Nama</td>
                      <td width="10">:</td>
                      <td><?=$pegawai->nama?></td>
                      <input type="hidden" name="id_pegawai" value="<?=$pegawai->id_pegawai?>">
                      <input type="hidden" name="pin" value="<?=$pegawai->pin?>">
                      <input type="hidden" name="nomor_induk" value="<?=$pegawai->nomor_induk?>">
                    </tr>
                    <tr>
                      <td>Nomor Induk / PIN</td>
                      <td>:</td>
                      <td><?=$pegawai->nomor_induk?> / <?=$pegawai->pin?></td>
                    </tr>
                    <tr>
                      <td width="200">Ruang</td>
                      <td width="10">:</td>
                      <td><?=$pegawai->ruang?></td>
                    </tr>
                    <tr>
                      <td width="200">Jabatan</td>
                      <td width="10">:</td>
                      <td><?=$pegawai->jabatan?></td>
                    </tr>
                    <tr>
                      <td>Status</td>
                      <td>:</td>
                      <td><?=$pegawai->status_pegawai?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="col-md-6">
                <table class="table table-bordered table-condensed table-striped">
                  <tbody>
                    <tr>
                      <td width="200">Bulan</td>
                      <td width="10">:</td>
                      <td><?=month_id($search['bulan'])?></td>
                      <input type="hidden" name="bulan" val="<?=$search['bulan']?>">
                    </tr>
                    <tr>
                      <td>Tahun</td>
                      <td>:</td>
                      <td><?=$search['tahun']?></td>
                      <input type="hidden" name="tahun" val="<?=$search['tahun']?>">
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <table class="table table-bordered table-condensed table-striped">
              <thead>
                <tr>
                  <th class="text-center" width="200">Tanggal</th>
                  <th class="text-center">Shift</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  $date = $search['tahun']."-".$search['bulan']."-01";
                  $num_day = date("t", strtotime($date));
                ?>
                <?php for ($i=1; $i <= intval($num_day) ; $i++): ?>
                  <?php 
                    $tanggal = $search['tahun']."-".$search['bulan']."-".str_pad($i, 2, '0', STR_PAD_LEFT);
                  ?>
                  <tr>
                    <td class="text-center" style="vertical-align:middle !important">
                      <input type="hidden" name="tanggal[]" value="<?=$tanggal?>">
                      <?php echo date_to_id($tanggal); ?>
                    </td>
                    <td>
                      <select class="form-control select2" name="kode[]" id="kode[]">
                        <option value="">-- Pilih Shift --</option>
                        <?php foreach($shift as $row2):?>
                          <option value="<?=$row2->kode?>" <?php if(@$mapping[$tanggal]['kode'] == $row2->kode){echo 'selected';}?>>
                            [<?=@$mapping[$tanggal]['kode']?>] <?=$row2->shift?><?php if($row2->kode != '0'){echo ' ('.$row2->jam_datang.' - '.$row2->jam_pulang.')';};?> 
                          </option>
                        <?php endforeach; ?>
                      </select>
                    </td>
                  </tr>
                <?php endfor; ?>
              </tbody>
            </table>
          </div>
          <div class="box-footer text-right">
            <button class="btn btn-flat btn-success btn-action" type="submit"><i class="fa fa-save"></i> Simpan</button>
            <a class="btn btn-flat btn-default btn-action" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Batal</a>
            <a id="btn-progress" class="btn btn-flat btn-success disabled"><i class="fa fa-spinner fa-spin"></i> Proses...</a>
          </div>
        </form>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<script>
  $(document).ready(function () {
    $('#btn-progress').hide();
    // form validator
    $('#form').validate({
      submitHandler: function (form) {
        $('#btn-progress').show();
        $('.btn-action').hide();
        form.submit();
      }
    });
  })
</script>