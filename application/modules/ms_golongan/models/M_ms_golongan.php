<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ms_golongan extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_golongan')
      ->get('ms_golongan',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_golongan')
      ->get('ms_golongan')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('ms_golongan')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id_golongan',$id)->get('ms_golongan')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_golongan','asc')->get('ms_golongan')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_golongan','desc')->get('ms_golongan')->row();
  }

  public function insert($data)
  {
    $this->db->insert('ms_golongan',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_golongan',$id)->update('ms_golongan',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id_golongan',$id)->update('ms_golongan',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_golongan',$id)->delete('ms_golongan');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ms_golongan");
  }

}
