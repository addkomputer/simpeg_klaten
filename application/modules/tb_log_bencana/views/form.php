<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-red"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">Administrasi Kehadiran</li>
        <li class="active text-orange"><?=$access->menu?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-danger">
        <div class="box-header with-border">
          <i class="fa fa-table text-maroon"></i>
          <h3 class="box-title">Form <?=$access->menu?></h3>
        </div>
        <form id="form" class="form-horizontal" action="<?=base_url().$this->access->controller?>/<?=$action?>" method="post" enctype="multipart/form-data" autocomplete="off"> 
          <div class="box-body">
            <input type="hidden" class="form-control" name="id" id="id" value="<?php if($main != null){echo $main->id;};?>">
            <div class="form-group">
              <label class="col-sm-2 control-label">Instansi</label>
              <div class="col-sm-5">
                <select class="form-control select2" name="id_instansi" id="id_instansi" <?php if($action == 'update'){echo 'disabled';}?>>
                  <?php if($this->session->userdata('id_grup') == 3): ?>
                    <?php foreach($instansi as $row): ?>
                      <?php if($this->session->userdata('id_instansi') == $row->id_instansi):?>
                        <option value="<?=$row->id_instansi?>" <?php if($main){if($main->id_instansi == $row->id_instansi){echo 'selected';}} ?>><?=$row->instansi?></option>
                      <?php endif; ?>
                    <?php endforeach;?>
                  <?php else: ?>
                    <option value="">-- Pilih Instansi --</option>
                    <?php foreach($instansi as $row): ?>
                      <option value="<?=$row->id_instansi?>" <?php if($main){if($main->id_instansi == $row->id_instansi){echo 'selected';}} ?>><?=$row->instansi?></option>
                    <?php endforeach;?>
                  <?php endif; ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Pegawai</label>
              <div class="col-sm-6">
                <select class="form-control select2" name="id_pegawai" id="id_pegawai" <?php if($action == 'update'){echo 'disabled';}?>>
                  <option value="">-- Pilih Pegawai --</option>
                  <?php foreach($pegawai as $row): ?>
                    <option value="<?=$row->id_pegawai?>" data-chained="<?=$row->id_instansi?>" <?php if($main){if($main->id_pegawai == $row->id_pegawai){echo 'selected';}} ?>>
                      <?php 
                        echo $row->gelar_depan.' '.$row->nama;
                        if ($row->gelar_belakang != ''){
                          echo ', '.$row->gelar_belakang;
                        }
                      ?> - <?=$row->nomor_induk?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>
            <?php if($action == 'insert'): ?>  
              <div class="form-group">
                <label class="col-sm-2 control-label">Periode Tanggal</label>
                <div class="col-sm-2">
                  <div class="input-group">
                    <input class="form-control datepicker" type="text" name="tanggal_mulai" value="" required>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                  </div>
                </div>
                <label class="col-sm-2 control-label" style="width:max-content !important;">Sampai Dengan</label>
                <div class="col-sm-2">
                  <div class="input-group">
                    <input class="form-control datepicker" type="text" name="tanggal_akhir" value="" required>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                  </div>
                </div>
              </div>
            <?php endif;?>
            <?php if($action == 'update'): ?>
              <div class="form-group">
                <label class="col-sm-2 control-label">Periode Tanggal</label>
                <div class="col-sm-2">
                  <div class="input-group">
                    <input class="form-control <?php if($action == 'insert'){echo 'datepicker';}?>" type="text" name="tanggal" value="<?php if($main){echo date_to_id($main->tanggal);}?>" required readonly>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                  </div>
                </div>
              </div>
            <?php endif;?>
            <div class="form-group">
              <label class="col-sm-2 control-label">Tipe</label>
              <div class="col-sm-3">
                <select class="form-control select2" name="id_tipe" id="id_tipe">
                  <option value="6" <?php if($main){if($main->id_tipe == '6'){echo 'selected';}} ?>>Bencana</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">File Bukti</label>
              <div class="col-sm-10">
                <input type="file" name="file_bukti" <?php if($action == 'insert'){echo 'required';} ?>/>
                <?php if($action == 'update'): ?>
                  <input type="hidden" name="old_file_bukti" value="<?php if($main){echo $main->file_bukti;}?>">
                  <?php if($main->file_bukti != ''): ?>
                    File : <a href="<?=base_url()?>berkas/bukti/<?=$main->file_bukti?>" target="_blank"><i class="fa fa-download"></i> <?=$main->file_bukti?></a>
                  <?php endif;?>
                <?php endif;?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Keterangan</label>
              <div class="col-sm-5">
                <textarea name="keterangan" id="keterangan" class="form-control"><?php if($main){echo $main->keterangan;}?></textarea>
              </div>
            </div>
            <hr>
            <div class="form-group">
              <div class="col-md-offset-2 col-sm-8">
                <button class="btn btn-flat btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button>
                <a class="btn btn-flat btn-default" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Batal</a>
              </div>
            </div>
          </div>
        </form>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<script>
  $(document).ready(function () {
    //chained
    <?php if($action == 'insert'):?>
      $("#id_pegawai").chained("#id_instansi");
    <?php endif;?>
    // form validator
    $('#form').validate({
      rules : {
        id_instansi : {
          valueNotEquals : ''
        },
        id_pegawai : {
          valueNotEquals : ''
        }
      }
    });
  })
</script>