<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <?php $this->load->view('pr_data/sidebar') ?>
        </div>
        <div class="col-md-9">
          <?php $this->load->view('pr_data/header'); ?>
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="<?=base_url().$access->controller?>/data_pokok/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Data Pokok</a></li>
              <li class="update"><a href="<?=base_url().$access->controller?>/posisi_jabatan/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Posisi dan Jabatan</a></li>
              <li class="update"><a href="<?=base_url().$access->controller?>/keluarga/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Keluarga</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                <?php echo $this->session->flashdata('status'); ?>
                <form id="form_data_pegawai" class="form-horizontal" method="post" action="<?=base_url().$access->controller?>/data_pokok_action/<?=$action?>" enctype="multipart/form-data" autocomplete="off">
                  <input type="hidden" class="form-control" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}else{echo $id_pegawai;}?>" readonly>
                  <div class="form-group">
                    <label class="col-md-2 control-label" >No. Kontak/HP</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control" name="no_hp" id="no_hp" value="<?php if($pegawai){echo $pegawai->no_hp;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label" >Ruang</label>
                    <div class="col-md-5">
                      <select class="form-control select2" name="id_ruang" id="id_ruang">
                        <option value="0">-- Pilih --</option>
                        <?php foreach($ruang as $row): ?>
                          <option value="<?=$row->id_ruang?>" <?php if($pegawai){if($pegawai->id_ruang == $row->id_ruang){echo 'selected';}}?>><?=$row->ruang?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Status PU PNS 2015</label>
                    <div class="col-md-3">
                      <select class="form-control select2" name="status_pupns_2015" id="status_pupns_2015">
                        <option value="0" <?php if($pegawai){if($pegawai->status_pupns_2015 == 0){echo 'selected';}}?>>Belum Terdaftar</option>
                        <option value="1" <?php if($pegawai){if($pegawai->status_pupns_2015 == 1){echo 'selected';}}?>>Terdaftar PU PNS 2015</option>
                      </select>
                    </div>
                    <label class="col-md-2 control-label no_registrasi_pupns" style="width:max-content !important;">No. Registrasi *</label>
                    <div class="col-md-3 no_registrasi_pupns">
                      <input type="text" class="form-control" name="no_registrasi_pupns" id="no_registrasi_pupns" value="<?php if($pegawai){echo $pegawai->no_registrasi_pupns;}?>">
                      <small class="text-red">*) Diisi setelah registrasi PU PNS</small>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">NIK</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control" name="nik" id="nik" value="<?php if($pegawai){echo $pegawai->nik;}?>" required>
                    </div>
                    <label class="col-md-2 control-label" style="width:max-content !important;">Status KPE</label>
                    <div class="col-md-3  ">
                      <select class="form-control select2" name="" id="">
                        <option value="0" <?php if($pegawai){if($pegawai->status_kpe == 0){echo 'selected';}}?>> Belum memiliki KPE </option>
                        <option value="1" <?php if($pegawai){if($pegawai->status_kpe == 1){echo 'selected';}}?>> Sudah memiliki KPE </option>
                        <option value="2" <?php if($pegawai){if($pegawai->status_kpe == 2){echo 'selected';}}?>> Proses ralat karena rusak </option>
                        <option value="3" <?php if($pegawai){if($pegawai->status_kpe == 3){echo 'selected';}}?>> Proses ralat karena hilang </option>
                        <option value="4" <?php if($pegawai){if($pegawai->status_kpe == 4){echo 'selected';}}?>> Belum memiliki tetapi sudah foto </option>
                        <option value="5" <?php if($pegawai){if($pegawai->status_kpe == 5){echo 'selected';}}?>> Belum memiliki dan belum foto </option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">NIP Baru</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control" name="nomor_induk" id="nomor_induk" value="<?php if($pegawai){echo $pegawai->nomor_induk;}?>" required>
                    </div>
                    <label class="col-md-2 control-label" style="width:max-content !important;">NIP Lama</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control" name="nip_lama" id="nip_lama" value="<?php if($pegawai){echo $pegawai->nip_lama;}?>" required>
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Nama Pegawai</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control" name="gelar_depan" id="gelar_depan" placeholder="Gelar Depan" value="<?php if($pegawai){echo $pegawai->gelar_depan;}?>">
                    </div>
                    <div class="col-md-4">
                      <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Lengkap" value="<?php if($pegawai){echo $pegawai->nama;}?>" required>
                    </div>
                    <div class="col-md-3">
                      <input type="text" class="form-control" name="gelar_belakang" id="gelar_belakang" placeholder="Gelar Belakang" value="<?php if($pegawai){echo $pegawai->gelar_belakang;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Tempat Lahir</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control" name="tempat_lhr" id="tempat_lhr" value="<?php if($pegawai){echo $pegawai->tempat_lhr;}?>" required>
                    </div>
                    <label class="col-md-2 control-label" style="width:max-content !important;">Tanggal Lahir</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control datepicker" name="tgl_lhr" id="tgl_lhr" value="<?php if($pegawai){echo date_to_id($pegawai->tgl_lhr);}?>" placeholder="dd-mm-yyyy" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Agama</label>
                    <div class="col-md-2">
                      <select class="form-control select2" name="agama" id="agama">
                        <option value="0" <?php if($pegawai){if($pegawai->agama == 0){echo 'selected';}}?>> -- Pilih -- </option>
                        <option value="1" <?php if($pegawai){if($pegawai->agama == 1){echo 'selected';}}?>> Islam </option>
                        <option value="2" <?php if($pegawai){if($pegawai->agama == 2){echo 'selected';}}?>> Protestan </option>
                        <option value="3" <?php if($pegawai){if($pegawai->agama == 3){echo 'selected';}}?>> Katholik </option>
                        <option value="4" <?php if($pegawai){if($pegawai->agama == 4){echo 'selected';}}?>> Hindu </option>
                        <option value="5" <?php if($pegawai){if($pegawai->agama == 5){echo 'selected';}}?>> Budha </option>
                      </select>
                    </div>
                    <label class="col-md-2 control-label" style="width:max-content !important;">Jenis Kelamin</label>
                    <div class="col-md-2">
                      <select class="form-control select2" name="jenis_kelamin" id="jenis_kelamin">
                        <option value="0" <?php if($pegawai){if($pegawai->jenis_kelamin == 0){echo 'selected';}}?>> -- Pilih -- </option>
                        <option value="1" <?php if($pegawai){if($pegawai->jenis_kelamin == 1){echo 'selected';}}?>> Laki-laki </option>
                        <option value="2" <?php if($pegawai){if($pegawai->jenis_kelamin == 2){echo 'selected';}}?>> Perempuan </option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Alamat Rumah</label>
                    <div class="col-md-5">
                      <input type="text" class="form-control" name="alamat" id="alamat" value="<?php if($pegawai){echo $pegawai->alamat;}?>" required>
                    </div>
                    <label class="col-md-2 control-label" style="width:max-content !important;">Kode POS</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control" name="kode_pos_rumah" id="kode_pos_rumah" value="<?php if($pegawai){echo $pegawai->kode_pos_rumah;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Mutasi Dari</label>
                    <div class="col-md-5">
                      <input type="text" class="form-control" name="" id="" value="">
                    </div>
                    <label class="col-md-2 control-label" style="width:max-content !important;">TMT Mutasi</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control datepicker" name="" id="" value="" placeholder="dd-mm-yyyy">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Status Pegawai</label>
                    <div class="col-md-2">
                      <select class="form-control select2" name="id_status_pegawai" id="id_status_pegawai">
                        <?php foreach($status_pegawai as $row): ?>
                          <option value="<?=$row->id_status_pegawai?>" <?php if($pegawai){if($row->id_status_pegawai == $pegawai->id_status_pegawai){echo 'selected';};}?>><?=$row->status_pegawai?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                    <label class="col-md-2 control-label" style="width:max-content !important;">TMT CPNS</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control datepicker" name="tmt_cpns" id="tmt_cpns" value="<?php if($pegawai){echo date_to_id($pegawai->tmt_cpns);}?>" placeholder="dd-mm-yyyy" required>
                    </div>
                    <label class="col-md-2 control-label" style="width:max-content !important;">TMT PNS</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control datepicker" name="tmt_pns" id="tmt_pns" value="<?php if($pegawai){echo date_to_id($pegawai->tmt_pns);}?>" placeholder="dd-mm-yyyy" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Seri Karpeg</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control" name="seri_karpeg" id="seri_karpeg" value="<?php if($pegawai){echo $pegawai->seri_karpeg;}?>">
                    </div>
                    <label class="col-md-2 control-label" style="width:max-content !important;">Tanggal</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control datepicker" name="tgl_karpeg" id="tgl_karpeg" value="<?php if($pegawai){echo date_to_id($pegawai->tgl_karpeg);}?>" placeholder="dd-mm-yyyy">
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Pendidikan CPNS</label>
                    <div class="col-md-2">
                      <select class="form-control select2" name="tingkat_pend_cpns" id="tingkat_pend_cpns">
                        <option value="0" <?php if($pegawai){if($pegawai->tingkat_pend_cpns == 0){echo 'selected';}}?>> -- Pilih -- </option>
                        <option value="1" <?php if($pegawai){if($pegawai->tingkat_pend_cpns == 1){echo 'selected';}}?>> SD / PAKET A</option>
                        <option value="2" <?php if($pegawai){if($pegawai->tingkat_pend_cpns == 2){echo 'selected';}}?>> SMP / PAKET B</option>
                        <option value="3" <?php if($pegawai){if($pegawai->tingkat_pend_cpns == 3){echo 'selected';}}?>> SMA / SMK / PAKET C</option>
                        <option value="4" <?php if($pegawai){if($pegawai->tingkat_pend_cpns == 4){echo 'selected';}}?>> D-1 </option>
                        <option value="5" <?php if($pegawai){if($pegawai->tingkat_pend_cpns == 5){echo 'selected';}}?>> D-2 </option>
                        <option value="6" <?php if($pegawai){if($pegawai->tingkat_pend_cpns == 6){echo 'selected';}}?>> D-3 </option>
                        <option value="7" <?php if($pegawai){if($pegawai->tingkat_pend_cpns == 7){echo 'selected';}}?>> D-4 </option>
                        <option value="8" <?php if($pegawai){if($pegawai->tingkat_pend_cpns == 8){echo 'selected';}}?>> S-1 </option>
                        <option value="9" <?php if($pegawai){if($pegawai->tingkat_pend_cpns == 9){echo 'selected';}}?>> S-2 </option>
                        <option value="10" <?php if($pegawai){if($pegawai->tingkat_pend_cpns == 10){echo 'selected';}}?>> S-3 </option>
                      </select>
                    </div>
                    <div class="col-md-4">
                      <input type="text" class="form-control" name="pend_cpns" id="pend_cpns" value="<?php if($pegawai){echo $pegawai->pend_cpns;}?>" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Pendidikan Terakhir</label>
                    <div class="col-md-2">
                      <select class="form-control select2" name="tingkat_pend_akhir" id="tingkat_pend_akhir">
                        <option value="0" <?php if($pegawai){if($pegawai->tingkat_pend_akhir == 0){echo 'selected';}}?>> -- Pilih -- </option>
                        <option value="1" <?php if($pegawai){if($pegawai->tingkat_pend_akhir == 1){echo 'selected';}}?>> SD / PAKET A</option>
                        <option value="2" <?php if($pegawai){if($pegawai->tingkat_pend_akhir == 2){echo 'selected';}}?>> SMP / PAKET B</option>
                        <option value="3" <?php if($pegawai){if($pegawai->tingkat_pend_akhir == 3){echo 'selected';}}?>> SMA / SMK / PAKET C</option>
                        <option value="4" <?php if($pegawai){if($pegawai->tingkat_pend_akhir == 4){echo 'selected';}}?>> D-1 </option>
                        <option value="5" <?php if($pegawai){if($pegawai->tingkat_pend_akhir == 5){echo 'selected';}}?>> D-2 </option>
                        <option value="6" <?php if($pegawai){if($pegawai->tingkat_pend_akhir == 6){echo 'selected';}}?>> D-3 </option>
                        <option value="7" <?php if($pegawai){if($pegawai->tingkat_pend_akhir == 7){echo 'selected';}}?>> D-4 </option>
                        <option value="8" <?php if($pegawai){if($pegawai->tingkat_pend_akhir == 8){echo 'selected';}}?>> S-1 </option>
                        <option value="9" <?php if($pegawai){if($pegawai->tingkat_pend_akhir == 9){echo 'selected';}}?>> S-2 </option>
                        <option value="10" <?php if($pegawai){if($pegawai->tingkat_pend_akhir == 10){echo 'selected';}}?>> S-3 </option>
                      </select>
                    </div>
                    <div class="col-md-4">
                      <input type="text" class="form-control" name="pend_terakhir" id="pend_terakhir" value="<?php if($pegawai){echo $pegawai->pend_terakhir;}?>" required>
                    </div>
                    <label class="col-md-2 control-label" style="width:max-content !important;">Tahun Lulus</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control" name="thn_lulus_pend" id="thn_lulus_pend" value="<?php if($pegawai){echo $pegawai->thn_lulus_pend;}?>" required>
                    </div>
                    <div class="col-md-offset-2 col-md-5">
                      <small class="text-red">*) Di isi berdasarkan pendidikan terakhir yang diakui secara kedinasan</small>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Golongan Ruang</label>
                    <div class="col-md-4">
                      <select class="form-control select2" name="id_golongan" id="id_golongan">
                        <option value="0" <?php if($pegawai){if($pegawai->id_golongan == 0){echo 'selected';}}?>> -- Pilih -- </option>
                        <?php foreach($golongan as $row): ?>
                          <option value="<?=$row->id_golongan?>" <?php if($pegawai){if($pegawai->id_golongan == $row->id_golongan){echo 'selected';}}?>><?=$row->golongan.' - '.$row->pangkat?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                    <label class="col-md-2 control-label" style="width:max-content !important;">TMT Golongan</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control datepicker" name="tmt_gol" id="tmt_gol" value="<?php if($pegawai){echo date_to_id($pegawai->tmt_gol);}?>" placeholder="dd-mm-yyyy" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Masa Kerja</label>
                    <div class="col-md-1">
                      <input type="text" class="form-control" name="masakerja_gol_thn" id="masakerja_gol_thn" value="<?php if($pegawai){echo $pegawai->masakerja_gol_thn;}?>" required>
                    </div>
                    <label class="col-md-2 control-label" style="width:max-content !important;">Tahun</label>
                    <div class="col-md-1">
                      <input type="text" class="form-control" name="masakerja_gol_bln" id="masakerja_gol_bln" value="<?php if($pegawai){echo $pegawai->masakerja_gol_bln;}?>" required>
                    </div>
                    <label class="col-md-2 control-label" style="width:max-content !important;">Bulan</label>
                    <div class="col-md-offset-2 col-md-10">
                      <small class="text-red">	*) Di isi berdasarkan masa kerja di SK Kenaikan Pangkat/ Kenaikan Gaji berkala terakhir</small>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">NPWP</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control" name="npwp" id="npwp" value="<?php if($pegawai){echo $pegawai->npwp;}?>">
                    </div>
                    <div class="col-md-offset-2 col-md-10">
                      <small class="text-red">*) Nomor NPWP yang dimiliki Pegawai</small>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Kedudukan Pegawai</label>
                    <div class="col-md-4">
                      <select class="form-control select2" name="id_kedudukan_pns" id="id_kedudukan_pns">
                        <option value="0" <?php if($pegawai){if($pegawai->id_kedudukan_pns == 0){echo 'selected';}}?>> -- Pilih -- </option>
                        <option value="1" <?php if($pegawai){if($pegawai->id_kedudukan_pns == 1){echo 'selected';}}?>> Pegawai Aktif </option>
                        <option value="2" <?php if($pegawai){if($pegawai->id_kedudukan_pns == 2){echo 'selected';}}?>>Pensiun (Batas Usia Pensiun)</option>
                        <option value="3" <?php if($pegawai){if($pegawai->id_kedudukan_pns == 3){echo 'selected';}}?>>Diberhentikan karena Hukuman Disiplin</option>
                        <option value="4" <?php if($pegawai){if($pegawai->id_kedudukan_pns == 4){echo 'selected';}}?>>Meninggal Dunia/Janda/Duda</option>
                        <option value="5" <?php if($pegawai){if($pegawai->id_kedudukan_pns == 5){echo 'selected';}}?>>Berhenti Atas Permintaan Sendiri (APS)</option>
                        <option value="6" <?php if($pegawai){if($pegawai->id_kedudukan_pns == 6){echo 'selected';}}?>>Mutasi keluar</option>
                        <option value="7" <?php if($pegawai){if($pegawai->id_kedudukan_pns == 7){echo 'selected';}}?>>Mengundurkan Diri dari PNS</option>
                        <option value="8" <?php if($pegawai){if($pegawai->id_kedudukan_pns == 8){echo 'selected';}}?>>Mengundurkan Diri dari CPNS</option>
                        <option value="9" <?php if($pegawai){if($pegawai->id_kedudukan_pns == 9){echo 'selected';}}?>>Cuti diluar Tanggungan Negara</option>
                        <option value="10" <?php if($pegawai){if($pegawai->id_kedudukan_pns == 10){echo 'selected';}}?>>Tidak Diketahui</option>
                      </select>
                    </div>
                  </div>
                  <hr>
                  <?php
                    if ($action=='update') {
                      ?>
                      <h4>Upload File</h4>
                      <div class="form-group">
                        <label class="col-md-2 control-label">File Kartu Pegawai</label>
                        <div class="col-md-5">
                          <input type="file" class="form-control" name="file_karpeg" />
                        </div>
                        <div class="col-md-3">
                          <?php if($pegawai->file_karpeg!= null){ ?>
                            <a class="btn btn-sm btn-success btn-flat" target="_blank" rel="noopener noreferrer" href="<?=base_url()?>berkas/data_pokok/<?=$pegawai->file_karpeg?>" style="padding-right:0.5em;"><i class="fa fa-eye"></i> Lihat File</a>
                          <?php }else{ ?>
                            <a class="badge btn-danger btn-flat" href="#" style="padding-right:0.5em;"><i class="fa fa-warning"> Belum Ada</i></a>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                          <em>Sebelum mengunggah file, pastikan nama file yang akan diunggah sudah benar sesuai dengan isi file. Jenis file yang diterima adalah *.jpg,*.png,*.jpeg dan *.pdf</em>
                        </div>
                      </div>
                      <hr>
                      <?php
                      }
                  ?>
                  <div class="form-group">
                    <div class="col-md-offset-2 col-sm-8">
                      <button class="btn btn-success btn-flat" type="submit"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
        </div>
      </div>

    </section>
  </div>
<!-- /.content-wrapper -->
<!-- /script -->
<script>
  $(document).ready(function () {
    // form validator
    $('#form_data_pegawai').validate({
      rules:{
        no_hp : {
          number : true
        },
        agama : {
          valueNotEquals : '0'
        },
        jenis_kelamin : {
          valueNotEquals : '0'
        },
        nik : {
          number : true
        },
        nomor_induk : {
          number : true
        },
        nip_lama : {
          number : true
        },
        kode_pos_rumah : {
          number : true
        },
        thn_lulus_pend : {
          number : true
        },
        tingkat_pend_cpns : {
          valueNotEquals : '0'
        },
        tingkat_pend_akhir : {
          valueNotEquals : '0'
        },
        id_golongan : {
          valueNotEquals : '0'
        },
        masakerja_gol_thn : {
          number : true
        },
        masakerja_gol_bln : {
          number : true
        },
        id_kedudukan_pns : {
          valueNotEquals : '0'
        }
      },
      submitHandler: function(form) {
        $(form).ajaxSubmit();
      }
    });
    //status pu pns
    <?php if($action == 'insert'): ?>
      $('.no_registrasi_pupns').hide();
      $('.update').hide();
    <?php else:?>
      if ($('#status_pupns_2015').val() == 0) {
        $('.no_registrasi_pupns').hide();
      }else{
        $('.no_registrasi_pupns').show();
      }
    <?php endif;?>
    $('#status_pupns_2015').on('change', function() {
      if(this.value == 0){
        $('.no_registrasi_pupns').hide();
      }else{
        $('.no_registrasi_pupns').show();
      }
    });
  })
</script>
