<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <?php $this->load->view('pr_data/sidebar'); ?>
        </div>
        <div class="col-md-9">
          <?php $this->load->view('pr_data/header'); ?>
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="update"><a href="<?=base_url()?>pr_data/riwayat_ppk/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Riwayat PPK</a></li>
              <li class="update"><a href="<?=base_url()?>pr_data/skp/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">SKP</a></li>
              <li class="active"><a href="<?=base_url()?>pr_data/realisasi/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Realisasi</a></li>
              <li class="update"><a href="<?=base_url()?>pr_data/perilaku_kerja/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Perilaku</a></li>
              <li class="update"><a href="<?=base_url()?>pr_data/tugas_belajar/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Tugas Belajar</a></li>
              <li class="update"><a href="<?=base_url()?>pr_data/cetak_tugas_tambahan/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Cetak SK Tugas Tambahan</a></li>
              <li class="update"><a href="<?=base_url()?>pr_data/penilaian_gabung/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Penilaian Gabung</a></li>
              <li class="update"><a href="<?=base_url()?>pr_data/cetak_ppk/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Cetak PPK</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                <form id="form" class="form-horizontal" action="<?=base_url().$access->controller?>/realisasi_action/<?=$action_tgs_pokok?>" method="post" autocomplete="off"> 
                  <h4 class="text-orange">Realisasi Kerja Pegawai</h4>
                  <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
                  <input type="hidden" name="id" id="id" value="<?php if($tgs_pokok){echo $tgs_pokok->id;}?>">
                  <div class="form-group">
                    <label class="col-md-2 control-label">Tugas Jabatan</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" name="tgs" id="tgs" value="<?php if($tgs_pokok){echo $tgs_pokok->tgs;}?>" readonly required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">AK</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control" name="t_ak" id="t_ak" value="<?php if($tgs_pokok){echo $tgs_pokok->t_ak;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Kuantitas</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control" name="t_kuant" id="t_kuant" value="<?php if($tgs_pokok){echo $tgs_pokok->t_kuant;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Satuan</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control" name="satuan_output" id="satuan_output" value="<?php if($tgs_pokok){echo $tgs_pokok->satuan_output;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Kualitas/Mutu</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control" name="t_kual" id="t_kual" value="<?php if($tgs_pokok){echo $tgs_pokok->t_kual;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Waktu</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control" name="t_waktu" id="t_waktu" value="<?php if($tgs_pokok){echo $tgs_pokok->t_waktu;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Satuan Waktu</label>
                    <div class="col-md-2">
                      <select class="form-control select2" name="satuan_waktu" id="satuan_waktu">
                        <option value="0" <?php if($tgs_pokok){if($tgs_pokok->satuan_waktu = 0){echo 'selected';}}?>> -- Pilih -- </option>
                        <option value="1" <?php if($tgs_pokok){if($tgs_pokok->satuan_waktu = 1){echo 'selected';}}?>> Menit </option>
                        <option value="2" <?php if($tgs_pokok){if($tgs_pokok->satuan_waktu = 2){echo 'selected';}}?>> Jam </option>
                        <option value="3" <?php if($tgs_pokok){if($tgs_pokok->satuan_waktu = 3){echo 'selected';}}?>> Hari </option>
                        <option value="4" <?php if($tgs_pokok){if($tgs_pokok->satuan_waktu = 4){echo 'selected';}}?>> Bulan </option>
                        <option value="5" <?php if($tgs_pokok){if($tgs_pokok->satuan_waktu = 5){echo 'selected';}}?>> Tahun </option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Biaya</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control" name="t_biaya" id="t_biaya" value="<?php if($tgs_pokok){echo $tgs_pokok->t_biaya;}?>">
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <div class="col-md-offset-2 col-sm-8">
                      <button class="btn btn-flat btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button>
                      <a class="btn btn-flat btn-default" href="<?=base_url()?>pr_data/realisasi/<?=$pegawai->id_pegawai?>"><i class="fa fa-close"></i> Batal</a>
                    </div>
                  </div>
                </form>
                <hr>  
                <form id="form" class="form-horizontal" action="<?=base_url().$access->controller?>/tgs_tambahan_action/<?=$action_tgs_tambahan?>" method="post" autocomplete="off"> 
                  <h4 class="text-orange">Tugas Tambahan</h4>
                  <p>
                    Tugas Tambahan adalah Tugas Lain atau tugas-tugas yang ada hubunganya dengan tugas jabatan yang bersangkutan dan TIDAK ADA dalam SKP yang ditetapkan (awal tahun)
                    Isikan TUGAS TAMBAHAN dan NO. SK yang MINIMAL ditandatangani oleh Eselon II) <br>
                    <i class="text-blue">Contoh : Menjadi Tim Teknis Penerimaan CPNS (SK Bupati Kebumen No. XXI/20/2015)</i>
                  </p>
                  <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
                  <input type="hidden" name="id" id="id" value="<?php if($tgs_tambahan){echo $tgs_tambahan->id;}?>">
                  <div class="form-group">
                    <label class="col-md-2 control-label">Tugas Tambahan</label>
                    <div class="col-md-8">
                      <input type="text" class="form-control" name="tgs_tambahan" id="tgs_tambahan" value="<?php if($tgs_tambahan){echo $tgs_tambahan->tgs_tambahan;}?>" required>
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <div class="col-md-offset-2 col-sm-8">
                      <button class="btn btn-flat btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button>
                      <a class="btn btn-flat btn-default" href="<?=base_url()?>pr_data/realisasi/<?=$pegawai->id_pegawai?>"><i class="fa fa-close"></i> Batal</a>
                    </div>
                  </div>
                  <hr>
                </form>
                <form id="form" class="form-horizontal" action="<?=base_url().$access->controller?>/tgs_kreatifitas_action/<?=$action_tgs_kreatifitas?>" method="post" autocomplete="off"> 
                  <h4 class="text-orange">Kreatifitas</h4>
                  <p>
                    Isikan Kreatifitas dan NO. SK <br>
                    <i class="text-blue">Contoh : Menjadi Penemu Bahan Bakar Air (SK Bupati Kebumen No. XXI/20/2015)</i>
                  </p>
                  <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
                  <input type="hidden" name="id" id="id" value="<?php if($tgs_kreatifitas){echo $tgs_kreatifitas->id;}?>">
                  <div class="form-group">
                    <label class="col-md-2 control-label">Tugas Kreatifitas</label>
                    <div class="col-md-8">
                      <input type="text" class="form-control" name="tgs_kreatifitas" id="tgs_kreatifitas" value="<?php if($tgs_kreatifitas){echo $tgs_kreatifitas->tgs_kreatifitas;}?>" required>
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <div class="col-md-offset-2 col-sm-8">
                      <button class="btn btn-flat btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button>
                      <a class="btn btn-flat btn-default" href="<?=base_url()?>pr_data/realisasi/<?=$pegawai->id_pegawai?>"><i class="fa fa-close"></i> Batal</a>
                    </div>
                  </div>
                  <hr>
                </form>
                <table class="table table-striped table-bordered table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" style="vertical-align:middle" width="30" rowspan="2">No.</th>
                    <th class="text-center" style="vertical-align:middle" width="60" rowspan="2">Aksi</th>
                    <th class="text-center" style="vertical-align:middle" width="" rowspan="2">Kegiatan Tugas Tambahan</th>
                    <th class="text-center" style="vertical-align:middle" width="" rowspan="2">AK</th>
                    <th class="text-center" style="vertical-align:middle" width="" colspan="6">Target</th>
                  </tr>
                  <tr>
                    <th class="text-center" colspan="2">Kuant/Output</th>
                    <th class="text-center">Kual/Mutu</th>
                    <th class="text-center" colspan="2">Waktu</th>
                    <th class="text-center">Biaya</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if ($riwayat_tgs_pokok != null): ?>
                    <?php $i=1;foreach ($riwayat_tgs_pokok as $row): ?>
                      <tr>
                        <td class="text-center"><?=$i++?></td>
                        <td class="text-center">
                          <?php if($row->id != 0): ?>
                            <a class="btn btn-xs btn-flat btn-warning" href="<?=base_url()?>pr_data/realisasi/<?=$pegawai->id_pegawai?>/1/<?=$row->id?>" style="padding-right:0.250em;"><i class="fa fa-pencil"></i></a> 
                          <?php endif;?>
                        </td>
                        <td><?=$row->tgs?></td>
                        <td class="text-center"><?=$row->t_ak?></td>
                        <td class="text-center"><?=$row->t_kuant?></td>
                        <td class="text-center"><?=$row->satuan_output?></td>
                        <td class="text-center"><?=$row->t_kual?></td>
                        <td class="text-center"><?=$row->t_waktu?></td>
                        <?php 
                          $satuan_waktu = '';
                          switch ($row->satuan_waktu) {
                            case 1:
                              $satuan_waktu = 'Menit';
                              break;
                            case 2:
                              $satuan_waktu = 'Jam';
                              break;
                            case 3:
                              $satuan_waktu = 'Hari';
                              break;
                            case 4:
                              $satuan_waktu = 'Bulan';
                              break;
                            case 5:
                              $satuan_waktu = 'Tahun';
                              break;
                          }
                        ?>
                        <td class="text-center"><?=$satuan_waktu?></td>
                        <td class="text-center"><?=$row->t_biaya?></td>
                      </tr>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <tr>
                      <td class="text-center" colspan="99">Data tidak ada!</td>
                    </tr>
                  <?php endif; ?>
                  <tr>
                    <th colspan="99">Tugas Tambahan</th>
                  </tr>
                  <tr>
                    <th class="text-center">No.</th>
                    <th class="text-center" width="60">Aksi</th>
                    <th class="text-center" colspan="99">Tugas Tambahan</th>
                  </tr>
                  <?php if($riwayat_tgs_tambahan != null): ?>
                    <?php $i=1;foreach($riwayat_tgs_tambahan as $row): ?>
                      <tr>
                        <td><?=$i++?></td>
                        <td class="text-center">
                          <a class="btn btn-xs btn-flat btn-warning" href="<?=base_url()?>pr_data/realisasi/<?=$pegawai->id_pegawai?>/2/<?=$row->id?>" style="padding-right:0.250em;"><i class="fa fa-pencil"></i></a> 
                          <a class="btn btn-xs btn-flat btn-danger" href="#" onclick="del_tgs_tambahan(<?=$row->id?>)"><i class="fa fa-trash"></i></a>
                        </td>
                        <td colspan="99"><?=$row->tgs_tambahan?></td>
                      </tr>
                    <?php endforeach;?>
                  <?php else: ?>
                    <tr>
                      <td class="text-center" colspan="99">Data tidak ada!</td>
                    </tr>
                  <?php endif;?>
                  <tr>
                    <th colspan="99">Kreatifitas</th>
                  </tr>
                  <tr>
                    <th class="text-center">No.</th>
                    <th class="text-center" width="60">Aksi</th>
                    <th class="text-center" colspan="99">Tugas Kreatifitas</th>
                  </tr>
                  <?php if($riwayat_tgs_kreatifitas != null): ?>
                    <?php $i=1;foreach($riwayat_tgs_kreatifitas as $row): ?>
                      <tr>
                        <td><?=$i++?></td>
                        <td class="text-center">
                          <a class="btn btn-xs btn-flat btn-warning" href="<?=base_url()?>pr_data/realisasi/<?=$pegawai->id_pegawai?>/3/<?=$row->id?>" style="padding-right:0.250em;"><i class="fa fa-pencil"></i></a> 
                          <a class="btn btn-xs btn-flat btn-danger" href="#" onclick="del_tgs_kreatifitas(<?=$row->id?>)"><i class="fa fa-trash"></i></a>
                        </td>
                        <td colspan="99"><?=$row->tgs_kreatifitas?></td>
                      </tr>
                    <?php endforeach;?>
                  <?php else: ?>
                    <tr>
                      <td class="text-center" colspan="99">Data tidak ada!</td>
                    </tr>
                  <?php endif;?>
                </tbody>
              </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
<!-- /.content-wrapper -->
<!-- Modal Delete -->
<div id="modal_delete_tgs_tambahan" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>pr_data/tgs_tambahan_action/delete/" method="post">
        <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
        <input type="hidden" name="id" id="id_delete_tgs_tambahan">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-flat btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Modal Delete -->
<div id="modal_delete_tgs_kreatifitas" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>pr_data/tgs_kreatifitas_action/delete/" method="post">
        <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
        <input type="hidden" name="id" id="id_delete_tgs_kreatifitas">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-flat btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /script -->
<script>
  $(document).ready(function () {
    $('#form').validate({
      rules : {
        id_hukuman : {
          valueNotEquals : '0'
        }
      }
    })
  })
  function del_tgs_tambahan(id) {
    $("#modal_delete_tgs_tambahan").modal('show');
    $("#id_delete_tgs_tambahan").val(id);
  }
  function del_tgs_kreatifitas(id) {
    $("#modal_delete_tgs_kreatifitas").modal('show');
    $("#id_delete_tgs_kreatifitas").val(id);
  }
</script>