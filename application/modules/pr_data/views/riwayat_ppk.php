<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <?php $this->load->view('pr_data/sidebar'); ?>
        </div>
        <div class="col-md-9">
          <?php $this->load->view('pr_data/header'); ?>
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="<?=base_url()?>pr_data/riwayat_ppk/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Riwayat PPK</a></li>
              <li class="update"><a href="<?=base_url()?>pr_data/skp/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">SKP</a></li>
              <li class="update"><a href="<?=base_url()?>pr_data/realisasi/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Realisasi</a></li>
              <li class="update"><a href="<?=base_url()?>pr_data/perilaku_kerja/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Perilaku</a></li>
              <li class="update"><a href="<?=base_url()?>pr_data/tugas_belajar/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Tugas Belajar</a></li>
              <li class="update"><a href="<?=base_url()?>pr_data/cetak_tugas_tambahan/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Cetak SK Tugas Tambahan</a></li>
              <li class="update"><a href="<?=base_url()?>pr_data/penilaian_gabung/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Penilaian Gabung</a></li>
              <li class="update"><a href="<?=base_url()?>pr_data/cetak_ppk/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Cetak PPK</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                <form id="form" class="form-horizontal" action="<?=base_url().$access->controller?>/riwayat_ppk_action/<?=$action?>" method="post" autocomplete="off"> 
                  <h4 class="text-orange">Isikan Nilai Resmi Penilaian Prestasi Kerja PNS</h4>
                  <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
                  <input type="hidden" name="id" id="id" value="<?php if($ppk){echo $ppk->id;}?>">
                  <div class="form-group">
                    <label class="col-md-2 control-label">Tahun Penilaian</label>
                    <div class="col-md-2">
                      <input type="number" class="form-control" name="tahun" id="tahun" value="<?php if($ppk){echo $ppk->tahun;}?>" required>
                    </div>
                  </div>
                  <h5 class="text-blue">Nilai Sasaran Kerja</h5>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Realisasi SKP (belum dikali 60%)</label>
                    <div class="col-md-2">
                      <input type="number" class="form-control" name="nilai_capaian_skp" id="nilai_capaian_skp" value="<?php if($ppk){echo $ppk->nilai_capaian_skp;}?>">
                    </div>
                  </div>
                  <h5 class="text-blue">Penilaian Perilaku</h5>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Orientasi Pelayanan</label>
                    <div class="col-md-2">
                      <input type="number" class="form-control" name="op" id="op" value="<?php if($ppk){echo $ppk->op;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Intergritas</label>
                    <div class="col-md-2">
                      <input type="number" class="form-control" name="integritas" id="integritas" value="<?php if($ppk){echo $ppk->integritas;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Komitmen</label>
                    <div class="col-md-2">
                      <input type="number" class="form-control" name="kom" id="kom" value="<?php if($ppk){echo $ppk->kom;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Disiplin</label>
                    <div class="col-md-2">
                      <input type="number" class="form-control" name="dis" id="dis" value="<?php if($ppk){echo $ppk->dis;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Kerjasama</label>
                    <div class="col-md-2">
                      <input type="number" class="form-control" name="kerjasama" id="kerjasama" value="<?php if($ppk){echo $ppk->kerjasama;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Kepemimpinan</label>
                    <div class="col-md-2">
                      <input type="number" class="form-control" name="kepemimpinan" id="kepemimpinan" value="<?php if($ppk){echo $ppk->kepemimpinan;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Rata-rata Nilai Perilaku</label>
                    <div class="col-md-2">
                      <input type="number" class="form-control" name="ratarata_perilaku" id="ratarata_perilaku" value="<?php if($ppk){echo $ppk->ratarata_perilaku;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Nilai PPK PNS</label>
                    <div class="col-md-2">
                      <input type="number" class="form-control" name="nilai_ppk" id="nilai_ppk" value="<?php if($ppk){echo $ppk->nilai_ppk;}?>">
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <div class="col-md-offset-2 col-sm-8">
                      <button class="btn btn-flat btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                  </div>
                </form>
                <table class="table table-striped table-bordered table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" style="vertical-align:middle" width="30">No.</th>
                    <th class="text-center" style="vertical-align:middle" width="60">Aksi</th>
                    <th class="text-center" style="vertical-align:middle" width="">Tahun</th>
                    <th class="text-center" style="vertical-align:middle" width="">Capaian SKP di Lembar Realisasi SKP (belum dikalikan 60%)</th>
                    <th class="text-center" style="vertical-align:middle" width="">Orientasi Pelayanan</th>
                    <th class="text-center" style="vertical-align:middle" width="">Integritas</th>
                    <th class="text-center" style="vertical-align:middle" width="">Komitmen</th>
                    <th class="text-center" style="vertical-align:middle" width="">Disiplin</th>
                    <th class="text-center" style="vertical-align:middle" width="">Kerjasama</th>
                    <th class="text-center" style="vertical-align:middle" width="">Kepemimpinan</th>
                    <th class="text-center" style="vertical-align:middle" width="">Nilai Rata-rata Perilaku PNS di Lembar Penilaian Perilaku</th>
                    <th class="text-center" style="vertical-align:middle" width="">Nilai PPK</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if ($riwayat != null): ?>
                    <?php $i=1;foreach ($riwayat as $row): ?>
                      <tr>
                        <td class="text-center"><?=$i++?></td>
                        <td class="text-center">
                          <?php if($row->id != 0): ?>
                            <a class="btn btn-xs btn-flat btn-warning" href="<?=base_url()?>pr_data/riwayat_ppk/<?=$pegawai->id_pegawai?>/<?=$row->id?>" style="padding-right:0.250em;"><i class="fa fa-pencil"></i></a> 
                            <a class="btn btn-xs btn-flat btn-danger" href="#" onclick="del(<?=$row->id?>)"><i class="fa fa-trash"></i></a>
                          <?php endif;?>
                        </td>
                        <td><?=$row->tahun?></td>
                        <td class="text-center"><?=$row->nilai_capaian_skp?></td>
                        <td class="text-center"><?=$row->op?></td>
                        <td class="text-center"><?=$row->integritas?></td>
                        <td class="text-center"><?=$row->kom?></td>
                        <td class="text-center"><?=$row->dis?></td>
                        <td class="text-center"><?=$row->kerjasama?></td>
                        <td class="text-center"><?=$row->kepemimpinan?></td>
                        <td class="text-center"><?=$row->ratarata_perilaku?></td>
                        <td class="text-center"><?=$row->nilai_ppk?></td>
                      </tr>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <tr>
                      <td class="text-center" colspan="99">Data tidak ada!</td>
                    </tr>
                  <?php endif; ?>
                </tbody>
              </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
<!-- /.content-wrapper -->
<!-- Modal Delete -->
<div id="modal_delete" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>pr_data/riwayat_ppk_action/delete" method="post">
        <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
        <input type="hidden" name="id" id="id_delete">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-flat btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /script -->
<script>
  $(document).ready(function () {
    $('#form').validate({
      rules : {
        id_hukuman : {
          valueNotEquals : '0'
        }
      }
    })
  })
  function del(id) {
    $("#modal_delete").modal('show');
    $("#id_delete").val(id);
  }
</script>