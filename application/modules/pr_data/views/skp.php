<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <?php $this->load->view('pr_data/sidebar'); ?>
        </div>
        <div class="col-md-9">
          <?php $this->load->view('pr_data/header'); ?>
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="update"><a href="<?=base_url()?>pr_data/riwayat_ppk/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Riwayat PPK</a></li>
              <li class="active"><a href="<?=base_url()?>pr_data/skp/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">SKP</a></li>
              <li class="update"><a href="<?=base_url()?>pr_data/realisasi/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Realisasi</a></li>
              <li class="update"><a href="<?=base_url()?>pr_data/perilaku_kerja/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Perilaku</a></li>
              <li class="update"><a href="<?=base_url()?>pr_data/tugas_belajar/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Tugas Belajar</a></li>
              <li class="update"><a href="<?=base_url()?>pr_data/cetak_tugas_tambahan/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Cetak SK Tugas Tambahan</a></li>
              <li class="update"><a href="<?=base_url()?>pr_data/penilaian_gabung/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Penilaian Gabung</a></li>
              <li class="update"><a href="<?=base_url()?>pr_data/cetak_ppk/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Cetak PPK</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                <form id="form" class="form-horizontal" action="<?=base_url().$access->controller?>/skp_action/<?=$action?>" method="post" autocomplete="off"> 
                  <h4 class="text-orange">Sasaran Kerja Pegawai</h4>
                  <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
                  <input type="hidden" name="id" id="id" value="<?php if($skp){echo $skp->id;}?>">
                  <div class="form-group">
                    <label class="col-md-2 control-label">Tugas Jabatan</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" name="tgs" id="tgs" value="<?php if($skp){echo $skp->tgs;}?>" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">AK</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control" name="s_ak" id="s_ak" value="<?php if($skp){echo $skp->s_ak;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Kuantitas</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control" name="s_kuant" id="s_kuant" value="<?php if($skp){echo $skp->s_kuant;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Satuan</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control" name="satuan_output" id="satuan_output" value="<?php if($skp){echo $skp->satuan_output;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Kualitas/Mutu</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control" name="s_kual" id="s_kual" value="<?php if($skp){echo $skp->s_kual;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Waktu</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control" name="s_waktu" id="s_waktu" value="<?php if($skp){echo $skp->s_waktu;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Satuan Waktu</label>
                    <div class="col-md-2">
                      <select class="form-control select2" name="satuan_waktu" id="satuan_waktu">
                        <option value="0" <?php if($skp){if($skp->satuan_waktu = 0){echo 'selected';}}?>> -- Pilih -- </option>
                        <option value="1" <?php if($skp){if($skp->satuan_waktu = 1){echo 'selected';}}?>> Menit </option>
                        <option value="2" <?php if($skp){if($skp->satuan_waktu = 2){echo 'selected';}}?>> Jam </option>
                        <option value="3" <?php if($skp){if($skp->satuan_waktu = 3){echo 'selected';}}?>> Hari </option>
                        <option value="4" <?php if($skp){if($skp->satuan_waktu = 4){echo 'selected';}}?>> Bulan </option>
                        <option value="5" <?php if($skp){if($skp->satuan_waktu = 5){echo 'selected';}}?>> Tahun </option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Biaya</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control" name="s_biaya" id="s_biaya" value="<?php if($skp){echo $skp->s_biaya;}?>">
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <div class="col-md-offset-2 col-sm-8">
                      <button class="btn btn-flat btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                  </div>
                </form>
                <table class="table table-striped table-bordered table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" style="vertical-align:middle" width="30" rowspan="2">No.</th>
                    <th class="text-center" style="vertical-align:middle" width="60" rowspan="2">Aksi</th>
                    <th class="text-center" style="vertical-align:middle" width="" rowspan="2">Kegiatan Tugas Tambahan</th>
                    <th class="text-center" style="vertical-align:middle" width="" rowspan="2">AK</th>
                    <th class="text-center" style="vertical-align:middle" width="" colspan="6">Target</th>
                  </tr>
                  <tr>
                    <th class="text-center" colspan="2">Kuant/Output</th>
                    <th class="text-center">Kual/Mutu</th>
                    <th class="text-center" colspan="2">Waktu</th>
                    <th class="text-center">Biaya</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if ($riwayat != null): ?>
                    <?php $i=1;foreach ($riwayat as $row): ?>
                      <tr>
                        <td class="text-center"><?=$i++?></td>
                        <td class="text-center">
                          <?php if($row->id != 0): ?>
                            <a class="btn btn-flat btn-xs btn-warning" href="<?=base_url()?>pr_data/skp/<?=$pegawai->id_pegawai?>/<?=$row->id?>" style="padding-right:0.250em;"><i class="fa fa-pencil"></i></a> 
                            <a class="btn btn-flat btn-xs btn-danger" href="#" onclick="del(<?=$row->id?>)"><i class="fa fa-trash"></i></a>
                          <?php endif;?>
                        </td>
                        <td><?=$row->tgs?></td>
                        <td class="text-center"><?=$row->s_ak?></td>
                        <td class="text-center"><?=$row->s_kuant?></td>
                        <td class="text-center"><?=$row->satuan_output?></td>
                        <td class="text-center"><?=$row->s_kual?></td>
                        <td class="text-center"><?=$row->s_waktu?></td>
                        <?php 
                          $satuan_waktu = '';
                          switch ($row->satuan_waktu) {
                            case 1:
                              $satuan_waktu = 'Menit';
                              break;
                            case 2:
                              $satuan_waktu = 'Jam';
                              break;
                            case 3:
                              $satuan_waktu = 'Hari';
                              break;
                            case 4:
                              $satuan_waktu = 'Bulan';
                              break;
                            case 5:
                              $satuan_waktu = 'Tahun';
                              break;
                          }
                        ?>
                        <td class="text-center"><?=$satuan_waktu?></td>
                        <td class="text-center"><?=$row->s_biaya?></td>
                      </tr>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <tr>
                      <td class="text-center" colspan="99">Data tidak ada!</td>
                    </tr>
                  <?php endif; ?>
                </tbody>
              </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
<!-- /.content-wrapper -->
<!-- Modal Delete -->
<div id="modal_delete" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>pr_data/skp_action/delete" method="post">
        <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
        <input type="hidden" name="id" id="id_delete">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-flat btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /script -->
<script>
  $(document).ready(function () {
    $('#form').validate({
      rules : {
        id_hukuman : {
          valueNotEquals : '0'
        }
      }
    })
  })
  function del(id) {
    $("#modal_delete").modal('show');
    $("#id_delete").val(id);
  }
</script>