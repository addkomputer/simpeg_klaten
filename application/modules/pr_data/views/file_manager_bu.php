<?php
  function get_pendidikan($id)
  {
    switch ($id) {
      case 1:
        return 'SD / PAKET A';
        break;
      case 2:
        return 'SD / PAKET A';
        break;
      case 3:
        return 'SMP / PAKET B';
        break;
      case 4:
        return 'SMA / SMK / PAKET C';
        break;
      case 5:
        return 'D-1 ';
        break;
      case 6:
        return 'D-2 ';
        break;
      case 7:
        return 'D-3 ';
        break;
      case 8:
        return 'D-4 ';
        break;
      case 9:
        return 'S-1 ';
        break;
      case 10:
        return 'S-2 ';
        break;
      case 11:
        return 'S-3 ';
        break;
    }
  }
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <?php $this->load->view('pr_data/sidebar'); ?>
        </div>
        <div class="col-md-9">
          <?php $this->load->view('pr_data/header'); ?>
          <div class="box box-primary ">
            <div class="box-header with-border">
              <h3 class="box-title text-orange">File Manager</h3>
            </div>
            <div class="box-body">
              <h4>Data Utama</h4>
              <div class="form-group">
                <label class="col-md-3 control-label">File Kartu Pegawai</label>
                <div class="col-md-3">
                  <?php if($pegawai->file_karpeg!= null){ ?>
                      <a class="btn btn-sm btn-success" target="_blank" rel="noopener noreferrer" href="<?=base_url()?>berkas/data_pokok/<?=$pegawai->file_karpeg?>" style="padding-right:0.5em;"><i class="fa fa-download"> Unduh Data</i></a>
                    <?php }else{ ?>
                      <a class="btn btn-sm btn-danger" href="#" style="padding-right:0.5em;"><i class="fa fa-warning"> Belum Ada </i></a>
                  <?php } ?>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">File PAK</label>
                <div class="col-md-3">
                  <?php if($pegawai->file_pak!= null){ ?>
                      <a class="btn btn-sm btn-success" target="_blank" rel="noopener noreferrer" href="<?=base_url()?>berkas/posisi_jabatan/pak/<?=$pegawai->file_pak?>" style="padding-right:0.5em;"><i class="fa fa-download"> Unduh Data</i></a>
                  <?php }else{ ?>
                      <a class="btn btn-sm btn-danger" href="#" style="padding-right:0.5em;"><i class="fa fa-warning"> Belum Ada </i></a>
                  <?php } ?>
                </div>
              </div>
              <br>
              <div class="form-group">
                <label class="col-md-3 control-label">File STR</label>
                <div class="col-md-3">
                  <?php if($pegawai->file_str!= null){ ?>
                      <a class="btn btn-sm btn-success" target="_blank" rel="noopener noreferrer" href="<?=base_url()?>berkas/posisi_jabatan/str/<?=$pegawai->file_str?>" style="padding-right:0.5em;"><i class="fa fa-download"> Unduh Data</i></a>
                  <?php }else{ ?>
                      <a class="btn btn-sm btn-danger" href="#" style="padding-right:0.5em;"><i class="fa fa-warning"> Belum Ada </i></a>
                  <?php } ?>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">File SIP</label>
                <div class="col-md-3">
                  <?php if($pegawai->file_sip!= null){ ?>
                      <a class="btn btn-sm btn-success" target="_blank" rel="noopener noreferrer" href="<?=base_url()?>berkas/posisi_jabatan/sip/<?=$pegawai->file_sip?>" style="padding-right:0.5em;"><i class="fa fa-download"> Unduh Data</i></a>
                  <?php }else{ ?>
                      <a class="btn btn-sm btn-danger" href="#" style="padding-right:0.5em;"><i class="fa fa-warning"> Belum Ada </i></a>
                  <?php } ?>
                </div>
              </div>
              <br>
              <div class="form-group">
                <label class="col-md-3 control-label">File RKK</label>
                <div class="col-md-3">
                  <?php if($pegawai->file_rkk!= null){ ?>
                      <a class="btn btn-sm btn-success" target="_blank" rel="noopener noreferrer" href="<?=base_url()?>berkas/posisi_jabatan/rkk/<?=$pegawai->file_rkk?>" style="padding-right:0.5em;"><i class="fa fa-download"> Unduh Data</i></a>
                  <?php }else{ ?>
                      <a class="btn btn-sm btn-danger" href="#" style="padding-right:0.5em;"><i class="fa fa-warning"> Belum Ada </i></a>
                  <?php } ?>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label">File KTP</label>
                <div class="col-md-3">
                  <?php if($keluarga->file_ktp!= null){ ?>
                      <a class="btn btn-sm btn-success" target="_blank" rel="noopener noreferrer" href="<?=base_url()?>berkas/keluarga/ktp/<?=$keluarga->file_ktp?>" style="padding-right:0.5em;"><i class="fa fa-download"> Unduh Data</i></a>
                  <?php }else{ ?>
                      <a class="btn btn-sm btn-danger" href="#" style="padding-right:0.5em;"><i class="fa fa-warning"> Belum Ada </i></a>
                  <?php } ?>
                </div>
              </div>
              <br>
              <div class="form-group">
                <label class="col-md-3 control-label">File KK</label>
                <div class="col-md-3">
                  <?php if($keluarga->file_kk!= null){ ?>
                      <a class="btn btn-sm btn-success" target="_blank" rel="noopener noreferrer" href="<?=base_url()?>berkas/keluarga/kk/<?=$keluarga->file_kk?>" style="padding-right:0.5em;"><i class="fa fa-download"> Unduh Data</i></a>
                  <?php }else{ ?>
                      <a class="btn btn-sm btn-danger" href="#" style="padding-right:0.5em;"><i class="fa fa-warning"> Belum Ada </i></a>
                  <?php } ?>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label">File Karis/Karsu</label>
                <div class="col-md-3">
                  <?php if($keluarga->file_karsu!= null){ ?>
                      <a class="btn btn-sm btn-success" target="_blank" rel="noopener noreferrer" href="<?=base_url()?>berkas/keluarga/karsu/<?=$keluarga->file_karsu?>" style="padding-right:0.5em;"><i class="fa fa-download"> Unduh Data</i></a>
                  <?php }else{ ?>
                      <a class="btn btn-sm btn-danger" href="#" style="padding-right:0.5em;"><i class="fa fa-warning"> Belum Ada </i></a>
                  <?php } ?>
                </div>
              </div>
              <br>
              <hr>
              <h4>Pangkat / Gol</h4>
              <table class="table table-striped table-bordered table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" width="30">No.</th>
                    <th class="text-center" width="200">Pangkat</th>
                    <th class="text-center" width="50">Golongan/ Ruang</th>
                    <th class="text-center" width="100">TMT</th>
                    <th class="text-center" width="">No. SK</th>
                    <th class="text-center" width="100">Tanggal SK</th>
                    <th class="text-center" width="200">Masa Kerja</th>
                    <th class="text-center" width="80">File</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if ($riwayat != null): ?>
                    <?php $i=1;foreach ($riwayat as $row): ?>
                      <tr>
                        <td class="text-center"><?=$i++?></td>
                        <td><?=$row->pangkat?></td>
                        <td class="text-center"><?=$row->golongan?></td>
                        <td class="text-center"><?=date_to_id($row->tmt_riwayat_gol)?></td>
                        <td><?=$row->no_sk?></td>
                        <td class="text-center"><?=date_to_id($row->tgl_sk)?></td>
                        <td class="text-right"><?=$row->masakerja_thn.' tahun '.$row->masakerja_bln.' bulan'?></td>
                        <td class="text-center"><a class="text-green">
                          <?php if ($row->file != null): ?>
                              <?php foreach($row->file as $row2): ?>
                                <a target="_blank" href="<?=base_url()?>berkas/riwayat_pangkat/<?=$row2->file?>" title="<?=$row2->file?>"><i class="fa fa-download"></i></a>
                              <?php endforeach;?>
                          <?php else: ?>
                                <a class="btn btn-xs btn-danger" href="#" style="padding-right:0.5em;"><i class="fa fa-warning"> Belum Ada</i></a>
                          <?php endif; ?>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <tr>
                      <td class="text-center" colspan="99">Data tidak ada!</td>
                    </tr>
                  <?php endif; ?>
                </tbody>
              </table>
              <br>
              <hr>
              <h4>Pendidikan</h4>
              <table class="table table-striped table-bordered table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" width="30">No.</th>
                    <th class="text-center" width="200">Pendidikan</th>
                    <th class="text-center">Nama Sekolah</th>
                    <th class="text-center" width="100">Tanggal Lulus</th>
                    <th class="text-center" width="200">No. Ijazah</th>
                    <th class="text-center" width="80">File</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if ($riwayatp != null): ?>
                    <?php $i=1;foreach ($riwayatp as $row): ?>
                      <tr>
                        <td class="text-center"><?=$i++?></td>
                        <td class="text-center"><?=get_pendidikan($row->tingkat_pendidikan).' - '.$row->jurusan?></td>
                        <td><?=$row->nama_sekolah?></td>
                        <td class="text-center"><?=date_to_id($row->tgl_lulus)?></td>
                        <td><?=$row->no_ijazah?></td>
                        <td class="text-center"><a class="text-green">
                          <?php if ($row->file != null): ?>
                              <?php foreach($row->file as $row2): ?>
                                <a target="_blank" href="<?=base_url()?>berkas/riwayat_pendidikan/<?=$row2->file?>" title="<?=$row2->file?>"><i class="fa fa-download"></i></a>
                              <?php endforeach;?>
                          <?php else: ?>
                                <a class="btn btn-xs btn-danger" href="#" style="padding-right:0.5em;"><i class="fa fa-warning"> Belum Ada</i></a>
                          <?php endif; ?>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <tr>
                      <td class="text-center" colspan="99">Data tidak ada!</td>
                    </tr>
                  <?php endif; ?>
                </tbody>
              </table>
              <hr>
              <h4>Jabatan</h4>
              <table class="table table-striped table-bordered table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" width="30">No.</th>
                    <th class="text-center" width="">Jenis Jabatan</th>
                    <th class="text-center" width="80">Eselon</th>
                    <th class="text-center" width="100">TMT Jabatan</th>
                    <th class="text-center" width="200">No. SK</th>
                    <th class="text-center" width="100">Tgl SK</th>
                    <th class="text-center" width="80">File</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if ($riwayatj != null): ?>
                    <?php $i=1;foreach ($riwayatj as $row): ?>
                      <tr>
                        <td class="text-center"><?=$i++?></td>
                        <td><?=$row->idjabatan?></td>
                        <td class="text-center"><?=$row->eselon?></td>
                        <td class="text-center"><?=date_to_id($row->tmt_jabatan)?></td>
                        <td><?=$row->no_sk?></td>
                        <td class="text-center"><?=date_to_id($row->tgl_sk)?></td>
                        <td class="text-center"><a class="text-green">
                          <?php if ($row->file != null): ?>
                              <?php foreach($row->file as $row2): ?>
                                <a target="_blank" href="<?=base_url()?>berkas/riwayat_pangkat/<?=$row2->file?>" title="<?=$row2->file?>"><i class="fa fa-download"></i></a>
                              <?php endforeach;?>
                          <?php else: ?>
                                <a class="btn btn-xs btn-danger" href="#" style="padding-right:0.5em;"><i class="fa fa-warning"> Belum Ada</i></a>
                          <?php endif; ?>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <tr>
                      <td class="text-center" colspan="99">Data tidak ada!</td>
                    </tr>
                  <?php endif; ?>
                </tbody>
              </table>
              <hr>
              <h4>Riwayat Diklat</h4>
              <table class="table table-striped table-bordered table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" width="30">No.</th>
                    <th class="text-center" width="">Nama Diklat</th>
                    <th class="text-center" width="80">Tahun</th>
                    <th class="text-center" width="200">Lama Diklat</th>
                    <th class="text-center" width="200">File</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if ($riwayat_diklat != null): ?>
                    <?php $i=1;foreach ($riwayat_diklat as $row): ?>
                      <tr>
                        <td class="text-center"><?=$i++?></td>
                        <td><?=$row->nama_diklat?></td>
                        <td class="text-center"><?=$row->thn_diklat?></td>
                        <td class="text-center"><?=$row->lama_bln.' bulan '.$row->lama_hari.' hari '.$row->lama_jam.' jam'?></td>
                        <td class="text-center">
                          <a class="btn btn-xs btn-success" target="_blank" rel="noopener noreferrer" href="<?=base_url()?>berkas/riwayat_diklat/riwayat/<?=$row->file_sertifikat?>" style="padding-right:0.5em;"><i class="fa fa-eye"></i> Lihat Data</a>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <tr>
                      <td class="text-center" colspan="99">Data tidak ada!</td>
                    </tr>
                  <?php endif; ?>
                </tbody>
              </table>
              <hr>
              <h4>Usulan Diklat</h4>
              <table class="table table-striped table-bordered table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" width="30">No.</th>
                    <th class="text-center" width="">Nama Diklat</th>
                    <th class="text-center" width="80">Tahun</th>
                    <th class="text-center" width="200">File</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if ($riwayat_usulan_diklat != null): ?>
                    <?php $i=1;foreach ($riwayat_usulan_diklat as $row): ?>
                      <tr>
                        <td class="text-center"><?=$i++?></td>
                        <td><?=$row->nama_diklat?></td>
                        <td class="text-center"><?=$row->thn?></td>
                        <td class="text-center">
                            <a class="btn btn-xs btn-success" target="_blank" rel="noopener noreferrer" href="<?=base_url()?>berkas/riwayat_diklat/usulan_baru/<?=$row->file_sertifikat?>" style="padding-right:0.5em;"><i class="fa fa-eye"> Lihat Data</i></a>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <tr>
                      <td class="text-center" colspan="99">Data tidak ada!</td>
                    </tr>
                  <?php endif; ?>
                </tbody>
              </table>
              <hr>
              <h4>Usulan Diklat Baru yang belum ada di database</h4>
              <table class="table table-striped table-bordered table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" width="30">No.</th>
                    <th class="text-center" width="">Nama Diklat</th>
                    <th class="text-center" width="80">Tahun</th>
                    <th class="text-center" width="200">Lama Diklat</th>
                    <th class="text-center" width="200">File</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if ($riwayat_diklat_baru != null): ?>
                    <?php $i=1;foreach ($riwayat_diklat_baru as $row): ?>
                      <tr>
                        <td class="text-center"><?=$i++?></td>
                        <td><?=$row->nama_diklat?></td>
                        <td class="text-center"><?=$row->thn_diklat?></td>
                        <td class="text-center"><?=$row->lama_bln.' bulan '.$row->lama_hari.' hari '.$row->lama_jam.' jam'?></td>
                        <td class="text-center">
                            <a class="btn btn-xs btn-success" target="_blank" rel="noopener noreferrer" href="<?=base_url()?>berkas/riwayat_diklat/usulan_belum/<?=$row->file_sertifikat?>" style="padding-right:0.5em;"><i class="fa fa-eye"> Lihat Data</i></a>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <tr>
                      <td class="text-center" colspan="99">Data tidak ada!</td>
                    </tr>
                  <?php endif; ?>
                </tbody>
              </table>
              <hr>
              <h4>File Upload</h4>
              <table class="table table-striped table-bordered table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" width="30">No.</th>
                    <th class="text-center" width="">Judul</th>
                    <th class="text-center" width="200">Tanggal</th>
                    <th class="text-center" width="200">File</th>
                </thead>
                <tbody>
                  <?php if ($file_upload_all != null): ?>
                    <?php $i=1;foreach ($file_upload_all as $row): ?>
                      <tr>
                        <td class="text-center"><?=$i++?></td>
                        <td><?=$row->judul?></td>
                        <td class="text-center"><?=$row->created?></td>
                        <td class="text-center">
                            <a class="btn btn-xs btn-success" target="_blank" rel="noopener noreferrer" href="<?=base_url()?>berkas/file_upload/<?=$row->file?>" style="padding-right:0.5em;"><i class="fa fa-eye"> Lihat Data</i></a>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <tr>
                      <td class="text-center" colspan="99">Data tidak ada!</td>
                    </tr>
                  <?php endif; ?>
                </tbody>
              </table>
              <hr>
              <h4>Penilaian Klinis</h4>
              <table class="table table-striped table-bordered table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" width="30">No.</th>
                    <th class="text-center" width="">Judul</th>
                    <th class="text-center" width="200">Tanggal</th>
                    <th class="text-center" width="200">File</th>
                </thead>
                <tbody>
                  <?php if ($riwayat_penilaian_all != null): ?>
                    <?php $i=1;foreach ($riwayat_penilaian_all as $row): ?>
                      <tr>
                        <td class="text-center"><?=$i++?></td>
                        <td><?=$row->judul?></td>
                        <td class="text-center"><?=$row->created?></td>
                        <td class="text-center">
                            <a class="btn btn-xs btn-success" target="_blank" rel="noopener noreferrer" href="<?=base_url()?>berkas/riwayat_penilaian/<?=$row->file?>" style="padding-right:0.5em;"><i class="fa fa-eye"> Lihat Data</i></a>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <tr>
                      <td class="text-center" colspan="99">Data tidak ada!</td>
                    </tr>
                  <?php endif; ?>
                </tbody>
              </table>
              <hr>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
<!-- /.content-wrapper -->
<script>
  $(document).ready(function () {
    $('#form').validate({
      rules : {
        id_hukuman : {
          valueNotEquals : '0'
        }
      }
    })
  })
</script>
