<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <?php $this->load->view('pr_data/sidebar'); ?>
        </div>
        <div class="col-md-9">
          <?php $this->load->view('pr_data/header'); ?>
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title text-orange">Riwayat Penghargaan</h3>
            </div>
            <div class="box-body">
              <?php echo $this->session->flashdata('status'); ?>
              <form id="form" class="form-horizontal" action="<?=base_url().$access->controller?>/riwayat_penghargaan_action/<?=$action?>" method="post" autocomplete="off"> 
                <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
                <input type="hidden" name="id" id="id" value="<?php if($penghargaan){echo $penghargaan->id;}?>">
                <div class="form-group">
                  <label class="col-md-2 control-label">Jenis Penghargaan</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="penghargaan" id="penghargaan" value="<?php if($penghargaan){echo $penghargaan->penghargaan;}?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Tahun</label>
                  <div class="col-md-2">
                    <input type="text" class="form-control" name="thn_penghargaan" id="thn_penghargaan" value="<?php if($penghargaan){echo $penghargaan->thn_penghargaan;}?>" required>
                  </div>
                </div>
                <hr>
                <div class="form-group">
                  <div class="col-md-offset-2 col-sm-8">
                    <button class="btn btn-flat btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button>
                  </div>
                </div>
                <hr>
              </form>
              <table class="table table-striped table-bordered table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" width="30">No.</th>
                    <th class="text-center" width="60">Aksi</th>
                    <th class="text-center" width="">Penghargaan</th>
                    <th class="text-center" width="80">Tahun</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if ($riwayat != null): ?>
                    <?php $i=1;foreach ($riwayat as $row): ?>
                      <tr>
                        <td class="text-center"><?=$i++?></td>
                        <td class="text-center">
                          <?php if($row->id != 0): ?>
                            <a class="btn btn-flat btn-xs btn-warning" href="<?=base_url()?>pr_data/riwayat_penghargaan/<?=$pegawai->id_pegawai?>/<?=$row->id?>" style="padding-right:0.250em;"><i class="fa fa-pencil"></i></a> 
                            <a class="btn btn-flat btn-xs btn-danger" href="#" onclick="del(<?=$row->id?>)"><i class="fa fa-trash"></i></a>
                          <?php endif;?>
                        </td>
                        <td><?=$row->penghargaan?></td>
                        <td class="text-center"><?=$row->thn_penghargaan?></td>
                      </tr>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <tr>
                      <td class="text-center" colspan="99">Data tidak ada!</td>
                    </tr>
                  <?php endif; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
<!-- /.content-wrapper -->
<!-- Modal Delete -->
<div id="modal_delete" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>pr_data/riwayat_penghargaan_action/delete" method="post">
        <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
        <input type="hidden" name="id" id="id_delete">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-flat btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /script -->
<script>
  $(document).ready(function () {
    $('#form').validate({
      rules : {
        thn_penghargaan : {
          number : true
        }
      }
    })
  })
  function del(id) {
    $("#modal_delete").modal('show');
    $("#id_delete").val(id);
  }
</script>