<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ms_unit_kerja extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_unit_kerja')
      ->get('ms_unit_kerja',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_unit_kerja')
      ->get('ms_unit_kerja')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('ms_unit_kerja')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id_unit_kerja',$id)->get('ms_unit_kerja')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_unit_kerja','asc')->get('ms_unit_kerja')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_unit_kerja','desc')->get('ms_unit_kerja')->row();
  }

  public function insert($data)
  {
    $this->db->insert('ms_unit_kerja',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_unit_kerja',$id)->update('ms_unit_kerja',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id_unit_kerja',$id)->update('ms_unit_kerja',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_unit_kerja',$id)->delete('ms_unit_kerja');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ms_unit_kerja");
  }

}
