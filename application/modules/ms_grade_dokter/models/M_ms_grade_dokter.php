<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ms_grade_dokter extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_grade_dokter')
      ->get('ms_grade_dokter',$number,$offset)
      ->result_array();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_grade_dokter')
      ->get('ms_grade_dokter')
      ->num_rows();
  }
  
  function num_rows_total(){  
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_grade_dokter')
      ->get('ms_grade_dokter')
      ->num_rows();
	}

	public function get_all_grade_dokter()
  {
    return $this->db
      ->order_by('id_grade_dokter','asc')
      ->where('is_deleted','0')
      ->where('is_active','1')
      ->get('ms_grade_dokter')->result_array();
  }

  public function get_by_id($id)
  {
    return $this->db->where('id_grade_dokter',$id)->get('ms_grade_dokter')->row_array();
  }

  public function get_first()
  {
    return $this->db->order_by('id_grade_dokter','asc')->get('ms_grade_dokter')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_grade_dokter','desc')->get('ms_grade_dokter')->row();
  }

  public function insert($data)
  {
    $this->db->insert('ms_grade_dokter',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_grade_dokter',$id)->update('ms_grade_dokter',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id_grade_dokter',$id)->update('ms_grade_dokter',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_grade_dokter',$id)->delete('ms_grade_dokter');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ms_grade_dokter");
  }

}
