<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tb_log_apel extends MY_Controller {

	var $access, $id_grup;

  function __construct(){
		parent::__construct();

		$controller = 'tb_log_apel';

    if($this->session->userdata('menu') != $controller){
      $this->session->unset_userdata('search');
      $this->session->set_userdata(array('menu' => 'tb_log_apel'));
    }
    $this->load->model('ap_konfigurasi/m_ap_konfigurasi');
		$this->id_grup = $this->session->userdata('id_grup');
		$this->access = $this->m_ap_konfigurasi->get_permission($this->id_grup, $controller);
		if ($this->access == null) {
			redirect(base_url().'ap_error/error_403');
		}
		$this->load->model('m_tb_log_apel');
		$this->load->model('ms_jabatan/m_ms_jabatan');
		$this->load->model('ms_instansi/m_ms_instansi');
		$this->load->model('ms_status_pegawai/m_ms_status_pegawai');
		$this->load->model('dt_pegawai/m_dt_pegawai');
	}
	
	public function index()
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Daftar';
		$data['jabatan'] = $this->m_ms_jabatan->get_all();
		$data['instansi'] = $this->m_ms_instansi->get_all();
		$data['status_pegawai'] = $this->m_ms_status_pegawai->get_all();

		if ($this->access->_read) {
			$search = null;
			
			if($this->session->userdata('search') != null){
				$search = $this->session->userdata('search');
			}else{
				$search = array(
					'tanggal' => date("d-m-Y"),
					'id_tipe' => '',
					'id_instansi' => '',
					'id_jabatan' => '',
					'id_status_pegawai' => '',
					'term' => ''
				);
			}

			$data['search'] = $search;
	
			$config['base_url'] = base_url().'tb_log_apel/index/';
			$config['per_page'] = 10;
	
			$from = $this->uri->segment(3);
			if ($from == null) {
				$from = 0;
			}
				
			$num_rows = $this->m_tb_log_apel->num_rows($search);

			$config['total_rows'] = $num_rows;
			$this->pagination->initialize($config);

			$data['main'] = $this->m_tb_log_apel->get_list($config['per_page'],$from,$search);
			
			$data['num_rows'] = $num_rows;
			$data['num_rows_total'] = $this->m_tb_log_apel->num_rows_total($search);
			insert_log('read',$this->access->menu);
			$this->view('index',$data);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function search()
	{
		$search = $_POST;
		$this->session->set_userdata(array('search' => $search));
		redirect(base_url().$this->access->controller);
	}

	public function search_tanggal($tanggal)
	{
		$search['tanggal'] = $tanggal;
		if (!$this->session->userdata('search')['id_jabatan']) {
			$search['id_jabatan'] = '';
			$search['id_status_pegawai'] = '';
			$search['id_instansi'] = '';
			$search['id_tipe'] = '';
			$search['term'] = '';
			if ($this->session->userdata('id_grup') == 3) {
				$search['id_instansi']	= $this->session->userdata('id_instansi');
			}
		}else{
			$search['id_status_pegawai'] = $this->session->userdata('search')['id_jabatan'];
			$search['id_jabatan'] = $this->session->userdata('search')['id_jabatan'];
			$search['id_instansi'] = $this->session->userdata('search')['id_instansi'];
			$search['id_tipe'] = $this->session->userdata('search')['id_tipe'];
			$search['term'] = $this->session->userdata('search')['term'];
		}
		$this->session->set_userdata(array('search' => $search));
		redirect(base_url().$this->access->controller);
	}
	
	public function form($id = null,$tanggal = null)
	{
		$data['access'] = $this->access;
		$data['pegawai'] = $this->m_dt_pegawai->get_all();
		$data['instansi'] = $this->m_ms_instansi->get_all();

		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Tambah Data';
				$data['action'] = 'insert';
				$data['main'] = null;
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Ubah Data';
				$data['action'] = 'update';
				$data['main'] = $this->m_tb_log_apel->get_by_id($id,$tanggal);
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		}
	}

	public function detail($tanggal,$nomor_induk)
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Detail Kehadiran';
		$data['main'] = $this->m_tb_log_apel->detail($tanggal,$nomor_induk);
		$this->view('detail',$data);
	}

	public function reset_search()
  {
    $this->session->unset_userdata('search');
    redirect(base_url().'tb_log_apel/index');
  }

	public function insert()
	{
		$data = $_POST;
		if ($data != null) {
			$data['created_by'] = $this->session->userdata('nama');
			if(!isset($data['is_active'])){$data['is_active'] = 0;}
			
			$data['file_bukti'] = $this->uploadBukti();

			$tanggal = $data['tanggal_mulai'];
			$mulai = date_create(date_to_id($data['tanggal_mulai']));
			$akhir = date_create(date_to_id($data['tanggal_akhir']));
			$diff  = date_diff($mulai,$akhir)->format('%d');
			unset($data['tanggal'],$data['tanggal_mulai'],$data['tanggal_akhir']);

			for ($i=0; $i <= $diff; $i++) { 
				$data['tanggal'] = date('Y-m-d', strtotime($tanggal. '+'.$i.' days'));
				$this->m_tb_log_apel->insert($data);
			}
			insert_log('insert',$this->access->menu);
			$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
			redirect(base_url().'tb_log_apel/index');
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function update()
	{
		$data = $_POST;
		if ($data != null) {
			if($this->access->_update){
				$data['updated_by'] = $this->session->userdata('nama');
				if(!isset($data['is_active'])){$data['is_active'] = 0;}
				$id = $data['id'];
				if (!empty($_FILES["file_bukti"]["name"])) {
					$data['file_bukti'] = $this->uploadBukti();
				} else {
					$data['file_bukti'] = $data["old_file_bukti"];
				}
				$data['tanggal'] = date_to_id($data['tanggal']);
				unset($data['old_file_bukti']);
				$this->m_tb_log_apel->update($id, $data);
				insert_log('update',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				redirect(base_url().'tb_log_apel/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function delete()
	{
		$data = $_POST;
		if($data != null){
			if ($this->access->_delete) {
				$this->m_tb_log_apel->delete_permanent($data['id'],$data['tanggal']);
				insert_log('delete',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				redirect(base_url().'tb_log_apel/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function uploadBukti()
	{
		$config['upload_path']          = './berkas/bukti/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png|pdf';
		$config['overwrite']						= true;
		$config['max_size']             = 100000000;
		$config['remove_spaces'] 				= TRUE;
		$config['encrypt_name'] 				= TRUE;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('file_bukti')){
			$error = array('error' => $this->upload->display_errors());
		}else{
			return $this->upload->data("file_name");
		}
		
		return null;
	}

}