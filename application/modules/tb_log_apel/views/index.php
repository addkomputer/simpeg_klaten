  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-red"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">Administrasi Kehadiran</li>
        <li class="active text-orange"><?=$access->menu?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box box-danger">
        <div class="box-header with-border">
          <i class="fa fa-table text-maroon"></i>
          <h3 class="box-title">Tabel <?=$access->menu?></h3>
          <div class="box-tools pull-right">
            <span class="badge bg-maroon"><i class="fa fa-bar-chart"></i> Jumlah Keseluruhan Data : <?=$num_rows_total?></span>
          </div>
        </div>
        <div class="box-body">
          <form id="form_search" action="<?=base_url().$this->access->controller;?>/search" method="post" autocomplete="off">
            <div class="row">
              <div class="col-md-1">
                <a class="btn btn-primary btn-flat" href="<?=base_url().$this->access->controller?>/form"><i class="fa fa-plus"></i> Tambah</a>
              </div>
              <div class="col-md-2 col-md-offset-9">
                <div class="input-group">
                  <input type="text" class="form-control datepicker" id="tanggal" name="tanggal" placeholder="dd-mm-yyyy" value="<?php if($search != null){echo $search['tanggal'];}else{echo date('d-m-Y');}?>">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
              </div>
              <input type="hidden" name="id_tipe" val="8">
            </div>
            <br>
            <div class="row">
              <div class="col-md-3 col-md-offset-1">
                <div class="form-group">
                  <select name="id_instansi" class="form-control select2 input-sm" onchange="$('#form-index').submit()">
                    <?php if($this->session->userdata('id_grup') == 3):?>
                      <?php foreach($instansi as $row): ?>
                        <?php if($this->session->userdata('id_instansi') == $row->id_instansi): ?>
                          <option value="<?=$row->id_instansi?>"><?=$row->instansi?></option>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    <?php else: ?>
                      <option value="">-- Semua Instansi --</option>
                      <?php foreach($instansi as $row): ?>
                        <option value="<?=$row->id_instansi?>" <?php if($search){if($search['id_instansi'] == $row->id_instansi){echo 'selected';}} ?>><?=$row->instansi?></option>
                      <?php endforeach;?>
                    <?php endif; ?>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <select name="id_jabatan" class="form-control select2 input-sm" onchange="$('#form-index').submit()">
                    <option value="">-- Semua Jabatan --</option>
                    <?php foreach($jabatan as $row): ?>
                      <option value="<?=$row->id_jabatan?>" <?php if($search){if($search['id_jabatan'] == $row->id_jabatan){echo 'selected';}} ?>><?=$row->jabatan?></option>
                    <?php endforeach;?>
                  </select>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <select name="id_status_pegawai" class="form-control select2 input-sm" onchange="$('#form-index').submit()">
                    <option value="">-- Semua Status --</option>
                    <?php foreach($status_pegawai as $row): ?>
                      <option value="<?=$row->id_status_pegawai?>" <?php if($search){if($search['id_status_pegawai'] == $row->id_status_pegawai){echo 'selected';}} ?>><?=$row->status_pegawai?></option>
                    <?php endforeach;?>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="input-group pull-right">
                  <input type="text" name="term" class="form-control pull-right" placeholder="Pencarian nama..." value="<?php if($search != null){echo $search['term'];}?>">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                    <a class="btn btn-default btn-flat" href="<?=base_url().$this->access->controller?>/reset_search"><i class="fa fa-refresh"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <?php echo $this->session->flashdata('status'); ?>
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-condensed">
              <thead>
                <tr>
                  <th class="text-center" width="30">No.</th>
                  <th class="text-center" width="60">Aksi</th>
                  <th class="text-center" width="60">Status</th>
                  <th class="text-center">Nama Pegawai</th>
                  <th class="text-center">Instansi</th>
                  <th class="text-center">Jabatan</th>
                  <th class="text-center" width="80">Tanggal</th>
                  <th class="text-center" width="85">Jenis</th>
                  <th class="text-center">Keterangan</th>
                  <th class="text-center" width="10">Bukti</th>
                </tr>
              </thead>
              <tbody>
                <?php if ($main != null): ?>
                  <?php $i=1;foreach ($main as $row): ?>
                    <tr>
                      <td class="text-center"><?=$this->uri->segment('3')+$i++?></td>
                      <td class="text-center">                      
                        <a class="btn btn-xs btn-flat btn-warning" href="<?=base_url()?>tb_log_apel/form/<?=$row->id?>/<?=$row->tanggal?>" style="padding-right:0.250em;"><i class="fa fa-pencil"></i></a> 
                        <a class="btn btn-xs btn-flat btn-danger" href="#" onclick="del(<?=$row->id?>,'<?=$row->tanggal?>')"><i class="fa fa-trash"></i></a>
                      </td>
                      <td class="text-center"><?=$row->status_pegawai?></td>
                      <td>
                        <b>
                          <?php 
                            echo $row->gelar_depan.' '.$row->nama;
                            if ($row->gelar_belakang != ''){
                              echo ', '.$row->gelar_belakang;
                            }
                          ?>
                        </b>
                        <br>
                        <?=$row->nomor_induk?><br>
                      <td><?=$row->instansi?></td>
                      <td><?=$row->jabatan?></td>
                      <td class="text-center"><?=date_to_id($row->tanggal)?></td>
                      <td class="text-center"><?=$row->tipe?></td>
                      <td><?=$row->keterangan?></td>
                      <td class="text-center">
                        <a class="btn btn-default btn-xs btn-flat" target="_blank" href="<?=base_url()?>berkas/bukti/<?=$row->file_bukti?>"><i class="fa fa-download"></i> Bukti</a>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                <?php else: ?>
                  <tr>
                    <td class="text-center" colspan="99">Tidak ada data!</td>
                  </tr>
                <?php endif; ?>
              </tbody>
            </table>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
          <span class="badge bg-purple">Jumlah Data : <?=$num_rows?></span>
          <?php echo $this->pagination->create_links(); ?>
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- Modal Delete -->
  <div id="modal_delete" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <form action="<?=base_url().$access->controller?>/delete" method="post">
          <input type="hidden" name="id" id="id">
          <input type="hidden" name="tanggal" id="tgl">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Hapus Data</h4>
          </div>
          <div class="modal-body">
            <p>Anda yakin ingin menghapus data ini?</p>
            <b class="cl-danger">Peringatan!</b>
            <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
            <button type="submit" class="btn btn-flat btn-danger"><i class="fa fa-trash"></i> Hapus</button>
          </div>
        </form>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <!-- /.content-wrapper -->
  <script type="text/javascript">
    $(document).ready(function () {
      $('#tanggal').datepicker()
      .on('changeDate', function(e) {
        // $('#form_search').submit();
        window.location.replace("<?=base_url().$this->access->controller.'/search_tanggal/'?>"+e.format());
      });
      $('.select2').on('select2:select', function (e) {
        $('#form_search').submit();
      });
    })
    function del(id,tanggal) {
      $("#modal_delete").modal('show');
      $("#id").val(id);
      $("#tgl").val(tanggal);
    }
  </script>