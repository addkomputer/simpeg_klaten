<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rk_log_kehadiran_saya extends MY_Controller {

	var $access, $id_grup;

  function __construct(){
		parent::__construct();

		$controller = 'rk_log_kehadiran_saya';

    if($this->session->userdata('menu') != $controller){
      $this->session->unset_userdata('search');
      $this->session->set_userdata(array('menu' => 'rk_log_kehadiran_saya'));
    }
    $this->load->model('ap_konfigurasi/m_ap_konfigurasi');
		$this->id_grup = $this->session->userdata('id_grup');
		$this->access = $this->m_ap_konfigurasi->get_permission($this->id_grup, $controller);
		if ($this->access == null) {
			redirect(base_url().'ap_error/error_403');
		}
		$this->load->model('m_rk_log_kehadiran_saya');
		$this->load->model('ms_jabatan/m_ms_jabatan');
		$this->load->model('dt_pegawai/m_dt_pegawai');
		$this->load->model('ap_profil/m_ap_profil');
	}

	public function index()
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Daftar';
		$data['jabatan'] = $this->m_ms_jabatan->get_all();
		$data['pegawai'] = $this->m_dt_pegawai->get_all();

		if ($this->access->_read) {
			$search = null;
			if($search = $_POST){
				$this->session->set_userdata(array('search' => $search));
			}else{
				if($this->session->userdata('search') != null){
					$search = $this->session->userdata('search');
				}
			}

			$data['search'] = $search;

			$config['base_url'] = base_url().'rk_log_kehadiran_saya/index/';
			$config['per_page'] = 10;

			$from = $this->uri->segment(3);
			if ($from == '') {
				$from = 0;
			}

			if($search == null){
				$num_rows = $this->m_rk_log_kehadiran_saya->num_rows();

				$config['total_rows'] = $num_rows;
				$this->pagination->initialize($config);

				$data['main'] = $this->m_rk_log_kehadiran_saya->get_list($config['per_page'],$from,$search = null);
			}else{
				$search = $this->session->userdata('search');
				$num_rows = $this->m_rk_log_kehadiran_saya->num_rows($search);
				$config['total_rows'] = $num_rows;
				$this->pagination->initialize($config);

				$data['main'] = $this->m_rk_log_kehadiran_saya->get_list($config['per_page'],$from,$search);
			}

			$data['num_rows'] = $num_rows;
			$data['num_rows_total'] = $this->m_rk_log_kehadiran_saya->num_rows_total();
			insert_log('read',$this->access->menu);
			$this->view('index',$data);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function search()
	{
		$search = $_POST;
		$this->session->set_userdata(array('search' => $search));
		redirect(base_url().$this->access->controller);
	}

	public function form($id = null)
	{
		$data['access'] = $this->access;
		$data['pegawai'] = $this->m_dt_pegawai->get_all();
		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Tambah Data';
				$data['action'] = 'insert';
				$data['main'] = null;
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Ubah Data';
				$data['action'] = 'update';
				$data['main'] = $this->m_rk_log_kehadiran_saya->get_by_id($id);
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}

	public function detail($tanggal,$nomor_induk)
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Detail Kehadiran';
		$data['main'] = $this->m_rk_log_kehadiran_saya->detail($tanggal,$nomor_induk);
		$this->view('detail',$data);
	}

	public function reset_search()
  {
    $this->session->unset_userdata('search');
    redirect(base_url().'rk_log_kehadiran_saya/index');
  }

	public function insert()
	{
		$data = $_POST;
		if ($data != null) {
			$data['created_by'] = $this->session->userdata('nama');
			if(!isset($data['is_active'])){$data['is_active'] = 0;}

			$data['file_bukti'] = $this->uploadBukti();

			$tanggal = $data['tanggal_mulai'];
			$mulai = date_create(date_to_id($data['tanggal_mulai']));
			$akhir = date_create(date_to_id($data['tanggal_akhir']));
			$diff  = date_diff($mulai,$akhir)->format('%d');
			unset($data['tanggal'],$data['tanggal_mulai'],$data['tanggal_akhir']);

			for ($i=0; $i <= $diff; $i++) {
				$data['tanggal'] = date('Y-m-d', strtotime($tanggal. '+'.$i.' days'));
				$this->m_rk_log_kehadiran_saya->insert($data);
			}
			insert_log('insert',$this->access->menu);
			$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
			redirect(base_url().'rk_log_kehadiran_saya/index');
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function update()
	{
		$data = $_POST;
		if ($data != null) {
			if($this->access->_update){
				$data['updated_by'] = $this->session->userdata('nama');
				if(!isset($data['is_active'])){$data['is_active'] = 0;}
				$id = $data['id'];
				if (!empty($_FILES["file_bukti"]["name"])) {
					$data['file_bukti'] = $this->uploadBukti();
				} else {
					$data['file_bukti'] = $data["old_file_bukti"];
				}
				$data['tanggal'] = date_to_id($data['tanggal']);
				unset($data['old_file_bukti']);
				$this->m_rk_log_kehadiran_saya->update($id, $data);
				insert_log('update',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				redirect(base_url().'rk_log_kehadiran_saya/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function delete()
	{
		$data = $_POST;
		if($data != null){
			if ($this->access->_delete) {
				$this->m_rk_log_kehadiran_saya->delete_permanent($data['id']);
				insert_log('delete',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				redirect(base_url().'rk_log_kehadiran_saya/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function uploadBukti()
	{
		$config['upload_path']          = './berkas/bukti/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png|pdf';
		$config['file_name']            = uniqid();
		$config['overwrite']						= true;
		$config['max_size']             = 100000000;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('file_bukti')){
			$error = array('error' => $this->upload->display_errors());
		}else{
			return $this->upload->data("file_name");
		}

		return null;
	}

	function cetak_pdf()
	{
		// $data['pegawai'] = $this->m_rk_log_kehadiran_saya->get_print();
		//
    // $this->load->library('d_pdf');
		//
    // $this->d_pdf->setPaper('A4', 'potrait');
    // $this->d_pdf->filename = "laporan-kehadiran.pdf";
    // $this->d_pdf->load_view('laporan_pdf', $data);


		// $search = $this->session->userdata('search');
		// $profil = $this->m_ap_profil->get_first();
		// $this->load->library('pdf');

		// $pdf=new Pdf('l','mm','A4');
		// $pdf->SetTitle("REKAP KEHADIRAN PEGAWAI PERIODE BULAN ".strtoupper(month_id($search['bulan']))." ".$search['tahun']);
		// $pdf->AddPage();
		// //logo
		// $pdf->Image(base_url().'img/kebumen.png',7,7,15,0,'PNG');
		// $pdf->Image(base_url().'img/logo.png',270,7,20,0,'PNG');
		// // kop
		// $pdf->SetFont('Arial','',14);
		// $pdf->Cell(0,0,'PEMERINTAH KABUPATEN KEBUMEN',0,1,'C');
		// $pdf->SetFont('Arial','B',16);
		// $pdf->Cell(0,10,'RUMAH SAKIT UMUM DAERAH dr. SOEDIRMAN',0,1,'C');
		// $pdf->SetFont('Arial','',8);
		// $pdf->Cell(0,0,'Jalan Lingkar Selatan, Muktisari, Kebumen, Kabupaten Kebumen, Jawa Tengah 54351',0,1,'C');
		// $pdf->Cell(0,7,'Telp/Fax : 0287-3873318, 0287-381101, 0287-385274; WA : 0877-3427-026; Email : rsud@kebumenkab.go.id',0,1,'C');
		// $pdf->Line(7, 30, 290, 30);
		// $pdf->SetFont('Arial','B',12);
		// $pdf->Cell(0,20,'REKAP KEHADIRAN PEGAWAI PERIODE BULAN '.strtoupper(month_id($search['bulan']))." ".$search['tahun'],0,1,'C');
		// $pdf->Cell(0,0,'',0,1);


		// $pdf->SetFont('Arial','B',8);
		// $pdf->SetWidths(array(8,55,15,50,12,12,18,15,15,40,40));
		// $pdf->SetAligns(array('C','C','C','C','C','C','C','C','C','C','C'));
		// $pdf->Row(array("No","Nama","Status Pegawai","Jabatan","Gol/\nRuang","Eselon","Tanggal","Jam Datang","Jam Pulang","Keterlambatan","Keterangan"));
		// $pdf->Row(array("No","Status Pegawai","Jabatan","Gol/\nRuang","Eselon","Tanggal","Jam Datang","Jam Pulang","Keterlambatan","Keterangan"));








		// $text = "Loerm ipsum doolor";
		// $pdf->Cell(20,30,$pdf->Write(5,'Visit '),1,0,'C');
		// $current_y = $pdf->GetY();
		// $pdf->Ln();
		// $pdf->Ln();
		// $pdf->SetWidths(array(8,55,15,50,12,12,18,15,15,40,40));
		// $pdf->SetAligns(array('C','C','C','C','C','C','C','C','C','C','C'));
		// $pdf->Row(array("No","Nama","Status Pegawai","Jabatan","Gol/\nRuang","Eselon","Tanggal","Jam Datang","Jam Pulang","Keterlambatan","Keterangan"));
		// $data = $this->m_rk_log_kehadiran_saya->get_print();
		// $pdf->SetFont('Arial','',8);
		// $i=1;
		// $pdf->SetAligns(array('C','L','C','L','C','C','C','C','C','C','C'));
		// foreach($data as $row){
		// 	$nama = '';
		// 	if ($row->gelar_depan != '') {
		// 		$nama .= $row->gelar_depan.' ';
		// 	}
		// 	$nama .= $row->nama;
		// 	if ($row->gelar_belakang != '') {
		// 		$nama .= ', '.$row->gelar_belakang;
		// 	}
		// 	$pdf->Row(array($i++,$nama));
		// }
		// $pdf->Ln();
		// $pdf->SetFont('Arial','B',8);
		// $pdf->SetWidths(array(50,20));
		// $pdf->SetAligns(array('C','C'));
		// $pdf->Row(array("Kategori","Jumlah"));
		// $pdf->SetFont('Arial','',8);
		// $pdf->SetAligns(array('L','C'));
		// $count_category = $this->m_rk_log_kehadiran_saya->count_category($search);
		// foreach ($count_category as $row) {
		// 	$pdf->Row(array($row->tipe,$row->total));
		// }
		//ttd
		// $direktur = $this->m_ap_profil->get_direktur();
		// $nama_direktur = '';
		// if ($direktur->gelar_depan != '') {
		// 	$nama_direktur .= $direktur->gelar_depan.' ';
		// }
		// $nama_direktur .= $direktur->nama;
		// if ($direktur->gelar_belakang != '') {
		// 	$nama_direktur .= ', '.$direktur->gelar_belakang;
		// }
		// $pdf->SetFont('Arial','',10);
		// $pdf->Cell(450,10,'Mengetahui,',0,1,'C');
		// $pdf->Cell(450,0,'Direktur',0,1,'C');
		// $pdf->Cell(450,20,'',0,1,'C');
		// $pdf->Cell(450,0,$nama_direktur,0,1,'C');
		// $pdf->SetFont('Arial','U',10);
		// $pdf->Cell(450,8,$direktur->pangkat.' ('.$direktur->golongan.')',0,1,'C');
		// $pdf->SetFont('Arial','',10);
		// $pdf->Cell(450,0,'NIP. '.$direktur->nomor_induk,0,1,'C');
		// $pdf->Output("I","REKAP KEHADIRAN PEGAWAI PERIODE BULAN ".strtoupper(month_id($search['bulan']))." ".$search['tahun'].".pdf");

		$constructor = [
			'mode' => '',
			'format' => 'A4',
			'default_font_size' => 0,
			'default_font' => '',
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 5,
			'margin_bottom' => 10,
			'margin_header' => 5,
			'margin_footer' => 5,
			'orientation' => 'L',
		];
		$mpdf= new \Mpdf\Mpdf($constructor);
		$stylesheet = file_get_contents(base_url().'dist/css/pdf.css'); // external css
		// HEADER

		// $data['jabatan'] = $this->m_ms_jabatan->get_all();
		// $data['pegawai'] = $this->m_dt_pegawai->get_all();
		//
		// if ($this->access->_read) {
		// 	$search = null;
		// 	if($search = $_POST){
		// 		$this->session->set_userdata(array('search' => $search));
		// 	}else{
		// 		if($this->session->userdata('search') != null){
		// 			$search = $this->session->userdata('search');
		// 		}
		// 	}

			// $data['search'] = $search;
			//
			$config['base_url'] = base_url().'rk_log_kehadiran_saya/index/';
			$config['per_page'] = 10;

			$from = $this->uri->segment(3);
			if ($from == '') {
				$from = 0;
			}
			//
			// if($search == null){
			// 	$num_rows = $this->m_rk_log_kehadiran_saya->num_rows();
			//
			// 	$config['total_rows'] = $num_rows;
			// 	$this->pagination->initialize($config);
			//
			// 	$data['main'] = $this->m_rk_log_kehadiran_saya->get_list($config['per_page'],$from,$search = null);
			// }else{
			// 	$search = $this->session->userdata('search');
			// 	$num_rows = $this->m_rk_log_kehadiran_saya->num_rows($search);
			// 	$config['total_rows'] = $num_rows;
			// 	$this->pagination->initialize($config);
			//
			// 	$data['main'] = $this->m_rk_log_kehadiran_saya->get_list($config['per_page'],$from,$search);
			// }

		$mpdf->WriteHTML($stylesheet,1);
		$mpdf->setFooter('Hello');
		$mpdf->setFooter(date('d-m-Y H:i:s').' - Sistem Informasi Kepegawaian - Halaman {PAGENO}/{nb}');
		$mpdf->simpleTables = true;
		$search = $this->session->userdata('search');
		$header['search'] = $search;
		$header['profil'] = $this->m_ap_profil->get_first();
		$header['judul'] = "REKAP DATA KEHADIRAN";
		$header = $this->load->view('header', $header, TRUE);
		$mpdf->WriteHTML($header);
		// BODY
		$body = [];
		$body = $this->load->view('body', $body, TRUE);
		$mpdf->WriteHTML($body);
		//write row
		// $pegawai = $this->m_rk_log_kehadiran_pegawai->get_print($search);
		$pegawai = $this->m_rk_log_kehadiran_saya->get_list($config['per_page'],$from,$search);

		if ($pegawai != null){
			$i=1;
			foreach ($pegawai as $row){
				$nama = $row->gelar_depan.' '.$row->nama;
				if ($row->gelar_belakang != ''){
					$nama .= ', '.$row->gelar_belakang;
				}
				$table_row = '<tr>'.
'					<td class="text-center">'.$i++.'</td>'.
					'<td>'.$nama.'<br>'.$row->nomor_induk.'</td>'.
					'<td class="text-center">'.$row->status_pegawai.'</td>'.
					'<td>'.$row->jabatan.'</td>'.
					'<td class="text-center">'.$row->golongan.'</td>'.
					// '<td class="text-center">0</td>'.
					'<td class="text-center">'.$row->izin.'</td>'.
					'<td class="text-center">'.$row->sakit.'</td>'.
					'<td class="text-center">'.$row->cuti.'</td>'.
					'<td class="text-center">'.$row->tanpa_keterangan.'</td>'.
					'<td class="text-center">'.$row->dinas_luar.'</td>'.
					'<td class="text-center">'.$row->dinas_dalam.'</td>'.
					'<td class="text-center">'.$row->terlambat_datang.'</td>'.
					'<td></td>'.
				'</tr>';
				$mpdf->WriteHTML($table_row);
			};
		};
		// FOOTER
		// $footer['total'] = $this->m_rk_log_kehadiran_pegawai->count_print($search);
		$footer['direktur'] = $this->m_ap_profil->get_direktur();
		$footer = $this->load->view('footer', $footer, TRUE);
		$mpdf->WriteHTML($footer);
		$mpdf->Output('rekap-status-pegawai.pdf', 'I');
	}

}
