<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
      <small><?=$subtitle?></small>
    </h1>
    <ol class="breadcrumb">
      <li class="active text-orange"><?=$access->menu?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <i class="fa fa-table text-maroon"></i>
        <h3 class="box-title">Tabel <?=$access->menu?></h3>
        <div class="box-tools pull-right">
          <span class="badge bg-maroon"><i class="fa fa-bar-chart"></i> Jumlah Keseluruhan Data : <?=$num_rows_total?></span>
        </div>
      </div>
      <div class="box-body">
        <form id="form_search" action="<?=base_url().$this->access->controller;?>/search" method="post" autocomplete="off">
          <div class="row">
            <div class="col-md-2">
              <div class="dropdown pull-left">
                <button class="btn btn-success btn-flat dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                  <i class="fa fa-print"></i> Cetak Laporan
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                  <li><a href="<?=base_url().$access->controller?>/cetak_pdf" target="_blank"><i class="fa fa-file-pdf-o"></i> Cetak PDF</a></li>
                  <!-- <li><a href="#"><i class="fa fa-file-excel-o"></i> Cetak Excel</a></li> -->
                </ul>
              </div>
            </div>
            <div class="col-md-2 col-md-offset-5">
              <div class="form-group">
                <select name="bulan" class="form-control select2 input-sm">
                  <option value="01" <?php if($search){if($search['bulan'] == '01'){echo 'selected';}}else{if(date('m') == '01'){echo 'selected';}} ?>>Januari</option>
                  <option value="02" <?php if($search){if($search['bulan'] == '02'){echo 'selected';}}else{if(date('m') == '02'){echo 'selected';}} ?>>Februari</option>
                  <option value="03" <?php if($search){if($search['bulan'] == '03'){echo 'selected';}}else{if(date('m') == '03'){echo 'selected';}} ?>>Maret</option>
                  <option value="04" <?php if($search){if($search['bulan'] == '04'){echo 'selected';}}else{if(date('m') == '04'){echo 'selected';}} ?>>April</option>
                  <option value="05" <?php if($search){if($search['bulan'] == '05'){echo 'selected';}}else{if(date('m') == '05'){echo 'selected';}} ?>>Mei</option>
                  <option value="06" <?php if($search){if($search['bulan'] == '06'){echo 'selected';}}else{if(date('m') == '06'){echo 'selected';}} ?>>Juni</option>
                  <option value="07" <?php if($search){if($search['bulan'] == '07'){echo 'selected';}}else{if(date('m') == '07'){echo 'selected';}} ?>>Juli</option>
                  <option value="08" <?php if($search){if($search['bulan'] == '08'){echo 'selected';}}else{if(date('m') == '08'){echo 'selected';}} ?>>Agustus</option>
                  <option value="09" <?php if($search){if($search['bulan'] == '09'){echo 'selected';}}else{if(date('m') == '09'){echo 'selected';}} ?>>September</option>
                  <option value="10" <?php if($search){if($search['bulan'] == '10'){echo 'selected';}}else{if(date('m') == '10'){echo 'selected';}} ?>>Oktober</option>
                  <option value="11" <?php if($search){if($search['bulan'] == '11'){echo 'selected';}}else{if(date('m') == '11'){echo 'selected';}} ?>>November</option>
                  <option value="12" <?php if($search){if($search['bulan'] == '12'){echo 'selected';}}else{if(date('m') == '12'){echo 'selected';}} ?>>Desember</option>
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <select name="tahun" class="form-control input-sm select2">
                  <?php for($i=2018; $i <= date('Y'); $i++):?>
                    <option value="<?=$i?>" <?php if($search){if($search['tahun'] == $i){echo 'selected';}}else{if(date('Y') == $i){echo 'selected';}} ?>><?=$i?></option>
                  <?php endfor; ?>
                </select>
              </div>
            </div>
            <div class="col-md-1">
              <a class="btn btn-default btn-flat" href="<?=base_url().$access->controller?>/reset_search"><i class="fa fa-refresh"></i></a>
            </div>
          </div>
        </form>
        <?php echo $this->session->flashdata('status'); ?>
        <table class="table table-striped table-bordered table-condensed">
          <thead>
            <tr>
              <th style="vertical-align: middle;" class="text-center" width="1%" rowspan="3">No</th>
              <th style="vertical-align: middle;" class="text-center" width="5%" rowspan="3">Nama/NIP</th>
              <th style="vertical-align: middle;" class="text-center" width="3%" rowspan="3">Status Pegawai</th>
              <th style="vertical-align: middle;" class="text-center" width="5%" rowspan="3">Jabatan</th>
              <th style="vertical-align: middle;" class="text-center" width="3%" rowspan="3">Gol.<br>Ruang</th>
              <th style="vertical-align: middle;" class="text-center" width="3%" rowspan="3">Total<br>Hari<br>Kerja</th>
              <th style="vertical-align: middle;" class="text-center" width="3%" colspan="4">Jumlah Ketidakhadiran</th>
              <th style="vertical-align: middle;" class="text-center" width="3%" rowspan="3">Jumlah<br>Dinas Luar</th>
              <th style="vertical-align: middle;" class="text-center" width="3%" rowspan="3">Jumlah<br>Dinas Dalam</th>
              <th style="vertical-align: middle;" class="text-center" width="3%" rowspan="3">Jumlah<br>Keterlambatan<br>(menit)</th>
              <th style="vertical-align: middle;" class="text-center" width="3%" rowspan="3">Keterangan</th>                                        
            </tr>
            <tr>
              <th style="vertical-align: middle;" class="text-center" width="3%">Izin</th>
              <th style="vertical-align: middle;" class="text-center" width="3%">Sakit</th>
              <th style="vertical-align: middle;" class="text-center" width="3%">Cuti</th>
              <th style="vertical-align: middle;" class="text-center" width="3%">TK</th>
            </tr>
            <tr>
              <th style="vertical-align: middle;" class="text-center" width="3%">Hari</th>
              <th style="vertical-align: middle;" class="text-center" width="3%">Hari</th>
              <th style="vertical-align: middle;" class="text-center" width="3%">Hari</th>
              <th style="vertical-align: middle;" class="text-center" width="3%">Hari</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($main != null): ?>
              <?php $i=1;foreach ($main as $row): ?>
                <tr>
                  <td class="text-center"><?=$i++?></td>
                  <td>
                    <?php 
                      echo $row->gelar_depan.' '.$row->nama;
                      if ($row->gelar_belakang != ''){
                        echo ', '.$row->gelar_belakang;
                      }
                    ?>
                    <br>
                    <?=$row->nomor_induk?>
                  </td>
                  <td class="text-center"><?=$row->status_pegawai?></td>
                  <td><?=$row->jabatan?></td>
                  <td class="text-center"><?=$row->golongan?></td>
                  <td class="text-center">0</td>
                  <td class="text-center"><?=$row->izin?></td>
                  <td class="text-center"><?=$row->sakit?></td>
                  <td class="text-center"><?=$row->cuti?></td>
                  <td class="text-center"><?=$row->tanpa_keterangan?></td>
                  <td class="text-center"><?=$row->dinas_luar?></td>
                  <td class="text-center"><?=$row->dinas_dalam?></td>
                  <td class="text-center"><?=$row->terlambat_datang?></td>
                  <td></td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td class="text-center" colspan="99">Tidak ada data!</td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <span class="badge bg-blue">Jumlah Data : <?=$num_rows?></span>
        <?php echo $this->pagination->create_links(); ?>
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- Modal Delete -->
<div id="modal_delete" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url().$access->controller?>/delete" method="post">
        <input type="hidden" name="id" id="id">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /.content-wrapper -->
<script type="text/javascript">
  $(document).ready(function () {
    $('.select2').on('select2:select', function (e) {
      $('#form_search').submit();
    });
  })
  function del(id) {
    $("#modal_delete").modal('show');
    $("#id").val(id);
  }
</script>