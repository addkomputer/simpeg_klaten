      </tbody>
    </table>
    <br><br>
  
    <?php
      if($direktur){
        $nama_direktur = '';
        if ($direktur->gelar_depan != '') {
          $nama_direktur .= $direktur->gelar_depan.' ';
        }
        $nama_direktur .= $direktur->nama;
        if ($direktur->gelar_belakang != '') {
          $nama_direktur .= ', '.$direktur->gelar_belakang;
        }
      }
    ?>
    <table id="sign_table">
      <tbody>
        <tr>
          <td class="text-center">
            <?=$profil->kop_ttd_direktur?>
          </td>
        </tr>
        <tr>
          <td><br><br><br><br></td>
        </tr>
        <tr>
          <td class="text-center">
            <u><?=$nama_direktur?></u><br>
            NIP. <?=$direktur->nomor_induk?>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>
