<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-primary">
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <table class="table table-striped table-condensed">
                <tr>
                  <td width="100">Nama</td>
                  <td width="10">:</td>
                  <td>
                    <?php 
                      echo $main->gelar_depan.' '.$main->nama;
                      if ($main->gelar_belakang != ''){
                        echo ', '.$main->gelar_belakang;
                      }
                    ?>
                  </td>
                </tr>
                <tr>
                  <td>Nomor Induk</td>
                  <td>:</td>
                  <td>
                    <?=$main->nomor_induk?>
                  </td>
                </tr>
              </table>
            </div>
            <div class="col-md-12">
              <table class="table table-striped table-bordered table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" width="50">No</th>
                    <th class="text-center">Mesin SN</th>
                    <th class="text-center">Lokasi</th>
                    <th class="text-center">ScanDate</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i=1;foreach ($main->detail as $row): ?>
                    <tr>
                      <td class="text-center"><?=$i++?></td>
                      <td class="text-center"><?=$row->device_sn?></td>
                      <td class="text-center"><?=$row->lokasi?></td>
                      <td class="text-center"><?=$row->ScanDate?></td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<script>
  $(document).ready(function () {
    // form validator
    $('#form').validate();
  })
</script>