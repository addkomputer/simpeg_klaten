<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_rk_log_kehadiran_saya extends CI_Model {

  private $suffix,$bulan,$tahun,$id_pegawai;

  function __construct(){
    parent::__construct();
    if ($this->session->userdata('search')) {
      $this->bulan = $this->session->userdata('search')['bulan'];
      $this->tahun = $this->session->userdata('search')['tahun'];
      $this->suffix = $this->tahun.'_'.$this->bulan;
      $this->id_pegawai = $this->session->userdata('id_pegawai');
    }else{ 
      $this->bulan = date('m');
      $this->tahun = date('Y');
      $this->suffix = date('Y_m');
      $this->id_pegawai = $this->session->userdata('id_pegawai');
      $search = array(
        'bulan' => $this->bulan,
        'tahun' => $this->tahun,
        'suffix' => $this->suffix,
        'id_pegawai' => $this->id_pegawai
      );
      $this->session->set_userdata(array('search' => $search));
    }
    $this->create_table($this->suffix);
  }
  
  public function create_table()
	{
		$this->db->query(
			"CREATE TABLE IF NOT EXISTS `tb_log_kehadiran_$this->suffix` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`id_pegawai` varchar(50) NOT NULL DEFAULT '0',
				`nomor_induk` varchar(50) NOT NULL DEFAULT '0',
				`pin` varchar(50) NOT NULL DEFAULT '0',
				`id_tipe` int(1) NOT NULL DEFAULT '0',
				`tanggal` date DEFAULT NULL,
				`jam_datang` time DEFAULT NULL,
				`jam_batas_datang` time DEFAULT NULL,
				`jam_datang_pegawai` time DEFAULT NULL,
				`terlambat_datang` int(11) NOT NULL DEFAULT 0,
				`ket_datang` text,
				`jam_pulang` time DEFAULT NULL,
				`jam_batas_pulang` time DEFAULT NULL,
				`jam_pulang_pegawai` time DEFAULT NULL,
				`ket_pulang` text,
				`ket_umum` text,
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;"
		);
	}

	public function get_list($number,$offset,$search = null)
  {
    $pegawai = $this->db->where('id_pegawai',$this->id_pegawai)->get('dt_pegawai')->row();
    $where = "WHERE id_kedudukan_pns = 1 ";
    $where = "AND a.nomor_induk = '$pegawai->nomor_induk'";
    
    $query = $this->db->query(
      "SELECT 
        COUNT(CASE WHEN a.id_tipe LIKE '0%' THEN 1 END) AS tanpa_keterangan,
        COUNT(CASE WHEN a.id_tipe LIKE '1%' THEN 1 END) AS masuk,
        COUNT(CASE WHEN a.id_tipe = '21' THEN 1 END) AS dinas_dalam,
        COUNT(CASE WHEN a.id_tipe = '22' THEN 1 END) AS dinas_luar,
        COUNT(CASE WHEN a.id_tipe LIKE '3%' THEN 1 END) AS sakit,
        COUNT(CASE WHEN a.id_tipe LIKE '4%' THEN 1 END) AS cuti,
        COUNT(CASE WHEN a.id_tipe LIKE '5%' THEN 1 END) AS izin,
        COUNT(CASE WHEN a.id_tipe LIKE '6%' THEN 1 END) AS data_lain,
        SUM(a.terlambat_datang) AS terlambat_datang,
        b.nomor_induk, b.gelar_depan, b.nama, b.gelar_belakang,
        c.jabatan,
        d.golongan,
        e.status_pegawai
      FROM tb_log_kehadiran_$this->suffix a
        JOIN dt_pegawai b ON a.nomor_induk = b.nomor_induk
        LEFT JOIN ms_jabatan c ON b.id_jabatan = c.id_jabatan
        LEFT JOIN ms_golongan d ON b.id_golongan = d.id_golongan
        JOIN ms_status_pegawai e ON b.id_status_pegawai = e.id_status_pegawai
      $where
      GROUP BY 
        nomor_induk
      ORDER BY
        b.id_golongan DESC
      LIMIT $offset,$number"
    );

    return $query->result();
  }

  public function get_print()
  {
    $where = "WHERE id_kedudukan_pns = 1 ";
    
    $query = $this->db->query(
      "SELECT 
        COUNT(CASE WHEN a.id_tipe LIKE '0%' THEN 1 END) AS tanpa_keterangan,
        COUNT(CASE WHEN a.id_tipe LIKE '1%' THEN 1 END) AS masuk,
        COUNT(CASE WHEN a.id_tipe = '21' THEN 1 END) AS dinas_dalam,
        COUNT(CASE WHEN a.id_tipe = '22' THEN 1 END) AS dinas_luar,
        COUNT(CASE WHEN a.id_tipe LIKE '3%' THEN 1 END) AS sakit,
        COUNT(CASE WHEN a.id_tipe LIKE '4%' THEN 1 END) AS cuti,
        COUNT(CASE WHEN a.id_tipe LIKE '5%' THEN 1 END) AS izin,
        COUNT(CASE WHEN a.id_tipe LIKE '6%' THEN 1 END) AS data_lain,
        SUM(a.terlambat_datang) AS terlambat_datang,
        b.nomor_induk, b.gelar_depan, b.nama, b.gelar_belakang,
        c.jabatan,
        d.golongan,
        e.status_pegawai
      FROM tb_log_kehadiran_$this->suffix a
        JOIN dt_pegawai b ON a.nomor_induk = b.nomor_induk
        LEFT JOIN ms_jabatan c ON b.id_jabatan = c.id_jabatan
        LEFT JOIN ms_golongan d ON b.id_golongan = d.id_golongan
        JOIN ms_status_pegawai e ON b.id_status_pegawai = e.id_status_pegawai
      $where
      GROUP BY 
        nomor_induk
      ORDER BY
        b.id_golongan DESC"
    );

    return $query->result();
  }

  function num_rows($search = null){
    $pegawai = $this->db->where('id_pegawai',$this->id_pegawai)->get('dt_pegawai')->row();
    $where = "WHERE id_kedudukan_pns = 1 ";
    $where = "AND a.nomor_induk = '$pegawai->nomor_induk'";
    
    
    $query = $this->db->query(
      "SELECT 
        COUNT(CASE WHEN a.id_tipe LIKE '0%' THEN 1 END) AS tanpa_keterangan,
        COUNT(CASE WHEN a.id_tipe LIKE '1%' THEN 1 END) AS masuk,
        COUNT(CASE WHEN a.id_tipe = '21' THEN 1 END) AS dinas_dalam,
        COUNT(CASE WHEN a.id_tipe = '22' THEN 1 END) AS dinas_luar,
        COUNT(CASE WHEN a.id_tipe LIKE '3%' THEN 1 END) AS sakit,
        COUNT(CASE WHEN a.id_tipe LIKE '4%' THEN 1 END) AS cuti,
        COUNT(CASE WHEN a.id_tipe LIKE '5%' THEN 1 END) AS izin,
        COUNT(CASE WHEN a.id_tipe LIKE '6%' THEN 1 END) AS data_lain,
        SUM(a.terlambat_datang) AS terlambat_datang,
        b.nomor_induk, b.gelar_depan, b.nama, b.gelar_belakang,
        c.jabatan,
        d.golongan,
        e.status_pegawai
      FROM tb_log_kehadiran_$this->suffix a
        JOIN dt_pegawai b ON a.nomor_induk = b.nomor_induk
        LEFT JOIN ms_jabatan c ON b.id_jabatan = c.id_jabatan
        LEFT JOIN ms_golongan d ON b.id_golongan = d.id_golongan
        JOIN ms_status_pegawai e ON b.id_status_pegawai = e.id_status_pegawai
      $where
      GROUP BY 
        nomor_induk
      ORDER BY
        b.id_golongan DESC"
    );

    return $query->num_rows();
  }
  
  function num_rows_total(){  
    $pegawai = $this->db->where('id_pegawai',$this->id_pegawai)->get('dt_pegawai')->row();
    $where = "WHERE id_kedudukan_pns = 1 ";
    $where = "AND a.nomor_induk = '$pegawai->nomor_induk'";
    
    
    $query = $this->db->query(
      "SELECT 
        COUNT(CASE WHEN a.id_tipe LIKE '0%' THEN 1 END) AS tanpa_keterangan,
        COUNT(CASE WHEN a.id_tipe LIKE '1%' THEN 1 END) AS masuk,
        COUNT(CASE WHEN a.id_tipe = '21' THEN 1 END) AS dinas_dalam,
        COUNT(CASE WHEN a.id_tipe = '22' THEN 1 END) AS dinas_luar,
        COUNT(CASE WHEN a.id_tipe LIKE '3%' THEN 1 END) AS sakit,
        COUNT(CASE WHEN a.id_tipe LIKE '4%' THEN 1 END) AS cuti,
        COUNT(CASE WHEN a.id_tipe LIKE '5%' THEN 1 END) AS izin,
        COUNT(CASE WHEN a.id_tipe LIKE '6%' THEN 1 END) AS data_lain,
        SUM(a.terlambat_datang) AS terlambat_datang,
        b.nomor_induk, b.gelar_depan, b.nama, b.gelar_belakang,
        c.jabatan,
        d.golongan,
        e.status_pegawai
      FROM tb_log_kehadiran_$this->suffix a
        JOIN dt_pegawai b ON a.nomor_induk = b.nomor_induk
        LEFT JOIN ms_jabatan c ON b.id_jabatan = c.id_jabatan
        LEFT JOIN ms_golongan d ON b.id_golongan = d.id_golongan
        JOIN ms_status_pegawai e ON b.id_status_pegawai = e.id_status_pegawai
      $where
      GROUP BY 
        nomor_induk
      ORDER BY
        b.id_golongan DESC"
    );

    return $query->num_rows();
  }
  
  public function detail($tanggal,$nomor_induk)
  {
    $data = $this->db->where('nomor_induk',$nomor_induk)->get('dt_pegawai')->row();
    $data->detail = $this->db
      ->join('tb_device b','a.SN = b.device_sn')
      ->like('a.ScanDate',$tanggal)
      ->where('a.nomor_induk',$nomor_induk)
      ->get('tb_scanlog_'.$this->suffix.' a')->result();
    return $data;
  }

	public function get_all()
	{
		return $this->db
			->where('is_active','1')
			->get('tb_log_kehadiran_'.$this->suffix)->result();
  }

  public function get_by_id($id)
  {
    return $this->db->where('id',$id)->get('tb_log_kehadiran_'.$this->suffix)->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id','asc')->get('tb_log_kehadiran_'.$this->suffix)->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id','desc')->get('tb_log_kehadiran_'.$this->suffix)->row();
  }

  public function insert($data)
  {
    $this->db->insert('tb_log_kehadiran_'.$this->suffix,$data);
    $log = array(
      'nomor_induk' => $data['nomor_induk'],
      'tanggal' => $data['tanggal'],
      'id_tipe' => $data['id_tipe']
    );
    $cek_log = $this->db
      ->where('nomor_induk',$data['nomor_induk'])
      ->where('tanggal',$data['tanggal'])
      ->get('tb_log_kehadiran_'.$this->suffix)
      ->row();
    if ($cek_log == NULL) {
      //insert
      $this->db->insert('tb_log_kehadiran_'.$this->suffix,$log);
    }else{
      //update
      $this->db
        ->where('nomor_induk',$data['nomor_induk'])
        ->where('tanggal',$data['tanggal'])
        ->update('tb_log_kehadiran_'.$this->suffix,$log);
    }
  }

  public function update($id,$data)
  {
    $this->db->where('id',$id)->update('tb_log_kehadiran_'.$this->suffix,$data);
    $log = array(
      'nomor_induk' => $data['nomor_induk'],
      'tanggal' => $data['tanggal'],
      'id_tipe' => $data['id_tipe']
    );
    $cek_log = $this->db
      ->where('nomor_induk',$data['nomor_induk'])
      ->where('tanggal',$data['tanggal'])
      ->get('tb_log_kehadiran_'.$this->suffix)
      ->row();
    if ($cek_log == NULL) {
      //insert
      $this->db->insert('tb_log_kehadiran_'.$this->suffix,$log);
    }else{
      //update
      $this->db
        ->where('nomor_induk',$data['nomor_induk'])
        ->where('tanggal',$data['tanggal'])
        ->update('tb_log_kehadiran_'.$this->suffix,$log);
    }
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id',$id)->update('tb_log_kehadiran_'.$this->suffix,array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $data = $this->db->where('id',$id)->get('tb_log_kehadiran_'.$this->suffix)->row();
    $this->db
      ->where('nomor_induk',$data->nomor_induk)
      ->where('tanggal',$data->tanggal)
      ->update('tb_log_kehadiran_'.$this->suffix,array('ket_umum' => '','id_tipe' => 1));
    $this->db->where('id',$id)->delete('tb_log_kehadiran_'.$this->suffix);
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE tb_log_kehadiran_2018_10");
  }

}
