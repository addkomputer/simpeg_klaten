<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tb_log_perbaikan_jam extends MY_Controller {

	var $access, $id_grup;

  function __construct(){
		parent::__construct();

		$controller = 'tb_log_perbaikan_jam';

    if($this->session->userdata('menu') != $controller){
      $this->session->unset_userdata('search');
      $this->session->set_userdata(array('menu' => 'tb_log_perbaikan_jam'));
    }
    $this->load->model('ap_konfigurasi/m_ap_konfigurasi');
		$this->id_grup = $this->session->userdata('id_grup');
		$this->access = $this->m_ap_konfigurasi->get_permission($this->id_grup, $controller);
		if ($this->access == null) {
			redirect(base_url().'ap_error/error_403');
		}
		$this->load->model('m_tb_log_perbaikan_jam');
		$this->load->model('ms_jabatan/m_ms_jabatan');
		$this->load->model('ms_ruang/m_ms_ruang');
		$this->load->model('ms_status_pegawai/m_ms_status_pegawai');
		$this->load->model('dt_pegawai/m_dt_pegawai');
	}
	
	public function index()
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Daftar';
		$data['jabatan'] = $this->m_ms_jabatan->get_all();
		$data['ruang'] = $this->m_ms_ruang->get_all();
		$data['status_pegawai'] = $this->m_ms_status_pegawai->get_all();

		if ($this->access->_read) {
			$search = null;
			
			if($this->session->userdata('search') != null){
				$search = $this->session->userdata('search');
			}
			
			$data['search'] = $search;
	
			$config['base_url'] = base_url().'tb_log_perbaikan_jam/index/';
			$config['per_page'] = 10;
	
			$from = $this->uri->segment(3);
			if ($from == '') {
				$from = 0;
			}
				
			$num_rows = $this->m_tb_log_perbaikan_jam->num_rows($search);
			$config['total_rows'] = $num_rows;
			$this->pagination->initialize($config);

			$data['main'] = $this->m_tb_log_perbaikan_jam->get_list($config['per_page'],$from,$search);
			
			$data['num_rows'] = $num_rows;
			$data['num_rows_total'] = $this->m_tb_log_perbaikan_jam->num_rows_total($search);
			insert_log('read',$this->access->menu);
			$this->view('index',$data);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function search()
	{
		$search = $_POST;
		$this->session->set_userdata(array('search' => $search));
		redirect(base_url().$this->access->controller);
	}
	
	public function form($id = null)
	{
		$data['access'] = $this->access;
		$data['pegawai'] = $this->m_dt_pegawai->get_all();
		$data['ruang'] = $this->m_ms_ruang->get_all();

		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Tambah Data';
				$data['action'] = 'insert';
				$data['main'] = null;
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Ubah Data';
				$data['action'] = 'update';
				$data['main'] = $this->m_tb_log_perbaikan_jam->get_by_id($id);
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		}
	}

	public function reset_search()
  {
    $this->session->unset_userdata('search');
    redirect(base_url().'tb_log_perbaikan_jam/index');
  }

	public function insert()
	{
		$data = $_POST;
		if ($data != null) {
			$data['created_by'] = $this->session->userdata('nama');
			if(!isset($data['is_active'])){$data['is_active'] = 0;}
			$data['tanggal'] = date_to_id($data['tanggal']);
			insert_log('insert',$this->access->menu);
			$this->m_tb_log_perbaikan_jam->insert($data);
			$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
			redirect(base_url().'tb_log_perbaikan_jam/index');
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function get_kehadiran()
	{
		$data = $_POST;
		$main = $this->m_tb_log_perbaikan_jam->get_kehadiran($data);
		echo json_encode($main);
	}

}