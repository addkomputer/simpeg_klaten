<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">Administrasi Kehadiran</li>
        <li class="active text-orange"><?=$access->menu?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-primary">
        <div class="box-header with-border">
          <i class="fa fa-table text-maroon"></i>
          <h3 class="box-title">Form <?=$access->menu?></h3>
        </div>
        <form id="form" class="form-horizontal" action="<?=base_url().$this->access->controller?>/<?=$action?>" method="post" enctype="multipart/form-data" autocomplete="off"> 
          <div class="box-body">
            <input type="hidden" class="form-control" name="id" id="id" value="<?php if($main != null){echo $main->id;};?>">
            <div class="form-group">
              <label class="col-sm-2 control-label">Ruang</label>
              <div class="col-sm-5">
                <select class="form-control select2" name="id_ruang" id="id_ruang" <?php if($action == 'update'){echo 'disabled';}?>>
                  <?php if($this->session->userdata('id_grup') == 4): ?>
                    <?php foreach($ruang as $row): ?>
                      <?php if($this->session->userdata('id_ruang') == $row->id_ruang):?>
                        <option value="<?=$row->id_ruang?>" <?php if($main){if($main->id_ruang == $row->id_ruang){echo 'selected';}} ?>><?=$row->ruang?></option>
                      <?php endif; ?>
                    <?php endforeach;?>
                  <?php else: ?>
                    <option value="">-- Pilih Ruang --</option>
                    <?php foreach($ruang as $row): ?>
                      <option value="<?=$row->id_ruang?>" <?php if($main){if($main->id_ruang == $row->id_ruang){echo 'selected';}} ?>><?=$row->ruang?></option>
                    <?php endforeach;?>
                  <?php endif; ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Pegawai</label>
              <div class="col-sm-5">
                <select class="form-control select2" name="id_pegawai" id="id_pegawai" <?php if($action == 'update'){echo 'disabled';}?>>
                  <option value="">-- Pilih Pegawai --</option>
                  <?php foreach($pegawai as $row): ?>
                    <option value="<?=$row->id_pegawai?>" data-chained="<?=$row->id_ruang?>" <?php if($main){if($main->id_pegawai == $row->id_pegawai){echo 'selected';}} ?>>
                      <?php 
                        echo $row->gelar_depan.' '.$row->nama;
                        if ($row->gelar_belakang != ''){
                          echo ', '.$row->gelar_belakang;
                        }
                      ?> - <?=$row->pin?> - <?=$row->nomor_induk?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Tanggal</label>
              <div class="col-sm-2">
                <div class="input-group">
                  <input class="form-control datepicker" id="tanggal" type="text" name="tanggal" value="" required>
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
              </div>
            </div>
            <div id="jam">            
              <div class="form-group">
                <label class="col-sm-2 control-label">Jam Datang</label>
                <div class="col-sm-2">
                  <div class="input-group">
                    <input class="form-control" id="jam_datang_pegawai" type="text" name="jam_datang_pegawai" value="" required>
                    <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                  </div>
                </div>
                <label class="col-sm-2 control-label">Jam Pulang</label>
                <div class="col-sm-2">
                  <div class="input-group">
                    <input class="form-control" type="text" id="jam_pulang_pegawai" name="jam_pulang_pegawai" value="" required>
                    <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Keterangan</label>
                <div class="col-sm-5">
                  <textarea name="keterangan" id="keterangan" class="form-control"><?php if($main){echo $main->keterangan;}?></textarea>
                </div>
              </div>
              <hr>
              <div class="form-group">
                <div class="col-md-offset-2 col-sm-8">
                  <button class="btn btn-flat btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button>
                  <a class="btn btn-flat btn-default" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Batal</a>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-6 col-sm-offset-2">
                <div class="alert alert-danger">
                  
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<script>
  $(document).ready(function () {
    //hide
    $("#jam").hide();
    $(".alert").hide();
    //tanggal
    $('#tanggal').attr('readonly', true);
    $('#tanggal').datepicker("destroy");
    var timepicker = $('.timepicker');
    // $('.timepicker').val("");
    //chained
    $("#id_pegawai").chained("#id_ruang");
    //timepicker
    $('#id_pegawai').on('change', function() {
      var id_pegawai = $("#id_pegawai option:selected").val();
      if(id_pegawai != ''){
        $('#tanggal').attr('readonly', false);
        $('#tanggal').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy',
          clearBtn: true
        })
        //tanggal change
        $('#tanggal').datepicker()
        .on('changeDate', function(e) {
          var id_pegawai = $("#id_pegawai").val();
          var tanggal = e.format();

          $.ajax({
            type : 'post',
            url : '<?=base_url($access->controller)?>/get_kehadiran',
            data : 'id_pegawai='+id_pegawai+'&tanggal='+tanggal,
            dataType : 'json',
            success : function (data) {
              if(data != null){
                $("#jam").show();
                $("#jam_datang_pegawai").val(data.jam_datang_pegawai);
                if (data.is_datang == true) {
                  $('#jam_datang_pegawai').attr('readonly', true);
                }else{
                  $('#jam_datang_pegawai').timepicker({
                    defaultTime: "00:00:00",
                    showInputs: true,
                    showMeridian: false,
                    showSeconds: true
                  })
                }
                $("#jam_pulang_pegawai").val(data.jam_pulang_pegawai);
                if (data.is_pulang == true) {
                  $('#jam_pulang_pegawai').attr('readonly', true);
                }else{
                  $('#jam_pulang_pegawai').timepicker({
                    defaultTime: "00:00:00",
                    showInputs: true,
                    showMeridian: false,
                    showSeconds: true
                  })
                }
              }else{
                $("#jam").hide();
                $(".alert").html('<i class="fa fa-exclamation-triangle"></i> Data tidak ada');
                $(".alert").fadeIn();
                setInterval(function(){ $(".alert").fadeOut(); }, 1500);
              }
            }
          })
        });
      }else{
        $("#jam").hide();
        $('#tanggal').attr('readonly', true);
        $('#tanggal').val("");
        $('#tanggal').datepicker("destroy");
      }
    })
    // form validator
    $('#form').validate({
      rules : {
        id_ruang : {
          valueNotEquals : ''
        },
        id_pegawai : {
          valueNotEquals : ''
        }
      }
    });
  })
</script>