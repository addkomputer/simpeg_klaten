<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_tb_log_perbaikan_jam extends CI_Model {

  private $suffix,$bulan,$tahun,$id_ruang,$id_jabatan,$id_status_pegawai,$term;

  function __construct(){
    parent::__construct();
    if ($this->session->userdata('search')) {
      $this->bulan = $this->session->userdata('search')['bulan'];
      $this->tahun = $this->session->userdata('search')['tahun'];
      $this->id_ruang = $this->session->userdata('search')['id_ruang'];
      $this->id_jabatan = $this->session->userdata('search')['id_jabatan'];
      $this->id_status_pegawai = $this->session->userdata('search')['id_status_pegawai'];
      $this->term = $this->session->userdata('search')['term'];
      $this->suffix = $this->tahun.'_'.$this->bulan;
    }else{
      $this->bulan = date('m');
      $this->tahun = date('Y');
      $this->id_ruang = '';
      if ($this->session->userdata('id_grup') == 4) {
        $this->id_ruang = $this->session->userdata('id_ruang');
      }
      $this->id_jabatan = '';
      $this->term = '';
      $this->suffix = date('Y_m');
      $this->id_status_pegawai = '';
      $search = array(
        'bulan' => $this->bulan,
        'tahun' => $this->tahun,
        'id_ruang' => $this->id_ruang,
        'id_jabatan' => $this->id_jabatan,
        'id_status_pegawai' => $this->id_status_pegawai,
        'term' => $this->term
      );
      $this->session->set_userdata(array('search' => $search));
    }
    $this->create_table($this->suffix);
  }
  
  public function create_table($suffix)
  {
    $this->db->query(
      "CREATE TABLE IF NOT EXISTS `tb_log_perbaikan_jam_$suffix` (
        `id` bigint(20) NOT NULL AUTO_INCREMENT,
        `id_pegawai` varchar(50) NOT NULL DEFAULT '0',
        `nomor_induk` varchar(50) DEFAULT '0',
        `pin` varchar(50) DEFAULT '0',
        `id_ruang` int(11) DEFAULT '0',
        `tanggal` date NOT NULL,
        `jam_datang_awal` time DEFAULT NULL,
        `jam_datang_akhir` time DEFAULT NULL,
        `jam_pulang_awal` time DEFAULT NULL,
        `jam_pulang_akhir` time DEFAULT NULL,
        `keterangan` text,
        `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `created_by` varchar(50) NOT NULL DEFAULT 'System',
        `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
        `updated_by` varchar(50) DEFAULT NULL,
        `is_active` tinyint(1) NOT NULL DEFAULT '1',
        `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
        PRIMARY KEY (`id`)
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1;"
    );
  }

	public function get_list($number,$offset,$search = null)
  {
    $where = '';
    if($search != null){
      $where = 'WHERE 1=1 ';
      if ($search['id_jabatan'] != '') {
        $where .= "AND b.id_jabatan='".$search['id_jabatan']."' ";
      }
      if ($search['id_ruang'] != '') {
        $where .= "AND b.id_ruang='".$search['id_ruang']."' ";
      }
      if ($search['id_status_pegawai'] != '') {
        $where .= "AND b.id_status_pegawai='".$search['id_status_pegawai']."' ";
      }
      if ($search['term'] != '') {
        $where .= "AND b.nama LIKE '%".$search['term']."%' ";
      }
    }
    return $this->db->query(
      "SELECT 
        a.*,
        b.gelar_depan, b.nama, b.gelar_belakang,
        c.status_pegawai,
        d.jabatan,
        e.ruang
      FROM tb_log_perbaikan_jam_$this->suffix a
      JOIN dt_pegawai b ON a.id_pegawai = b.id_pegawai
      JOIN ms_status_pegawai c ON b.id_status_pegawai =  c.id_status_pegawai
      JOIN ms_jabatan d ON b.id_jabatan = d.id_jabatan
      JOIN ms_ruang e ON b.id_ruang = e.id_ruang
        $where
      LIMIT $offset,$number
      "
    )->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      if ($search['id_jabatan'] != '') {
        $this->db->where('b.id_jabatan',$search['id_jabatan']);
      }
      if ($search['id_ruang'] != '') {
        $this->db->where('b.id_ruang',$search['id_ruang']);
      }
      if ($search['id_status_pegawai'] != '') {
        $this->db->where('b.id_status_pegawai',$search['id_status_pegawai']);
      }
      if ($search['term'] != '') {
        $this->db->like('b.nama',$search['term']);
        $this->db->or_like('a.id_pegawai',$search['term']);
      }
    }
    
    return 0;
  }
  
  function num_rows_total($search = null){  
    if ($this->session->userdata('id_grup') == 4) {
      if ($search != null) {
        if ($search['id_ruang'] != '') {
          $this->db->where('b.id_ruang',$search['id_ruang']);
        }
     }
    }
    return 0;
  }
  
  public function get_kehadiran($data)
  {
    $tanggal = date_to_id($data['tanggal']);
    $suffix = substr($tanggal,0,4)."_".substr($tanggal,5,2);
    $table = "tb_log_kehadiran_".$suffix;
    $id_pegawai = $data['id_pegawai'];
    $query = $this->db
      ->where('id_pegawai',$id_pegawai)
      ->where('tanggal',$tanggal)
      ->get($table)->row();
    return $query;
  }

	public function get_all()
	{
		return $this->db
			->where('is_active','1')
			->get('tb_log_perbaikan_jam_'.$this->suffix)->result();
  }

  public function get_by_id($id)
  {
    return $this->db->where('id',$id)->get('tb_log_perbaikan_jam_'.$this->suffix)->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id','asc')->get('tb_log_perbaikan_jam_'.$this->suffix)->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id','desc')->get('tb_log_perbaikan_jam_'.$this->suffix)->row();
  }

  public function insert($data)
  {
    $suffix = substr($data['tanggal'],0,4)."_".substr($data['tanggal'],5,2);
    $table_kehadiran = "tb_log_kehadiran_".$suffix;
    $table_perbaikan_jam = "tb_log_perbaikan_jam_".$suffix;
    $kehadiran = $this->db->query(
      "SELECT a.*,b.id_ruang 
      FROM $table_kehadiran a
      LEFT JOIN dt_pegawai b ON a.id_pegawai=b.id_pegawai
      WHERE 
        a.tanggal='".$data['tanggal']."' AND 
        a.id_pegawai='".$data['id_pegawai']."'"
    )->row_array();
    if ($kehadiran != null) {
      //insert into tb_log_perbaikan_jam
      $p = array(
        'id_pegawai' => $kehadiran['id_pegawai'],
        'pin' => $kehadiran['pin'],
        'nomor_induk' => $kehadiran['nomor_induk'],
        'id_ruang' => $kehadiran['id_ruang'],
        'tanggal' => $data['tanggal'],
        'jam_datang_awal' => $kehadiran['jam_datang_pegawai'],
        'jam_datang_akhir' => $data['jam_datang_pegawai'],
        'jam_pulang_awal' => $kehadiran['jam_pulang_pegawai'],
        'jam_pulang_akhir' => $data['jam_pulang_pegawai'],
        'keterangan' => $data['keterangan'],
        'created_by' => $this->session->userdata('nama')
      );
      $this->db->insert($table_perbaikan_jam,$p);
      //update jam di kehadiran
      $time_jam_datang_pegawai = strtotime($data['jam_datang_pegawai']);
      $time_datang = strtotime($kehadiran['jam_datang']);
      $time_batas_datang = strtotime($kehadiran['jam_batas_datang']);
      $time_datang_pegawai = strtotime($data['jam_datang_pegawai']);
      $terlambat_datang = 0;
      $ket_datang = "";
      if ($time_datang_pegawai > $time_batas_datang) {
        $terlambat_datang = intval(($time_datang_pegawai - $time_datang) / 60);
        $ket_datang = "Terlambat ".$terlambat_datang." menit";
      }

      $time_jam_pulang_pegawai = strtotime($data['jam_pulang_pegawai']);
      $time_pulang = strtotime($kehadiran['jam_pulang']);
      $time_batas_pulang = strtotime($kehadiran['jam_batas_pulang']);
      $time_pulang_pegawai = strtotime($data['jam_pulang_pegawai']);
      $mendahului_pulang = 0;
      $ket_pulang = "";
      if ($time_pulang_pegawai < $time_pulang) {
        $mendahului_pulang = intval(($time_pulang - $time_pulang_pegawai) / 60);
        $ket_pulang = "Mendahului ".$mendahului_pulang." menit";
      }
      
      $d = array(
        'jam_datang_pegawai' => $data['jam_datang_pegawai'],
        'is_datang' => 1,
        'terlambat_datang' => $terlambat_datang,
        'ket_datang' => $ket_datang,
        'jam_pulang_pegawai' => $data['jam_pulang_pegawai'],
        'is_pulang' => 1,
        'mendahului_pulang' => $mendahului_pulang,
        'ket_pulang' => $ket_pulang
      );

      //update
      $this->db->where('id', $kehadiran['id'])->update($table_kehadiran,$d);
    }else{
      return 0;
    }
  }

  public function update($id,$data)
  {
    $this->db->where('id',$id)->update('tb_log_perbaikan_jam_'.$this->suffix,$data);
    $log = array(
      'id_tipe' => '3',
      'ket_umum' => $data['keterangan']
    );
    $perbaikan_jam = $this->db->where('id',$id)->get('tb_log_perbaikan_jam_'.$this->suffix)->row_array();
    $this->db
      ->where('id_pegawai',$perbaikan_jam['id_pegawai'])
      ->where('tanggal',$perbaikan_jam['tanggal'])
      ->update('tb_log_kehadiran_'.$this->suffix,$log);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id',$id)->update('tb_log_perbaikan_jam_'.$this->suffix,array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $data = $this->db->where('id',$id)->get('tb_log_perbaikan_jam_'.$this->suffix)->row();
    $this->db
      ->where('id_pegawai',$data->id_pegawai)
      ->where('tanggal',$data->tanggal)
      ->update('tb_log_kehadiran_'.$this->suffix,array('ket_umum' => '','id_tipe' => -1));
    $this->db->where('id',$id)->delete('tb_log_perbaikan_jam_'.$this->suffix);
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE tb_log_perbaikan_jam_2018_10");
  }

}
