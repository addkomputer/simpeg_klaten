<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <?php $this->load->view('dt_pns/sidebar'); ?>
        </div>
        <div class="col-md-9">
          <?php $this->load->view('dt_pns/header'); ?>
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="update"><a href="<?=base_url()?>dt_pns/riwayat_ppk/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Riwayat PPK</a></li>
              <li class="update"><a href="<?=base_url()?>dt_pns/skp/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">SKP</a></li>
              <li class="update"><a href="<?=base_url()?>dt_pns/realisasi/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Realisasi</a></li>
              <li class="update"><a href="<?=base_url()?>dt_pns/perilaku_kerja/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Perilaku</a></li>
              <li class="active"><a href="<?=base_url()?>dt_pns/tugas_belajar/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Tugas Belajar</a></li>
              <!-- <li class="update"><a href="<?=base_url()?>dt_pns/cetak_tugas_tambahan/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Cetak SK Tugas Tambahan</a></li> -->
              <li class="update"><a href="<?=base_url()?>dt_pns/penilaian_gabung/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Penilaian Gabung</a></li>
              <!-- <li class="update"><a href="<?=base_url()?>dt_pns/cetak_ppk/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Cetak PPK</a></li> -->
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                <form id="form" class="form-horizontal" action="<?=base_url().$access->controller?>/perilaku_kerja_tubel_action/<?=$action_perilaku_kerja_tubel?>" method="post" autocomplete="off"> 
                  <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
                  <input type="hidden" name="id" id="id" value="<?php if($perilaku_kerja_tubel){echo $perilaku_kerja_tubel->id;}?>">
                  <h4 class="text-orange">Penilaian PNS Tugas Belajar</h4>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Index Prestasi Komulatif</label>
                    <div class="col-md-2">
                      <input type="number" class="form-control" name="ipk" id="ipk" value="<?php if($perilaku_kerja_tubel){echo $perilaku_kerja_tubel->ipk;}?>">
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <div class="col-md-offset-2 col-sm-8">
                      <button class="btn btn-flat btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                  </div>
                  <hr>
                </form>
                <form id="form" class="form-horizontal" action="<?=base_url().$access->controller?>/perilaku_kerja_tubel_action/<?=$action_perilaku_kerja_tubel?>" method="post" autocomplete="off"> 
                  <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
                  <input type="hidden" name="id" id="id" value="<?php if($perilaku_kerja_tubel){echo $perilaku_kerja_tubel->id;}?>">
                  <h4 class="text-orange">Penilaian Perilaku</h4>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Orientasi Pelayanan</label>
                    <div class="col-md-2">
                      <input type="number" class="form-control" name="nilai_op" id="nilai_op" value="<?php if($perilaku_kerja_tubel){echo $perilaku_kerja_tubel->nilai_op;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Intergritas</label>
                    <div class="col-md-2">
                      <input type="number" class="form-control" name="nilai_integritas" id="nilai_integritas" value="<?php if($perilaku_kerja_tubel){echo $perilaku_kerja_tubel->nilai_integritas;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Komitmen</label>
                    <div class="col-md-2">
                      <input type="number" class="form-control" name="nilai_komitmen" id="nilai_komitmen" value="<?php if($perilaku_kerja_tubel){echo $perilaku_kerja_tubel->nilai_komitmen;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Disiplin</label>
                    <div class="col-md-2">
                      <input type="number" class="form-control" name="nilai_disiplin" id="nilai_disiplin" value="<?php if($perilaku_kerja_tubel){echo $perilaku_kerja_tubel->nilai_disiplin;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Kerjasama</label>
                    <div class="col-md-2">
                      <input type="number" class="form-control" name="nilai_kerjasama" id="nilai_kerjasama" value="<?php if($perilaku_kerja_tubel){echo $perilaku_kerja_tubel->nilai_kerjasama;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Kepemimpinan</label>
                    <div class="col-md-2">
                      <input type="number" class="form-control" name="nilai_kepemimpinan" id="nilai_kepemimpinan" value="<?php if($perilaku_kerja_tubel){echo $perilaku_kerja_tubel->nilai_kepemimpinan;}?>">
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <div class="col-md-offset-2 col-sm-8">
                      <button class="btn btn-flat btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                  </div>
                </form>
                <hr>
                <h4 class="text-orange">Proses Penilaian</h4>
                <form id="form_proses" class="form-horizontal" action="<?=base_url().$access->controller?>/proses_penilaian_action/<?=$action_proses_penilaian?>" method="post" autocomplete="off"> 
                  <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
                  <input type="hidden" name="id" id="id" value="<?php if($proses_penilaian){echo $proses_penilaian->id;}?>">
                  <table class="table table-striped table-bordered table-condensed">
                    <thead>
                      <tr>
                        <th class="text-center" width="250">Proses</th>
                        <th class="text-center">Alasan</th>
                        <th class="text-center" width="100">Tanggal</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Keberatan dari PNS yang dinilai</td>
                        <td>
                          <input type="text" class="form-control" name="alasan_keberatan" id="alasan_keberatan" value="<?php if($proses_penilaian){echo $proses_penilaian->alasan_keberatan;}?>">
                        </td>
                        <td>
                          <input type="text" class="form-control datepicker" name="tgl_keberatan" id="tgl_keberatan" value="<?php if($proses_penilaian){echo $proses_penilaian->tgl_keberatan;}?>" placeholder="dd-mm-yyyy">
                        </td>
                      </tr>
                      <tr>
                        <td>Tanggapan Pejabat Penilai atas keberatan</td>
                        <td>
                          <input type="text" class="form-control" name="alasan_tanggapan" id="alasan_tanggapan" value="<?php if($proses_penilaian){echo $proses_penilaian->alasan_tanggapan;}?>">
                        </td>
                        <td>
                          <input type="text" class="form-control datepicker" name="tgl_tanggapan" id="tgl_tanggapan" value="<?php if($proses_penilaian){echo $proses_penilaian->tgl_tanggapan;}?>" placeholder="dd-mm-yyyy">
                        </td>
                      </tr>
                      <tr>
                        <td>Keputusan Pejabat Penilai atas Keberatan</td>
                        <td>
                          <input type="text" class="form-control" name="alasan_kep_penilaian" id="alasan_kep_penilaian" value="<?php if($proses_penilaian){echo $proses_penilaian->alasan_kep_penilaian;}?>">
                        </td>
                        <td>
                          <input type="text" class="form-control datepicker" name="tgl_kep_penilaian" id="tgl_kep_penilaian" value="<?php if($proses_penilaian){echo $proses_penilaian->tgl_kep_penilaian;}?>" placeholder="dd-mm-yyyy">
                        </td>
                      </tr>
                      <tr>
                        <td>Rekomendasi</td>
                        <td>
                          <input type="text" class="form-control" name="alasan_rekomendasi" id="alasan_rekomendasi" value="<?php if($proses_penilaian){echo $proses_penilaian->alasan_rekomendasi;}?>">
                        </td>
                        <td>
                          <input type="text" class="form-control datepicker" name="tgl_rekomendasi" id="tgl_rekomendasi" value="<?php if($proses_penilaian){echo $proses_penilaian->tgl_rekomendasi;}?>" placeholder="dd-mm-yyyy">
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <hr>
                  <div class="form-group">
                    <div class="col-md-offset-2 col-sm-8">
                      <button class="btn btn-flat btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                  </div>
                  <hr>
                </form>
                <table class="table table-striped table-bordered table-condensed">
                  <thead>
                    <tr>
                      <th class="text-center" style="vertical-align:middle" width="30">No.</th>
                      <th class="text-center" style="vertical-align:middle" width="60">Aksi</th>
                      <th class="text-center" style="vertical-align:middle" width="">IPK</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if ($riwayat_perilaku_kerja_tubel != null): ?>
                      <?php $i=1;foreach ($riwayat_perilaku_kerja_tubel as $row): ?>
                        <tr>
                          <td class="text-center"><?=$i++?></td>
                          <td class="text-center">
                            <a class="btn btn-xs btn-flat btn-warning" href="<?=base_url()?>dt_pns/tugas_belajar/<?=$pegawai->id_pegawai?>/1/<?=$row->id?>" style="padding-right:0.250em;"><i class="fa fa-pencil"></i></a> 
                            <a class="btn btn-xs btn-flat btn-danger" href="#" onclick="del_perilaku_kerja_tubel(<?=$row->id?>)"><i class="fa fa-trash"></i></a>
                          </td>
                          <td class="text-center"><?=$row->ipk?></td>
                        </tr>
                      <?php endforeach; ?>
                    <?php else: ?>
                      <tr>
                        <td class="text-center" colspan="99">Data tidak ada!</td>
                      </tr>
                    <?php endif; ?>
                  </tbody>
                </table>
                <table class="table table-striped table-bordered table-condensed">
                  <thead>
                    <tr>
                      <th class="text-center" style="vertical-align:middle" width="30">No.</th>
                      <th class="text-center" style="vertical-align:middle" width="60">Aksi</th>
                      <th class="text-center" style="vertical-align:middle" width="">Orientasi Pelayanan</th>
                      <th class="text-center" style="vertical-align:middle" width="">Integritas</th>
                      <th class="text-center" style="vertical-align:middle" width="">Komitmen</th>
                      <th class="text-center" style="vertical-align:middle" width="">Disiplin</th>
                      <th class="text-center" style="vertical-align:middle" width="">Kerjasama</th>
                      <th class="text-center" style="vertical-align:middle" width="">Kepemimpinan</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if ($riwayat_perilaku_kerja_tubel != null): ?>
                      <?php $i=1;foreach ($riwayat_perilaku_kerja_tubel as $row): ?>
                        <tr>
                          <td class="text-center"><?=$i++?></td>
                          <td class="text-center">
                            <a class="btn btn-xs btn-flat btn-warning" href="<?=base_url()?>dt_pns/tugas_belajar/<?=$pegawai->id_pegawai?>/1/<?=$row->id?>" style="padding-right:0.250em;"><i class="fa fa-pencil"></i></a> 
                            <a class="btn btn-xs btn-flat btn-danger" href="#" onclick="del_perilaku_kerja_tubel(<?=$row->id?>)"><i class="fa fa-trash"></i></a>
                          </td>
                          <td class="text-center"><?=$row->nilai_op?></td>
                          <td class="text-center"><?=$row->nilai_integritas?></td>
                          <td class="text-center"><?=$row->nilai_komitmen?></td>
                          <td class="text-center"><?=$row->nilai_disiplin?></td>
                          <td class="text-center"><?=$row->nilai_kerjasama?></td>
                          <td class="text-center"><?=$row->nilai_kepemimpinan?></td>
                        </tr>
                      <?php endforeach; ?>
                    <?php else: ?>
                      <tr>
                        <td class="text-center" colspan="99">Data tidak ada!</td>
                      </tr>
                    <?php endif; ?>
                  </tbody>
                </table>
                <br>
                <table class="table table-striped table-bordered table-condensed">
                  <thead>
                    <tr>
                      <th class="text-center" style="vertical-align:middle" width="30">No.</th>
                      <th class="text-center" style="vertical-align:middle" width="60">Aksi</th>
                      <th class="text-center" style="vertical-align:middle" width="">Keberatan dari PNS yang dinilai</th>
                      <th class="text-center" style="vertical-align:middle" width="">Tanggapan Pejabat Penilai atas keberatan</th>
                      <th class="text-center" style="vertical-align:middle" width="">Keputusan Pejabat Penilai atas Keberatan</th>
                      <th class="text-center" style="vertical-align:middle" width="">Rekomendasi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if ($riwayat_proses_penilaian != null): ?>
                      <?php $i=1;foreach ($riwayat_proses_penilaian as $row): ?>
                        <tr>
                          <td class="text-center"><?=$i++?></td>
                          <td class="text-center">
                            <a class="btn btn-xs btn-flat btn-warning" href="<?=base_url()?>dt_pns/tugas_belajar/<?=$pegawai->id_pegawai?>/2/<?=$row->id?>" style="padding-right:0.250em;"><i class="fa fa-pencil"></i></a> 
                            <a class="btn btn-xs btn-flat btn-danger" href="#" onclick="del_proses_penilaian(<?=$row->id?>)"><i class="fa fa-trash"></i></a>
                          </td>
                          <td class="text-center"><?=$row->alasan_keberatan?></td>
                          <td class="text-center"><?=$row->alasan_tanggapan?></td>
                          <td class="text-center"><?=$row->alasan_kep_penilaian?></td>
                          <td class="text-center"><?=$row->alasan_rekomendasi?></td>
                        </tr>
                      <?php endforeach; ?>
                    <?php else: ?>
                      <tr>
                        <td class="text-center" colspan="99">Data tidak ada!</td>
                      </tr>
                    <?php endif; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
<!-- /.content-wrapper -->
<!-- Modal Delete -->
<div id="modal_delete_perilaku_kerja_tubel" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>dt_pns/perilaku_kerja_tubel_action/delete" method="post">
        <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
        <input type="hidden" name="id" id="id_delete_perilaku_kerja_tubel">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-flat btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Modal Delete -->
<div id="modal_delete_proses_penilaian" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>dt_pns/proses_penilaian_action/delete" method="post">
        <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
        <input type="hidden" name="id" id="id_delete_proses_penilaian">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-flat btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /script -->
<script>
  $(document).ready(function () {
    $('#form').validate({
      rules : {
        id_hukuman : {
          valueNotEquals : '0'
        }
      }
    })
  })
  function del_perilaku_kerja_tubel(id) {
    $("#modal_delete_perilaku_kerja_tubel").modal('show');
    $("#id_delete_perilaku_kerja_tubel").val(id);
  }
  function del_proses_penilaian(id) {
    $("#modal_delete_proses_penilaian").modal('show');
    $("#id_delete_proses_penilaian").val(id);
  }
</script>