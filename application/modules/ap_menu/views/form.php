<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-red"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">Pengaturan</li>
        <li class="active text-orange"><?=$access->menu?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-danger">
        <div class="box-header with-border">
          <i class="fa fa-table text-maroon"></i>
          <h3 class="box-title">Form <?=$access->menu?></h3>
        </div>
        <form id="form" class="form-horizontal" action="<?=base_url()?>ap_menu/<?=$action?>" method="post" autocomplete="off">
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-2 control-label">Induk</label>
              <div class="col-sm-5">
                <select class="form-control select2" name="induk" id="induk">
                  <option value="">00 - Tidak Ada</option>
                  <?php foreach ($menu_list as $row): ?>
                    <option value="<?=$row->id_menu?>" <?php if($menu !=null){if($menu->induk == $row->id_menu){echo 'selected';}}?>><?=$row->id_menu.' - '.$row->menu?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Id</label>
              <div class="col-sm-2">
                <input type="text" class="form-control" name="id_menu" id="id_menu" value="<?php if($menu != null){echo $menu->id_menu;};?>" required>
                <input type="hidden" class="form-control" name="id_menu_old" id="id_menu_old" value="<?php if($menu != null){echo $menu->id_menu;};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Tipe</label>
              <div class="col-sm-2">
                <select class="form-control select2" name="tipe" id="tipe">
                  <option value="1" <?php if($menu){if($menu->tipe == 1){echo 'selected';}}?>>Menu</option>
                  <option value="2" <?php if($menu){if($menu->tipe == 2){echo 'selected';}}?>>Header</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Nama</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="menu" id="menu" value="<?php if($menu != null){echo $menu->menu;};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Controller</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="controller" id="controller" value="<?php if($menu != null){echo $menu->controller;};?>" >
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Ikon</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="icon" id="icon" value="<?php if($menu != null){echo $menu->icon;};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">URL</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="url" id="url" value="<?php if($menu != null){echo $menu->url;};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Aktif</label>
              <div class="col-sm-5">
                <input class="iCheckbox" type="checkbox" name="is_active" value="1" <?php if($menu != null){if($menu->is_active == 1){echo 'checked';}}else{echo 'checked';}?>>
              </div>
            </div>
            <hr>
            <div class="form-group">
              <div class="col-md-offset-2 col-sm-8">
                <button class="btn btn-flat btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button>
                <a class="btn btn-flat btn-default" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Batal</a>
              </div>
            </div>
          </div>
        </form>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<script>
  $(document).ready(function () {
    // form validator
    $('#form').validate();
  })
</script>