<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ap_menu extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_menu')
      ->get('ap_menu',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_menu')
      ->get('ap_menu')
      ->num_rows();
  }
  
  function num_rows_total(){
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_menu')
      ->get('ap_menu')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('ap_menu')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id_menu',$id)->get('ap_menu')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_menu','asc')->get('ap_menu')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_menu','desc')->get('ap_menu')->row();
  }

  public function insert($data)
  {
    $this->db->insert('ap_menu',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_menu',$id)->update('ap_menu',$data);
  }

  public function delete_temp($id)
  {
    $this->db->where('id_menu',$id)->update('ap_menu',array('is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_menu',$id)->delete('ap_menu');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ap_menu");
  }

}
