<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-<?=$access->icon?> text-red"></i> <?=$access->menu?>
      <small><?=$subtitle?></small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">Data Kepegawaian</li>
      <li class="active text-orange"><?=$access->menu?></li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box box-danger">
      <div class="box-header with-border">
        <i class="fa fa-table text-maroon"></i>
        <h3 class="box-title">Tabel <?=$access->menu?></h3>
        <div class="box-tools pull-right">
          <span class="badge bg-maroon"><i class="fa fa-bar-chart"></i> Jumlah Keseluruhan Data : <?=$num_rows_total?></span>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-md-1">
            <a class="btn btn-primary btn-flat" href="<?=base_url()?><?=$access->controller?>/data_pokok/"><i class="fa fa-plus"></i> Tambah</a>
          </div>
          <form id="form_search" action="<?=base_url()?><?=$access->controller?>/search/" method="post">
            <div class="col-md-4">
              <div class="form-group">
                <select name="id_instansi" class="form-control select2 input-sm" onchange="$('#form-index').submit()">
                  <?php if($this->session->userdata('id_grup') == 3):?>
                    <?php foreach($instansi as $row): ?>
                      <?php if($this->session->userdata('id_instansi') == $row->id_instansi): ?>
                        <option value="<?=$row->id_instansi?>"><?=$row->instansi?></option>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <option value="">-- Semua Instansi --</option>
                    <?php foreach($instansi as $row): ?>
                      <option value="<?=$row->id_instansi?>" <?php if($search){if($search['id_instansi'] == $row->id_instansi){echo 'selected';}} ?>><?=$row->instansi?></option>
                    <?php endforeach;?>
                  <?php endif; ?>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <select class="form-control select2" name="id_jabatan" id="id_jabatan">
                  <option value="">-- Semua Jabatan --</option>
                  <?php foreach($jabatan as $row): ?>
                    <option value="<?=$row->id_jabatan?>" <?php if($search){if($search['id_jabatan'] == $row->id_jabatan){echo 'selected';}}?>><?=$row->jabatan?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="input-group">
                <input type="text" name="term" class="form-control pull-right" placeholder="Pencarian" value="<?php if($search){echo $search['term'];}?>">
                <div class="input-group-btn">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                  <a class="btn btn-default btn-flat" href="<?=base_url()?><?=$access->controller?>/reset_search/"><i class="fa fa-refresh"></i></a>
                </div>
              </div>
            </div>
          </form>
        </div>
        <?php echo $this->session->flashdata('status'); ?>
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
                <th class="text-center" style="vertical-align:middle !important" width="30">No.</th>
                <th class="text-center" style="vertical-align:middle !important" width="60">Aksi</th>
                <th class="text-center" style="vertical-align:middle !important" width="">Nama<br>Nomor Induk</th>
                <th class="text-center" style="vertical-align:middle !important" width="10">JK</th>
                <th class="text-center" style="vertical-align:middle !important" width="">Instansi</th>
                <th class="text-center" style="vertical-align:middle !important" width="">Jabatan</th>
                <th class="text-center" style="vertical-align:middle !important" width="20">Eselon</th>
                <th class="text-center" style="vertical-align:middle !important" width="">Pangkat/Golongan</th>
                <th class="text-center" style="vertical-align:middle !important" width="10">Status</th>
              </tr>
            </thead>
            <tbody>
              <?php if ($pegawai != null): ?>
                <?php $i=1;foreach ($pegawai as $row): ?>
                  <tr>
                    <td class="text-center"><?=$this->uri->segment('4')+$i++?></td>
                    <td class="text-center">
                      <a class="btn btn-xs btn-warning btn-flat" href="<?=base_url()?><?=$access->controller?>/data_pokok/<?=$row->id_pegawai?>" style="padding-right:0.5em;"><i class="fa fa-pencil"></i></a> 
                      <a class="btn btn-xs btn-danger btn-flat" href="#" onclick="del('<?=$row->id_pegawai?>')"><i class="fa fa-trash"></i></a>
                    </td>
                    <td>
                      <b><?php 
                        echo $row->gelar_depan.' '.$row->nama;
                        if ($row->gelar_belakang != ''){
                          echo ', '.$row->gelar_belakang;
                        }
                      ?>
                      </b>
                      <br>
                      <?=$row->nomor_induk?>
                    </td>
                    <td class="text-center"><?php echo $jk = ($row->jenis_kelamin == '1') ? 'L' : 'P';?></td>
                    <td><?=$row->instansi?></td>
                    <td><?=$row->jabatan?></td>
                    <td class="text-center"><?php echo $eselon = ($row->eselon != '') ? $row->eselon : '-' ;?></td>
                    <td>
                      <?=$row->golongan?> - <?=$row->pangkat?>
                    </td>
                    <td class="text-center"><?=$row->status_pegawai ?></td>
                  </tr>
                <?php endforeach; ?>
              <?php else: ?>
                <tr>
                  <td class="text-center" colspan="99">Tidak ada data!</td>
                </tr>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <span class="badge bg-purple">Jumlah Data : <?=$num_rows?></span>
        <?php echo $this->pagination->create_links(); ?>
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- Modal Delete -->
<div id="modal_delete" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?><?=$access->controller?>/delete" method="post">
        <input type="hidden" name="id_pegawai" id="id_pegawai">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-flat btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
  function del(id) {
    $("#modal_delete").modal('show');
    $("#id_pegawai").val(id);
  }
  $('.select2').on('select2:select', function (e) {
    $('#form_search').submit();
  });
</script>