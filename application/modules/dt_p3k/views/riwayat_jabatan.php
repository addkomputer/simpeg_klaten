<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <?php $this->load->view('dt_pns/sidebar'); ?>
        </div>
        <div class="col-md-9">
          <?php $this->load->view('dt_pns/header'); ?>
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title text-orange">Riwayat Jabatan</h3> </div>
            <div class="box-body">
              <?php echo $this->session->flashdata('status'); ?>
              <form id="form" class="form-horizontal" enctype="multipart/form-data" action="<?=base_url().$access->controller?>/riwayat_jabatan_action/<?=$action?>" method="post" autocomplete="off"> 
                <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
                <input type="hidden" name="id" id="id" value="<?php if($jabatan){echo $jabatan->id;}?>">
                <div class="form-group">
                  <label class="col-md-2 control-label">Jenis Jabatan</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="idjabatan" id="idjabatan" value="<?php if($jabatan){echo $jabatan->idjabatan;}?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Eselon</label>
                  <div class="col-md-1">
                    <input type="text" class="form-control" name="eselon" id="eselon" value="<?php if($jabatan){echo $jabatan->eselon;}?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">TMT Jabatan</label>
                  <div class="col-md-2">
                    <input type="text" class="form-control datepicker" name="tmt_jabatan" id="tmt_jabatan" value="<?php if($jabatan){echo date_to_id($jabatan->tmt_jabatan);}?>" placeholder="dd-mm-yyyy" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">No. SK</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control" name="no_sk" id="no_sk" value="<?php if($jabatan){echo $jabatan->no_sk;}?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Tanggal SK</label>
                  <div class="col-md-2">
                    <input type="text" class="form-control datepicker" name="tgl_sk" id="tgl_sk" value="<?php if($jabatan){echo date_to_id($jabatan->tgl_sk);}?>" placeholder="dd-mm-yyyy">
                  </div>
                </div>
                <?php if($jabatan): ?>
                  <?php foreach($jabatan->file as $row): ?>
                    <div class="form-group">
                      <div class="col-md-5 col-md-offset-2">
                        <?=$row->file?>
                      </div>
                      <div class="col-md-2">
                        <button class="btn btn-xs btn-danger del-file" onclick="del_file('<?=$row->id?>','<?=$jabatan->id?>')" type="button"><i class="fa fa-minus"></i> Hapus File</a>
                      </div>
                    </div>
                  <?php endforeach;?>
                <?php endif;?> 
                <div id="files">
                
                </div>
                <div class="form-group">
                  <div class="col-md-2 col-md-offset-2">
                    <button class="btn btn-xs btn-default" type="button" onclick="add_file()"><i class="fa fa-plus"></i> Tambah File</button>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-8 col-md-offset-2">
                    <em>Sebelum mengunggah file, pastikan nama file yang akan diunggah sudah benar sesuai dengan isi file. Jenis file yang diterima adalah *.jpg,*.png,*.jpeg dan *.pdf</em>
                  </div>
                </div>
                <hr>
                <div class="form-group">
                  <div class="col-md-offset-2 col-sm-8">
                    <button class="btn btn-flat btn-success" type="submit" value='Upload' name='upload'><i class="fa fa-save"></i> Simpan</button>
                    <a class="btn btn-flat btn-default" href="<?=base_url()?>dt_pns/riwayat_jabatan/<?=$pegawai->id_pegawai?>"><i class="fa fa-close"></i> Batal</a>
                  </div>
                </div>
                <hr>
              </form>
              <table class="table table-striped table-bordered table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" width="30">No.</th>
                    <th class="text-center" width="60">Aksi</th>
                    <th class="text-center" width="">Jenis Jabatan</th>
                    <th class="text-center" width="80">Eselon</th>
                    <th class="text-center" width="100">TMT Jabatan</th>
                    <th class="text-center" width="200">No. SK</th>
                    <th class="text-center" width="100">Tgl SK</th>
                    <th class="text-center" width="80">File</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if ($riwayat != null): ?>
                    <?php $i=1;foreach ($riwayat as $row): ?>
                      <tr>
                        <td class="text-center"><?=$i++?></td>
                        <td class="text-center">
                          <?php if($row->id != 0): ?>
                            <a class="btn btn-xs btn-flat btn-warning" href="<?=base_url()?>dt_pns/riwayat_jabatan/<?=$pegawai->id_pegawai?>/<?=$row->id?>" style="padding-right:0.250em;"><i class="fa fa-pencil"></i></a> 
                            <a class="btn btn-xs btn-flat btn-danger" href="#" onclick="del(<?=$row->id?>)"><i class="fa fa-trash"></i></a>
                          <?php endif;?>
                        </td>
                        <td><?=$row->idjabatan?></td>
                        <td class="text-center"><?=$row->eselon?></td>
                        <td class="text-center"><?=date_to_id($row->tmt_jabatan)?></td>
                        <td><?=$row->no_sk?></td>
                        <td class="text-center"><?=date_to_id($row->tgl_sk)?></td>
                        <td class="text-center"><a class="text-green">
                          <?php foreach($row->file as $row2): ?>
                            <a target="_blank" href="<?=base_url()?>berkas/riwayat_jabatan/<?=$row2->file?>" title="<?=$row2->file?>"><i class="fa fa-download"></i></a> 
                          <?php endforeach;?>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <tr>
                      <td class="text-center" colspan="99">Data tidak ada!</td>
                    </tr>
                  <?php endif; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
<!-- /.content-wrapper -->
<!-- Modal Delete -->
<div id="modal_delete" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>dt_pns/riwayat_jabatan_action/delete" method="post">
        <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
        <input type="hidden" name="id" id="id_delete">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /script -->
<div id="modal_delete_file" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>dt_pns/file_riwayat_jabatan_action/delete" method="post">
        <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
        <input type="hidden" name="id" id="id_delete_file">
        <input type="hidden" name="id_riwayat" id="id_riwayat">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus File</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin file ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Tindakan ini tidak bisa dibatalkan.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
  $(document).ready(function () {
    $('#form').validate({
      rules : {
        id_gol : {
          valueNotEquals : '0'
        },
        masakerja_thn : {
          number : true
        },
        masakerja_bln : {
          number : true
        }
      }
    })
  })
  function del(id) {
    $("#modal_delete").modal('show');
    $("#id_delete").val(id);
  }
  function del_file(id,id_riwayat) {
    $("#modal_delete_file").modal('show');
    $("#id_delete_file").val(id);
    $("#id_riwayat").val(id_riwayat);
  }
  function add_file() {
    html =  '<div class="form-group row-file">'+
              '<label class="col-md-2 control-label">File Scan</label>'+
              '<div class="col-md-5">'+
                '<input type="file" class="" name="files[]" value="">'+
              '</div>'+
              '<div class="col-md-2">'+
                '<button class="btn btn-xs btn-danger del-file" type="button"><i class="fa fa-minus"></i> Hapus File</button>'+
              '</div>'+
            '</div>';
    $('#files').append(html);
  }
</script>