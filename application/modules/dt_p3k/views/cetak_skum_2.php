<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cetak SKUm</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>
    td {
      vertical-align: top;
    }
  </style>
</head>
<body>
  <h5 class="text-center">
    DAFTAR ANAK ANAK <br>
    I. ANAK KANDUNG (ak), ANAK TIRI (at) dan ANAK ANGKAT (aa) YANG MASIH MENJADI TANGGUNGAN, <br>
    BELUM MEMPUNYAI PENGHASILAN SENDIRI DAN MASUK DALAM DAFTAR GAJI
  </h5>
  <table class="bordered">
    <thead>
      <tr>
        <th class="text-center" width="30" rowspan="2">No.</th>
        <th class="text-center" width="" rowspan="2">Nama</th>
        <th class="text-center" width="" rowspan="2">Status anak (aa), (ak), (at)</th>
        <th class="text-center" width="120" rowspan="2">Tgl Lahir</th>
        <th class="text-center" width="" rowspan="2">Belum Pernah Kawin</th>
        <th class="text-center" width="" rowspan="2">Bersekolah/Kuliah pada</th>
        <th class="text-center" width="" colspan="3">Lahir Dari Perkawinan</th>
        <th class="text-center" width="" rowspan="2">Tidak Mendapat<br>1.Beasiswa/darma siswa<br>2.Ikatan Dinas</th>
        <th class="text-center" width="" rowspan="2">Keterangan: Diangkat menurut: a. Putusan Pengadilan Negeri b.Hukum Adopsi bagi keturunan Tionghoa</th>
      </tr>
      <tr>
        <th class="text-center" width="">Ayah</th>
        <th class="text-center" width="">Ibu</th>
        <th class="text-center" width="">Tgl. meninggalnya/ diceraikannya/ Ayah/ Ibu</th>
      </tr>
    </thead>
    <tbody>
      <?php if ($list_anak1 != null): ?>
        <?php $i=1;foreach ($list_anak1 as $row): ?>
          <tr>
            <td class="text-center"><?=$i++?></td>
            <td><?=$row->nama?></td>
            <td><?=$row->status?></td>
            <td class="text-center"><?=date_to_id($row->tgl_lahir)?></td>
            <td><?php echo $kawin = ($row->is_kawin == 0) ? 'Belum' : 'Sudah';?></td>
            <td><?=$row->sekolah?></td>
            <td><?=$row->beasiswa?></td>
            <td><?=$row->ayah?></td>
            <td><?=$row->ibu?></td>
            <td class="text-center"><?=date_to_id($row->tgl)?></td>
            <td><?=$row->keterangan?></td>
          </tr>
        <?php endforeach; ?>
      <?php else: ?>
        <tr>
          <td class="text-center" colspan="99">Data tidak ada!</td>
        </tr>
      <?php endif; ?>
    </tbody>
  </table>
  <h5 class="text-center">
    DAFTAR ANAK ANAK <br>
    I. ANAK KANDUNG (ak), ANAK TIRI (at) dan ANAK ANGKAT (aa) YANG MASIH MENJADI TANGGUNGAN, <br>
    TETAPI TIDAK MASUK DALAM DAFTAR GAJI
  </h5>
  <table class="bordered">
    <thead>
      <tr>
        <th class="text-center" width="30">No.</th>
        <th class="text-center" width="">Nama</th>
        <th class="text-center" width="">Status anak (aa), (ak), (at)</th>
        <th class="text-center" width="120">Tgl Lahir</th>
        <th class="text-center" width="">Belum Pernah Kawin</th>
        <th class="text-center" width="">Bersekolah/Kuliah pada</th>
        <th class="text-center" width="">Tidak Mendapat<br>1.Beasiswa/darma siswa<br>2.Ikatan Dinas</th>
        <th class="text-center" width="">Bekerja / Tidak</th>
        <th class="text-center" width="">Keterangan</th>
      </tr>
    </thead>
    <tbody>
      <?php if ($list_anak2 != null): ?>
        <?php $i=1;foreach ($list_anak2 as $row): ?>
          <tr>
            <td class="text-center"><?=$i++?></td>
            <td><?=$row->nama?></td>
            <td class="text-center"><?=$row->status?></td>
            <td class="text-center"><?=date_to_id($row->tgl_lahir)?></td>
            <td><?php echo $kawin = ($row->is_kawin == 0) ? 'Belum' : 'Sudah';?></td>
            <td><?=$row->sekolah?></td>
            <td><?=$row->beasiswa?></td>
            <td><?php echo $kerja = ($row->is_kerja == 1) ? 'Kerja' : 'Tidak';?></td>
            <td><?=$row->keterangan?></td>
          </tr>
        <?php endforeach; ?>
      <?php else: ?>
        <tr>
          <td class="text-center" colspan="99">Data tidak ada!</td>
        </tr>
      <?php endif; ?>
    </tbody>
  </table>
  <br>
  <em style="font-size:11px">
    1. Supaya dilampirkan salinan Keputusan Pengadilan Negeri yang telah disyahkan. <br>
    2. Hanya diisi jika anak dilahirkan dari istri (suami) yang telah meninggal dunia atau diceraikan.
  </em>

</body>
</html>