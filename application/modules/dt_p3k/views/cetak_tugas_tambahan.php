<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <?php $this->load->view('dt_pns/sidebar'); ?>
        </div>
        <div class="col-md-9">
          <?php $this->load->view('dt_pns/header'); ?>
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="update"><a href="<?=base_url()?>dt_pns/riwayat_ppk/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Riwayat PPK</a></li>
              <li class="update"><a href="<?=base_url()?>dt_pns/skp/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">SKP</a></li>
              <li class="update"><a href="<?=base_url()?>dt_pns/realisasi/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Realisasi</a></li>
              <li class="update"><a href="<?=base_url()?>dt_pns/perilaku_kerja/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Perilaku</a></li>
              <li class="update"><a href="<?=base_url()?>dt_pns/tugas_belajar/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Tugas Belajar</a></li>
              <li class="active"><a href="<?=base_url()?>dt_pns/cetak_tugas_tambahan/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Cetak SK Tugas Tambahan</a></li>
              <li class="update"><a href="<?=base_url()?>dt_pns/penilaian_gabung/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Penilaian Gabung</a></li>
              <li class="update"><a href="<?=base_url()?>dt_pns/cetak_ppk/<?php if($pegawai){echo $pegawai->id_pegawai;}?>">Cetak PPK</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
<!-- /.content-wrapper -->
<!-- Modal Delete -->
<div id="modal_delete_perilaku_kerja_tubel" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>dt_pns/perilaku_kerja_tubel_action/delete" method="post">
        <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
        <input type="hidden" name="id" id="id_delete_perilaku_kerja_tubel">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-flat btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Modal Delete -->
<div id="modal_delete_proses_penilaian" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>dt_pns/proses_penilaian_action/delete" method="post">
        <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
        <input type="hidden" name="id" id="id_delete_proses_penilaian">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-flat btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /script -->
<script>
  $(document).ready(function () {
    $('#form').validate({
      rules : {
        id_hukuman : {
          valueNotEquals : '0'
        }
      }
    })
  })
  function del_perilaku_kerja_tubel(id) {
    $("#modal_delete_perilaku_kerja_tubel").modal('show');
    $("#id_delete_perilaku_kerja_tubel").val(id);
  }
  function del_proses_penilaian(id) {
    $("#modal_delete_proses_penilaian").modal('show');
    $("#id_delete_proses_penilaian").val(id);
  }
</script>