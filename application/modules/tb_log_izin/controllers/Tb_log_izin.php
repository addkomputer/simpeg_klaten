<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tb_log_izin extends MY_Controller {

	var $access, $id_grup;

  function __construct(){
		parent::__construct();

		$controller = 'tb_log_izin';

    if($this->session->userdata('menu') != $controller){
      $this->session->unset_userdata('search');
      $this->session->set_userdata(array('menu' => 'tb_log_izin'));
    }
    $this->load->model('ap_konfigurasi/m_ap_konfigurasi');
		$this->id_grup = $this->session->userdata('id_grup');
		$this->access = $this->m_ap_konfigurasi->get_permission($this->id_grup, $controller);
		if ($this->access == null) {
			redirect(base_url().'ap_error/error_403');
		}
		$this->load->model('m_tb_log_izin');
		$this->load->model('ms_jabatan/m_ms_jabatan');
		$this->load->model('ms_ruang/m_ms_ruang');
		$this->load->model('ms_status_pegawai/m_ms_status_pegawai');
		$this->load->model('dt_pegawai/m_dt_pegawai');
	}
	
	public function index()
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Daftar';
		$data['jabatan'] = $this->m_ms_jabatan->get_all();
		$data['ruang'] = $this->m_ms_ruang->get_all();
		$data['status_pegawai'] = $this->m_ms_status_pegawai->get_all();

		if ($this->access->_read) {
			$search = null;
			
			if($this->session->userdata('search') != null){
				$search = $this->session->userdata('search');
			}
			
			$data['search'] = $search;
	
			$config['base_url'] = base_url().'tb_log_izin/index/';
			$config['per_page'] = 10;
	
			$from = $this->uri->segment(3);
				
			$num_rows = $this->m_tb_log_izin->num_rows($search);
			$config['total_rows'] = $num_rows;
			$this->pagination->initialize($config);

			$data['main'] = $this->m_tb_log_izin->get_list($config['per_page'],$from,$search);
			
			$data['num_rows'] = $num_rows;
			$data['num_rows_total'] = $this->m_tb_log_izin->num_rows_total($search);
			insert_log('read',$this->access->menu);
			$this->view('index',$data);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function search()
	{
		$search = $_POST;
		$this->session->set_userdata(array('search' => $search));
		redirect(base_url().$this->access->controller);
	}
	
	public function form($id = null)
	{
		$data['access'] = $this->access;
		$data['pegawai'] = $this->m_dt_pegawai->get_all();
		$data['ruang'] = $this->m_ms_ruang->get_all();

		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Tambah Data';
				$data['action'] = 'insert';
				$data['main'] = null;
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Ubah Data';
				$data['action'] = 'update';
				$data['main'] = $this->m_tb_log_izin->get_by_id($id);
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		}
	}

	public function detail($tanggal,$nomor_induk)
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Detail Kehadiran';
		$data['main'] = $this->m_tb_log_izin->detail($tanggal,$nomor_induk);
		$this->view('detail',$data);
	}

	public function reset_search()
  {
    $this->session->unset_userdata('search');
    redirect(base_url().'tb_log_izin/index');
  }

	public function insert()
	{
		$data = $_POST;
		if ($data != null) {
			$data['created_by'] = $this->session->userdata('nama');
			if(!isset($data['is_active'])){$data['is_active'] = 0;}
			
			$data['file_bukti'] = $this->uploadBukti();

			$tanggal = $data['tanggal_mulai'];
			$mulai = date_create(date_to_id($data['tanggal_mulai']));
			$akhir = date_create(date_to_id($data['tanggal_akhir']));
			$diff  = date_diff($mulai,$akhir)->format('%d');
			unset($data['tanggal'],$data['tanggal_mulai'],$data['tanggal_akhir']);

			for ($i=0; $i <= $diff; $i++) { 
				$data['tanggal'] = date('Y-m-d', strtotime($tanggal. '+'.$i.' days'));
				$this->m_tb_log_izin->insert($data);
			}
			insert_log('insert',$this->access->menu);
			$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
			redirect(base_url().'tb_log_izin/index');
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function update()
	{
		$data = $_POST;
		if ($data != null) {
			if($this->access->_update){
				$data['updated_by'] = $this->session->userdata('nama');
				if(!isset($data['is_active'])){$data['is_active'] = 0;}
				$id = $data['id'];
				if (!empty($_FILES["file_bukti"]["name"])) {
					$data['file_bukti'] = $this->uploadBukti();
				} else {
					$data['file_bukti'] = $data["old_file_bukti"];
				}
				$data['tanggal'] = date_to_id($data['tanggal']);
				unset($data['old_file_bukti']);
				$this->m_tb_log_izin->update($id, $data);
				insert_log('update',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				redirect(base_url().'tb_log_izin/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function delete()
	{
		$data = $_POST;
		if($data != null){
			if ($this->access->_delete) {
				$this->m_tb_log_izin->delete_permanent($data['id']);
				insert_log('delete',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				redirect(base_url().'tb_log_izin/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function uploadBukti()
	{
		$config['upload_path']          = './berkas/bukti/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png|pdf';
		$config['overwrite']						= true;
		$config['max_size']             = 100000000;
		$config['remove_spaces'] 				= TRUE;
		$config['encrypt_name'] 				= TRUE;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('file_bukti')){
			$error = array('error' => $this->upload->display_errors());
		}else{
			return $this->upload->data("file_name");
		}
		
		return null;
	}

}