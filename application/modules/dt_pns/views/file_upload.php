<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <?php $this->load->view('dt_pns/sidebar'); ?>
        </div>
        <div class="col-md-9">
          <?php $this->load->view('dt_pns/header'); ?>
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title text-orange">Upload File Tambahan</h3> </div>
            <div class="box-body">
              <?php echo $this->session->flashdata('status'); ?>
              <form id="form" class="form-horizontal" action="<?=base_url().$access->controller?>/file_upload_action/<?=$action?>" method="post" enctype="multipart/form-data" autocomplete="off">
                <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
                <input type="hidden" name="id" id="id" value="<?php if($file_upload){echo $file_upload->id;}?>">
                <div class="form-group">
                  <label class="col-md-2 control-label">Judul File</label>
                  <div class="col-md-6">
                    <input type="text" class="form-control input-sm" name="judul" value="<?php if($file_upload){echo $file_upload->judul;}?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">File</label>
                  <div class="col-md-3">
                    <input type="file" class="form-control input-sm" name="file" />
                  </div>
                  <div class="col-md-3">
                    <?php
                        if($file_upload){
                          ?>
                          <a class="btn btn-sm btn-success" target="_blank" rel="noopener noreferrer" href="<?=base_url()?>berkas/file_upload/<?=$file_upload->file?>" style="padding-right:0.5em;"><i class="fa fa-eye"> Lihat Data</i></a>
                          <?php
                        }
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-8 col-md-offset-2">
                    <em>Sebelum mengunggah file, pastikan nama file yang akan diunggah sudah benar sesuai dengan isi file. Jenis file yang diterima adalah *.jpg,*.png,*.jpeg dan *.pdf</em>
                  </div>
                </div>
                <hr>
                <div class="form-group">
                  <div class="col-md-offset-2 col-sm-8">
                    <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
                  </div>
                </div>
                <hr>
              </form>
              <table class="table table-striped table-bordered table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" width="30">No.</th>
                    <th class="text-center" width="60">Aksi</th>
                    <th class="text-center" width="">Judul</th>
                    <th class="text-center" width="200">Tanggal</th>
                    <th class="text-center" width="200">File</th>
                </thead>
                <tbody>
                  <?php if ($file_upload_all != null): ?>
                    <?php $i=1;foreach ($file_upload_all as $row): ?>
                      <tr>
                        <td class="text-center"><?=$i++?></td>
                        <td class="text-center">
                          <?php if($row->id != 0): ?>
                            <a class="text-orange" href="<?=base_url()?>dt_pns/file_upload/<?=$pegawai->id_pegawai?>/<?=$row->id?>" style="padding-right:0.250em;"><i class="fa fa-pencil"></i></a>
                            <a class="text-red" href="#" onclick="del(<?=$row->id?>)"><i class="fa fa-trash"></i></a>
                          <?php endif;?>
                        </td>
                        <td><?=$row->judul?></td>
                        <td class="text-center"><?=$row->created?></td>
                        <td class="text-center">
                            <a class="btn btn-sm btn-success" target="_blank" rel="noopener noreferrer" href="<?=base_url()?>berkas/file_upload/<?=$row->file?>" style="padding-right:0.5em;"><i class="fa fa-eye"> Lihat Data</i></a>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <tr>
                      <td class="text-center" colspan="99">Data tidak ada!</td>
                    </tr>
                  <?php endif; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
<!-- /.content-wrapper -->
<!-- Modal Delete -->
<div id="modal_delete" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>dt_pns/file_upload_action/delete" method="post">
        <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
        <input type="hidden" name="id" id="id_delete">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /script -->
<script>
  $(document).ready(function () {
    $('#form').validate({
      rules : {
        id_hukuman : {
          valueNotEquals : '0'
        }
      }
    })
  })
  function del(id) {
    $("#modal_delete").modal('show');
    $("#id_delete").val(id);
  }
</script>
