<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <?php $this->load->view('dt_pns/sidebar'); ?>
        </div>
        <div class="col-md-9">
          <?php $this->load->view('dt_pns/header'); ?>
          <div class="box box-primary ">
            <div class="box-header with-border">
              <h3 class="box-title text-orange">SKUM</h3>
            </div>
            <div class="box-body">
              <form id="form_skum" class="form-horizontal" method="post" action="<?=base_url()?>dt_pns/skum_action" enctype="multipart/form-data" autocomplete="off">
                <input type="hidden" name="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
                <div class="form-group">
                  <label class="col-md-2 control-label">NAMA</label>
                  <?php 
                    $nama = $pegawai->gelar_depan.' '.$pegawai->nama;
                    if ($pegawai->gelar_belakang != ''){
                      $nama .= ', '.$pegawai->gelar_belakang;
                    }
                  ?>
                  <div class="col-md-5">
                    <input type="text" class="form-control" name="" id="" value="<?php if($pegawai){echo $nama;}?>" disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">NIP</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control" name="nomor_induk" id="nomor_induk" value="<?php if($pegawai){echo $pegawai->nomor_induk;}?>" disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Agama</label>
                  <div class="col-md-2">
                    <select class="form-control select2" name="agama" id="agama" disabled>
                      <option value="0" <?php if($pegawai){if($pegawai->agama == 0){echo 'selected';}}?>> -- Pilih -- </option>
                      <option value="1" <?php if($pegawai){if($pegawai->agama == 1){echo 'selected';}}?>> Islam </option>
                      <option value="2" <?php if($pegawai){if($pegawai->agama == 2){echo 'selected';}}?>> Protestan </option>
                      <option value="3" <?php if($pegawai){if($pegawai->agama == 3){echo 'selected';}}?>> Katholik </option>
                      <option value="4" <?php if($pegawai){if($pegawai->agama == 4){echo 'selected';}}?>> Hindu </option>
                      <option value="5" <?php if($pegawai){if($pegawai->agama == 5){echo 'selected';}}?>> Budha </option>
                    </select>
                  </div>
                  <label class="col-md-2 control-label" style="width:max-content !important;">Jenis Kelamin</label>
                  <div class="col-md-2">
                    <select class="form-control select2" name="jenis_kelamin" id="jenis_kelamin" disabled>
                      <option value="0" <?php if($pegawai){if($pegawai->jenis_kelamin == 0){echo 'selected';}}?>> -- Pilih -- </option>
                      <option value="1" <?php if($pegawai){if($pegawai->jenis_kelamin == 1){echo 'selected';}}?>> Laki-laki </option>
                      <option value="2" <?php if($pegawai){if($pegawai->jenis_kelamin == 2){echo 'selected';}}?>> Perempuan </option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Warga Negara</label>
                  <div class="col-md-3">
                    <select class="form-control select2" name="warganegara" id="warganegara" disabled>
                      <option value="0" <?php if($pegawai){if($pegawai->warganegara == 0){echo 'selected';}}?>> -- Pilih -- </option>
                      <option value="1" <?php if($pegawai){if($pegawai->warganegara == 1){echo 'selected';}}?>> Indonesia </option>
                      <option value="2" <?php if($pegawai){if($pegawai->warganegara == 2){echo 'selected';}}?>> Asing </option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Golongan Ruang</label>
                  <div class="col-md-4">
                    <select class="form-control select2" name="id_golongan" id="id_golongan" disabled>
                      <option value="0" <?php if($pegawai){if($pegawai->id_golongan == 0){echo 'selected';}}?>> -- Pilih -- </option>
                      <?php foreach($golongan as $row): ?>
                        <option value="<?=$row->id_golongan?>" <?php if($pegawai){if($pegawai->id_golongan == $row->id_golongan){echo 'selected';}}?>><?=$row->golongan.' - '.$row->pangkat?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Jabatan</label>
                  <div class="col-sm-5">
                    <select class="form-control select2" name="id_jabatan" id="id_jabatan" disabled>
                      <option value="0" <?php if($pegawai){if($pegawai->id_jabatan == '0'){echo 'selected';}}else{echo 'selected';}?>>-- Pilih --</option>
                      <?php foreach($jabatan as $row): ?>
                        <option value="<?=$row->id_jabatan?>" <?php if($pegawai){if($pegawai->id_jabatan == $row->id_jabatan){echo 'selected';}}?>><?=$row->jabatan?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Instansi</label>
                  <div class="col-md-5">
                    <input type="text" class="form-control" name="" id="" value="Pemerintah Kabupaten Kebumen" disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Masa Kerja Golongan</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control" name="" id="" value="<?=dateDifference(date('Y-m-d'),$pegawai->tmt_gol,'%y Tahun %m Bulan')?>" disabled>
                  </div>
                  <label class="col-md-2 control-label">Masa Kerja Seluruhnya</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control" name="" id="" value="<?=dateDifference(date('Y-m-d'),$pegawai->tmt_pns,'%y Tahun %m Bulan')?>" disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Digaji Menurut</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control" name="gaji_menurut" id="gaji_menurut" value="<?=$pegawai->gaji_menurut?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Gaji Pokok</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control" name="gaji_pokok" id="gaji_pokok" value="<?=$pegawai->gaji_pokok?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Alamat</label>
                  <div class="col-md-5">
                    <input type="text" class="form-control" name="alamat" id="alamat" value="<?=$pegawai->alamat?>" disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Jabatan Tambahan</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="jabatan_samping" id="jabatan_samping" value="<?=$pegawai->jabatan_samping?>">
                  </div>
                  <label class="col-md-2 control-label">Gaji</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control" name="gaji_samping" id="gaji_samping" value="<?=$pegawai->gaji_samping?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Pensiun / Pensiun Janda</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control" name="pensiun" id="pensiun" value="<?=$pegawai->pensiun?>">
                  </div>
                </div>
                <h4>Pasangan</h4>
                <div class="form-group">
                  <label class="col-md-2 control-label">Nama Pasangan</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control" name="nama_pasangan" id="nama_pasangan" value="<?php if($keluarga){echo $keluarga->nama_pasangan;}?>" disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Tanggal Lahir</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control datepicker" name="tgl_lahir_pasangan" id="tgl_lahir_pasangan" value="<?php if($keluarga){echo date_to_id($keluarga->tgl_lahir_pasangan);}?>" disabled> 
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Tanggal Perkawinan</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control datepicker" name="tgl_perkawinan" id="tgl_perkawinan" value="<?php if($keluarga){echo date_to_id($keluarga->tgl_perkawinan);}?>" disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Pekerjaan</label>
                  <div class="col-sm-5">
                    <select class="form-control select2" name="pekerjaan_pasangan" id="pekerjaan_pasangan" disabled>
                      <option value="0" <?php if($keluarga){if($keluarga->pekerjaan_pasangan == '0'){echo 'selected';}}?>> -- Pilih -- </option>
                      <option value="1" <?php if($keluarga){if($keluarga->pekerjaan_pasangan == '1'){echo 'selected';}}?>> PNS </option>
                      <option value="2" <?php if($keluarga){if($keluarga->pekerjaan_pasangan == '2'){echo 'selected';}}?>> TNI / POLRI </option>
                      <option value="3" <?php if($keluarga){if($keluarga->pekerjaan_pasangan == '3'){echo 'selected';}}?>> Pengusaha / Wira Swasta </option>
                      <option value="4" <?php if($keluarga){if($keluarga->pekerjaan_pasangan == '4'){echo 'selected';}}?>> Petani/Nelayan </option>
                      <option value="5" <?php if($keluarga){if($keluarga->pekerjaan_pasangan == '5'){echo 'selected';}}?>> Karyawan Swasta </option>
                      <option value="6" <?php if($keluarga){if($keluarga->pekerjaan_pasangan == '6'){echo 'selected';}}?>> Mengurus Rumah Tangga </option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Keterangan</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control" name="keterangan" id="keterangan" value="<?php if($keluarga){echo $keluarga->keterangan;}?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Gaji Pasangan</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control" name="gaji_pasangan" id="gaji_pasangan" value="<?php if($keluarga){echo $keluarga->gaji_pasangan;}?>">
                  </div>
                </div>
                <hr>
                <div class="form-group">
                  <div class="col-md-offset-2 col-sm-8">
                    <button class="btn btn-flat btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title text-orange">Anak yang masih menjadi tanggungan, belum punya penghasilan sendiri dan masuk dalam daftar gaji.</h3>
            </div>
            <div class="box-body">
              <?php echo $this->session->flashdata('status_usulan'); ?>
              <form id="form_anak1" class="form-horizontal" action="<?=base_url().$access->controller?>/skum_anak1_action/<?=$action_anak1?>" method="post" enctype="multipart/form-data" autocomplete="off">
                <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
                <input type="hidden" name="id" id="id" value="<?php if($anak1){echo $anak1->id;}?>">
                <div class="form-group">
                  <label class="col-md-2 control-label">Nama</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="nama" id="nama" value="<?php if($anak1){echo $anak1->nama;}?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Status Anak</label>
                  <div class="col-sm-5">
                    <select class="form-control select2" name="status" id="status">
                      <option value="aa" <?php if($anak1){if($anak1->is_kawin == 'aa'){echo 'selected';}}?>> Anak Angkat </option>
                      <option value="ak" <?php if($anak1){if($anak1->is_kawin == 'ak'){echo 'selected';}}?>> Anak Kandung </option>
                      <option value="at" <?php if($anak1){if($anak1->is_kawin == 'at'){echo 'selected';}}?>> Anak Tiri </option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Tanggal Lahir</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control datepicker" name="tgl_lahir" id="tgl_lahir" value="<?php if($anak1){echo date_to_id($anak1->tgl_lahir);}?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Status Perkawinan</label>
                  <div class="col-sm-5">
                    <select class="form-control select2" name="is_kawin" id="is_kawin">
                      <option value="0" <?php if($anak1){if($anak1->is_kawin == '0'){echo 'selected';}}?>> Belum Kawin </option>
                      <option value="1" <?php if($anak1){if($anak1->is_kawin == '1'){echo 'selected';}}?>> Kawin </option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Bersekolah/Kuliah pada</label>
                  <div class="col-md-6">
                    <input type="text" class="form-control" name="sekolah" id="sekolah" value="<?php if($anak1){echo $anak1->sekolah;}?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Beasiswa</label>
                  <div class="col-sm-5">
                    <select class="form-control select2" name="beasiswa" id="beasiswa">
                      <option value="0" <?php if($anak1){if($anak1->beasiswa == '0'){echo 'selected';}}?>>-- Pilih -- </option>
                      <option value="0" <?php if($anak1){if($anak1->beasiswa == '1'){echo 'selected';}}?>>1. Beasiswa/Darma Siswa </option>
                      <option value="1" <?php if($anak1){if($anak1->beasiswa == '2'){echo 'selected';}}?>>2. Ikatan Dinas </option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Ayah</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="ayah" id="ayah" value="<?php if($anak1){echo $anak1->ayah;}?>">
                  </div>
                  <label class="col-md-2 control-label"  style="width:max-content !important;">Ibu</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="ibu" id="ibu" value="<?php if($anak1){echo $anak1->ibu;}?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Tgl. meninggal/cerai</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control datepicker" name="tgl" id="tgl" value="<?php if($anak1){echo date_to_id($anak1->tgl);}?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Keterangan</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control" name="keterangan" id="keterangan" value="<?php if($anak1){echo $anak1->keterangan;}?>">  
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-offset-2 col-sm-8">
                    <button class="btn btn-success btn-flat" type="submit"><i class="fa fa-save"></i> Simpan</button>
                  </div>
                </div>
                <hr>
              </form>
              <div class="table-responsive">

                <table class="table table-striped table-bordered table-condensed">
                  <thead>
                    <tr>
                      <th class="text-center" width="30" rowspan="2">No.</th>
                      <th class="text-center" width="60" rowspan="2">Aksi</th>
                      <th class="text-center" width="" rowspan="2">Nama</th>
                      <th class="text-center" width="" rowspan="2">Status anak (aa), (ak), (at)</th>
                      <th class="text-center" width="120" rowspan="2">Tgl Lahir</th>
                      <th class="text-center" width="" rowspan="2">Belum Pernah Kawin</th>
                      <th class="text-center" width="" rowspan="2">Bersekolah/Kuliah pada</th>
                      <th class="text-center" width="" colspan="3">Lahir Dari Perkawinan</th>
                      <th class="text-center" width="" rowspan="2">Tidak Mendapat<br>1.Beasiswa/darma siswa<br>2.Ikatan Dinas</th>
                      <th class="text-center" width="" rowspan="2">Keterangan: Diangkat menurut: a. Putusan Pengadilan Negeri b.Hukum Adopsi bagi keturunan Tionghoa</th>
                    </tr>
                    <tr>
                      <th class="text-center" width="">Ayah</th>
                      <th class="text-center" width="">Ibu</th>
                      <th class="text-center" width="">Tgl. meninggalnya/ diceraikannya/ Ayah/ Ibu</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if ($list_anak1 != null): ?>
                      <?php $i=1;foreach ($list_anak1 as $row): ?>
                        <tr>
                          <td class="text-center"><?=$i++?></td>
                          <td class="text-center">
                            <a class="btn btn-xs btn-warning btn-flat" href="<?=base_url()?>dt_pns/skum/<?=$pegawai->id_pegawai?>/1/<?=$row->id?>" style="padding-right:0.250em;"><i class="fa fa-pencil"></i></a>
                            <a class="btn btn-xs btn-danger btn-flat" href="#" onclick="del_anak1(<?=$row->id?>)"><i class="fa fa-trash"></i></a>
                          </td>
                          <td><?=$row->nama?></td>
                          <td><?=$row->status?></td>
                          <td class="text-center"><?=date_to_id($row->tgl_lahir)?></td>
                          <td><?php echo $kawin = ($row->is_kawin == 0) ? 'Belum' : 'Sudah';?></td>
                          <td><?=$row->sekolah?></td>
                          <td><?=$row->beasiswa?></td>
                          <td><?=$row->ayah?></td>
                          <td><?=$row->ibu?></td>
                          <td class="text-center"><?=date_to_id($row->tgl)?></td>
                          <td><?=$row->keterangan?></td>
                        </tr>
                      <?php endforeach; ?>
                    <?php else: ?>
                      <tr>
                        <td class="text-center" colspan="99">Data tidak ada!</td>
                      </tr>
                    <?php endif; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title text-orange">Anak yang masih menjadi tanggungantetapi tidak masuk dalam daftar gaji</h3>
            </div>
            <div class="box-body">
              <?php echo $this->session->flashdata('status_usulan'); ?>
              <form id="form_anak2" class="form-horizontal" action="<?=base_url().$access->controller?>/skum_anak2_action/<?=$action_anak2?>" method="post" enctype="multipart/form-data" autocomplete="off">
                <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
                <input type="hidden" name="id" id="id" value="<?php if($anak2){echo $anak2->id;}?>">
                <div class="form-group">
                  <label class="col-md-2 control-label">Nama</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="nama" id="nama" value="<?php if($anak2){echo $anak2->nama;}?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Status Anak</label>
                  <div class="col-sm-5">
                    <select class="form-control select2" name="status" id="status">
                      <option value="aa" <?php if($anak2){if($anak2->is_kawin == 'aa'){echo 'selected';}}?>> Anak Angkat </option>
                      <option value="ak" <?php if($anak2){if($anak2->is_kawin == 'ak'){echo 'selected';}}?>> Anak Kandung </option>
                      <option value="at" <?php if($anak2){if($anak2->is_kawin == 'at'){echo 'selected';}}?>> Anak Tiri </option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Tanggal Lahir</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control datepicker" name="tgl_lahir" id="tgl_lahir" value="<?php if($anak2){echo date_to_id($anak2->tgl_lahir);}?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Status Perkawinan</label>
                  <div class="col-sm-5">
                    <select class="form-control select2" name="is_kawin" id="is_kawin">
                      <option value="0" <?php if($anak2){if($anak2->is_kawin == '0'){echo 'selected';}}?>> Belum Kawin </option>
                      <option value="1" <?php if($anak2){if($anak2->is_kawin == '1'){echo 'selected';}}?>> Kawin </option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Bersekolah/Kuliah pada</label>
                  <div class="col-md-6">
                    <input type="text" class="form-control" name="sekolah" id="sekolah" value="<?php if($anak2){echo $anak2->sekolah;}?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Beasiswa</label>
                  <div class="col-sm-5">
                    <select class="form-control select2" name="beasiswa" id="beasiswa">
                      <option value="0" <?php if($anak2){if($anak2->beasiswa == '0'){echo 'selected';}}?>>-- Pilih -- </option>
                      <option value="1" <?php if($anak2){if($anak2->beasiswa == '1'){echo 'selected';}}?>>1. Beasiswa/Darma Siswa </option>
                      <option value="2" <?php if($anak2){if($anak2->beasiswa == '2'){echo 'selected';}}?>>2. Ikatan Dinas </option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Bekerja/Tidak</label>
                  <div class="col-sm-5">
                    <select class="form-control select2" name="is_kerja" id="is_kerja">
                      <option value="1" <?php if($anak2){if($anak2->beasiswa == '1'){echo 'selected';}}?>>1. Bekerja </option>
                      <option value="2" <?php if($anak2){if($anak2->beasiswa == '2'){echo 'selected';}}?>>2. Tidak </option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Keterangan</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control" name="keterangan" id="keterangan" value="<?php if($anak2){echo $anak2->keterangan;}?>">  
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-offset-2 col-sm-8">
                    <button class="btn btn-success btn-flat" type="submit"><i class="fa fa-save"></i> Simpan</button>
                  </div>
                </div>
                <hr>
              </form>
              <table class="table table-striped table-bordered table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" width="30">No.</th>
                    <th class="text-center" width="60">Aksi</th>
                    <th class="text-center" width="">Nama</th>
                    <th class="text-center" width="">Status anak (aa), (ak), (at)</th>
                    <th class="text-center" width="120">Tgl Lahir</th>
                    <th class="text-center" width="">Belum Pernah Kawin</th>
                    <th class="text-center" width="">Bersekolah/Kuliah pada</th>
                    <th class="text-center" width="">Tidak Mendapat<br>1.Beasiswa/darma siswa<br>2.Ikatan Dinas</th>
                    <th class="text-center" width="">Bekerja / Tidak</th>
                    <th class="text-center" width="">Keterangan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if ($list_anak2 != null): ?>
                    <?php $i=1;foreach ($list_anak2 as $row): ?>
                      <tr>
                        <td class="text-center"><?=$i++?></td>
                        <td class="text-center">
                          <a class="btn btn-xs btn-warning btn-flat" href="<?=base_url()?>dt_pns/skum/<?=$pegawai->id_pegawai?>/2/<?=$row->id?>" style="padding-right:0.250em;"><i class="fa fa-pencil"></i></a>
                          <a class="btn btn-xs btn-danger btn-flat" href="#" onclick="del_anak2(<?=$row->id?>)"><i class="fa fa-trash"></i></a>
                        </td>
                        <td><?=$row->nama?></td>
                        <td class="text-center"><?=$row->status?></td>
                        <td class="text-center"><?=date_to_id($row->tgl_lahir)?></td>
                        <td><?php echo $kawin = ($row->is_kawin == 0) ? 'Belum' : 'Sudah';?></td>
                        <td><?=$row->sekolah?></td>
                        <td><?=$row->beasiswa?></td>
                        <td><?php echo $kerja = ($row->is_kerja == 1) ? 'Kerja' : 'Tidak';?></td>
                        <td><?=$row->keterangan?></td>
                      </tr>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <tr>
                      <td class="text-center" colspan="99">Data tidak ada!</td>
                    </tr>
                  <?php endif; ?>
                </tbody>
              </table>
            </div>
          </div>
          <div class="box box-primary">
            <div class="box-body">
              <a target="_blank" class="btn btn-default" href="<?=base_url()?>dt_pns/cetak_skum/<?=$pegawai->id_pegawai?>"><i class="fa fa-print"></i> Cetak SKUM</a>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- Modal Delete -->
<div id="modal_del_anak1" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>dt_pns/delete_anak1/delete" method="post">
        <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
        <input type="hidden" name="id" id="id_anak1">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-flat btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /.content-wrapper -->
<div id="modal_del_anak2" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>dt_pns/delete_anak2/delete" method="post">
        <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
        <input type="hidden" name="id" id="id_anak2">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-flat btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
  $(document).ready(function () {
    $('#form').validate({
      rules : {
        id_hukuman : {
          valueNotEquals : '0'
        }
      }
    })
  })

  function del_anak1(id) {
    $('#id_anak1').val(id);
    $('#modal_del_anak1').modal('show');
  }

  function del_anak2(id) {
    $('#id_anak2').val(id);
    $('#modal_del_anak2').modal('show');
  }
</script>
