<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dt_pns extends MY_Controller {

	var $access, $id_pegawai;

  function __construct(){
		parent::__construct();

		$controller = 'dt_pns';

    if($this->session->userdata('menu') != $controller){
      $this->session->unset_userdata('search');
      $this->session->set_userdata(array('menu' => 'dt_pns'));
    }
    $this->load->model('ap_konfigurasi/m_ap_konfigurasi');
		$this->id_pegawai = $this->session->userdata('id_pegawai');
		$this->access = $this->m_ap_konfigurasi->get_permission($this->id_pegawai, $controller);
		if ($this->access == null) {
			redirect(base_url().'ap_error/error_403');
		}
		$this->load->model('m_dt_pns');
		$this->load->model('ms_instansi/m_ms_instansi');
		$this->load->model('ms_golongan/m_ms_golongan');
		$this->load->model('ms_kecamatan/m_ms_kecamatan');
		$this->load->model('ms_unit_kerja/m_ms_unit_kerja');
		$this->load->model('ms_unit_kerja/m_ms_unit_kerja');
		$this->load->model('ms_instansi/m_ms_instansi');
		$this->load->model('ms_rumpun_jabatan/m_ms_rumpun_jabatan');
		$this->load->model('ms_jabatan/m_ms_jabatan');
		$this->load->model('ms_status_pegawai/m_ms_status_pegawai');
		$this->load->model('ms_tingkat/m_ms_tingkat');
		$this->load->model('ms_periode_penilaian/m_ms_periode_penilaian');
		$this->load->model('dt_anak/m_dt_anak');
		$this->load->model('ap_profil/m_ap_profil');
		$this->load->model('dt_tingkat/m_dt_tingkat');
		$this->load->model('dt_atasan/m_dt_atasan');
		$this->load->model('dt_keluarga/m_dt_keluarga');
		$this->load->model('dt_riwayat_pangkat/m_dt_riwayat_pangkat');
		$this->load->model('dt_file_riwayat_pangkat/m_dt_file_riwayat_pangkat');
		$this->load->model('dt_riwayat_pendidikan/m_dt_riwayat_pendidikan');
		$this->load->model('dt_file_riwayat_pendidikan/m_dt_file_riwayat_pendidikan');
		$this->load->model('dt_riwayat_jabatan/m_dt_riwayat_jabatan');
		$this->load->model('dt_file_riwayat_jabatan/m_dt_file_riwayat_jabatan');
		$this->load->model('ms_jenis_diklat/m_ms_jenis_diklat');
		$this->load->model('dt_riwayat_diklat/m_dt_riwayat_diklat');
		$this->load->model('dt_file_upload/m_dt_file_upload');
		$this->load->model('dt_riwayat_penilaian/m_dt_riwayat_penilaian');
		$this->load->model('dt_riwayat_diklat_baru/m_dt_riwayat_diklat_baru');
		$this->load->model('dt_riwayat_usulan_diklat/m_dt_riwayat_usulan_diklat');
		$this->load->model('dt_riwayat_penghargaan/m_dt_riwayat_penghargaan');
		$this->load->model('ms_hukuman_disiplin/m_ms_hukuman_disiplin');
		$this->load->model('dt_riwayat_hukuman/m_dt_riwayat_hukuman');
		$this->load->model('dt_riwayat_ppk/m_dt_riwayat_ppk');
		$this->load->model('dt_tgs_pokok/m_dt_tgs_pokok');
		$this->load->model('dt_tgs_tambahan/m_dt_tgs_tambahan');
		$this->load->model('dt_tgs_kreatifitas/m_dt_tgs_kreatifitas');
		$this->load->model('dt_perilaku_kerja/m_dt_perilaku_kerja');
		$this->load->model('dt_proses_penilaian/m_dt_proses_penilaian');
		$this->load->model('dt_perilaku_kerja_tubel/m_dt_perilaku_kerja_tubel');
		$this->load->model('dt_perilaku_kerja_manual/m_dt_perilaku_kerja_manual');
		$this->load->model('dt_skum_anak1/m_dt_skum_anak1');
		$this->load->model('dt_skum_anak2/m_dt_skum_anak2');
	}

	public function index()
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Daftar';
		$data['instansi'] = $this->m_ms_instansi->get_all();
		$data['jabatan'] = $this->m_ms_jabatan->get_all();
		$data['golongan'] = $this->m_ms_golongan->get_all();

		if ($this->access->_read) {
			$search = null;
			if($this->session->userdata('search') != null){
				$search = $this->session->userdata('search');
			}else{
				$search['id_instansi'] = '';
				if ($this->session->userdata('id_grup') == 3) {
					$search ['id_instansi'] = $this->session->userdata('id_instansi');
				}
				$search['term'] = '';
				$search['id_jabatan'] = '';
			}
			
			$data['search'] = $search;

			$config['base_url'] = base_url().$this->access->controller.'/index/';
			$config['per_page'] = 10;

			$from = $this->uri->segment(3);

			$num_rows = $this->m_dt_pns->num_rows($search);

			$config['total_rows'] = $num_rows;
			$this->pagination->initialize($config);

			$data['pegawai'] = $this->m_dt_pns->get_list($config['per_page'],$from,$search);

			$data['num_rows'] = $num_rows;
			$data['num_rows_total'] = $this->m_dt_pns->num_rows_all();
			insert_log('read',$this->access->menu);
			$this->view('index',$data);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function reset_search()
  {
		$this->session->unset_userdata('search');
		redirect(base_url().'dt_pns');
	}
	
	public function search()
	{
		$data = $_POST;
		$this->session->set_userdata(array('search' => $data));
		redirect(base_url().$this->access->controller);
	}

	public function data_pokok($id = null)
	{
		$data['access'] = $this->access;
		$data['instansi'] = $this->m_ms_instansi->get_all();
		$data['golongan'] = $this->m_ms_golongan->get_all();
		$data['kecamatan'] = $this->m_ms_kecamatan->get_all();
		$data['instansi'] = $this->m_ms_instansi->get_all();
		$data['rumpun_jabatan'] = $this->m_ms_rumpun_jabatan->get_all();
		$data['jabatan_list'] = $this->m_ms_jabatan->get_all();
		$data['tingkat_list'] = $this->m_ms_tingkat->get_all();
		$data['periode_penilaian'] = $this->m_ms_periode_penilaian->get_all();
		$data['status_pegawai'] = $this->m_ms_status_pegawai->get_all();

		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Tambah Data';
				$data['action'] = 'insert';
				$data['pegawai'] = null;
				$data['jabatan'] = null;
				$data['tingkat'] = null;
				$data['anak'] = null;
				$data['atasan'] = null;
				$data['keluarga'] = null;
				$data['id_pegawai'] = uniqid();
				$data['pin'] = $this->m_dt_pns->pin_max();

				$this->view('data_pokok', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Data Pokok';
				$data['action'] = 'update';
				$data['pegawai'] = $this->m_dt_pns->get_by_id($id);
				$data['jabatan'] = $this->m_dt_pns->get_jabatan_by_id($id);
				$data['tingkat'] = $this->m_dt_pns->get_tingkat_by_id($id);
				$data['anak'] = $this->m_dt_anak->get_by_id_pegawai($id);
				$data['atasan'] = $this->m_dt_atasan->get_by_id($id);
				$data['keluarga'] = $this->m_dt_keluarga->get_by_id($id);
				// echo '<pre>' . var_export($data['jabatan'], true) . '</pre>';
				$this->view('data_pokok', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}

	public function ganti_pass()
	{
		$data = $_POST;
		$id = $data['id_pegawai'];
		$data['passwd_default'] = $data['passwd'];
		$data['passwd'] = md5($data['passwd']);
		unset($data['passwd_ulang']);
		$this->m_dt_pns->update($id,$data);
		$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Kata sandi berhasil diubah!</div>');
		redirect(base_url().$this->access->controller.'/data_pokok/'.$data['id_pegawai']);
	}

	public function data_pokok_action($action)
	{
		$data = $_POST;
		if ($data != null) {
			$profil = $this->m_ap_profil->get_first();
			switch ($action) {
				case 'insert':
					if ($this->access->_create) {
						$data['tgl_lhr'] = $data['lhr_thn']."-".$data['lhr_bln']."-".$data["lhr_tgl"];
						unset($data['lhr_thn'],$data['lhr_bln'],$data["lhr_tgl"]);
						$data['id_status_pegawai'] = 2;
						$data['tmt_cpns'] = id_to_date($data['tmt_cpns']);
						$data['tmt_pns'] = id_to_date($data['tmt_pns']);
						$data['tgl_karpeg'] = id_to_date($data['tgl_karpeg']);
						$data['tgl_karpeg'] = id_to_date($data['tgl_karpeg']);
						$data['tmt_gol'] = id_to_date($data['tmt_gol']);
						$data['id_unit_kerja'] = $profil->id_unit_kerja;
						$data['created_by'] = $this->session->userdata('nama');
						if (!empty($_FILES["photo"]["name"])) {
							$data['photo'] = $this->uploadPhoto();
						} 
						$data['nama'] = strtoupper($data['nama']);
						$data['tmt_mutasi'] = id_to_date($data['tmt_mutasi']);
						unset($data['old_photo']);
						$data['passwd'] = md5($data['passwd_default']);
						$this->m_dt_pns->insert($data);
						// $this->m_ap_id->increment(2);

						insert_log('insert',$this->access->menu);
						$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
						redirect(base_url().$this->access->controller.'/data_pokok/'.$data['id_pegawai']);
					}else{
						redirect(base_url().'ap_error/error_403');
					}
					break;

				case 'update':
					if ($this->access->_update) {
						$id = $data['id_pegawai'];
						$data['tgl_lhr'] = $data['lhr_thn']."-".$data['lhr_bln']."-".$data["lhr_tgl"];
						unset($data['lhr_thn'],$data['lhr_bln'],$data["lhr_tgl"]);
						$data['nama'] = strtoupper($data['nama']);
						$data['tmt_cpns'] = id_to_date($data['tmt_cpns']);
						$data['tmt_pns'] = id_to_date($data['tmt_pns']);
						$data['tgl_karpeg'] = id_to_date($data['tgl_karpeg']);
						$data['tmt_gol'] = id_to_date($data['tmt_gol']);
						$data['id_unit_kerja'] = $profil->id_unit_kerja;
						$data['updated_by'] = $this->session->userdata('nama');
						$data['nama'] = strtoupper($data['nama']);
						$data['tmt_mutasi'] = id_to_date($data['tmt_mutasi']);
						$config['upload_path'] = './berkas/data_pokok';
						$config['allowed_types'] = 'jpg|jpeg|png|pdf';
						$config['max_size'] = '5000'; // max_size in kb
						$config['file_name'] = uniqid().'-'.$data['id_pegawai'].'-File-KARPEG';
						if (!empty($_FILES["photo"]["name"])) {
							$data['photo'] = $this->uploadPhoto();
						} else {
							$data['photo'] = $data["old_photo"];
						}
						unset($data['old_photo']);
						$this->load->library('upload', $config);
 					 // File upload
	 					if($this->upload->do_upload('file_karpeg')){
	 						 // Get data about the file
	 						 $uploadData = $this->upload->data();
	 						 $filename = $uploadData['file_name'];

	 						 // Initialize array
							 $idp=$data['id_pegawai'];
	 						 $data_file = array(
	 							 'file_karpeg' => $filename
	 						 );
	 						 $this->m_dt_pns->update($idp,$data_file);
	 					 }

						insert_log('update',$this->access->menu);
						$this->m_dt_pns->update($id,$data);
						$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
						redirect(base_url().$this->access->controller.'/data_pokok/'.$data['id_pegawai']);
					}else{
						redirect(base_url().'ap_error/error_403');
					}
					break;

				case 'delete':
					if ($this->access->_delete) {
						$this->m_dt_pns->delete_temp($data['id_pegawai']);
						insert_log('delete',$this->access->menu);
						$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
						redirect(base_url().$this->access->controller.'/data_pokok/'.$data['id_pegawai']);
					}else{
						redirect(base_url().'ap_error/error_403');
					}
					break;
			}
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function posisi_jabatan($id)
	{
		$data['access'] = $this->access;
		$data['golongan'] = $this->m_ms_golongan->get_all();
		$data['kecamatan'] = $this->m_ms_kecamatan->get_all();
		$data['unit_kerja'] = $this->m_ms_unit_kerja->get_all();
		$data['instansi'] = $this->m_ms_instansi->get_all();
		$data['rumpun_jabatan'] = $this->m_ms_rumpun_jabatan->get_all();
		$data['jabatan_list'] = $this->m_ms_jabatan->get_all();
		$data['tingkat_list'] = $this->m_ms_tingkat->get_all();
		$data['periode_penilaian'] = $this->m_ms_periode_penilaian->get_all();

		if ($this->access->_update) {
			$data['subtitle'] = 'Posisi dan Jabatan';
			$data['action'] = 'update';
			$data['pegawai'] = $this->m_dt_pns->get_by_id($id);
			$data['jabatan'] = $this->m_dt_pns->get_jabatan_by_id($id);
			$data['tingkat'] = $this->m_dt_pns->get_tingkat_by_id($id);
			$data['anak'] = $this->m_dt_anak->get_by_id_pegawai($id);
			$data['atasan'] = $this->m_dt_atasan->get_by_id($id);
			$data['keluarga'] = $this->m_dt_keluarga->get_by_id($id);
			$this->view('posisi_jabatan', $data);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function posisi_jabatan_action($action)
	{
		$data = $_POST;
		if ($data != null) {
			switch ($action) {
				case 'update':
					if($this->access->_update){
						if ($data['pak_berikutnya'] == '' || $data['pak_berikutnya'] == '00-00-0000') {
							$pembebasan = date('Y-m-d', strtotime("+60 months", strtotime(date_to_id($data['tmt_pangkat']))));
						}else{
							$pembebasan = '';
							$data['pak_berikutnya'] = date_to_id($data['pak_berikutnya']);
						}
						$pegawai = array(
							'id_jabatan' => $data['id_jabatan'],
							'tmt_jabatan' => date_to_id($data['tmt_jabatan']),
							'tmt_pangkat' => date_to_id($data['tmt_pangkat']),
							'usulan_pak_berikutnya' => date('Y-m-d', strtotime("+30 months", strtotime(date_to_id($data['tmt_pangkat'])))),
							'pak_berikutnya' => $data['pak_berikutnya'],
							'pembebasan' => $pembebasan,
							'pangkat_berikutnya' => date('Y-m-d', strtotime("+48 months", strtotime(date_to_id($data['tmt_pangkat'])))),
							'str' => $data['str'],
							'tgl_str' => date_to_id($data['tgl_str']),
							'tgl_str_berlaku' => date_to_id($data['tgl_str_berlaku']),
							'sip' => $data['sip'],
							'tgl_sip' => date_to_id($data['tgl_sip']),
							'tgl_sip_berlaku' => date_to_id($data['tgl_sip_berlaku'])
						);
						$this->m_dt_pns->update($data['id_pegawai'], $pegawai);
						$tingkat = array(
							'id_pegawai' => $data['id_pegawai'],
							'id_tingkat' => $data['id_tingkat']
						);
						if($this->m_dt_tingkat->get_by_id($data['id_pegawai'])){
							$this->m_dt_tingkat->update($data['id_pegawai'],$tingkat);
						}else{
							$this->m_dt_tingkat->insert($tingkat);
						};
						$atasan = array(
							'id_pegawai' => $data['id_pegawai'],
							'nip_atasan' => $data['nip_atasan'],
							'plh_plt_pp' => $data['plh_plt_pp'],
							'id_jab_plh_plt_pp' => $data['id_jab_plh_plt_pp'],
							'nip_app' => $data['nip_app'],
							'plh_plt_app' => $data['plh_plt_app'],
							'id_jab_plh_plt_app' => $data['id_jab_plh_plt_app'],
							'id_periode' => $data['id_periode']
						);
						if($this->m_dt_atasan->get_by_id($data['id_pegawai'])){
							$this->m_dt_atasan->update($data['id_pegawai'],$atasan);
						}else{
							$this->m_dt_atasan->insert($atasan);
						};

						$config['upload_path'] = './berkas/posisi_jabatan/pak';
						$config['allowed_types'] = 'jpg|jpeg|png|pdf';
						$config['max_size'] = '5000'; // max_size in kb
						$config['file_name'] = uniqid().'-'.$data['id_pegawai'].'-File-PAK';

						$this->load->library('upload', $config);
 					 // File upload
	 					if($this->upload->do_upload('file_pak')){
	 						 // Get data about the file
	 						 $uploadData = $this->upload->data();
	 						 $filename = $uploadData['file_name'];

	 						 // Initialize array
							 $idp=$data['id_pegawai'];
	 						 $data_file = array(
	 							 'file_pak' => $filename
	 						 );
	 						 $this->m_dt_pns->update($idp,$data_file);
	 					 }


						$configs['upload_path'] = './berkas/posisi_jabatan/str';
 						$configs['allowed_types'] = 'jpg|jpeg|png|pdf';
 						$configs['max_size'] = '5000'; // max_size in kb
 						$configs['file_name'] = uniqid().'-'.$data['id_pegawai'].'-File-STR';

						$this->upload->initialize($configs);
 						//$this->load->library('upload', $config);
  					 // File upload
 	 					if($this->upload->do_upload('file_str')){
 	 						 // Get data about the file
 	 						 $uploadData = $this->upload->data();
 	 						 $filename = $uploadData['file_name'];

 	 						 // Initialize array
 							 $idp=$data['id_pegawai'];
 	 						 $data_file = array(
 	 							 'file_str' => $filename
 	 						 );
 	 						 $this->m_dt_pns->update($idp,$data_file);
 	 					 }

						 	$configa['upload_path'] = './berkas/posisi_jabatan/sip';
  						$configa['allowed_types'] = 'jpg|jpeg|png|pdf';
  						$configa['max_size'] = '5000'; // max_size in kb
  						$configa['file_name'] = uniqid().'-'.$data['id_pegawai'].'-File-SIP';

							$this->upload->initialize($configa);
  						//$this->load->library('upload', $config);
   					 // File upload
  	 					if($this->upload->do_upload('file_sip')){
  	 						 // Get data about the file
  	 						 $uploadData = $this->upload->data();
  	 						 $filename = $uploadData['file_name'];

  	 						 // Initialize array
  							 $idp=$data['id_pegawai'];
  	 						 $data_file = array(
  	 							 'file_sip' => $filename
  	 						 );
  	 						 $this->m_dt_pns->update($idp,$data_file);
  	 					 }


							$configz['upload_path'] = './berkas/posisi_jabatan/rkk';
   						$configz['allowed_types'] = 'jpg|jpeg|png|pdf';
   						$configz['max_size'] = '5000'; // max_size in kb
   						$configz['file_name'] = uniqid().'-'.$data['id_pegawai'].'-File-RKK';

   						$this->upload->initialize($configz);
    					 // File upload
   	 					if($this->upload->do_upload('file_rkk')){
   	 						 // Get data about the file
   	 						 $uploadData = $this->upload->data();
   	 						 $filename = $uploadData['file_name'];

   	 						 // Initialize array
   							 $idp=$data['id_pegawai'];
   	 						 $data_file = array(
   	 							 'file_rkk' => $filename
   	 						 );
   	 						 $this->m_dt_pns->update($idp,$data_file);
   	 					 }

						insert_log('update',$this->access->menu);
						$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
						redirect(base_url().$this->access->controller.'/posisi_jabatan/'.$data['id_pegawai']);
					}else{
						redirect(base_url().'ap_error/error_403');
					}
					break;
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function keluarga($id)
	{
		$data['access'] = $this->access;
		$data['golongan'] = $this->m_ms_golongan->get_all();
		$data['kecamatan'] = $this->m_ms_kecamatan->get_all();
		$data['unit_kerja'] = $this->m_ms_unit_kerja->get_all();
		$data['instansi'] = $this->m_ms_instansi->get_all();
		$data['rumpun_jabatan'] = $this->m_ms_rumpun_jabatan->get_all();
		$data['jabatan_list'] = $this->m_ms_jabatan->get_all();
		$data['tingkat_list'] = $this->m_ms_tingkat->get_all();
		$data['periode_penilaian'] = $this->m_ms_periode_penilaian->get_all();

		if ($this->access->_update) {
			$data['subtitle'] = 'Keluarga';
			$data['action'] = 'update';
			$data['pegawai'] = $this->m_dt_pns->get_by_id($id);
			$data['jabatan'] = $this->m_dt_pns->get_jabatan_by_id($id);
			$data['tingkat'] = $this->m_dt_pns->get_tingkat_by_id($id);
			$data['anak'] = $this->m_dt_anak->get_by_id_pegawai($id);
			$data['atasan'] = $this->m_dt_atasan->get_by_id($id);
			$data['keluarga'] = $this->m_dt_keluarga->get_by_id_pegawai($id);
			// var_dump($data['keluarga']);exit();
			$this->view('keluarga', $data);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function keluarga_action($action)
	{
		$data = $_POST;
		if ($data != null) {
			switch ($action) {
				case 'update':
					if($this->access->_update){
						$pegawai = array(
							'status_perkawinan' => $data['status_perkawinan']
						);
						$this->m_dt_pns->update($data['id_pegawai'],$pegawai);
						$keluarga = array(
							'id_pegawai' => $data['id_pegawai'],
							'nama_pasangan' => $data['nama_pasangan'],
							'nip_pasangan' => $data['nip_pasangan'],
							'pekerjaan_pasangan' => $data['pekerjaan_pasangan'],
							'seri_karis' => $data['seri_karis'],
							'tgl_lahir_pasangan' => date_to_id($data['tgl_lahir_pasangan']),
							'tgl_perkawinan' => date_to_id($data['tgl_perkawinan']),
							'tgl_karis' => date_to_id($data['tgl_karis']),
							'jml_anak' => $data['jml_anak']
						);
						if($this->m_dt_keluarga->get_by_id($data['id_pegawai'])){
							$this->m_dt_keluarga->update($data['id_pegawai'],$keluarga);
						}else{
							$this->m_dt_keluarga->insert($keluarga);
						};
						foreach ($data['id'] as $key => $val) {
							if($data['nama'][$key] != '' || $data['nama'][$key] != null){
								$anak = array(
									'id_pegawai' => $data['id_pegawai'],
									'nama' => $data['nama'][$key],
									'tgl_lahir' => date_to_id($data['tgl_lahir'][$key])
								);
								echo $val;
								if($val == '' || $val == null){
									//insert
									$this->m_dt_anak->insert($anak);
								}else{
									//update
									$this->m_dt_anak->update($val,$anak);
								}
							}else{
								$this->m_dt_anak->delete_permanent($val);
							}
						}

						$config['upload_path'] = './berkas/keluarga/ktp';
						$config['allowed_types'] = 'jpg|jpeg|png|pdf';
						$config['max_size'] = '5000'; // max_size in kb
						$config['file_name'] = uniqid().'-'.$data['id_pegawai'].'-File-KTP';

						$this->load->library('upload', $config);
 					 // File upload
	 					if($this->upload->do_upload('file_ktp')){
	 						 // Get data about the file
	 						 $uploadData = $this->upload->data();
	 						 $filename = $uploadData['file_name'];

	 						 // Initialize array
							 $idp=$data['id_pegawai'];
	 						 $data_file = array(
	 							 'file_ktp' => $filename
	 						 );
	 						 $this->m_dt_keluarga->update($idp,$data_file);
	 					 }


						$configs['upload_path'] = './berkas/keluarga/kk';
 						$configs['allowed_types'] = 'jpg|jpeg|png|pdf';
 						$configs['max_size'] = '5000'; // max_size in kb
 						$configs['file_name'] = uniqid().'-'.$data['id_pegawai'].'-File-KK';

						$this->upload->initialize($configs);
 						//$this->load->library('upload', $config);
  					 // File upload
 	 					if($this->upload->do_upload('file_kk')){
 	 						 // Get data about the file
 	 						 $uploadData = $this->upload->data();
 	 						 $filename = $uploadData['file_name'];

 	 						 // Initialize array
 							 $idp=$data['id_pegawai'];
 	 						 $data_file = array(
 	 							 'file_kk' => $filename
 	 						 );
 	 						 $this->m_dt_keluarga->update($idp,$data_file);
 	 					 }

						 	$configa['upload_path'] = './berkas/keluarga/karsu';
  						$configa['allowed_types'] = 'jpg|jpeg|png|pdf';
  						$configa['max_size'] = '5000'; // max_size in kb
  						$configa['file_name'] = uniqid().'-'.$data['id_pegawai'].'-File-KARSU';

							$this->upload->initialize($configa);
  						//$this->load->library('upload', $config);
   					 // File upload
  	 					if($this->upload->do_upload('file_karsu')){
  	 						 // Get data about the file
  	 						 $uploadData = $this->upload->data();
  	 						 $filename = $uploadData['file_name'];

  	 						 // Initialize array
  							 $idp=$data['id_pegawai'];
  	 						 $data_file = array(
  	 							 'file_karsu' => $filename
  	 						 );
  	 						 $this->m_dt_keluarga->update($idp,$data_file);
  	 					 }

						insert_log('update',$this->access->menu);
						$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
						redirect(base_url().$this->access->controller.'/keluarga/'.$data['id_pegawai']);
					}else{
						redirect(base_url().'ap_error/error_403');
					}
					break;
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function riwayat_pangkat($id_pegawai,$id = null)
	{
		$data['access'] = $this->access;
		$data['golongan'] = $this->m_ms_golongan->get_all();
		$data['riwayat'] = $this->m_dt_riwayat_pangkat->get_by_id_pegawai($id_pegawai);
		$data['pegawai'] = $this->m_dt_pns->get_by_id($id_pegawai);

		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Riwayat Pangkat';
				$data['action'] = 'insert';
				$data['pangkat'] = null;
				$this->view('riwayat_pangkat', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Riwayat Pangkat';
				$data['action'] = 'update';
				$data['pangkat'] = $this->m_dt_riwayat_pangkat->get_by_id($id);
				$this->view('riwayat_pangkat', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}

	public function riwayat_pangkat_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$data['tmt_riwayat_gol'] = date_to_id($data['tmt_riwayat_gol']);
				$data['tgl_sk'] = date_to_id($data['tgl_sk']);

				if($this->input->post('upload') != NULL ){
					//insert into riwayat
					unset($data['upload']);
					$this->m_dt_riwayat_pangkat->insert($data);
					$id = $this->db->insert_id();
					$files = array();

					// Count total files
					if (isset($_FILES['files'])) {
						$countfiles = count($_FILES['files']['name']);
					}else{
						$countfiles = 0;
					}

					// Loog all files
					for($i=0;$i<$countfiles;$i++){

						if(!empty($_FILES['files']['name'][$i])){

							// Define new $_FILES array - $_FILES['file']
							$_FILES['file']['name'] = $_FILES['files']['name'][$i];
							$_FILES['file']['type'] = $_FILES['files']['type'][$i];
							$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
							$_FILES['file']['error'] = $_FILES['files']['error'][$i];
							$_FILES['file']['size'] = $_FILES['files']['size'][$i];

							// Set preference
							$config['upload_path'] = './berkas/riwayat_pangkat';
							$config['allowed_types'] = 'jpg|jpeg|png|pdf';
							$config['max_size'] = '5000'; // max_size in kb
							$config['file_name'] = uniqid().'-'.$_FILES['files']['name'][$i];

							//Load upload library
							$this->load->library('upload',$config);

							// File upload
							if($this->upload->do_upload('file')){
								// Get data about the file
								$uploadData = $this->upload->data();
								$filename = $uploadData['file_name'];

								// Initialize array
								$files['filenames'][] = $filename;
								$data_file = array(
									'id_pegawai' => $data['id_pegawai'],
									'id_riwayat' => $id,
									'file' => $filename
								);
								$this->m_dt_file_riwayat_pangkat->insert($data_file);
							}
						}
					}
				}
				break;

			case 'update':
				$data['tmt_riwayat_gol'] = date_to_id($data['tmt_riwayat_gol']);
				$data['tgl_sk'] = date_to_id($data['tgl_sk']);

				if($this->input->post('upload') != NULL ){
					//insert into riwayat
					unset($data['upload']);
					$this->m_dt_riwayat_pangkat->update($data['id'],$data);
					$id = $data['id'];
					$files = array();

					// Count total files
					if (isset($_FILES['files'])) {
						$countfiles = count($_FILES['files']['name']);
					}else{
						$countfiles = 0;
					}

					// Looping all files
					for($i=0;$i<$countfiles;$i++){

						if(!empty($_FILES['files']['name'][$i])){

							// Define new $_FILES array - $_FILES['file']
							$_FILES['file']['name'] = $_FILES['files']['name'][$i];
							$_FILES['file']['type'] = $_FILES['files']['type'][$i];
							$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
							$_FILES['file']['error'] = $_FILES['files']['error'][$i];
							$_FILES['file']['size'] = $_FILES['files']['size'][$i];

							// Set preference
							$config['upload_path'] = './berkas/riwayat_pangkat';
							$config['allowed_types'] = 'jpg|jpeg|png|pdf';
							$config['max_size'] = '5000'; // max_size in kb
							$config['file_name'] = uniqid().'-'.$_FILES['files']['name'][$i];
							var_dump($config);

							//Load upload library
							$this->load->library('upload',$config);

							// File upload
							if($this->upload->do_upload('file')){
								// Get data about the file
								$uploadData = $this->upload->data();
								$filename = $uploadData['file_name'];

								// Initialize array
								$files['filenames'][] = $filename;
								$data_file = array(
									'id_pegawai' => $data['id_pegawai'],
									'id_riwayat' => $id,
									'file' => $filename
								);
								$this->m_dt_file_riwayat_pangkat->insert($data_file);
							}
						}
					}
				}
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_riwayat_pangkat->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		// insert_log('update',$this->access->menu);
		redirect(base_url().$this->access->controller.'/riwayat_pangkat/'.$data['id_pegawai']);
	}

	public function file_riwayat_pangkat_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'delete':
				$this->m_dt_file_riwayat_pangkat->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().$this->access->controller.'/riwayat_pangkat/'.$data['id_pegawai'].'/'.$data['id_riwayat']);
	}

	public function riwayat_pendidikan($id_pegawai,$id = null)
	{
		$data['access'] = $this->access;
		$data['golongan'] = $this->m_ms_golongan->get_all();
		$data['riwayat'] = $this->m_dt_riwayat_pendidikan->get_by_id_pegawai($id_pegawai);
		$data['pegawai'] = $this->m_dt_pns->get_by_id($id_pegawai);

		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Riwayat Pendidikan';
				$data['action'] = 'insert';
				$data['pendidikan'] = null;
				$this->view('riwayat_pendidikan', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Riwayat Pendidikan';
				$data['action'] = 'update';
				$data['pendidikan'] = $this->m_dt_riwayat_pendidikan->get_by_id($id);
				$this->view('riwayat_pendidikan', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}

	public function riwayat_pendidikan_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				if($this->input->post('upload') != NULL ){
					$data['tgl_lulus'] = date_to_id($data['tgl_lulus']);
					//insert into riwayat
					unset($data['upload']);
					unset($data['files']);
					$this->m_dt_riwayat_pendidikan->insert($data);
					$id = $this->db->insert_id();
					$files = array();

					// Count total files
					if (isset($_FILES['files'])) {
						$countfiles = count($_FILES['files']['name']);
					}else{
						$countfiles = 0;
					}

					echo $countfiles;

					// Looping all files
					for($i=0;$i<$countfiles;$i++){

						if(!empty($_FILES['files']['name'][$i])){

							// Define new $_FILES array - $_FILES['file']
							$_FILES['file']['name'] = $_FILES['files']['name'][$i];
							$_FILES['file']['type'] = $_FILES['files']['type'][$i];
							$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
							$_FILES['file']['error'] = $_FILES['files']['error'][$i];
							$_FILES['file']['size'] = $_FILES['files']['size'][$i];

							// Set preference
							$config['upload_path'] = './berkas/riwayat_pendidikan';
							$config['allowed_types'] = 'jpg|jpeg|png|pdf';
							$config['max_size'] = '5000'; // max_size in kb
							$config['file_name'] = uniqid().'-'.$_FILES['files']['name'][$i];

							//Load upload library
							$this->load->library('upload',$config);

							// File upload
							if($this->upload->do_upload('file')){
								// Get data about the file
								$uploadData = $this->upload->data();
								$filename = $uploadData['file_name'];

								// Initialize array
								$files['filenames'][] = $filename;
								$data_file = array(
									'id_pegawai' => $data['id_pegawai'],
									'id_riwayat' => $id,
									'file' => $filename
								);
								$this->m_dt_file_riwayat_pendidikan->insert($data_file);
							}
							var_dump($files);
						}
						echo 'kosong';
					}
				}
				break;

			case 'update':
				if($this->input->post('upload') != NULL ){
					$data['tgl_lulus'] = date_to_id($data['tgl_lulus']);
					//insert into riwayat
					unset($data['upload']);
					unset($data['files']);
					$this->m_dt_riwayat_pendidikan->update($data['id'],$data);
					$id = $data['id'];
					$files = array();

					// Count total files
					if (isset($_FILES['files'])) {
						$countfiles = count($_FILES['files']['name']);
					}else{
						$countfiles = 0;
					}

					// Looping all files
					for($i=0;$i<$countfiles;$i++){

						if(!empty($_FILES['files']['name'][$i])){

							// Define new $_FILES array - $_FILES['file']
							$_FILES['file']['name'] = $_FILES['files']['name'][$i];
							$_FILES['file']['type'] = $_FILES['files']['type'][$i];
							$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
							$_FILES['file']['error'] = $_FILES['files']['error'][$i];
							$_FILES['file']['size'] = $_FILES['files']['size'][$i];

							// Set preference
							$config['upload_path'] = './berkas/riwayat_pendidikan';
							$config['allowed_types'] = 'jpg|jpeg|png|pdf';
							$config['max_size'] = '5000'; // max_size in kb
							$config['file_name'] = uniqid().'-'.$_FILES['files']['name'][$i];

							//Load upload library
							$this->load->library('upload',$config);

							// File upload
							if($this->upload->do_upload('file')){
								// Get data about the file
								$uploadData = $this->upload->data();
								$filename = $uploadData['file_name'];

								// Initialize array
								$files['filenames'][] = $filename;
								$data_file = array(
									'id_pegawai' => $data['id_pegawai'],
									'id_riwayat' => $id,
									'file' => $filename
								);
								$this->m_dt_file_riwayat_pendidikan->insert($data_file);
							}
						}
					}
				}
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_riwayat_pendidikan->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().$this->access->controller.'/riwayat_pendidikan/'.$data['id_pegawai']);
	}

	public function file_riwayat_pendidikan_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'delete':
				$this->m_dt_file_riwayat_pendidikan->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().$this->access->controller.'/riwayat_pendidikan/'.$data['id_pegawai'].'/'.$data['id_riwayat']);
	}

	public function riwayat_jabatan($id_pegawai,$id = null)
	{
		$data['access'] = $this->access;
		$data['riwayat'] = $this->m_dt_riwayat_jabatan->get_by_id_pegawai($id_pegawai);
		$data['pegawai'] = $this->m_dt_pns->get_by_id($id_pegawai);

		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Riwayat Jabatan';
				$data['action'] = 'insert';
				$data['jabatan'] = null;
				$this->view('riwayat_jabatan', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Riwayat Jabatan';
				$data['action'] = 'update';
				$data['jabatan'] = $this->m_dt_riwayat_jabatan->get_by_id($id);
				$this->view('riwayat_jabatan', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}

	public function riwayat_jabatan_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$data['tmt_jabatan'] = date_to_id($data['tmt_jabatan']);
				$data['tgl_sk'] = date_to_id($data['tgl_sk']);

				if($this->input->post('upload') != NULL ){
					//insert into riwayat
					unset($data['upload']);
					$this->m_dt_riwayat_jabatan->insert($data);
					$id = $this->db->insert_id();
					$files = array();

					// Count total files
					if (isset($_FILES['files'])) {
						$countfiles = count($_FILES['files']['name']);
					}else{
						$countfiles = 0;
					}

					// Looping all files
					for($i=0;$i<$countfiles;$i++){

						if(!empty($_FILES['files']['name'][$i])){

							// Define new $_FILES array - $_FILES['file']
							$_FILES['file']['name'] = $_FILES['files']['name'][$i];
							$_FILES['file']['type'] = $_FILES['files']['type'][$i];
							$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
							$_FILES['file']['error'] = $_FILES['files']['error'][$i];
							$_FILES['file']['size'] = $_FILES['files']['size'][$i];

							// Set preference
							$config['upload_path'] = './berkas/riwayat_jabatan';
							$config['allowed_types'] = 'jpg|jpeg|png|pdf';
							$config['max_size'] = '5000'; // max_size in kb
							$config['file_name'] = uniqid().'-'.$_FILES['files']['name'][$i];

							//Load upload library
							$this->load->library('upload',$config);

							// File upload
							if($this->upload->do_upload('file')){
								// Get data about the file
								$uploadData = $this->upload->data();
								$filename = $uploadData['file_name'];

								// Initialize array
								$files['filenames'][] = $filename;
								$data_file = array(
									'id_pegawai' => $data['id_pegawai'],
									'id_riwayat' => $id,
									'file' => $filename
								);
								$this->m_dt_file_riwayat_jabatan->insert($data_file);
							}
						}
					}
				}
				break;

			case 'update':
				$data['tmt_jabatan'] = date_to_id($data['tmt_jabatan']);
				$data['tgl_sk'] = date_to_id($data['tgl_sk']);

				if($this->input->post('upload') != NULL ){
					//insert into riwayat
					unset($data['upload']);
					$this->m_dt_riwayat_jabatan->update($data['id'],$data);
					$id = $data['id'];
					$files = array();

					// Count total files
					if (isset($_FILES['files'])) {
						$countfiles = count($_FILES['files']['name']);
					}else{
						$countfiles = 0;
					}

					// Looping all files
					for($i=0;$i<$countfiles;$i++){

						if(!empty($_FILES['files']['name'][$i])){

							// Define new $_FILES array - $_FILES['file']
							$_FILES['file']['name'] = $_FILES['files']['name'][$i];
							$_FILES['file']['type'] = $_FILES['files']['type'][$i];
							$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
							$_FILES['file']['error'] = $_FILES['files']['error'][$i];
							$_FILES['file']['size'] = $_FILES['files']['size'][$i];

							// Set preference
							$config['upload_path'] = './berkas/riwayat_jabatan';
							$config['allowed_types'] = 'jpg|jpeg|png|pdf';
							$config['max_size'] = '5000'; // max_size in kb
							$config['file_name'] = uniqid().'-'.$_FILES['files']['name'][$i];
							var_dump($config);

							//Load upload library
							$this->load->library('upload',$config);

							// File upload
							if($this->upload->do_upload('file')){
								// Get data about the file
								$uploadData = $this->upload->data();
								$filename = $uploadData['file_name'];

								// Initialize array
								$files['filenames'][] = $filename;
								$data_file = array(
									'id_pegawai' => $data['id_pegawai'],
									'id_riwayat' => $id,
									'file' => $filename
								);
								$this->m_dt_file_riwayat_jabatan->insert($data_file);
							}
						}
					}
				}
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_riwayat_jabatan->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		// insert_log('update',$this->access->menu);
		redirect(base_url().$this->access->controller.'/riwayat_jabatan/'.$data['id_pegawai']);
	}

	public function file_riwayat_jabatan_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'delete':
				$this->m_dt_file_riwayat_jabatan->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().$this->access->controller.'/riwayat_jabatan/'.$data['id_pegawai'].'/'.$data['id_riwayat']);
	}

	public function riwayat_diklat($id_pegawai,$jenis=null,$id = null)
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Riwayat Diklat';
		$data['pegawai'] = $this->m_dt_pns->get_by_id($id_pegawai);
		$data['jenis_diklat'] = $this->m_ms_jenis_diklat->get_all();
		$data['riwayat_diklat'] = $this->m_dt_riwayat_diklat->get_by_id_pegawai($id_pegawai);
		$data['riwayat_diklat_baru'] = $this->m_dt_riwayat_diklat_baru->get_by_id_pegawai($id_pegawai);
		$data['riwayat_usulan_diklat'] = $this->m_dt_riwayat_usulan_diklat->get_by_id_pegawai($id_pegawai);

		//action list
		$data['action_diklat'] = 'insert';
		$data['action_diklat_baru'] = 'insert';
		$data['action_usulan_diklat'] = 'insert';

		//list row edit
		$data['diklat'] = null;
		$data['diklat_baru'] = null;
		$data['usulan_diklat'] = null;

		switch ($jenis) {
			case 1:
				$data['action_diklat'] = 'update';
				$data['diklat'] = $this->m_dt_riwayat_diklat->get_by_id($id);
				break;
			case 2:
				$data['action_usulan_diklat'] = 'update';
				$data['usulan_diklat'] = $this->m_dt_riwayat_usulan_diklat->get_by_id($id);
				break;
			case 3:
				$data['action_diklat_baru'] = 'update';
				$data['diklat_baru'] = $this->m_dt_riwayat_diklat_baru->get_by_id($id);
				break;
		}

		$this->view('riwayat_diklat', $data);
	}

	public function riwayat_diklat_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':

				$this->m_dt_riwayat_diklat->insert($data);
				$insert_id = $this->db->insert_id();

				$config['upload_path'] = './berkas/riwayat_diklat/riwayat';
				$config['allowed_types'] = 'jpg|jpeg|png|pdf';
				$config['max_size'] = '5000'; // max_size in kb
				$config['file_name'] = uniqid().'-'.$data['id_pegawai'].'-File-Sertifikat';

				$this->load->library('upload', $config);
			 // File upload
				if($this->upload->do_upload('file_sertifikat')){
					 // Get data about the file
					 $uploadData = $this->upload->data();
					 $filename = $uploadData['file_name'];

					 // Initialize array
					 $idp=$insert_id;
					 $data_file = array(
						 'file_sertifikat' => $filename
					 );
					 $this->m_dt_riwayat_diklat->update($idp,$data_file);
				 }

				$this->session->set_flashdata('status_diklat', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_riwayat_diklat->update($data['id'],$data);
				$config['upload_path'] = './berkas/riwayat_diklat/riwayat';
				$config['allowed_types'] = 'jpg|jpeg|png|pdf';
				$config['max_size'] = '5000'; // max_size in kb
				$config['file_name'] = uniqid().'-'.$data['id_pegawai'].'-File-Sertifikat';

				$this->load->library('upload', $config);
			 // File upload
				if($this->upload->do_upload('file_sertifikat')){
					 // Get data about the file
					 $uploadData = $this->upload->data();
					 $filename = $uploadData['file_name'];

					 // Initialize array
					 $idp=$data['id'];
					 $data_file = array(
						 'file_sertifikat' => $filename
					 );
					 $this->m_dt_riwayat_diklat->update($idp,$data_file);
				 }
				$this->session->set_flashdata('status_diklat', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_riwayat_diklat->delete_permanent($data['id']);
				$this->session->set_flashdata('status_diklat', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().$this->access->controller.'/riwayat_diklat/'.$data['id_pegawai']);
	}

	public function riwayat_usulan_diklat_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_riwayat_usulan_diklat->insert($data);
				$insert_id = $this->db->insert_id();

				$config['upload_path'] = './berkas/riwayat_diklat/usulan_baru';
				$config['allowed_types'] = 'jpg|jpeg|png|pdf';
				$config['max_size'] = '5000'; // max_size in kb
				$config['file_name'] = uniqid().'-'.$data['id_pegawai'].'-File-Sertifikat';

				$this->load->library('upload', $config);
			 // File upload
				if($this->upload->do_upload('file_sertifikat')){
					 // Get data about the file
					 $uploadData = $this->upload->data();
					 $filename = $uploadData['file_name'];

					 // Initialize array
					 $idp=$insert_id;
					 $data_file = array(
						 'file_sertifikat' => $filename
					 );
					 $this->m_dt_riwayat_usulan_diklat->update($idp,$data_file);
				 }
				$this->session->set_flashdata('status_usulan', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_riwayat_usulan_diklat->update($data['id'],$data);
				$config['upload_path'] = './berkas/riwayat_diklat/usulan_baru';
				$config['allowed_types'] = 'jpg|jpeg|png|pdf';
				$config['max_size'] = '5000'; // max_size in kb
				$config['file_name'] = uniqid().'-'.$data['id_pegawai'].'-File-Sertifikat';

				$this->load->library('upload', $config);
			 // File upload
				if($this->upload->do_upload('file_sertifikat')){
					 // Get data about the file
					 $uploadData = $this->upload->data();
					 $filename = $uploadData['file_name'];

					 // Initialize array
					 $idp=$data['id'];
					 $data_file = array(
						 'file_sertifikat' => $filename
					 );
					 $this->m_dt_riwayat_usulan_diklat->update($idp,$data_file);
				 }
				$this->session->set_flashdata('status_usulan', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_riwayat_usulan_diklat->delete_permanent($data['id']);
				$this->session->set_flashdata('status_usulan', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().$this->access->controller.'/riwayat_diklat/'.$data['id_pegawai']);
	}

	public function riwayat_diklat_baru_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_riwayat_diklat_baru->insert($data);
				$insert_id = $this->db->insert_id();

				$config['upload_path'] = './berkas/riwayat_diklat/usulan_belum';
				$config['allowed_types'] = 'jpg|jpeg|png|pdf';
				$config['max_size'] = '5000'; // max_size in kb
				$config['file_name'] = uniqid().'-'.$data['id_pegawai'].'-File-Sertifikat';

				$this->load->library('upload', $config);
			 // File upload
				if($this->upload->do_upload('file_sertifikat')){
					 // Get data about the file
					 $uploadData = $this->upload->data();
					 $filename = $uploadData['file_name'];

					 // Initialize array
					 $idp=$insert_id;
					 $data_file = array(
						 'file_sertifikat' => $filename
					 );
					 $this->m_dt_riwayat_diklat_baru->update($idp,$data_file);
				 }
				$this->session->set_flashdata('status_baru', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_riwayat_diklat_baru->update($data['id'],$data);
				$config['upload_path'] = './berkas/riwayat_diklat/usulan_belum';
				$config['allowed_types'] = 'jpg|jpeg|png|pdf';
				$config['max_size'] = '5000'; // max_size in kb
				$config['file_name'] = uniqid().'-'.$data['id_pegawai'].'-File-Sertifikat';

				$this->load->library('upload', $config);
			 // File upload
				if($this->upload->do_upload('file_sertifikat')){
					 // Get data about the file
					 $uploadData = $this->upload->data();
					 $filename = $uploadData['file_name'];

					 // Initialize array
					 $idp=$data['id'];
					 $data_file = array(
						 'file_sertifikat' => $filename
					 );
					 $this->m_dt_riwayat_diklat_baru->update($idp,$data_file);
				 }
				$this->session->set_flashdata('status_baru', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_riwayat_diklat_baru->delete_permanent($data['id']);
				$this->session->set_flashdata('status_baru', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().$this->access->controller.'/riwayat_diklat/'.$data['id_pegawai']);
	}

	public function riwayat_penghargaan($id_pegawai,$id = null)
	{
		$data['access'] = $this->access;
		$data['riwayat'] = $this->m_dt_riwayat_penghargaan->get_by_id_pegawai($id_pegawai);
		$data['pegawai'] = $this->m_dt_pns->get_by_id($id_pegawai);

		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Riwayat Jabatan';
				$data['action'] = 'insert';
				$data['penghargaan'] = null;
				$this->view('riwayat_penghargaan', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Riwayat Jabatan';
				$data['action'] = 'update';
				$data['penghargaan'] = $this->m_dt_riwayat_penghargaan->get_by_id($id_pegawai);
				$this->view('riwayat_penghargaan', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}

	public function riwayat_penghargaan_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_riwayat_penghargaan->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_riwayat_penghargaan->update($data['id'],$data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_riwayat_penghargaan->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().$this->access->controller.'/riwayat_penghargaan/'.$data['id_pegawai']);
	}

	public function riwayat_hukuman($id_pegawai,$id = null)
	{
		$data['access'] = $this->access;
		$data['riwayat'] = $this->m_dt_riwayat_hukuman->get_by_id_pegawai($id_pegawai);
		$data['pegawai'] = $this->m_dt_pns->get_by_id($id_pegawai);
		$data['hukuman_disiplin'] = $this->m_ms_hukuman_disiplin->get_all();
		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Riwayat Jabatan';
				$data['action'] = 'insert';
				$data['hukuman'] = null;
				$this->view('riwayat_hukuman', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Riwayat Jabatan';
				$data['action'] = 'update';
				$data['hukuman'] = $this->m_dt_riwayat_hukuman->get_by_id($id_pegawai);
				$this->view('riwayat_hukuman', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}

	public function riwayat_hukuman_action($action)
	{
		$data = $_POST;
		$data['tmt_hukuman'] = date_to_id($data['tmt_hukuman']);
		switch ($action) {
			case 'insert':
				$this->m_dt_riwayat_hukuman->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_riwayat_hukuman->update($data['id'],$data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_riwayat_hukuman->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().$this->access->controller.'/riwayat_hukuman/'.$data['id_pegawai']);
	}

	public function riwayat_ppk($id_pegawai,$id = null)
	{
		$data['access'] = $this->access;
		$data['riwayat'] = $this->m_dt_riwayat_ppk->get_by_id_pegawai($id_pegawai);
		$data['pegawai'] = $this->m_dt_pns->get_by_id($id_pegawai);

		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Riwayat PPK';
				$data['action'] = 'insert';
				$data['ppk'] = null;
				$this->view('riwayat_ppk', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Riwayat PPK';
				$data['action'] = 'update';
				$data['ppk'] = $this->m_dt_riwayat_ppk->get_by_id($id);
				$this->view('riwayat_ppk', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}

	public function riwayat_ppk_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_riwayat_ppk->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_riwayat_ppk->update($data['id'],$data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_riwayat_ppk->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().$this->access->controller.'/riwayat_ppk/'.$data['id_pegawai']);
	}

	public function skp($id_pegawai,$id = null)
	{
		$data['access'] = $this->access;
		$data['riwayat'] = $this->m_dt_tgs_pokok->get_by_id_pegawai($id_pegawai);
		$data['pegawai'] = $this->m_dt_pns->get_by_id($id_pegawai);

		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Sasaran Kerja Pegawai';
				$data['action'] = 'insert';
				$data['skp'] = null;
				$this->view('skp', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Sasaran Kerja Pegawai';
				$data['action'] = 'update';
				$data['skp'] = $this->m_dt_tgs_pokok->get_by_id($id);
				$this->view('skp', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}

	public function skp_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_tgs_pokok->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_tgs_pokok->update($data['id'],$data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_tgs_pokok->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().$this->access->controller.'/skp/'.$data['id_pegawai']);
	}

	public function realisasi($id_pegawai,$tipe = null,$id = null)
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Sasaran Kerja Pegawai';
		$data['riwayat_tgs_pokok'] = $this->m_dt_tgs_pokok->get_by_id_pegawai($id_pegawai);
		$data['riwayat_tgs_tambahan'] = $this->m_dt_tgs_tambahan->get_by_id_pegawai($id_pegawai);
		$data['riwayat_tgs_kreatifitas'] = $this->m_dt_tgs_kreatifitas->get_by_id_pegawai($id_pegawai);
		$data['pegawai'] = $this->m_dt_pns->get_by_id($id_pegawai);

		//action list
		$data['action_tgs_pokok'] = '';
		$data['action_tgs_tambahan'] = 'insert';
		$data['action_tgs_kreatifitas'] = 'insert';

		// list row edit
		$data['tgs_pokok'] = null;
		$data['tgs_tambahan'] = null;
		$data['tgs_kreatifitas'] = null;

		switch ($tipe) {
			case 1:
				$data['action_tgs_pokok'] = 'update';
				$data['tgs_pokok'] = $this->m_dt_tgs_pokok->get_by_id($id);
				break;
			case 2:
				$data['action_tgs_tambahan'] = 'update';
				$data['tgs_tambahan'] = $this->m_dt_tgs_tambahan->get_by_id($id);
				break;
			case 3:
				$data['action_tgs_kreatifitas'] = 'update';
				$data['tgs_kreatifitas'] = $this->m_dt_tgs_kreatifitas->get_by_id($id);
				break;
		}

		$this->view('realisasi', $data);
	}

	public function realisasi_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_tgs_pokok->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_tgs_pokok->update($data['id'],$data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_tgs_pokok->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().$this->access->controller.'/realisasi/'.$data['id_pegawai']);
	}

	public function tgs_tambahan_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_tgs_tambahan->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_tgs_tambahan->update($data['id'],$data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_tgs_tambahan->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().$this->access->controller.'/realisasi/'.$data['id_pegawai']);
	}

	public function tgs_kreatifitas_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_tgs_kreatifitas->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_tgs_kreatifitas->update($data['id'],$data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_tgs_kreatifitas->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().$this->access->controller.'/realisasi/'.$data['id_pegawai']);
	}

	public function perilaku_kerja($id_pegawai,$tipe = null,$id = null)
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Perilaku';
		$data['riwayat_perilaku_kerja'] = $this->m_dt_perilaku_kerja->get_by_id_pegawai($id_pegawai);
		$data['riwayat_proses_penilaian'] = $this->m_dt_proses_penilaian->get_by_id_pegawai($id_pegawai);
		$data['pegawai'] = $this->m_dt_pns->get_by_id($id_pegawai);

		//action list
		$data['action_perilaku_kerja'] = 'insert';
		$data['action_proses_penilaian'] = 'update';

		//list row edit
		$data['perilaku_kerja'] = null;
		$data['proses_penilaian'] = null;

		switch ($tipe) {
			case 1:
				$data['action_perilaku_kerja'] = 'update';
				$data['perilaku_kerja'] = $this->m_dt_perilaku_kerja->get_by_id($id);
				break;
			case 2:
				$data['action_proses_penilaian'] = 'update';
				$data['proses_penilaian'] = $this->m_dt_proses_penilaian->get_by_id($id);
				break;
		}

		$this->view('perilaku_kerja', $data);
	}

	public function perilaku_kerja_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_perilaku_kerja->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_perilaku_kerja->update($data['id'],$data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_perilaku_kerja->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().$this->access->controller.'/perilaku_kerja/'.$data['id_pegawai']);
	}

	public function proses_penilaian_action($action)
	{
		$data = $_POST;
		$data['tgl_keberatan'] = date_to_id($data['tgl_keberatan']);
		$data['tgl_tanggapan'] = date_to_id($data['tgl_tanggapan']);
		$data['tgl_kep_penilaian'] = date_to_id($data['tgl_kep_penilaian']);
		$data['tgl_rekomendasi'] = date_to_id($data['tgl_rekomendasi']);
		switch ($action) {
			case 'insert':
				$this->m_dt_proses_penilaian->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$exist = $this->m_dt_proses_penilaian->get_by_id_pegawai($data['id_pegawai']);
				if ($exist != null) {
					$this->m_dt_proses_penilaian->update($data['id_pegawai'],$data);
				}else{
					$this->m_dt_proses_penilaian->insert($data);
				}
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_proses_penilaian->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().$this->access->controller.'/perilaku_kerja/'.$data['id_pegawai']);
	}

	public function tugas_belajar($id_pegawai,$tipe = null,$id = null)
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Perilaku';
		$data['riwayat_perilaku_kerja_tubel'] = $this->m_dt_perilaku_kerja_tubel->get_by_id_pegawai($id_pegawai);
		$data['riwayat_proses_penilaian'] = $this->m_dt_proses_penilaian->get_by_id_pegawai($id_pegawai);
		$data['pegawai'] = $this->m_dt_pns->get_by_id($id_pegawai);

		//action list
		$data['action_perilaku_kerja_tubel'] = 'update';
		$data['action_proses_penilaian'] = 'update';

		//list row edit
		$data['perilaku_kerja_tubel'] = null;
		$data['proses_penilaian'] = null;

		switch ($tipe) {
			case 1:
				$data['action_perilaku_kerja_tubel'] = 'update';
				$data['perilaku_kerja_tubel'] = $this->m_dt_perilaku_kerja_tubel->get_by_id($id);
				break;
			case 2:
				$data['action_proses_penilaian'] = 'update';
				$data['proses_penilaian'] = $this->m_dt_proses_penilaian->get_by_id($id);
				break;
		}

		$this->view('tugas_belajar', $data);
	}

	public function perilaku_kerja_tubel_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_perilaku_kerja_tubel->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$exist = $this->m_dt_perilaku_kerja_tubel->get_by_id_pegawai($data['id_pegawai']);
				if ($exist != null) {
					$this->m_dt_perilaku_kerja_tubel->update($data['id_pegawai'],$data);
				}else{
					$this->m_dt_perilaku_kerja_tubel->insert($data);
				}
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_perilaku_kerja_tubel->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().$this->access->controller.'/tugas_belajar/'.$data['id_pegawai']);
	}

	public function proses_penilaian_tubel_action($action)
	{
		$data = $_POST;
		$data['tgl_keberatan'] = date_to_id($data['tgl_keberatan']);
		$data['tgl_tanggapan'] = date_to_id($data['tgl_tanggapan']);
		$data['tgl_kep_penilaian'] = date_to_id($data['tgl_kep_penilaian']);
		$data['tgl_rekomendasi'] = date_to_id($data['tgl_rekomendasi']);
		switch ($action) {
			case 'insert':
				$this->m_dt_proses_penilaian->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$exist = $this->m_dt_proses_penilaian->get_by_id_pegawai($data['id_pegawai']);
				if ($exist != null) {
					$this->m_dt_proses_penilaian->update($data['id_pegawai'],$data);
				}else{
					$this->m_dt_proses_penilaian->insert($data);
				}
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_proses_penilaian->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().$this->access->controller.'/perilaku_kerja/'.$data['id_pegawai']);
	}

	public function penilaian_gabung($id_pegawai,$tipe = null,$id = null)
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Perilaku';
		$data['riwayat_perilaku_kerja_manual'] = $this->m_dt_perilaku_kerja_manual->get_by_id_pegawai($id_pegawai);
		$data['riwayat_proses_penilaian'] = $this->m_dt_proses_penilaian->get_by_id_pegawai($id_pegawai);
		$data['pegawai'] = $this->m_dt_pns->get_by_id($id_pegawai);

		//action list
		$data['action_perilaku_kerja_manual'] = 'update';
		$data['action_proses_penilaian'] = 'update';

		//list row edit
		$data['perilaku_kerja_manual'] = null;
		$data['proses_penilaian'] = null;

		switch ($tipe) {
			case 1:
				$data['action_perilaku_kerja_manual'] = 'update';
				$data['perilaku_kerja_manual'] = $this->m_dt_perilaku_kerja_tubel->get_by_id($id);
				break;
			case 2:
				$data['action_proses_penilaian'] = 'update';
				$data['proses_penilaian'] = $this->m_dt_proses_penilaian->get_by_id($id);
				break;
		}

		$this->view('penilaian_gabung', $data);
	}

	public function perilaku_kerja_manual_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_perilaku_kerja_manual->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$exist = $this->m_dt_perilaku_kerja_manual->get_by_id_pegawai($data['id_pegawai']);
				if ($exist != null) {
					$this->m_dt_perilaku_kerja_manual->update($data['id_pegawai'],$data);
				}else{
					$this->m_dt_perilaku_kerja_manual->insert($data);
				}
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_perilaku_kerja_manual->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().$this->access->controller.'/penilaian_gabung/'.$data['id_pegawai']);
	}

	public function proses_penilaian_manual_action($action)
	{
		$data = $_POST;
		$data['tgl_keberatan'] = date_to_id($data['tgl_keberatan']);
		$data['tgl_tanggapan'] = date_to_id($data['tgl_tanggapan']);
		$data['tgl_kep_penilaian'] = date_to_id($data['tgl_kep_penilaian']);
		$data['tgl_rekomendasi'] = date_to_id($data['tgl_rekomendasi']);
		switch ($action) {
			case 'insert':
				$this->m_dt_proses_penilaian->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$exist = $this->m_dt_proses_penilaian->get_by_id_pegawai($data['id_pegawai']);
				if ($exist != null) {
					$this->m_dt_proses_penilaian->update($data['id_pegawai'],$data);
				}else{
					$this->m_dt_proses_penilaian->insert($data);
				}
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_proses_penilaian->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().$this->access->controller.'/penilaian_gabung/'.$data['id_pegawai']);
	}

	public function file_upload($id_pegawai,$id = null)
	{
		$data['access'] = $this->access;
		$data['pegawai'] = $this->m_dt_pns->get_by_id($id_pegawai);
		$data['file_upload_all'] = $this->m_dt_file_upload->get_by_id_pegawai($id_pegawai);

		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'File Upload Tambahan';
				$data['action'] = 'insert';
				$data['file_upload'] = null;
				$this->view('file_upload', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'File Upload Tambahan';
				$data['action'] = 'update';
				$data['file_upload'] = $this->m_dt_file_upload->get_by_id($id);
				$this->view('file_upload', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}

	public function file_upload_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_file_upload->insert($data);
				$insert_id = $this->db->insert_id();

				$config['upload_path'] = './berkas/file_upload';
				$config['allowed_types'] = 'jpg|jpeg|png|pdf';
				$config['max_size'] = '5000'; // max_size in kb
				$config['file_name'] = uniqid().'-'.$data['id_pegawai'].'-File-Tambahan';

				$this->load->library('upload', $config);
			 // File upload
				if($this->upload->do_upload('file')){
					 // Get data about the file
					 $uploadData = $this->upload->data();
					 $filename = $uploadData['file_name'];

					 // Initialize array
					 $idp=$insert_id;
					 $data_file = array(
						 'file' => $filename
					 );
					 $this->m_dt_file_upload->update($idp,$data_file);
				 }

				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_file_upload->update($data['id'],$data);
				$config['upload_path'] = './berkas/file_upload';
				$config['allowed_types'] = 'jpg|jpeg|png|pdf';
				$config['max_size'] = '5000'; // max_size in kb
				$config['file_name'] = uniqid().'-'.$data['id_pegawai'].'-File-Tambahan';

				$this->load->library('upload', $config);
			 // File upload
				if($this->upload->do_upload('file')){
					 // Get data about the file
					 $uploadData = $this->upload->data();
					 $filename = $uploadData['file_name'];

					 // Initialize array
					 $idp=$data['id'];
					 $data_file = array(
						 'file' => $filename
					 );
					 $this->m_dt_file_upload->update($idp,$data_file);
				 }
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_file_upload->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().$this->access->controller.'/file_upload/'.$data['id_pegawai']);
	}


	public function riwayat_penilaian($id_pegawai,$id = null)
	{
		$data['access'] = $this->access;
		$data['pegawai'] = $this->m_dt_pns->get_by_id($id_pegawai);
		$data['riwayat_penilaian_all'] = $this->m_dt_riwayat_penilaian->get_by_id_pegawai($id_pegawai);

		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Riwayat Penilaian';
				$data['action'] = 'insert';
				$data['riwayat_penilaian'] = null;
				$this->view('riwayat_penilaian', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Riwayat Penilaian';
				$data['action'] = 'update';
				$data['riwayat_penilaian'] = $this->m_dt_riwayat_penilaian->get_by_id($id);
				$this->view('riwayat_penilaian', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}

	public function riwayat_penilaian_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_riwayat_penilaian->insert($data);
				$insert_id = $this->db->insert_id();

				$config['upload_path'] = './berkas/riwayat_penilaian';
				$config['allowed_types'] = 'jpg|jpeg|png|pdf';
				$config['max_size'] = '5000'; // max_size in kb
				$config['file_name'] = uniqid().'-'.$data['id_pegawai'].'-File-Penilaian';

				$this->load->library('upload', $config);
			 // File upload
				if($this->upload->do_upload('file')){
					 // Get data about the file
					 $uploadData = $this->upload->data();
					 $filename = $uploadData['file_name'];

					 // Initialize array
					 $idp=$insert_id;
					 $data_file = array(
						 'file' => $filename
					 );
					 $this->m_dt_riwayat_penilaian->update($idp,$data_file);
				 }

				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_riwayat_penilaian->update($data['id'],$data);
				$config['upload_path'] = './berkas/riwayat_penilaian';
				$config['allowed_types'] = 'jpg|jpeg|png|pdf';
				$config['max_size'] = '5000'; // max_size in kb
				$config['file_name'] = uniqid().'-'.$data['id_pegawai'].'-File-Penilaian';

				$this->load->library('upload', $config);
			 // File upload
				if($this->upload->do_upload('file')){
					 // Get data about the file
					 $uploadData = $this->upload->data();
					 $filename = $uploadData['file_name'];

					 // Initialize array
					 $idp=$data['id'];
					 $data_file = array(
						 'file' => $filename
					 );
					 $this->m_dt_riwayat_penilaian->update($idp,$data_file);
				 }
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_riwayat_penilaian->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().$this->access->controller.'/riwayat_penilaian/'.$data['id_pegawai']);
	}

	public function delete()
	{
		$data = $_POST;
		$this->m_dt_pns->delete_temp($data['id_pegawai']);
		redirect(base_url().$this->access->controller);
	}

	public function cetak_tugas_tambahan($id_pegawai)
	{
		$data['access'] = $this->access;
		$data['subtitle'] = "Cetak Tugas Tambahan";
		$data['pegawai'] = $this->m_dt_pns->get_by_id($id_pegawai);
		$this->view('cetak_tugas_tambahan',$data);
	}

	public function file_manager($id_pegawai)
	{
		$data['access'] = $this->access;
		$data['subtitle'] = "File Manager";
		$data['pegawai'] = $this->m_dt_pns->get_by_id($id_pegawai);
		$data['riwayat'] = $this->m_dt_riwayat_pangkat->get_by_id_pegawai($id_pegawai);
		$data['riwayatp'] = $this->m_dt_riwayat_pendidikan->get_by_id_pegawai($id_pegawai);
		$data['riwayatj'] = $this->m_dt_riwayat_jabatan->get_by_id_pegawai($id_pegawai);
		$data['riwayat_diklat'] = $this->m_dt_riwayat_diklat->get_by_id_pegawai($id_pegawai);
		$data['riwayat_diklat_baru'] = $this->m_dt_riwayat_diklat_baru->get_by_id_pegawai($id_pegawai);
		$data['riwayat_usulan_diklat'] = $this->m_dt_riwayat_usulan_diklat->get_by_id_pegawai($id_pegawai);
		$data['file_upload_all'] = $this->m_dt_file_upload->get_by_id_pegawai($id_pegawai);
		$data['riwayat_penilaian_all'] = $this->m_dt_riwayat_penilaian->get_by_id_pegawai($id_pegawai);
		$data['keluarga'] = $this->m_dt_keluarga->get_by_id($id_pegawai);
		$this->view('file_manager',$data);
	}

	public function skum($id_pegawai,$tipe = null,$id = null)
	{
		$data['access'] = $this->access;
		$data['subtitle'] = "SKUM";
		$data['pegawai'] = $this->m_dt_pns->get_by_id($id_pegawai);
		$data['golongan'] = $this->m_ms_golongan->get_all();
		$data['jabatan'] = $this->m_ms_jabatan->get_all();
		$data['keluarga'] = $this->m_dt_keluarga->get_by_id($id_pegawai);
		$data['list_anak1'] = $this->m_dt_skum_anak1->get_by_id_pegawai($id_pegawai);
		$data['list_anak2'] = $this->m_dt_skum_anak2->get_by_id_pegawai($id_pegawai);
		if ($tipe == null) {
			$data['action_anak1'] = 'insert';
			$data['action_anak2'] = 'insert';
			$data['anak1'] = null;
			$data['anak2'] = null;
		}else if($tipe == 1){
			$data['action_anak1'] = 'update';
			$data['action_anak2'] = 'insert';
			$data['anak1'] = $this->m_dt_skum_anak1->get_by_id($id);
			$data['anak2'] = null;
		}else if($tipe == 2){
			$data['action_anak1'] = 'insert';
			$data['action_anak2'] = 'update';
			$data['anak1'] = null;
			$data['anak2'] = $this->m_dt_skum_anak2->get_by_id($id);
		}
		$this->view('skum',$data);
	}

	public function skum_action()
	{
		$data = $_POST;
		$id_pegawai = $data['id_pegawai'];
		$pegawai = array(
			'gaji_menurut' => $data['gaji_menurut'],
			'gaji_pokok' => $data['gaji_pokok'],
			'jabatan_samping' => $data['jabatan_samping'],
			'gaji_samping' => $data['gaji_samping'],
			'pensiun' => $data['pensiun']
		);
		//update pegawai
		$this->m_dt_pns->update($id_pegawai,$pegawai);
		$keluarga = array(
			'keterangan' => $data['keterangan'],
			'gaji_pasangan' => $data['gaji_pasangan']
		);
		//update keluarga
		$this->m_dt_keluarga->update($id_pegawai,$keluarga);
		redirect(base_url().$this->access->controller.'/skum/'.$id_pegawai);
	}

	public function skum_anak1_action($action)
	{
		$data = $_POST;
		$data['tgl'] = date_to_id($data['tgl']);
		$data['tgl_lahir'] = date_to_id($data['tgl_lahir']);
		if ($action == 'insert') {
			$this->m_dt_skum_anak1->insert($data);
		}else{
			$this->m_dt_skum_anak1->update($data['id'],$data);
		}
		redirect(base_url().$this->access->controller.'/skum/'.$data['id_pegawai']);
	}

	public function delete_anak1()
	{
		$data = $_POST;
		$this->m_dt_skum_anak1->delete_permanent($data['id']);
		redirect(base_url().$this->access->controller.'/skum/'.$data['id_pegawai']);
	}

	public function skum_anak2_action($action)
	{
		$data = $_POST;
		$data['tgl_lahir'] = date_to_id($data['tgl_lahir']);
		if ($action == 'insert') {
			$this->m_dt_skum_anak2->insert($data);
		}else{
			$this->m_dt_skum_anak2->update($data['id'],$data);
		}
		redirect(base_url().$this->access->controller.'/skum/'.$data['id_pegawai']);
	}

	public function delete_anak2()
	{
		$data = $_POST;
		$this->m_dt_skum_anak2->delete_permanent($data['id']);
		redirect(base_url().$this->access->controller.'/skum/'.$data['id_pegawai']);
	}

	public function cetak_skum($id_pegawai)
	{
		$constructor = [
			'mode' => '',
			'format' => 'A4',
			'default_font_size' => 0,
			'default_font' => '',
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 5,
			'margin_bottom' => 10,
			'margin_header' => 5,
			'margin_footer' => 5,
			'orientation' => 'P',
		];
		$mpdf= new \Mpdf\Mpdf($constructor);
		$stylesheet = file_get_contents(base_url().'dist/css/pdf.css'); // external css
		// HEADER
		$mpdf->WriteHTML($stylesheet,1);
		$data['pegawai'] = $this->m_dt_pns->get_by_id($id_pegawai);
		$data['golongan'] = $this->m_ms_golongan->get_all();
		$data['jabatan'] = $this->m_ms_jabatan->get_all();
		$data['keluarga'] = $this->m_dt_keluarga->get_by_id($id_pegawai);
		$data['list_anak1'] = $this->m_dt_skum_anak1->get_by_id_pegawai($id_pegawai);
		$data['list_anak2'] = $this->m_dt_skum_anak2->get_by_id_pegawai($id_pegawai);
		$data['direktur'] = $this->m_ap_profil->get_direktur();
		$mpdf->WriteHTML($this->load->view('cetak_skum_1',$data,TRUE));
		$mpdf->AddPage('L');
		$mpdf->WriteHTML($this->load->view('cetak_skum_2',$data,TRUE));
		$mpdf->Output('skum-'.$id_pegawai.'.pdf', 'I');
	}

	public function uploadPhoto()
	{
		$config['upload_path']          = './berkas/photo/';
		$config['allowed_types']        = 'jpg|jpeg|png';
		$config['overwrite']						= true;
		$config['max_size']             = 100000000;
		$config['remove_spaces'] 				= TRUE;
		$config['encrypt_name'] 				= TRUE;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('photo')){
			$error = array('error' => $this->upload->display_errors());
			var_dump($error);
			exit();
		}else{
			return $this->upload->data("file_name");
		}
		
		return null;
	}

}
