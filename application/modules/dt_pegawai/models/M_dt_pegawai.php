<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dt_pegawai extends CI_Model {

	public function get_all()
  {
    $profil = $this->db->get('ap_profil')->row();

    return $this->db
      ->join('ms_jabatan b', 'a.id_jabatan = b.id_jabatan','left')
      ->join('ms_eselon c', 'c.id_eselon = right(a.id_jabatan,2)','left')
      ->join('ms_golongan d', 'a.id_golongan = d.id_golongan','left')
      ->join('ms_status_pegawai e', 'a.id_status_pegawai = e.id_status_pegawai','left')
      ->join('ms_instansi f', 'a.id_instansi = f.id_instansi','left')
      ->where('a.is_deleted', 0)
      ->order_by('a.id_golongan','desc')
      ->order_by('a.id_pegawai')
      ->get('dt_pegawai a')
      ->result();
  }

  public function get_by_id($id)
  {
    $profil = $this->db->get('ap_profil')->row();

    return $this->db
      ->join('ms_jabatan b', 'a.id_jabatan = b.id_jabatan','left')
      ->join('ms_eselon c', 'c.id_eselon = right(a.id_jabatan,2)','left')
      ->join('ms_golongan d', 'a.id_golongan = d.id_golongan','left')
      ->join('ms_status_pegawai e', 'a.id_status_pegawai = e.id_status_pegawai','left')
      ->join('ms_instansi f', 'a.id_instansi = f.id_instansi','left')
      ->where('a.id_pegawai',$id)  
      ->order_by('a.id_golongan','desc')
      ->order_by('a.id_pegawai')
      ->get('dt_pegawai a')
      ->row();
  }

  public function get_by_nomor_induk($n)
  {
    $profil = $this->db->get('ap_profil')->row();

    return $this->db
      ->join('ms_jabatan b', 'a.id_jabatan = b.id_jabatan','left')
      ->join('ms_eselon c', 'c.id_eselon = right(a.id_jabatan,2)','left')
      ->join('ms_golongan d', 'a.id_golongan = d.id_golongan','left')
      ->where('a.nomor_induk',$n)  
      ->order_by('a.id_golongan','desc')
      ->order_by('a.id_pegawai')
      ->get('dt_pegawai a')
      ->row();
  }

  public function update_by_nomor_induk($ni,$data)
  {
    $this->db->where('nomor_induk',$ni)->update('dt_pegawai',$data);
  }

}
