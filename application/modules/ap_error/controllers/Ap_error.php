<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ap_error extends MX_Controller {

	public function error_403()
	{
		$id_grup = $this->session->userdata('id_grup');
		$this->load->model('ap_konfigurasi/m_ap_konfigurasi');
		$data['menu'] = $this->m_ap_konfigurasi->get_menu_first($id_grup);
		$this->load->view('error_403',$data);
	}

	public function error_404()
	{
		$this->output->set_status_header('404'); 
		$this->load->view('error_404');
	}

}
