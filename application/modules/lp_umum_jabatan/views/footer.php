      </tbody>
    </table>
    <br><br>
    <table id="count_table">
      <tbody>
        <?php $grand=0;foreach($total as $row): ?>  
          <tr>
            <td class="text-left" width="100"><?=$row->status_pegawai?></td>
            <td>:</td>
            <td class="text-right" width="40"><?=$row->jumlah?></td>
          </tr>
        <?php $grand += $row->jumlah; endforeach;?>
        <tr>
          <th class="text-left">Total</th>
          <th>:</th>
          <th class="text-right"><?=$grand?></th>
        </tr>
      </tbody>
    </table>
    <br><br>
    <?php 
      if($direktur){
        $nama_direktur = '';
        if ($direktur->gelar_depan != '') {
          $nama_direktur .= $direktur->gelar_depan.' ';
        }
        $nama_direktur .= $direktur->nama;
        if ($direktur->gelar_belakang != '') {
          $nama_direktur .= ', '.$direktur->gelar_belakang;
        }
      }
    ?>
    <table id="sign_table">
      <tbody>
        <tr>
          <td class="text-center">
            <?=$profil->kop_ttd_direktur?>
          </td>
        </tr>
        <tr>
          <td><br><br><br><br></td>
        </tr>
        <tr>
          <td class="text-center">
            <u><?=$nama_direktur?></u><br>
            NIP. <?=$direktur->nomor_induk?>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>