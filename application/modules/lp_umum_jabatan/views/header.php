<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Rekap Data Pegawai Berdasarkan Jabatan</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
  <table id="kop">
    <tbody>
      <tr>
        <td class="text-center logo"><img src="<?=base_url()?>img/kebumen.png" height="80"></td>
        <td class="text-center">
          <span id="kop_1">PEMERINTAH KABUPATEN KEBUMEN </span><br>
          <span id="kop_2">RUMAH SAKIT UMUM DAERAH KEBUMEN dr. SOEDIRMAN</span><br>
          <span id="kop_3">
            Jalan Lingkar Selatan, Muktisari, Kebumen, Kabupaten Kebumen, Jawa Tengah 54351 <br>
            Telp/Fax : 0287-3873318, 0287-381101, 0287-385274; WA : 0877-3427-026; Email : rsud@kebumenkab.go.id
          </span>
        </td>
        <td class="text-center logo"><img src="<?=base_url()?>img/logo.png" height="80"></td>
      </tr>
    </tbody>
  </table>
  <div class="garis"></div>
  <h3 class="text-center"><?=$judul?></h3>
  <table id="filter_table">
    <tbody>
      <tr>
        <td>Jabatan</td>
        <td>:</td>
        <td>
          <?php if($search['jabatan'] != null): ?>
            <?=$search['jabatan']->jabatan?>
          <?php else: ?>
            Semua
          <?php endif;?>
        </td>
      </tr>
      <tr>
        <td width="120">Ruang</td>
        <td>:</td>
        <td>
          <?php if($search['ruang'] != null): ?>
            <?=$search['ruang']->ruang?>
          <?php else: ?>
            Semua
          <?php endif;?>
        </td>
      </tr>
      <tr>
        <td>Status Pegawai</td>
        <td>:</td>
        <td>
          <?php if($search['status_pegawai'] != null): ?>
            <?=$search['status_pegawai']->status_pegawai?>
          <?php else: ?>
            Semua
          <?php endif;?>
        </td>
      </tr>
    </tbody>
  </table>
  <br>