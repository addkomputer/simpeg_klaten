<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-<?=$access->icon?> text-light-blue"></i> Laporan Data Pegawai <?=$access->menu?>
      <small><?=$subtitle?></small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">Administrasi Kehadiran</li>
      <li class="active text-orange"><?=$access->menu?></li>
    </ol>    
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box box-primary">
      <div class="box-body">
        <form id="form_search" method="post" action="<?=base_url($access->controller)?>/search">
          <div class="row">
            <div class="col-md-3 col-md-offset-3">
              <div class="form-group">
                <select class="form-control select2" name="id_jabatan" id="id_jabatan">
                  <option value="">- Semua Jabatan -</option>
                  <?php foreach($jabatan as $row): ?>
                    <option value="<?=$row->id_jabatan?>" <?php if($search){if($search['id_jabatan']==$row->id_jabatan){echo 'selected';}} ?>><?=$row->jabatan?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <select class="form-control select2" name="id_ruang" id="id_ruang">
                  <option value="">- Semua Ruang -</option>
                  <?php foreach($ruang as $row): ?>
                    <option value="<?=$row->id_ruang?>" <?php if($search){if($search['id_ruang']==$row->id_ruang){echo 'selected';}} ?>><?=$row->ruang?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <select class="form-control select2" name="id_status_pegawai" id="id_status_pegawai">
                  <option value="">- Semua Status -</option>
                  <?php foreach($status_pegawai as $row): ?>
                    <option value="<?=$row->id_status_pegawai?>" <?php if($search){if($search['id_status_pegawai']==$row->id_status_pegawai){echo 'selected';}} ?>><?=$row->status_pegawai?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>
            <div class="col-md-1">
              <a class="btn btn-default btn-flat" href="<?=base_url().$access->controller?>/reset_search"><i class="fa fa-refresh"></i></a>
            </div>
          </div>
        </form>
      </div>
    </div>
    <div class="box box-primary">
      <div class="box-body">
        <a class="btn btn-success btn-flat pull-right" href="<?=base_url().$access->controller?>/cetak_pdf" target="_blank"><i class="fa fa-file-pdf-o"></i> Cetak PDF</a>
        <br><br>
        <table class="table table-striped table-bordered table-condensed">
          <thead>
            <tr>
              <th class="text-center" width="30">No.</th>
              <th class="text-center" width="60">NIP/NPNP</th>
              <th class="text-center">Nama</th>
              <th class="text-center" width="10">JK</th>
              <th class="text-center" width="150">TTL</th>
              <th class="text-center" width="40">Usia</th>
              <th class="text-center" width="">Ruang</th>
              <th class="text-center" width="">Jabatan</th>
              <th class="text-center" width="20">Eselon</th>
              <th class="text-center" width="">Pangkat</th>
              <th class="text-center" width="10">Gol</th>
              <th class="text-center" width="10">Status</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($pegawai != null): ?>
              <?php $i=1;foreach ($pegawai as $row): ?>
                <tr>
                  <td class="text-center"><?=$this->uri->segment('3')+$i++?></td>
                  <td><?=$row->nomor_induk?></td>
                  <td>
                    <?php 
                      echo $row->gelar_depan.' '.$row->nama;
                      if ($row->gelar_belakang != ''){
                        echo ', '.$row->gelar_belakang;
                      }
                    ?></td>
                  <td class="text-center"><?php echo $jk = ($row->jenis_kelamin == '1') ? 'L' : 'P';?></td>
                  <td><?=$row->tempat_lhr.', '.date_to_id($row->tgl_lhr)?></td>
                  <td class="text-center"><?php if($row->tgl_lhr != ''){echo hitung_umur($row->tgl_lhr);}?></td>
                  <td><?=$row->ruang?></td>
                  <td><?=$row->jabatan?></td>
                  <td class="text-center"><?php echo $eselon = ($row->eselon != '') ? $row->eselon : '-' ;?></td>
                  <td><?=$row->pangkat?></td>
                  <td class="text-center"><?=$row->golongan?></td>
                  <td class="text-center"><?=$row->status_pegawai?></td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td class="text-center" colspan="99">Tidak ada data!</td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <span class="badge bg-blue">Jumlah Data : <?=$num_rows?></span>
        <?php echo $this->pagination->create_links(); ?>
      </div>
      <!-- /.box-footer-->
    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
  $(document).ready(function () {
    // form validator
    $('#form').validate({
      rules : {
        'jenis_kelamin' : {
          valueNotEquals : '0'
        }
      }
    });
    $('.select2').on('select2:select', function (e) {
      $('#form_search').submit();
    });
  })
</script>