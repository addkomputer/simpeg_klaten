<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lp_umum_jabatan extends MY_Controller {

	var $access, $id_pegawai;

  function __construct(){
		parent::__construct();

		$controller = 'lp_umum_jabatan';

    if($this->session->userdata('menu') != $controller){
      $this->session->unset_userdata('search');
      $this->session->set_userdata(array('menu' => 'lp_umum_jabatan'));
    }
    $this->load->model('ap_konfigurasi/m_ap_konfigurasi');
		$this->id_pegawai = $this->session->userdata('id_pegawai');
		$this->access = $this->m_ap_konfigurasi->get_permission($this->id_pegawai, $controller);
		if ($this->access == null) {
			redirect(base_url().'ap_error/error_403');
		}
		$this->load->model('ap_profil/m_ap_profil');
		$this->load->model('m_lp_umum_jabatan');
		$this->load->model('ms_ruang/m_ms_ruang');
		$this->load->model('ms_jabatan/m_ms_jabatan');
		$this->load->model('ms_golongan/m_ms_golongan');
		$this->load->model('ms_status_pegawai/m_ms_status_pegawai');
	}
	
	public function index()
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Daftar';
		$data['ruang'] = $this->m_ms_ruang->get_all();
		$data['jabatan'] = $this->m_ms_jabatan->get_all();
		$data['golongan'] = $this->m_ms_golongan->get_all();
		$data['status_pegawai'] = $this->m_ms_status_pegawai->get_all();

		if ($this->access->_read) {
			$search = null;
			
			if($this->session->userdata('search') != null){
				$search = $this->session->userdata('search');
			}
			
			$data['search'] = $search;
	
			$config['base_url'] = base_url().'lp_umum_jabatan/index/';
			$config['per_page'] = 10;
	
			$from = $this->uri->segment(3);
			if ($from == '') {
				$from = 0;
			}

			$num_rows = $this->m_lp_umum_jabatan->num_rows($search);
			$config['total_rows'] = $num_rows;
			$this->pagination->initialize($config);

			$data['pegawai'] = $this->m_lp_umum_jabatan->get_list($config['per_page'],$from,$search);
			
			$data['num_rows'] = $num_rows;
			insert_log('read',$this->access->menu);
			//var_dump($data);exit();
			$this->view('index',$data);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function search()
	{
		$data = $_POST;
		$this->session->set_userdata(array('search' => $data));
		redirect(base_url().$this->access->controller);
	}

	public function reset_search()
  {
    $this->session->unset_userdata('search');
    redirect(base_url().'lp_umum_jabatan/index');
	}

	public function cetak_pdf()
	{
		//data data
		$profil = $this->m_ap_profil->get_first();

		$ruang_search = 'Semua';
		$jabatan_search = 'Semua';
		$status_pegawai_search = 'Semua';
		
		$search = $this->session->userdata('search');
		if ($search != null) {
			if ($search['id_ruang'] != '') {				
				$ruang_search = $this->m_ms_ruang->get_by_id($search['id_ruang'])->ruang;
			}
			if ($search['id_jabatan'] != '') {
				$jabatan_search = $this->m_ms_jabatan->get_by_id($search['id_jabatan'])->jabatan;
			}
			if ($search['id_status_pegawai'] != '') {
				$status_pegawai_search = $this->m_ms_status_pegawai->get_by_id($search['id_status_pegawai'])->status_pegawai;
			}
		}
		//generate pdf
		$this->load->library('pdf');
		$pdf = new Pdf('l','mm',array(210,330));
		$pdf->AliasNbPages();
		$pdf->SetTitle('Rekap Pegawai Berdasarkan Jabatan');
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetFont('Arial','',13);
		// mencetak string 
		$pdf->Image(base_url().'img/kebumen.png',10,10,16,20);
		$pdf->Cell(0,6,'PEMERINTAH KABUPATEN KEBUMEN',0,1,'C');
		$pdf->SetFont('Arial','B',15);
		$pdf->Cell(0,6,'RUMAH SAKIT UMUM DAERAH KEBUMEN dr. SOEDIRMAN',0,1,'C');
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(0,4,'Jalan Lingkar Selatan, Muktisari, Kebumen, Kabupaten Kebumen, Jawa Tengah 54351',0,1,'C');
		$pdf->Cell(0,4,'Telp/Fax : 0287-3873318, 0287-381101, 0287-385274; WA : 0877-3427-026; Email : rsud@kebumenkab.go.id',0,1,'C');
		$pdf->Image(base_url().'img/logo.png',300,10,20,20);
		$pdf->Line(10, 35, 320, 35);
		// Memberikan space kebawah agar tidak terlalu rapat	
		$pdf->Cell(0,10,'',0,1,'C');
		$pdf->SetFont('Arial','B',12);
		$pdf->Cell(0,5,'REKAP PEGAWAI BERDASARKAN JABATAN',0,1,'C');
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(40,5,'Jabatan');
		$pdf->Cell(5,5,':');
		$pdf->Cell(0,5,$jabatan_search,0,1);
		$pdf->Cell(40,5,'Ruang');
		$pdf->Cell(5,5,':');
		$pdf->Cell(0,5,$ruang_search,0,1);
		$pdf->Cell(40,5,'Status Pegawai');
		$pdf->Cell(5,5,':');
		$pdf->Cell(0,5,$status_pegawai_search,0,1);
		//generate tabel
		$pdf->Cell(0,5,'',0,1,'C');
		$pdf->SetFont('Arial','B',9);
		$pdf->SetWidths(array('10','38','50','8','45','50','50','13','20','10','15'));
		$pdf->SetAligns(array('C','C','C','C','C','C','C','C','C','C','C'));
		$pdf->Row(array('No.','Nomor Induk','Nama','JK','TTL','Ruang','Jabatan','Eselon','Pangkat','Gol','Status'));
		$pdf->SetAligns(array('C','C','L','C','L','L','L','C','L','C','C'));
		$pdf->SetFont('Arial','',9);
		$pegawai = $this->m_lp_umum_jabatan->get_print($search);
		if($pegawai != null){
			$i=1;foreach ($pegawai as $row) {
				$nama = $row->gelar_depan.' '.$row->nama;
				if ($row->gelar_belakang != ''){
					$nama .= ', '.$row->gelar_belakang;
				}
				$jk = ($row->jenis_kelamin) ? 'L' : 'P' ;
				$umur = ($row->tgl_lhr != '') ? hitung_umur($row->tgl_lhr) : '-' ;
				$ttl = $row->tempat_lhr.', '.date_to_id($row->tgl_lhr);
				$eselon = ($row->eselon != '') ? $row->eselon : '-' ;
				$pdf->Row(array(
					$i++,
					$row->nomor_induk,
					$nama,
					$jk,
					$ttl,
					$row->ruang,
					$row->jabatan,
					$eselon,
					$row->pangkat,
					$row->golongan,
					$row->status_pegawai
				));
			}
			$total = $this->m_lp_umum_jabatan->count_print($search);
			$pdf->Cell(0,10,'',0,1,'');
			$pdf->SetWidths(array('50','5','10'));
			$pdf->SetAligns(array('L','C','R'));
			$jumlah_total = 0;
			foreach ($total as $row) {
				$pdf->Row(array($row->status_pegawai,':',$row->jumlah));
				$jumlah_total += $row->jumlah;
			}
			$pdf->SetFont('Arial','B',9);
			$pdf->Row(array('Total',':',$jumlah_total));
			//direktur
			$direktur = $this->m_ap_profil->get_direktur();
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(500,5,'Mengetahui,	',0,1,'C');
			$pdf->Cell(500,5,$profil->kop_ttd_direktur,0,1,'C');
			$pdf->Cell(500,20,'',0,1,'C');
			$pdf->SetFont('Arial','U',9);
			if ($direktur != null) {
				$direktur_nama = $direktur->gelar_depan.' '.$direktur->nama;
					if ($direktur->gelar_belakang != ''){
						$direktur_nama .= ', '.$direktur->gelar_belakang;
					}
				$pdf->Cell(500,3,$direktur_nama,0,1,'C');
				$pdf->SetFont('Arial','',9);
				$pdf->Cell(500,5,'NIP. '.$direktur->nomor_induk,0,1,'C');
			}
		}
		
		$pdf->Output('I',date('Ymdhis').'_rekap_pegawai_berdasarkan_jabatan.pdf');
	}

	function cetak_pdf_mpdf($id=null)
	{
		ini_set("memory_limit","-1");
		//
		$search = $this->session->userdata('search');
		$data['search'] = $search;
		$data['profil'] = $this->m_ap_profil->get_first();
		$data['judul'] = "REKAP DATA PEGAWAI BERDASAR JENIS KELAMIN";
		$data['pegawai'] = $this->m_lp_umum_jabatan->get_print($search);
		$data['total'] = $this->m_lp_umum_jabatan->count_print($search);
		$data['direktur'] = $this->m_ap_profil->get_direktur();
		//
		$pdfFilePath = str_replace(' ', '_', $data['judul']).'.pdf';
		$this->load->file(APPPATH . 'libraries/mpdf/mpdf.php');
		$pdf = new mPDF("en-GB-x",array(210,330),"","",10,10,10,10,7,7,"L");
		//
		$stylesheet = file_get_contents(base_url().'dist/css/pdf.css');
		$pdf->WriteHTML($stylesheet,1);
		$pdf->cacheTables = true;
		$pdf->simpleTables = true;
		$pdf->packTableData = true;
		//
		$html = $this->load->view('cetak_pdf',$data,true);
		$pdf->WriteHTML($html);
		$pdf->Output($pdfFilePath, "I");
	}
	
}