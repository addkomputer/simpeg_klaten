<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cetak SKUm</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?=base_url().'dist/css/pdf.css'?>">
  <style>
    td {
      vertical-align: top;
    }
    .bordered {
      border-collapse: collapse;
    }
    .bordered, .bordered th, .bordered td {
      border: 1px solid black;
    }
    

  </style>
</head>
<body>
  <h3 class="text-center">SURAT KETERANGAN <br>UNTUK MENDAPATKAN PEMBAYARAN TUNJANGAN KELUARGA</h3>
  <br>
  <?php 
    $nama = $pegawai->gelar_depan.' '.$pegawai->nama;
    if ($pegawai->gelar_belakang != ''){
      $nama .= ', '.$pegawai->gelar_belakang;
    }
  ?>
  <table>
    <tbody>
      <tr>
        <td width="20">1.</td>
        <td width="250">Nama Lengkap</td>
        <td>:</td>
        <td><?=$nama?></td>
      </tr>
      <tr>
        <td>2.</td>
        <td>NIP</td>
        <td>:</td>
        <td><?=$pegawai->nomor_induk?></td>
      </tr>
      <tr>
        <td>3.</td>
        <td>Tempat/Tanggal Lahir</td>
        <td>:</td>
        <td><?=$pegawai->tempat_lhr.', '.date_to_id($pegawai->tgl_lhr)?></td>
      </tr>
      <tr>
        <td>4.</td>
        <td>Jenis Kelamin</td>
        <td>:</td>
        <td><?php echo $jk = ($pegawai->jenis_kelamin == 1) ? 'Laki-laki' : 'Perempuan' ;?></td>
      </tr>
      <tr>
        <td>5.</td>
        <td>Agama</td>
        <td>:</td>
        <td><?php
          switch ($pegawai->agama) {
            case 1:
              echo 'Islam';
              break;

            case 2:
              echo 'Protestan';
              break;

            case 3:
              echo 'Katholik';
              break;

            case 4:
              echo 'Hindu';
              break;

            case 5:
              echo 'Budha';
              break;
          }
        $pegawai->agama?></td>
      </tr>
      <tr>
        <td>6.</td>
        <td>Kebangsaan</td>
        <td>:</td>
        <td><?php echo $wn = ($pegawai->warganegara == 1) ? 'WNI' : 'WNA' ;?></td>
      </tr>
      <tr>
        <td>7.</td>
        <td>Pangkat / Gol. Ruang</td>
        <td>:</td>
        <td><?=$pegawai->pangkat.' - '.$pegawai->golongan?></td>
      </tr>
      <tr>
        <td>8.</td>
        <td>Jabatan Struktural/ Fungsional</td>
        <td>:</td>
        <td><?=$pegawai->jabatan?></td>
      </tr>
      <tr>
        <td>9.</td>
        <td>Instansi</td>
        <td>:</td>
        <td>Pemerintah Kabupaten Kebumen</td>
      </tr>
      <tr>
        <td>10.</td>
        <td>Masa Kerja Golongan</td>
        <td>:</td>
        <td>
          <?=dateDifference(date('Y-m-d'),$pegawai->tmt_gol,'%y Tahun %m Bulan')?>
          <br>
          Masa kerja seluruhnya <?=dateDifference(date('Y-m-d'),$pegawai->tmt_pns,'%y Tahun %m Bulan')?>
        </td>
      </tr>
      <tr>
        <td>11.</td>
        <td>Digaji menurut</td>
        <td>:</td>
        <td><?=$pegawai->gaji_menurut?> Rp. <?=$pegawai->gaji_pokok?></td>
      </tr>
      <tr>
        <td>12.</td>
        <td>Alamat tempat tinggal</td>
        <td>:</td>
        <td><?=$pegawai->alamat?></td>
      </tr>
      <tr>
        <td>13.</td>
        <td colspan="3">
          Menerangkan dengan sesungguhnya bahwa saya : <br>
          a. Disamping Jabatan utama tersebut, bekerja pula sebagai : <?=$pegawai->jabatan_samping?><br>
          dengan mendapata penghasilan sebesar Rp. <?=$pegawai->gaji_samping?><br>
          b. Mempunyai pensiun / pensiun janda Rp. <?=$pegawai->pensiun?><br>
          c. Kawin syah dengan 
        </td>
      </tr>
    </tbody>
  </table>
  <table class="bordered">
    <tr>
      <td class="text-center" rowspan="2">No.</td>
      <td class="text-center" rowspan="2">Nama Istri/Suami Tanggungan</td>
      <td class="text-center" colspan="2">Tanggal</td>
      <td class="text-center" rowspan="2">Pekerjaan</td>
      <td class="text-center" rowspan="2">Penghasilan Sebulan</td>
      <td clsas="text-center" rowspan="2">Keterangan</td>
    </tr>
    <tr>
      <td class="text-center">Kelahiran (Umur)</td>
      <td class="text-center">Perkawinan</td>
    </tr>
    <tr>
      <td>1.</td>
      <td><?=$keluarga->nama_pasangan?></td>
      <td class="text-center"><?=date_to_id($keluarga->tgl_lahir_pasangan)?></td>
      <td class="text-center"><?=date_to_id($keluarga->tgl_perkawinan)?></td>
      <td>
        <?php if($keluarga){if($keluarga->pekerjaan_pasangan == '0'){echo '-- Pilih --';}}?>
        <?php if($keluarga){if($keluarga->pekerjaan_pasangan == '1'){echo 'PNS';}}?>
        <?php if($keluarga){if($keluarga->pekerjaan_pasangan == '2'){echo 'TNI / POLRI';}}?>
        <?php if($keluarga){if($keluarga->pekerjaan_pasangan == '3'){echo 'Pengusaha / Wira Swasta';}}?>
        <?php if($keluarga){if($keluarga->pekerjaan_pasangan == '4'){echo 'Petani/Nelayan';}}?>
        <?php if($keluarga){if($keluarga->pekerjaan_pasangan == '5'){echo 'Karyawan Swasta';}}?>
        <?php if($keluarga){if($keluarga->pekerjaan_pasangan == '6'){echo 'Mengurus Rumah Tangga';}}?>
      </td>
      <td>Rp. <?=$keluarga->gaji_pasangan?></td>
      <td><?=$keluarga->keterangan?></td>
    </tr>
  </table>
  <table>
    <tr>
      <td width="20"></td>
      <td>
        d. Mempunyai anak- anak seperti dalam daftar disebelah ini yaitu<br>
        I. ANAK KANDUNG (ak), ANAK TIRI (at)dan ANAK ANGKAT (aa) yang masih menjadi tanggungan, belum mempunyai pekerjaan sendiri dan masuk dalam daftar gaji. <br>
        II. ANAK KANDUNG (ak), ANAK TIRI (at)dan ANAK ANGKAT (aa) yang masih menjadi tanggungan tetapi tidak masuk dalam daftar gajid
      </td>
    </tr>
    <tr>
      <td></td>
      <td>e. Jumlah anak seluruhnya (yang menjadi tanggungan termasuk yang tidak termasuk dalam daftar gaji.</td>
    </tr>
    <tr>
      <td></td>
      <td>
        <br>
        Keterangan ini saya buat dengan sesungguhnya dan apabila keterangan ini ternyata tidak benar (palsu), saya 
        bersedia dituntut di muka pengadilan berdasarkan Undang-undang yang berlaku, dan bersedia mengembalikan
        semua tunjangan anak yang telah saya terima yang seharusnya bukan menjadi hak saya.
      </td>
    </tr>
  </table>
  <br>
  <!-- <?php 
    $nama_direktur = '';
    if ($direktur->gelar_depan != '') {
      $nama_direktur .= $direktur->gelar_depan.' ';
    }
    $nama_direktur .= $direktur->nama;
    if ($direktur->gelar_belakang != '') {
      $nama_direktur .= ', '.$direktur->gelar_belakang;
    }
  ?> -->
  <table>
    <tr>
      <td width="10"></td>
      <td width="350" class="text-center">
        Mengetahui : <br>
        Kepala Instansi <br>
        <br><br><br><br>
        <u><?=$nama_direktur?></u><br>
        NIP. : <?=$direktur->nomor_induk?>
      </td>
      <td width="100"></td>
      <td class="text-center">
        Kebumen, <?php echo date('d-m-Y') ?> <br>
        Yang menerangkan <br>
        <br><br><br><br>
        <u><?=$nama?><br></u>
        NIP. : <?=$pegawai->nomor_induk?>
      </td>
    </tr>
  </table>
</body>
</html>