<!-- Profile Image -->
<div class="box box-danger">
  <div class="box-header with-border">
    <h3 class="box-title">Profil Pegawai</h3>
  </div>
  <div class="box-body box-profile">
    <?php
      if ($pegawai) {
        $photo = base_url()."/berkas/photo/".$pegawai->photo;
        if (is_url_exists($photo)) {
          $foto = $photo;
        }else{
          $foto = base_url().'img/no-photo.jpg';
        }
      }else{
        $foto = base_url().'img/no-photo.jpg';
      }
    ?>
    <img class="profile-user-img img-responsive" src="<?=$foto?>" alt="User profile picture">
    <br>
    <div class="col-md-12">
      <div class="form-group">
        <label for="">Upload Photo</label>
        <input class="form-control" type="file" name="photo" id="photo">
        <input type="hidden" name="old_photo" value="<?=@$pegawai->photo?>">
      </div>
    </div>
    <hr>
    <h3 class="profile-username text-center">
      <?php if($pegawai){echo $pegawai->nama;} ?>
    </h3>
    <p class="text-muted text-center">
      <?php 
        $nama = '';
        if ($pegawai) {
          if ($pegawai->gelar_depan != '') {
            $nama .= $pegawai->gelar_depan.' ';
          }
          $nama .= $pegawai->nama;
          if ($pegawai->gelar_belakang != '') {
            $nama .= ', '.$pegawai->gelar_belakang;
          }
        }
        echo $nama;
      ?>
    </p>
    <p class=" text-center"><?php if($pegawai){echo @$pegawai->jabatan;}?></p>
    <ul class="list-group list-group-unbordered">
      <li class="list-group-item">
        <b>NIP</b> <a class="pull-right"><?php if($pegawai){echo $pegawai->nomor_induk;}?></a>
      </li>
      <li class="list-group-item">
        <b>TTL</b> <a class="pull-right"><?php if($pegawai){echo $pegawai->tempat_lhr.', '.date_to_id($pegawai->tgl_lhr);}?></a>
      </li>
      <li class="list-group-item">
        <b>No. Telp</b> <a class="pull-right"><?php if($pegawai){echo $pegawai->no_hp;}?></a>
      </li>
      <li class="list-group-item">
        <b>Golongan</b> <a class="pull-right"><?php if($pegawai){if($pegawai->id_golongan){echo $pegawai->pangkat.' - '.$pegawai->golongan;}} ?></a>
      </li>
      <li class="list-group-item">
        <b>Status Pegawai</b> <a class="pull-right"><?php if($pegawai){echo $pegawai->status_pegawai;}?></a>
      </li>
    </ul>
    <!-- <a href="#" class="btn bt/n-primary btn-block"><b>Follow</b></a> -->
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->

<!-- About Me Box -->
<div class="box box-danger">
  <div class="box-header with-border">
    <h3 class="box-title">Ringkasan</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <strong><i class="fa fa-university margin-r-5"></i> Instansi</strong>
    <p class="text-muted"><?php if($pegawai){echo $pegawai->instansi;}?></p>
    <hr>
    <strong><i class="fa fa-file-text-o margin-r-5"></i> Pendidikan Terakhir</strong>
    <p class="text-muted"><?php if($pegawai){echo $pegawai->pend_terakhir;}?></p>
    <hr>
    <strong><i class="fa fa-map-marker margin-r-5"></i> Alamat</strong>
    <p class="text-muted"><?php if($pegawai){echo $pegawai->alamat;}?></p>
    <hr>
    <strong><i class="fa fa-clock-o margin-r-5"></i> Masa Kerja</strong>
    <p class="text-muted"><?php if($pegawai){echo $pegawai->masakerja_gol_thn.' Tahun '.$pegawai->masakerja_gol_thn.' Bulan';}?></p>
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->