<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-<?=$access->icon?> text-red"></i> <?=$access->menu?>
      <small><?=$subtitle?></small>
    </h1>      
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box box-danger">
      <div class="box-header with-border">
        <i class="fa fa-table text-maroon"></i>
        <h3 class="box-title">Tabel <?=$access->menu?></h3>
        <div class="box-tools pull-right">
          <span class="badge bg-maroon"><i class="fa fa-bar-chart"></i> Jumlah Keseluruhan Data : <?=$num_rows_total?></span>
        </div>
      </div>
      <div class="box-body">
        <form id="form_search" action="<?=base_url().$this->access->controller;?>/search" method="post" autocomplete="off">
          <div class="row">
            <div class="col-md-1">
              <a class="btn btn-success btn-flat" href="<?=base_url().$access->controller?>/cetak_pdf" target="_blank"><i class="fa fa-file-pdf-o"></i> Cetak PDF</a>
            </div>
            <div class="col-md-2 col-md-offset-7">
              <div class="form-group">
                <select name="bulan" class="form-control select2 input-sm">
                  <option value="01" <?php if($search){if($search['bulan'] == '01'){echo 'selected';}}else{if(date('m') == '01'){echo 'selected';}} ?>>Januari</option>
                  <option value="02" <?php if($search){if($search['bulan'] == '02'){echo 'selected';}}else{if(date('m') == '02'){echo 'selected';}} ?>>Februari</option>
                  <option value="03" <?php if($search){if($search['bulan'] == '03'){echo 'selected';}}else{if(date('m') == '03'){echo 'selected';}} ?>>Maret</option>
                  <option value="04" <?php if($search){if($search['bulan'] == '04'){echo 'selected';}}else{if(date('m') == '04'){echo 'selected';}} ?>>April</option>
                  <option value="05" <?php if($search){if($search['bulan'] == '05'){echo 'selected';}}else{if(date('m') == '05'){echo 'selected';}} ?>>Mei</option>
                  <option value="06" <?php if($search){if($search['bulan'] == '06'){echo 'selected';}}else{if(date('m') == '06'){echo 'selected';}} ?>>Juni</option>
                  <option value="07" <?php if($search){if($search['bulan'] == '07'){echo 'selected';}}else{if(date('m') == '07'){echo 'selected';}} ?>>Juli</option>
                  <option value="08" <?php if($search){if($search['bulan'] == '08'){echo 'selected';}}else{if(date('m') == '08'){echo 'selected';}} ?>>Agustus</option>
                  <option value="09" <?php if($search){if($search['bulan'] == '09'){echo 'selected';}}else{if(date('m') == '09'){echo 'selected';}} ?>>September</option>
                  <option value="10" <?php if($search){if($search['bulan'] == '10'){echo 'selected';}}else{if(date('m') == '10'){echo 'selected';}} ?>>Oktober</option>
                  <option value="11" <?php if($search){if($search['bulan'] == '11'){echo 'selected';}}else{if(date('m') == '11'){echo 'selected';}} ?>>November</option>
                  <option value="12" <?php if($search){if($search['bulan'] == '12'){echo 'selected';}}else{if(date('m') == '12'){echo 'selected';}} ?>>Desember</option>
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <select name="tahun" class="form-control input-sm select2">
                  <?php for($i=2018; $i <= date('Y'); $i++):?>
                    <option value="<?=$i?>" <?php if($search){if($search['tahun'] == $i){echo 'selected';}}else{if(date('Y') == $i){echo 'selected';}} ?>><?=$i?></option>
                  <?php endfor; ?>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3 col-md-offset-1">
              <div class="form-group">
                <select name="id_instansi" class="form-control select2 input-sm" onchange="$('#form-index').submit()">
                  <?php if($this->session->userdata('id_grup') == 3):?>
                    <?php foreach($instansi as $row): ?>
                      <?php if($this->session->userdata('id_instansi') == $row->id_instansi): ?>
                        <option value="<?=$row->id_instansi?>"><?=$row->instansi?></option>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <option value="">-- Semua Instansi --</option>
                    <?php foreach($instansi as $row): ?>
                      <option value="<?=$row->id_instansi?>" <?php if($search){if($search['id_instansi'] == $row->id_instansi){echo 'selected';}} ?>><?=$row->instansi?></option>
                    <?php endforeach;?>
                  <?php endif; ?>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <select name="id_jabatan" class="form-control select2 input-sm" onchange="$('#form-index').submit()">
                  <option value="">-- Semua Jabatan --</option>
                  <?php foreach($jabatan as $row): ?>
                    <option value="<?=$row->id_jabatan?>" <?php if($search){if($search['id_jabatan'] == $row->id_jabatan){echo 'selected';}} ?>><?=$row->jabatan?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <select name="id_pegawai" class="form-control select2 input-sm" onchange="$('#form-index').submit()">
                  <option value="">-- Semua Pegawai --</option>
                  <?php foreach($pegawai as $row): ?>
                    <option value="<?=$row->id_pegawai?>" <?php if($search){if($search['id_pegawai'] == $row->id_pegawai){echo 'selected';}} ?>>
                      <?php 
                        echo $row->gelar_depan.' '.$row->nama;
                        if ($row->gelar_belakang != ''){
                          echo ', '.$row->gelar_belakang;
                        }
                      ?>
                    </option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>
            <div class="col-md-1">
              <div class="input-group pull-right">
                <div class="input-group-btn">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                  <a class="btn btn-default btn-flat" href="<?=base_url().$this->access->controller?>/reset_search"><i class="fa fa-refresh"></i></a>
                </div>
              </div>
            </div>
          </div>
        </form>
        <?php echo $this->session->flashdata('status'); ?>
        <table class="table table-striped table-bordered table-condensed">
          <thead>
            <tr>
              <th class="text-center" width="30">No.</th>
              <th class="text-center" width="20">Status Pegawai</th>
              <th class="text-center">Nama Pegawai</th>
              <th class="text-center">Instansi</th>
              <th class="text-center">Jabatan</th>
              <th class="text-center" width="80">Tanggal</th>
              <th class="text-center" width="80">Tipe</th>
              <th class="text-center" width="150">Keterangan</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($main != null): ?>
              <?php $i=1;foreach ($main as $row): ?>
                <tr>
                  <td class="text-center"><?=$this->uri->segment('3')+$i++?></td>
                  <td class="text-center"><?=$row->status_pegawai?></td>
                  <td>
                    <b>
                    <?php 
                      echo $row->gelar_depan.' '.$row->nama;
                      if ($row->gelar_belakang != ''){
                        echo ', '.$row->gelar_belakang;
                      }
                    ?>
                    </b>
                    <br>
                    <?=$row->nomor_induk?>
                  </td>
                  <td><?=$row->instansi?></td>
                  <td><?=$row->jabatan?></td>
                  <td class="text-center"><?=date_to_id($row->tanggal)?></td>
                  <?php 
                    $col_tipe = '';
                    switch (substr($row->id_tipe,0,1)) {
                      case '0':
                        $col_tipe = 'text-red';
                        break;
                      
                      case '1':
                        $col_tipe = 'text-green';
                        break;
                      
                      case '2':
                        $col_tipe = 'text-orange';
                        break;

                      case '3':
                        $col_tipe = 'text-teal';
                        break;
                      
                      case '4':
                        $col_tipe = 'text-maroon';
                        break;

                      case '5':
                        $col_tipe = 'text-purple';
                        break;

                      case '6':
                        $col_tipe = 'text-navy';
                        break;
                    }
                  ?>
                  <td class="text-center <?=$col_tipe?>">
                    <?php if($row->id_tipe != '1'){
                      echo $row->tipe;
                    }?>
                  </td>
                  <td><?=$row->keterangan?></td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td class="text-center" colspan="99">Tidak ada data!</td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <span class="badge bg-blue">Jumlah Data : <?=$num_rows?></span>
        <?php echo $this->pagination->create_links(); ?>
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
  $(document).ready(function () {
    $('#tanggal').datepicker()
    .on('changeDate', function(e) {
      // $('#form_search').submit();
      window.location.replace("<?=base_url().$this->access->controller.'/search_tanggal/'?>"+e.format());
    });
    $('.select2').on('select2:select', function (e) {
      $('#form_search').submit();
    });
  })
  function del(id) {
    $("#modal_delete").modal('show');
    $("#id").val(id);
  }
</script>