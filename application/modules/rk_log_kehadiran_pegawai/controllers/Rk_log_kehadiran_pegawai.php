<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rk_log_kehadiran_pegawai extends MY_Controller {

	var $access, $id_grup;

  function __construct(){
		parent::__construct();

		$controller = 'rk_log_kehadiran_pegawai';

    if($this->session->userdata('menu') != $controller){
      $this->session->unset_userdata('search');
      $this->session->set_userdata(array('menu' => 'rk_log_kehadiran_pegawai'));
    }
    $this->load->model('ap_konfigurasi/m_ap_konfigurasi');
		$this->id_grup = $this->session->userdata('id_grup');
		$this->access = $this->m_ap_konfigurasi->get_permission($this->id_grup, $controller);
		if ($this->access == null) {
			redirect(base_url().'ap_error/error_403');
		}
		$this->load->model('m_rk_log_kehadiran_pegawai');
		$this->load->model('ms_jabatan/m_ms_jabatan');
		$this->load->model('ms_instansi/m_ms_instansi');
		$this->load->model('ap_profil/m_ap_profil');
		$this->load->model('dt_pegawai/m_dt_pegawai');
	}
	
	public function index()
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Daftar';
		$data['jabatan'] = $this->m_ms_jabatan->get_all();
		$data['instansi'] = $this->m_ms_instansi->get_all();
		$data['pegawai'] = $this->m_dt_pegawai->get_all();

		if ($this->access->_read) {
			$search = $this->session->userdata('search');
			if ($search == null) {
				$search['bulan'] = date('m');
				$search['tahun'] = date('Y');
				$search['id_jabatan'] = '';
				$search['id_pegawai'] = '';
				$search['id_instansi'] = '';
				if ($this->session->userdata('id_grup') == 3) {
					$search['id_instansi']	= $this->session->userdata('id_instansi');
				}
			}
			
			$data['search'] = $search;
	
			$config['base_url'] = base_url().'rk_log_kehadiran_pegawai/index/';
			$config['per_page'] = 10;
	
			$from = $this->uri->segment(3);
			if ($from == '') {
				$from = 0;
			}
	
			$num_rows = $this->m_rk_log_kehadiran_pegawai->num_rows($search);
			$config['total_rows'] = $num_rows;
			$this->pagination->initialize($config);

			$data['main'] = $this->m_rk_log_kehadiran_pegawai->get_list($config['per_page'],$from,$search);
			
			$data['num_rows'] = $num_rows;
			$data['num_rows_total'] = $this->m_rk_log_kehadiran_pegawai->num_rows_total();
			// echo '<pre>' . var_export($data['main'], true) . '</pre>';exit();
			insert_log('read',$this->access->menu);
			$this->view('index',$data);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function search()
	{
		$data = $_POST;
		$this->session->set_userdata(array('search' => $data));
		redirect(base_url().$this->access->controller);
	}

	public function search_tanggal($tanggal)
	{
		$search['tanggal'] = $tanggal;
		if (!$this->session->userdata('search')['id_jabatan']) {
			$search['id_jabatan'] = '';
			$search['id_instansi'] = '';
			if ($this->session->userdata('id_grup') == 3) {
				$search['id_instansi']	= $this->session->userdata('id_instansi');
			}
		}else{
			$search['id_jabatan'] = $this->session->userdata('search')['id_jabatan'];
			$search['id_instansi'] = $this->session->userdata('search')['id_instansi'];
		}
		$this->session->set_userdata(array('search' => $search));
		redirect(base_url().$this->access->controller);
	}

	public function reset_search()
  {
    $this->session->unset_userdata('search');
    redirect(base_url().'rk_log_kehadiran_pegawai/index');
	}
	
	function cetak_pdf()
	{
		//data data
		$profil = $this->m_ap_profil->get_first();

		$instansi_search = 'Semua';
		$jabatan_search = 'Semua';
		$pegawai_search = 'Semua';
		$bulan_search = date('m');
		$tahun_search = date('Y');
		
		$search = $this->session->userdata('search');
		
		if ($search != null) {
			if ($search['id_instansi'] != '') {				
				$instansi_search = $this->m_ms_instansi->get_by_id($search['id_instansi'])->instansi;
			}
			if ($search['id_jabatan'] != '') {
				$jabatan_search = $this->m_ms_jabatan->get_by_id($search['id_jabatan'])->jabatan;
			}
			if ($search['id_pegawai'] != '') {
				$pegawai = $this->m_dt_pegawai->get_by_id($search['id_pegawai']);
				$pegawai_search = $pegawai->gelar_depan.' '.$pegawai->nama;
				if ($pegawai->gelar_belakang != ''){
					$pegawai_search .= ', '.$pegawai->gelar_belakang;
				}
			}
		}else{
			$search['bulan'] = date('m');
			$search['tahun'] = date('Y');
			$search['id_jabatan'] = '';
			$search['id_instansi'] = '';
			$search['id_pegawai'] = '';
			if ($this->session->userdata('id_grup') == 3) {
				$search['id_instansi']	= $this->session->userdata('id_instansi');
				$instansi_search = $this->m_ms_instansi->get_by_id($search['id_instansi'])->instansi;
			}
		}
		//generate pdf
		$this->load->library('pdf');
		$pdf = new Pdf('l','mm',array(210,330));
		$pdf->AliasNbPages();
		$pdf->SetTitle('Rekap Kehadiran Pegawai');
		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetFont('Arial','',13);
		// mencetak string 
		$pdf->Image(base_url().'img/'.$profil->logo,10,10,15,20);
		$pdf->Cell(0,6,$profil->pemda,0,1,'C');
		$pdf->SetFont('Arial','B',15);
		$pdf->Cell(0,6,$profil->nama_instansi,0,1,'C');
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(0,4,$profil->alamat_1,0,1,'C');
		$pdf->Cell(0,4,$profil->alamat_2,0,1,'C');
		$pdf->Line(10, 35, 320, 35);
		// Memberikan space kebawah agar tidak terlalu rapat	
		$pdf->Cell(0,10,'',0,1,'C');
		$pdf->SetFont('Arial','B',12);
		$pdf->Cell(0,5,'REKAP KEHADIRAN PEGAWAI',0,1,'C');
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(40,5,'Bulan');
		$pdf->Cell(5,5,':');
		$pdf->Cell(0,5,month_id($bulan_search)." ".$tahun_search,0,1);
		$pdf->Cell(40,5,'Instansi');
		$pdf->Cell(5,5,':');
		$pdf->Cell(0,5,$instansi_search,0,1);
		$pdf->Cell(40,5,'Jabatan');
		$pdf->Cell(5,5,':');
		$pdf->Cell(0,5,$jabatan_search,0,1);
		$pdf->Cell(40,5,'Pegawai');
		$pdf->Cell(5,5,':');
		$pdf->Cell(0,5,$pegawai_search,0,1);
		//generate tabel
		$pdf->Cell(0,5,'',0,1,'C');
		$pdf->SetFont('Arial','B',9);
		$pdf->SetWidths(array('10','12','65','65','60','20','30','50'));
		$pdf->SetAligns(array('C','C','C','C','C','C','C','C'));
		$pdf->Row(array('No.','Status','Nama','Instansi','Jabatan','Tanggal','Tipe','Keterangan'));
		$pdf->SetAligns(array('C','C','L','L','L','C','C','L'));
		$pdf->SetFont('Arial','',9);
		$pegawai = $this->m_rk_log_kehadiran_pegawai->get_print($search);
		if($pegawai != null){
			$i=1;foreach ($pegawai as $row) {
				$nama = $row->gelar_depan.' '.$row->nama;
				if ($row->gelar_belakang != ''){
					$nama .= ', '.$row->gelar_belakang;
				}
				$pdf->Row(array(
					$i++,
					$row->status_pegawai,
					$nama."\n".$row->nomor_induk,
					$row->instansi,
					$row->jabatan,
					date_to_id($row->tanggal),
					$row->tipe,
					$row->keterangan
				));
			}
			$total = $this->m_rk_log_kehadiran_pegawai->count_print($search);
			$pdf->Cell(0,10,'',0,1,'');
			$pdf->SetWidths(array('60','5','10'));
			$pdf->SetAligns(array('L','C','R'));
			$jumlah_total = 0;
			foreach ($total as $row) {
				$pdf->Row(array($row->tipe,':',$row->total));
			}
			$pdf->SetFont('Arial','B',9);
			//direktur
			$direktur = $this->m_ap_profil->get_direktur();
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(500,5,'Mengetahui,	',0,1,'C');
			$pdf->Cell(500,5,$profil->kop_ttd_kepala,0,1,'C');
			$pdf->Cell(500,20,'',0,1,'C');
			$pdf->SetFont('Arial','U',9);
			if ($direktur != null) {
				$direktur_nama = $direktur->gelar_depan.' '.$direktur->nama;
				if ($direktur->gelar_belakang != ''){
					$direktur_nama .= ', '.$direktur->gelar_belakang;
				}
				$pdf->Cell(500,3,$direktur_nama,0,1,'C');
				$pdf->SetFont('Arial','',9);
				$pdf->Cell(500,5,'NIP. '.$direktur->nomor_induk,0,1,'C');
			}
		}
		
		$pdf->Output('I',date('Ymdhis').'_rekap_kehadiran_pegawai.pdf');
	}

}