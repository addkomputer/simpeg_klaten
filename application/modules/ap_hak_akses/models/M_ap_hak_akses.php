<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ap_hak_akses extends CI_Model {

  public function get_by_id($id)
  {
    $query = $this->db->query(
      "SELECT a.*, b.id_grup, b._create, b._read, b._update, b._delete
      FROM 
        ap_menu a
      LEFT JOIN ap_hak_akses b ON a.id_menu = b.id_menu AND b.id_grup = '$id'
      ORDER BY a.id_menu ASC"
    );
    return $query->result();
  }

  public function insert($data)
  {
    $this->db->insert('ap_hak_akses',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_grup',$id)->update('ap_hak_akses',$data);
  }

  public function empty_by_id_grup($id)
  {
    $this->db->where('id_grup',$id)->delete('ap_hak_akses');
  }

}
