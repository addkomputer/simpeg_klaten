<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ap_hak_akses extends MY_Controller {

	var $access, $id_grup;

  function __construct(){
		parent::__construct();

		$menu_controller = 'ap_hak_akses';

    if($this->session->userdata('menu') != $menu_controller){
      $this->session->unset_userdata('search_term');
      $this->session->set_userdata(array('menu' => 'ap_hak_akses'));
    }
    $this->load->model('ap_konfigurasi/m_ap_konfigurasi');
		$this->id_grup = $this->session->userdata('id_grup');
		$this->access = $this->m_ap_konfigurasi->get_permission($this->id_grup, $menu_controller);
		if ($this->access == null) {
			redirect(base_url().'ap_error/error_403');
		}else{
			$this->load->model('m_ap_hak_akses');
			$this->load->model('ap_grup/m_ap_grup');
		}
	}
	
	public function index()
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Daftar';
 
		if ($this->access->_read) {
			if($this->input->post('search_term')){
				$search_term = $this->input->post('search_term');
				$this->session->set_userdata(array('search_term' => $search_term));
			}
	
			$config['base_url'] = base_url().'ap_hak_akses/index/';
			$config['per_page'] = 10;
	
			$from = $this->uri->segment(3);
	
			if($this->session->userdata('search_term') == null){
				$num_rows = $this->m_ap_grup->num_rows();
	
				$config['total_rows'] = $num_rows;
				$this->pagination->initialize($config);
	
				$data['grup'] = $this->m_ap_grup->get_list($config['per_page'],$from,$search_term = null);
			}else{
				$search_term = $this->session->userdata('search_term');
				$num_rows = $this->m_ap_grup->num_rows($search_term);
				$config['total_rows'] = $num_rows;
				$this->pagination->initialize($config);
	
				$data['grup'] = $this->m_ap_grup->get_list($config['per_page'],$from,$search_term);
			}
			insert_log('read',$this->access->menu);
			$data['num_rows'] = $num_rows;
			$this->view('index',$data);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}
	
	public function form($id = null)
	{
		$data['title'] = $this->access->menu;
		$data['access'] = $this->access;
		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Tambah Data';
				$data['action'] = 'insert';
				$data['hak_akses'] = null;
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Ubah Data';
				$data['action'] = 'update';
				$data['grup'] = $this->m_ap_grup->get_by_id($id);
				$data['hak_akses'] = $this->m_ap_hak_akses->get_by_id($id);
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		}
	}
	
	public function update()
	{
		$data = $_POST;
		if ($data != null) {
			if($this->access->_update){
				$this->m_ap_hak_akses->empty_by_id_grup($data['id_grup']);

				$data2 = array();

				if(isset($data['_create'])){
					foreach ($data['_create'] as $row) {
						$data2[$row]['_create'] = 1;
					}
				}

				if(isset($data['_read'])){
					foreach ($data['_read'] as $row) {
						$data2[$row]['_read'] = 1;
					}
				}

				if(isset($data['_update'])){
					foreach ($data['_update'] as $row) {
						$data2[$row]['_update'] = 1;
					}
				}

				if(isset($data['_delete'])){
					foreach ($data['_delete'] as $row) {
						$data2[$row]['_delete'] = 1;
					}
				}

				foreach ($data2 as $key => $val) {
					$val['id_menu'] = $key;
					$val['id_grup'] = $data['id_grup'];
					$val['created_by'] = $this->session->userdata('nama');
					$this->m_ap_hak_akses->insert($val);
				}
				$data['updated_by'] = $this->session->userdata('nama');
				insert_log('update',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				redirect(base_url().'ap_hak_akses/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

}