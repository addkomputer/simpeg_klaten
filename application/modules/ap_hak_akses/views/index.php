<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-<?=$access->icon?> text-red"></i> <?=$access->menu?>
      <small><?=$subtitle?></small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">Pengaturan</li>
      <li class="active text-orange"><?=$access->menu?></li>
    </ol>    
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box box-danger">
      <div class="box-header with-border">
        <i class="fa fa-table text-maroon"></i>
        <h3 class="box-title">Tabel <?=$access->menu?></h3>
      </div>
      <div class="box-body table-responsive">
        <?php echo $this->session->flashdata('status'); ?>
        <table class="table table-striped table-bordered table-condensed">
          <thead>
            <tr>
              <th class="text-center" width="30">No.</th>
              <th class="text-center" width="60">Aksi</th>
              <th class="text-center">Nama</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($grup != null): ?>
              <?php $i=1;foreach ($grup as $row): ?>
                <tr>
                  <td class="text-center"><?=$this->uri->segment('3')+$i++?></td>
                  <td class="text-center">
                    <?php if($row->id_grup != 0): ?>
                      <a class="btn btn-xs btn-flat btn-success" href="<?=base_url()?>ap_hak_akses/form/<?=$row->id_grup?>" style="padding-right:0.250em;"><i class="fa fa-list"></i></a> 
                    <?php endif;?>
                  </td>
                  <td><?=$row->grup?></td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td class="text-center" colspan="99">Tidak ada data!</td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <span class="badge bg-purple">Jumlah Data : <?=$num_rows?></span>
        <?php echo $this->pagination->create_links(); ?>
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<script type="text/javascript">
  function del(id) {
    $("#modal_delete").modal('show');
    $("#id_grup").val(id);
  }
</script>