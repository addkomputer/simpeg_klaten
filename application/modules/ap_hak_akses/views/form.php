<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-red"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">Administrasi Kehadiran</li>
        <li class="active text-orange"><?=$access->menu?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-danger">
        <div class="box-header with-border">
          <i class="fa fa-table text-maroon"></i>
          <h3 class="box-title">Form <?=$access->menu?></h3>
        </div>
        <form id="form" class="form-horizontal" action="<?=base_url()?>ap_hak_akses/<?=$action?>" method="post">
          <div class="box-body">
            <h4>Nama Role : <?=$grup->grup?></h4>
            <input type="hidden" name="id_grup" id="id_grup" value="<?=$grup->id_grup?>">
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" width="80">Kode</th>
                    <th class="text-center">Nama Modul</th>
                    <th class="text-center" width="80">Akses</th>
                    <th class="text-center" width="80">Tambah</th>
                    <th class="text-center" width="80">Ubah</th>
                    <th class="text-center" width="80">Hapus</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if ($hak_akses != null): ?>
                    <?php $i=1;foreach ($hak_akses as $row): ?>
                      <tr>
                        <td><?=$row->id_menu?></td>
                        <td>
                          <?php 
                            for ($j=1; $j < strlen($row->id_menu)-2; $j++) { 
                              echo '-';
                            };
                          ?>
                          <?=$row->menu?>
                        </td>
                        <td class="text-center">
                          <input class="iCheckbox read-<?=$i?>" onclick="check_all_row('<?=$i?>')" type="checkbox" name="_read[]" value="<?=$row->id_menu?>" <?php if($row->_read == 1){echo 'checked';}?>>
                        </td>
                        <td class="text-center">
                          <input class="iCheckbox create-<?=$i?>" type="checkbox" name="_create[]" value="<?=$row->id_menu?>" <?php if($row->_create == 1){echo 'checked';}?>>
                        </td>
                        <td class="text-center">
                          <input class="iCheckbox update-<?=$i?>" type="checkbox" name="_update[]" value="<?=$row->id_menu?>" <?php if($row->_update == 1){echo 'checked';}?>>
                        </td>
                        <td class="text-center">
                          <input class="iCheckbox delete-<?=$i?>" type="checkbox" name="_delete[]" value="<?=$row->id_menu?>" <?php if($row->_delete == 1){echo 'checked';}?>>
                        </td>
                      </tr>
                    <?php $i++;endforeach; ?>
                  <?php else: ?>
                    <tr>
                      <td class="text-center" colspan="99">Tidak ada data!</td>
                    </tr>
                  <?php endif; ?>
                </tbody>
              </table>
            </div>
            <div class="form-group pull-right">
              <div class="col-md-12">
                <button class="btn btn-flat btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button>
                <a class="btn btn-flat btn-default" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Batal</a>
              </div>
            </div>
          </div>
        </form>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<script>
  $(document).ready(function () {
    // form validator
    $('#form').validator();
  })
</script>