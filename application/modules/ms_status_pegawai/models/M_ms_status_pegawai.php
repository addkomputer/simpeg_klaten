<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ms_status_pegawai extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('status_pegawai')
      ->get('ms_status_pegawai',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('status_pegawai')
      ->get('ms_status_pegawai')
      ->num_rows();
  }
  
  function num_rows_total(){
    return $this->db
      ->where('is_deleted','0')
      ->order_by('status_pegawai')
      ->get('ms_status_pegawai')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('ms_status_pegawai')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id_status_pegawai',$id)->get('ms_status_pegawai')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_status_pegawai','asc')->get('ms_status_pegawai')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_status_pegawai','desc')->get('ms_status_pegawai')->row();
  }

  public function insert($data)
  {
    $this->db->insert('ms_status_pegawai',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_status_pegawai',$id)->update('ms_status_pegawai',$data);
  }

  public function delete_temp($id)
  {
    $this->db->where('id_status_pegawai',$id)->update('ms_status_pegawai',array('is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_status_pegawai',$id)->delete('ms_status_pegawai');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ms_status_pegawai");
  }

}
