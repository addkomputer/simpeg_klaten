<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_tb_log_tanpa_keterangan extends CI_Model {

  private $suffix,$bulan,$tahun;

  function __construct(){
    parent::__construct();
    if ($this->session->userdata('search')) {
      $this->bulan = $this->session->userdata('search')['bulan'];
      $this->tahun = $this->session->userdata('search')['tahun'];
      $this->suffix = $this->tahun.'_'.$this->bulan;
    }else{
      $this->bulan = date('m');
      $this->tahun = date('Y');
      $this->suffix = date('Y_m');
    }
    $this->create_table($this->suffix);
  }
  
  public function create_table()
	{
		$this->db->query(
			"CREATE TABLE IF NOT EXISTS `tb_log_kehadiran_$this->suffix` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`id_pegawai` varchar(50) NOT NULL DEFAULT '0',
				`nomor_induk` varchar(50) NOT NULL DEFAULT '0',
				`pin` varchar(50) NOT NULL DEFAULT '0',
				`id_tipe` int(1) NOT NULL DEFAULT '0',
				`tanggal` date DEFAULT NULL,
				`jam_datang` time DEFAULT NULL,
				`jam_batas_datang` time DEFAULT NULL,
				`jam_datang_pegawai` time DEFAULT NULL,
				`terlambat_datang` int(11) NOT NULL DEFAULT 0,
				`ket_datang` text,
				`jam_pulang` time DEFAULT NULL,
				`jam_batas_pulang` time DEFAULT NULL,
				`jam_pulang_pegawai` time DEFAULT NULL,
				`ket_pulang` text,
				`ket_umum` text,
				PRIMARY KEY (`id`)
			) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;"
		);
	}

	public function get_list($number,$offset,$search = null)
  {
    $where = "WHERE a.id_tipe = 0 AND ";
    $where .= "a.tanggal LIKE '".$this->tahun.'-'.$this->bulan."%'";
    if ($search != null) {
      if ($search['id_jabatan'] != '') {
        $where .= " AND b.id_jabatan = '".$search['id_jabatan']."'";
      }
      if ($search['id_ruang'] != '') {
        $where .= " AND b.id_ruang = '".$search['id_ruang']."'";
      }
      if ($search['term'] != '') {
        $where .= " AND b.nama LIKE '%".$search['term']."%'";
        $where .= " OR b.nomor_induk LIKE '%".$search['term']."%'";
      }
    }
    
    $query = $this->db->query(
      "SELECT 
        a.*,
        b.gelar_depan, b.nama, b.gelar_belakang,
        c.jabatan,
        d.tipe,
        e.ruang
      FROM tb_log_kehadiran_$this->suffix a 
        RIGHT JOIN dt_pegawai b ON a.nomor_induk = b.nomor_induk
        LEFT JOIN ms_jabatan c ON b.id_jabatan = c.id_jabatan
        JOIN tb_log_tipe d ON a.id_tipe=d.id_tipe 
        LEFT JOIN ms_ruang e ON b.id_ruang = e.id_ruang
      $where
      ORDER BY
        jam_datang_pegawai DESC
      LIMIT $offset,$number"
    );

    return $query->result();
  }

  function num_rows($search = null){
		$where = "WHERE a.id_tipe = 0 AND ";
    $where .= "a.tanggal LIKE '".$this->tahun.'-'.$this->bulan."%'";
    if ($search != null) {
      if ($search['id_jabatan'] != '') {
        $where .= " AND b.id_jabatan = '".$search['id_jabatan']."'";
      }
      if ($search['id_ruang'] != '') {
        $where .= " AND b.id_ruang = '".$search['id_ruang']."'";
      }
      if ($search['term'] != '') {
        $where .= " AND b.nama LIKE '%".$search['term']."%'";
        $where .= " OR b.nomor_induk LIKE '%".$search['term']."%'";
      }
    }
    
    $query = $this->db->query(
      "SELECT 
        a.*,
        b.gelar_depan, b.nama, b.gelar_belakang,
        c.jabatan,
        d.tipe,
        e.ruang
      FROM tb_log_kehadiran_$this->suffix a 
        RIGHT JOIN dt_pegawai b ON a.nomor_induk = b.nomor_induk
        LEFT JOIN ms_jabatan c ON b.id_jabatan = c.id_jabatan
        JOIN tb_log_tipe d ON a.id_tipe=d.id_tipe 
        LEFT JOIN ms_ruang e ON b.id_ruang = e.id_ruang
      $where
      ORDER BY
        jam_datang_pegawai DESC"
    );

    return $query->num_rows();
  }
  
  function num_rows_total(){  
    $where = "WHERE a.id_tipe = 0 AND ";
    $where .= "a.tanggal LIKE '".$this->tahun.'-'.$this->bulan."%'";
    
    $query = $this->db->query(
      "SELECT 
        a.*,
        b.gelar_depan, b.nama, b.gelar_belakang,
        c.jabatan,
        d.tipe,
        e.ruang
      FROM tb_log_kehadiran_$this->suffix a 
        RIGHT JOIN dt_pegawai b ON a.nomor_induk = b.nomor_induk
        LEFT JOIN ms_jabatan c ON b.id_jabatan = c.id_jabatan
        JOIN tb_log_tipe d ON a.id_tipe=d.id_tipe 
        LEFT JOIN ms_ruang e ON b.id_ruang = e.id_ruang
      $where
      ORDER BY
        jam_datang_pegawai DESC"
    );

    return $query->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_active','1')
			->get('tb_log_kehadiran_'.$this->suffix)->result();
  }

  public function get_by_id($id)
  {
    return $this->db->where('id',$id)->get('tb_log_kehadiran_'.$this->suffix)->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id','asc')->get('tb_log_kehadiran_'.$this->suffix)->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id','desc')->get('tb_log_kehadiran_'.$this->suffix)->row();
  }

  public function insert($data)
  {
    $this->db->insert('tb_log_kehadiran_'.$this->suffix,$data);
    $log = array(
      'nomor_induk' => $data['nomor_induk'],
      'tanggal' => $data['tanggal'],
      'id_tipe' => $data['id_tipe']
    );
    $cek_log = $this->db
      ->where('nomor_induk',$data['nomor_induk'])
      ->where('tanggal',$data['tanggal'])
      ->get('tb_log_kehadiran_'.$this->suffix)
      ->row();
    if ($cek_log == NULL) {
      //insert
      $this->db->insert('tb_log_kehadiran_'.$this->suffix,$log);
    }else{
      //update
      $this->db
        ->where('nomor_induk',$data['nomor_induk'])
        ->where('tanggal',$data['tanggal'])
        ->update('tb_log_kehadiran_'.$this->suffix,$log);
    }
  }

  public function update($id,$data)
  {
    $this->db->where('id',$id)->update('tb_log_kehadiran_'.$this->suffix,$data);
    $log = array(
      'nomor_induk' => $data['nomor_induk'],
      'tanggal' => $data['tanggal'],
      'id_tipe' => $data['id_tipe']
    );
    $cek_log = $this->db
      ->where('nomor_induk',$data['nomor_induk'])
      ->where('tanggal',$data['tanggal'])
      ->get('tb_log_kehadiran_'.$this->suffix)
      ->row();
    if ($cek_log == NULL) {
      //insert
      $this->db->insert('tb_log_kehadiran_'.$this->suffix,$log);
    }else{
      //update
      $this->db
        ->where('nomor_induk',$data['nomor_induk'])
        ->where('tanggal',$data['tanggal'])
        ->update('tb_log_kehadiran_'.$this->suffix,$log);
    }
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id',$id)->update('tb_log_kehadiran_'.$this->suffix,array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $data = $this->db->where('id',$id)->get('tb_log_kehadiran_'.$this->suffix)->row();
    $this->db
      ->where('nomor_induk',$data->nomor_induk)
      ->where('tanggal',$data->tanggal)
      ->update('tb_log_kehadiran_'.$this->suffix,array('ket_umum' => '','id_tipe' => 1));
    $this->db->where('id',$id)->delete('tb_log_kehadiran_'.$this->suffix);
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE tb_log_kehadiran_2018_10");
  }

}
