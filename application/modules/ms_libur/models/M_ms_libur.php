<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ms_libur extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('tanggal','asc')
      ->get('ms_libur',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('libur')
      ->get('ms_libur')
      ->num_rows();
  }
  
  function num_rows_total(){
    return $this->db
      ->where('is_deleted','0')
      ->order_by('libur')
      ->get('ms_libur')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
      ->where('is_active','1')
      ->order_by('tanggal','asc')
			->get('ms_libur')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id_libur',$id)->get('ms_libur')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_libur','asc')->get('ms_libur')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_libur','desc')->get('ms_libur')->row();
  }

  public function insert($data)
  {
    $this->db->insert('ms_libur',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_libur',$id)->update('ms_libur',$data);
  }

  public function delete_temp($id)
  {
    $this->db->where('id_libur',$id)->update('ms_libur',array('is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_libur',$id)->delete('ms_libur');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ms_libur");
  }

}
