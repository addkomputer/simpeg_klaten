<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tb_monitoring extends MY_Controller {

	var $access, $id_grup;

  function __construct(){
		parent::__construct();

		$controller = 'tb_monitoring';

    if($this->session->userdata('menu') != $controller){
      $this->session->unset_userdata('search');
      $this->session->set_userdata(array('menu' => 'tb_monitoring'));
    }
    $this->load->model('ap_konfigurasi/m_ap_konfigurasi');
		$this->id_grup = $this->session->userdata('id_grup');
		$this->access = $this->m_ap_konfigurasi->get_permission($this->id_grup, $controller);
		if ($this->access == null) {
			redirect(base_url().'ap_error/error_403');
		}
    $this->load->model('m_tb_device');
	}

	public function index()
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Daftar';

		if ($this->access->_read) {
			$search = null;
			if($search = $_POST){
				$this->session->set_userdata(array('search' => $search));
			}else{
				if($this->session->userdata('search') != null){
					$search = $this->session->userdata('search');
				}
			}
			$data['search'] = $search;

			$config['base_url'] = base_url().'tb_monitoring/index/';
			$config['per_page'] = 50;

			$from = $this->uri->segment(3);

			$data['device_one']=$this->m_tb_device->get_device_one();

			if($search == null){
				$num_rows = $this->m_tb_device->num_rows();

				$config['total_rows'] = $num_rows;
				$this->pagination->initialize($config);

				$data['device'] = $this->m_tb_device->get_list($config['per_page'],$from,$search = null);
			}else{
				$search = $this->session->userdata('search');
				$num_rows = $this->m_tb_device->num_rows($search);
				$config['total_rows'] = $num_rows;
				$this->pagination->initialize($config);


				$data['device'] = $this->m_tb_device->get_list($config['per_page'],$from,$search);
			}
			$data['num_rows'] = $num_rows;
			$data['num_rows_total'] = $this->m_tb_device->num_rows_total();
			insert_log('read',$this->access->menu);
			$this->view('index',$data);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function form($id = null)
	{
		$data['access'] = $this->access;
		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Tambah Data';
				$data['action'] = 'insert';
				$data['device'] = null;
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Info Alat Fingerprint';

				$data['device'] = $this->m_tb_device->get_by_id($id);
				$data['raw'] = null;
				$scanlog = webservice($data['device']->server_port,'localhost:8080/dev/info','sn='.$data['device']->device_sn);
				$data['raw'] = json_decode($scanlog, true);
				if(@$data['raw']['Result'] == true){
					$up = array(
						'timestamp' => date('Y-m-d H:i:s'),
						'is_active' => 1
					);
					$this->m_tb_device->update($id,$up);
				}else if(@$data['raw']['Result'] == false || @$data['raw'] == null){
					$up = array(
						'is_active' => 0
					);
					$this->m_tb_device->update($id,$up);
				}
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}

		public function form_delete($id = null)
	{
		$data['access'] = $this->access;
		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Tambah Data';
				$data['action'] = 'insert';
				$data['device'] = null;
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Info Alat Fingerprint';

				$data['device'] = $this->m_tb_device->get_by_id($id);
				$scanlogs = webservice($data['device']->server_port,'localhost:8080/scanlog/del','sn='.$data['device']->device_sn);
				$scanlog = webservice($data['device']->server_port,'localhost:8080/dev/info','sn='.$data['device']->device_sn);
				$data['raw'] = json_decode($scanlog, true);
				// var_dump($data['raw']);

				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data Scanlog berhasil dihapus!</div>');
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}

	public function form_syn_time($id = null)
	{
		$data['access'] = $this->access;
		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Tambah Data';
				$data['action'] = 'insert';
				$data['device'] = null;
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Info Alat Fingerprint';

				$data['device'] = $this->m_tb_device->get_by_id($id);
				$scanlogs = webservice($data['device']->server_port,'localhost:8080/dev/settime','sn='.$data['device']->device_sn);
				$scanlog = webservice($data['device']->server_port,'localhost:8080/dev/info','sn='.$data['device']->device_sn);
				$data['raw'] = json_decode($scanlog, true);
				// var_dump($data['raw']);

				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Waktu di Alat berhasil disinkonisasi!</div>');
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}

	public function getUser($id = null)
{
			$data['access'] = $this->access;
			$datax['device'] = $this->m_tb_device->get_by_id($id);
			$pagingLimit=25;
			$parameter = "sn=".$datax['device']->device_sn."&limit=".$pagingLimit;
			$scanlog = webservice($datax['device']->server_port,'localhost:8080/user/all/paging',$parameter);
			$content=json_decode($scanlog);
			foreach($content->Data as $entry){
				
				if($this->m_tb_device->is_user_exist($entry->PIN) == false) {
					$dataz['pin'] = $entry->PIN;
					$dataz['nama'] = $entry->Name;
					$dataz['rfid'] = $entry->RFID;
					$dataz['pwd'] = $entry->Password;
					$dataz['privilege'] = $entry->Privilege;
					$dataz['device_sn']= $datax['device']->device_sn;
					$this->m_tb_device->insertUser($dataz);

					foreach($entry->Template as $values){
						$datas['pin'] = $values->pin;
						$datas['finger_idx'] = $values->idx;
						$datas['alg_ver'] = $values->alg_ver;
						$datas['template'] = $values->template;
						$this->m_tb_device->insertTemplate($datas);
					}
				}
				
			}
			
			$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data User berhasil diambil!</div>');
			redirect(base_url().'tb_monitoring/index');
}

	public function reset_search()
  {
    $this->session->unset_userdata('search');
    redirect(base_url().'tb_device/index');
  }

	public function insert()
	{
		$data = $_POST;
		if ($data != null) {
			$data['created_by'] = $this->session->userdata('nama');
			if(!isset($data['is_active'])){$data['is_active'] = 0;}
			$this->m_tb_device->insert($data);
			insert_log('insert',$this->access->menu);
			$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
			redirect(base_url().'tb_device/index');
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function update()
	{
		$data = $_POST;
		if ($data != null) {
			if($this->access->_update){
				$data['updated_by'] = $this->session->userdata('nama');
				$id = $data['id_device'];
				$this->m_tb_device->update($id, $data);
				insert_log('update',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				redirect(base_url().'tb_device/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function updatea()
	{
		$data = $_POST;
		if ($data != null) {
			if($this->access->_update){
				$data['updated_by'] = $this->session->userdata('nama');
				// if(!isset($data['is_active'])){$data['is_active'] = 0;}
				$id = 1;
				$this->m_tb_device->update($id, $data);
				$end=$this->m_tb_device->num_rows_total();
				for ($i=2; $i<=$end ; $i++) {
					$device=$this->m_tb_device->get_by_no($i-1);
					$jadwal1 = strtotime($device->jadwal1);
					$jadwal2 = strtotime($device->jadwal2);
					$jadwal3 = strtotime($device->jadwal3);
					$jadwal['jadwal1'] = date("H:i", strtotime('+3 minutes', $jadwal1));
					$jadwal['jadwal2'] = date("H:i", strtotime('+3 minutes', $jadwal2));
					$jadwal['jadwal3'] = date("H:i", strtotime('+3 minutes', $jadwal3));
					$id = $i;
					$this->m_tb_device->update($id, $jadwal);
				}
				insert_log('update',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				redirect(base_url().'tb_monitoring/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function delete()
	{
		$data = $_POST;
		if($data != null){
			if ($this->access->_delete) {
				$this->m_tb_device->delete_temp($data['id_device']);
				insert_log('delete',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				redirect(base_url().'tb_device/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}

}
