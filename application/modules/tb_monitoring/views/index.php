<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
      <small><?=$subtitle?></small>
      <div class="label bg-orange pull-right"><i class="fa fa-database"></i> Jumlah Keseluruhan Data : <?=$num_rows_total?></div>
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box box-primary">
      <div class="box-body table-responsive">

        <div>
          <form action="<?=base_url()?>tb_monitoring/updatea/" method="post">
            <div class="input-group pull-right" style="width: 400px;">
              <div class="input-group">
                <input type="text" class="form-control timepicker" style="width:80px" name="jadwal1" value="<?=$device_one->jadwal1?>" required>
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-time"></span>
                </span>
                <input type="text" class="form-control timepicker" style="width:80px" name="jadwal2" value="<?=$device_one->jadwal2?>" required>
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-time"></span>
                </span>
                <input type="text" class="form-control timepicker" style="width:80px" name="jadwal3" value="<?=$device_one->jadwal3?>" required>
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-time"></span>
                </span>
              </div>
              <div class="input-group-btn">
                <button type="submit" class="btn btn-flat btn-primary">Atur</button>
                <!-- <a class="btn btn-default" href="<?=base_url()?>tb_device/reset_search"><i class="fa fa-refresh"></i></a> -->
              </div>
            </div>
          </form>
        </div>
        <br><br>
        <?php echo $this->session->flashdata('status'); ?>
        <?php if ($search != null): ?>
          <i class="search_result">Hasil pencarian dengan kata kunci: <b><?=$search['term'];?></b></i><br><br>
        <?php endif; ?>
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
                <th class="text-center" width="30">No.</th>
                <th class="text-center">Aksi</th>
                <th class="text-center">Device SN</th>
                <th class="text-center">Device IP</th>
                <th class="text-center">Lokasi</th>
                <th class="text-center">Jadwal 1</th>
                <th class="text-center">Jadwal 2</th>
                <th class="text-center">Jadwal 3</th>
                <th class="text-center">Terakhir Diakses</th>
                <th class="text-center">Status</th>
              </tr>
            </thead>
            <tbody>
              <?php if ($device != null): ?>
                <?php $i=1;foreach ($device as $row): ?>
                  <tr>
                    <td class="text-center"><?=$this->uri->segment('3')+$i++?></td>
                    <td class="text-center">
                      <?php if($row->id_device != 0): ?>
                        <a class="btn btn-xs btn-primary btn-flat" href="<?=base_url()?>tb_monitoring/form/<?=$row->id_device?>"><i class="fa fa-info"></i> Info</a>
                        <a class="btn btn-xs btn-success btn-flat" href="<?=base_url()?>tb_monitoring/getUser/<?=$row->id_device?>"><i class="fa fa-users"></i> Get User</a>
                      <?php endif;?>
                    </td>
                    <td class="text-center"><?=$row->device_sn?></td>
                    <td class="text-center"><?=$row->device_IP?></td>
                    <td><?=$row->lokasi?></td>
                    <td class="text-center text-blue"><?=$row->jadwal1?></td>
                    <td class="text-center text-blue"><?=$row->jadwal2?></td>
                    <td class="text-center text-blue"><?=$row->jadwal3?></td>
                    <td class="text-center">
                      <span class="label <?php echo $color = ($row->is_active == 1) ? 'bg-green' : 'bg-red' ;?>">
                        <?=time_elapsed_string($row->timestamp)?>
                      </span>
                    </td>
                    <!-- <td class="text-center"><?=date('d-m-Y h:i:s', strtotime($row->timestamp));?></td> -->
                    <td class="text-center">
                      <?php if ($row->is_active == 1): ?>
                        <span class="label bg-green">Aktiv</span>
                      <?php else: ?>
                        <span class="label bg-red">Tidak Aktiv</span>
                      <?php endif; ?>
                    </td>
                  </tr>
                <?php endforeach; ?>
              <?php else: ?>
                <tr>
                  <td class="text-center" colspan="6">Tidak ada data!</td>
                </tr>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <span class="badge bg-blue">Jumlah Data : <?=$num_rows?></span>
        <?php echo $this->pagination->create_links(); ?>
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- Modal Delete -->
<div id="modal_delete" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>tb_device/delete" method="post">
        <input type="hidden" name="id_device" id="id_device">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
  function del(id) {
    $("#modal_delete").modal('show');
    $("#id_device").val(id);
  }

  $('.clockpicker').clockpicker();
</script>
