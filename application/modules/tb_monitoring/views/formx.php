<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-primary">
        
          <div class="box-body">
            <input type="hidden" class="form-control input-sm" name="id_device" id="id_device" value="<?php if($device != null){echo $device->device_sn;};?>">
            <?php if($raw["Result"]): ?>
              
            <div class="form-group">
              <label class="col-sm-2 control-label">Jam</label>
              <div class="col-sm-3">
                00/00/0000 00:00:00
              </div>
            </div>
            <br>
            <div class="form-group">
              <label class="col-sm-2 control-label">Lokasi</label>
              <div class="col-sm-3">
                <?=$device->lokasi?>
              </div>
            </div>
            <br>
            <div class="form-group">
              <label class="col-sm-2 control-label">Admin</label>
              <div class="col-sm-3">
                0
              </div>
            </div>
            <br>
            <div class="form-group">
              <label class="col-sm-2 control-label">User</label>
              <div class="col-sm-3">
                0
              </div>
            </div>
            <br>
            <div class="form-group">
              <label class="col-sm-2 control-label">Total Presensi</label>
              <div class="col-sm-3">
                0
              </div>
            </div>
            <br>
            <div class="form-group">
              <label class="col-sm-2 control-label">Absensi Baru</label>
              <div class="col-sm-5">
                0
              </div>
            </div>
            
            <br>
            <hr>
            <div class="form-group">
              <div class="col-md-offset-2 col-sm-8">
                <button class="btn btn-sm btn-danger"><i class="fa fa-warning"></i> Hapus Log</button>
                <a class="btn btn-sm btn-success" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Kembali</a>
              </div>
            </div>
          </div>
            <?php else: ?>
            <div class="alert alert-danger">Perangkat tidak bisa dihapus, Silahkan Cek Monitoring Alat Fingerprint!  
              <a class="btn btn-sm btn-success" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Kembali</a>
            </div>
            <?php endif;?>
       
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<script>
  $(document).ready(function () {
    // form validator
    $('#form').validate();
  })
</script>
