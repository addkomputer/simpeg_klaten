<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">Perangkat Presensi</li>
        <li class="active text-orange"><?=$access->menu?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-primary">
        <div class="box-header with-border">
          <i class="fa fa-table text-maroon"></i>
          <h3 class="box-title">Form <?=$access->menu?></h3>
        </div>
        <form id="form" class="form-horizontal" action="<?=base_url()?>tb_device/<?=$action?>" method="post" autocomplete="off">
          <div class="box-body">
            <input type="hidden" class="form-control" name="id_device" id="id_device" value="<?php if($device != null){echo $device->id_device;};?>">
            <div class="form-group">
              <label class="col-sm-2 control-label">Device No</label>
              <div class="col-sm-1">
                <input type="text" class="form-control" name="device_no" value="<?php if($device != null){echo $device->device_no;};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Server IP</label>
              <div class="col-sm-2">
                <input type="text" class="form-control" name="server_IP" value="<?php if($device != null){echo $device->server_IP;};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Server Port</label>
              <div class="col-sm-2">
                <input type="number" class="form-control" name="server_port" value="<?php if($device != null){echo $device->server_port;};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Device SN</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" name="device_sn" value="<?php if($device != null){echo $device->device_sn;};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Device IP</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" name="device_IP" value="<?php if($device != null){echo $device->device_IP;};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Device Key</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="device_key" value="<?php if($device != null){echo $device->device_key;};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Lokasi</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" name="lokasi" value="<?php if($device != null){echo $device->lokasi;};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Aktif</label>
              <div class="col-sm-5">
                <input class="iCheckbox" type="checkbox" name="is_active" value="1" <?php if($device != null){if($device->is_active == 1){echo 'checked';}}else{echo 'checked';}?>>
              </div>
            </div>
            <hr>
            <div class="form-group">
              <div class="col-md-offset-2 col-sm-8">
                <button class="btn btn-flat btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button>
                <a class="btn btn-flat btn-default" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Batal</a>
              </div>
            </div>
          </div>
        </form>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<script>
  $(document).ready(function () {
    // form validator
    $('#form').validate();
  })
</script>
