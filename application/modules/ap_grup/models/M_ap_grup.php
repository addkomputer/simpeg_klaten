<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ap_grup extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_grup')
      ->get('ap_grup',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_grup')
      ->get('ap_grup')
      ->num_rows();
  }
  
  function num_rows_total(){  
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_grup')
      ->get('ap_grup')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('ap_grup')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id_grup',$id)->get('ap_grup')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_grup','asc')->get('ap_grup')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_grup','desc')->get('ap_grup')->row();
  }

  public function insert($data)
  {
    $this->db->insert('ap_grup',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_grup',$id)->update('ap_grup',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id_grup',$id)->update('ap_grup',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_grup',$id)->delete('ap_grup');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ap_grup");
  }

}
