<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-red"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">Pengaturan</li>
        <li class="active text-orange"><?=$access->menu?></li>
      </ol>
    </section>

    <!-- profil content -->
    <section class="content">
      <div class="box box-danger">
        <div class="box-header with-border">
          <i class="fa fa-table text-maroon"></i>
          <h3 class="box-title">Form <?=$access->menu?></h3>
        </div>
        <form id="form" class="form-horizontal" action="<?=base_url()?>ap_profil/<?=$action?>" method="post" autocomplete="off" enctype="multipart/form-data">
          <div class="box-body">
            <input type="hidden" class="form-control" name="id_profil" id="id_profil" value="<?php if($profil != null){echo $profil->id_profil;};?>">
            <div class="form-group">
              <label class="col-sm-2 control-label">Nama Aplikasi</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" name="nama_aplikasi" id="nama_aplikasi" value="<?php if($profil != null){echo $profil->nama_aplikasi;};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Singkatan</label>
              <div class="col-sm-2">
                <input type="text" class="form-control" name="singkatan" id="singkatan" value="<?php if($profil != null){echo $profil->singkatan;};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Pemda</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="pemda" id="pemda" value="<?php if($profil != null){echo $profil->pemda;};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Nama Instansi</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" name="nama_instansi" id="nama_instansi" value="<?php if($profil != null){echo $profil->nama_instansi;};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Alamat 1</label>
              <div class="col-sm-5">
                <textarea class="form-control" name="alamat_1" id="alamat_1"><?=@$profil->alamat_1?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Alamat 2</label>
              <div class="col-sm-5">
                <textarea class="form-control" name="alamat_2" id="alamat_2"><?=@$profil->alamat_2?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Kepala Instansi</label>
              <div class="col-sm-6">
                <select class="form-control select2" name="id_kepala" id="id_kepala">
                  <?php foreach($pegawai as $row): ?>
                    <?php 
                      $nama = $row->gelar_depan.' '.$row->nama;
                      if ($row->gelar_belakang != ''){
                        $nama .= ', '.$row->gelar_belakang;
                      }
                    ?>
                    <option value="<?=$row->id_pegawai?>" <?php if($profil->id_kepala == $row->id_pegawai){echo 'selected';}?>><?=$nama.' - '.$row->nomor_induk?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Kop Tanda Tangan Kepala Instansi</label>
              <div class="col-sm-4">
                <textarea class="form-control" name="kop_ttd_kepala" id="kop_ttd_kepala"><?=$profil->kop_ttd_kepala?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Logo</label>
              <div class="col-sm-2">
                <input type="file" name="logo" <?php if($action == 'insert'){echo 'required';} ?>/><br>
                <?php if($action == 'update'): ?>
                  <input type="hidden" name="old_logo" value="<?php if($profil){echo $profil->logo;}?>">
                  <?php if($profil->logo != ''): ?>
                    <img class="img-responsive thumbnail" src="<?=base_url('img/'.$profil->logo)?>" alt="">
                  <?php endif;?>
                <?php endif;?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Versi</label>
              <div class="col-sm-2">
                <input type="text" class="form-control" name="versi" id="versi" value="<?php if($profil != null){echo $profil->versi;};?>" required>
              </div>
            </div>
            <hr>
            <div class="form-group">
              <div class="col-md-offset-2 col-sm-8">
                <button class="btn btn-flat btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button>
                <a class="btn btn-flat btn-default" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Batal</a>
              </div>
            </div>
          </div>
        </form>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<script>
  $(document).ready(function () {
    // form validator
    $('#form').validate();
  })
</script>