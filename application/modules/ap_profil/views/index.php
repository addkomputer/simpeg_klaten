<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-<?=$access->icon?> text-red"></i> <?=$access->menu?>
      <small><?=$subtitle?></small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">Pengaturan</li>
      <li class="active text-orange"><?=$access->menu?></li>
    </ol> 
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box box-danger">
      <div class="box-header with-border">
        <i class="fa fa-table text-maroon"></i>
        <h3 class="box-title">Detail <?=$access->menu?></h3>
      </div>
      <div class="box-body table-responsive">
        <a class="btn btn-flat btn-warning pull-left" href="<?=base_url()?>ap_profil/form/<?=$profil->id_profil?>"><i class="fa fa-pencil"></i> Ubah</a>
        <br><br>
        <table class="table table-striped table-bordered table-condensed">
          <tbody>
            <tr>
              <td width="150">Nama Aplikasi</td>
              <td width="20" class="text-center">:</td>
              <td><?=$profil->nama_aplikasi?></td>
            </tr>
            <tr>
              <td>Singkatan</td>
              <td class="text-center">:</td>
              <td><?=$profil->singkatan?></td>
            </tr>
            <tr>
              <td>Pemda</td>
              <td class="text-center">:</td>
              <td><?=$profil->pemda?></td>
            </tr>
            <tr>
              <td>Nama Instansi</td>
              <td class="text-center">:</td>
              <td><?=$profil->nama_instansi?></td>
            </tr>
            <tr>
              <td>Alamat 1</td>
              <td class="text-center">:</td>
              <td><?=$profil->alamat_1?></td>
            </tr>
            <tr>
              <td>Alamat 2</td>
              <td class="text-center">:</td>
              <td><?=$profil->alamat_2?></td>
            </tr>
            <tr>
              <td>Nama Direktur</td>
              <td class="text-center">:</td>
              <td><?=$profil->gelar_depan.' '.$profil->nama.' '.$profil->gelar_belakang.' - '.$profil->nomor_induk?></td>
            </tr>
            <tr>
              <td>Versi Aplikasi</td>
              <td class="text-center">:</td>
              <td><?=$profil->versi?></td>
            </tr>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->