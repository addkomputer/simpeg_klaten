<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ap_profil extends CI_Model {

	public function get_list($number,$offset,$search_term = null)
  {
		if($search_term == null){
			return $this->db
        ->where('is_deleted','0')
				->get('ap_profil',$number,$offset)
				->result();
		}else{
			return $this->db
				->like('profile_name',$search_term,'both')
        ->where('is_deleted','0')
				->get('ap_profil',$number,$offset)
				->result();
		}
  }

  function num_rows($search_term = null){
		if($search_term == null){
      return $this->db
        ->where('is_deleted','0')
        ->get('ap_profil')->num_rows();
		}else{
      return $this->db
        ->like('profile_name',$search_term,'both')
        ->where('is_deleted','0')
        ->get('ap_profil')->num_rows();
		}
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('ap_profil')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id_profil',$id)->get('ap_profil')->row();
  }

  public function get_first()
  {
    return $this->db
      ->join('dt_pegawai b','a.id_kepala=b.id_pegawai','left')
      ->order_by('a.id_profil','asc')
      ->get('ap_profil a')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_profil','desc')->get('ap_profil')->row();
  }

  public function insert($data)
  {
    $this->db->insert('ap_profil',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_profil',$id)->update('ap_profil',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id_profil',$id)->update('ap_profil',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_profil',$id)->delete('ap_profil');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ap_profil");
  }

  public function get_direktur()
  {
    $profil = $this->db->get('ap_profil')->row();
    return $this->db
      ->where('a.id_pegawai',$profil->id_kepala)
      ->get('dt_pegawai a')
      ->row();
  }

}
