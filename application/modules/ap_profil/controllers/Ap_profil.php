<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ap_profil extends MY_Controller {

	var $access, $id_profil;

  function __construct(){
		parent::__construct();

		$module_controller = 'ap_profil';

    if($this->session->userdata('menu') != $module_controller){
      $this->session->unset_userdata('search_term');
      $this->session->set_userdata(array('menu' => 'ap_profil'));
    }
    $this->load->model('ap_konfigurasi/m_ap_konfigurasi');
		$this->id_profil = $this->session->userdata('id_profil');
		$this->access = $this->m_ap_konfigurasi->get_permission($this->id_profil, $module_controller);
		if ($this->access == null) {
			redirect(base_url().'ap_error/error_403');
		}
		$this->load->model('m_ap_profil');
		$this->load->model('ms_instansi/m_ms_instansi');
		$this->load->model('dt_pegawai/m_dt_pegawai');
	}
	
	public function index()
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Detail';
		if($this->access->_read == 1){
			$data['profil'] = $this->m_ap_profil->get_first();
			insert_log('read',$this->access->menu);
			$this->view('index',$data);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}
	
	public function form($id = null)
	{
		$data['access'] = $this->access;
		$data['instansi'] = $this->m_ms_instansi->get_all();
		$data['pegawai'] = $this->m_dt_pegawai->get_all();
		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Tambah Data';
				$data['action'] = 'insert';
				$data['profil'] = null;
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Ubah Data';
				$data['action'] = 'update';
				$data['profil'] = $this->m_ap_profil->get_by_id($id);
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		}
	}

	public function reset_search()
  {
    $this->session->unset_userdata('search_term');
    redirect(base_url().'ap_profil/index');
  }

	public function update()
	{
		$data = $_POST;
		if ($data != null) {
			if($this->access->_update){
				$data['updated_by'] = $this->session->userdata('nama');
				if(!isset($data['is_active'])){$data['is_active'] = 0;}
				$id = $data['id_profil'];
				if (!empty($_FILES["logo"]["name"])) {
					$data['logo'] = $this->uploadLogo();
				} else {
					$data['logo'] = $data["old_logo"];
				}
				unset($data['old_logo']);
				$this->m_ap_profil->update($id, $data);
				insert_log('update',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				redirect(base_url().'ap_profil/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function uploadLogo()
	{
		$config['upload_path']          = './img/';
		$config['allowed_types']        = 'jpg|jpeg|png|';
		$config['overwrite']						= true;
		$config['max_size']             = 100000000;
		$config['remove_spaces'] 				= TRUE;
		$config['encrypt_name'] 				= TRUE;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('logo')){
			$error = array('error' => $this->upload->display_errors());
		}else{
			return $this->upload->data("file_name");
		}
		
		return null;
	}

}