<!DOCTYPE html>
<html>
    <head>
        <title><?=$title?></title>
        <style type="text/css">
        h1{font-size:20px; font-family: "arial,helvetica,san-serif"; font-weight: bold; text-align: center;}
        h2{font-size:16px; font-family: "arial,helvetica,san-serif"; font-weight: bold;}
        h3{font-size:14px; font-family: "arial,helvetica,san-serif"; font-weight: normal;}
        table { border-collapse: collapse; font-family: "arial,helvetica,san-serif"; font-size: 10px;}
        table td { padding: 3px; }
        .content_title { font-weight: bold; font-size: 16px; font-family: arial;}
        .td-border-all { border: 1px solid #000; }
        .td-border-left { border-left: 1px solid #000; border-bottom: 1px solid #000; border-top: 1px solid #000;}
        .td-border-right { border-right: 1px solid #000; border-bottom: 1px solid #000; border-top: 1px solid #000;}
        .td-border-bottom { border-bottom: 1px solid #000;}
        .td-border-top { border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;}
        .valign-top{ vertical-align: top!important; }
        .text-center { text-align: center; }
        .text-right { text-align: right; }
        .bold { font-weight: bold; }
        </style>
    </head>
    <body>

    <table width="100%">
    <tr>
        <td width="10%" align="left"><img src="<?=base_url().'img/kebumen.png'?>" height="60px"></td>
        <td width="80%" class="text-center">
            <h2>PEMERINTAH KABUPATEN KEBUMEN</h2>
            <h1>RUMAH SAKIT UMUM DAERAH dr. SOEDIRMAN</h1>
            Jalan Lingkar Selatan, Muktisari, Kebumen, Kabupaten Kebumen, Jawa Tengah 54351<br>
            Telp/Fax : 0287-3873318, 0287-381101, 0287-385274; WA : 0877-3427-026; Email : rsud@kebumenkab.go.id<br>
        </td>
        <td width="10%" align="right"><img src="<?=base_url().'img/logo.png'?>" height="60px"></td>
    </tr>
    <tr>
        <td colspan="3" class="td-border-bottom"></td>
    </tr>
    </table>
    <h2 class="text-center">DAFTAR GAJI KARYAWAN</h2>
    <table width="100%">
    <thead>
        <tr>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 10px">No</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 220px">Nama</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 60px">Pendidikan</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 40px">Tahun</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 40px">Bulan</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 100px">Nama Jabatan</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 100px">Kode Jabatan</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 100px">Nilai Jabatan</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 80px">PIR</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 100px">Total Remun</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 60px">Nilai P1</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 60px">IKI IKU</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 100px">PAY FOR<br>POSITION<br>(P1)<br>30%</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 100px">PAY FOR<br>PERFORMANCE<br>(P2)<br>70%</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 100px">PAY FOR<br>PEOPLE<br>(P3)</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 100px">Jumlah Diterima</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 80px">Tanda Tangan</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $no=1; 
        $total_jumlah_p2=0;
        foreach($list_gaji as $row):?>
        <tr>
            <td class="td-border-all text-center"><?=$no?></td>
            <td class="td-border-all"><?=($row['gelar_depan'] != '' ? $row['gelar_depan'].' ' : '')?><?=$row['nama']?><?=($row['gelar_belakang'] != '' ? ' '.$row['gelar_belakang'] : '')?></td>
            <td class="td-border-all text-center"><?=$row['jenjang_pend']?></td>
            <td class="td-border-all text-center"><?=$row['tahun_kerja']?></td>
            <td class="td-border-all text-center"><?=$row['bulan_kerja']?></td>
            <td class="td-border-all text-center"><?=$row['jabatan']?></td>
            <td class="td-border-all text-center"><?=$row['kd_jabatan_grade']?></td>
            <td class="td-border-all text-center"><?=digit($row['nilai_jabatan_grade'])?></td>
            <td class="td-border-all text-center"><?=digit($row['pir'])?></td>
            <td class="td-border-all text-right"><?=digit($row['total_remun'])?></td>
            <td class="td-border-all text-center"><?=($row['nilai_p1'] != '') ? digit($row['nilai_p1']).'%' : ''?></td>
            <td class="td-border-all text-center"><?=($row['nilai_p2'] != '') ? digit($row['nilai_p2']).'%' : ''?></td>
            <td class="td-border-all text-right"><?=digit($row['jumlah_p1'])?></td>
            <td class="td-border-all text-right"><?=digit($row['jumlah_p2'])?></td>
            <td class="td-border-all text-right"><?=digit($row['jumlah_p3'])?></td>
            <td class="td-border-all text-right"><?=digit($row['jumlah_total'])?></td>
            <td class="td-border-all"></td>
        </tr>
        <?php 
        $total_jumlah_p2+=$row['jumlah_p2'];
        $no++; 
        endforeach;?>
    </tbody>
        <tr>
            <td class="td-border-all">&nbsp;</td>
            <td class="td-border-all">&nbsp;</td>
            <td class="td-border-all">&nbsp;</td>
            <td class="td-border-all">&nbsp;</td>
            <td class="td-border-all">&nbsp;</td>
            <td class="td-border-all">&nbsp;</td>
            <td class="td-border-all">&nbsp;</td>
            <td class="td-border-all">&nbsp;</td>
            <td class="td-border-all">&nbsp;</td>
            <td class="td-border-all">&nbsp;</td>
            <td class="td-border-all">&nbsp;</td>
            <td class="td-border-all">&nbsp;</td>
            <td class="td-border-all">&nbsp;</td>
            <td class="td-border-all">&nbsp;</td>
            <td class="td-border-all">&nbsp;</td>
            <td class="td-border-all">&nbsp;</td>
            <td class="td-border-all">&nbsp;</td>
        </tr>
        <tr>
            <td class="td-border-all"></td>
            <td class="td-border-all">TOTAL NILAI GRADE</td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all text-center"><?=digit($gaji_resume['total_nilai_jabatan_grade'])?></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
        </tr>
        <tr>
            <td class="td-border-all"></td>
            <td class="td-border-all">JASA YANG DIBAGI</td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all bold text-center"><?=digit($gaji_resume['jasa_dibagi'])?></td>
            <td class="td-border-all bold text-center"><?=digit($gaji_resume['jasa_dibagi_f1'])?></td>
            <td class="td-border-all bold text-center"><?=digit($gaji_resume['jasa_dibagi_f2'])?></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
        </tr>
        <tr>
            <td class="td-border-all"></td>
            <td class="td-border-all bold text-center">PIR</td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all bold text-center"><?=digit($gaji_resume['nominal_pir'])?></td>
            <td class="td-border-all"></td>
            <td class="td-border-all bold text-center"><?=digit($gaji_resume['nilai_pir'])?></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
        </tr>
        <tr>
            <td class="td-border-all"></td>
            <td class="td-border-all bold text-center">JUMLAH</td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all bold text-right"><?=digit($total_jumlah_p2)?></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
        </tr>
        <tr>
            <td class="" colspan="17">&nbsp;</td>
        </tr>
    </table>

    <table width="100%">    
        <!-- KOMITE RUMAH SAKIT -->        
        <tr>
            <td class="td-border-all" colspan="17"><b>E. KOMITE RUMAH SAKIT</b></td>
        </tr>        
        <tr>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 10px">NO</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 220px">NAMA PEGAWAI</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 60px">TGL MASUK</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 40px"></th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 40px"></th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 100px">NAMA JABATAN</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 100px">KODE JABATAN</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 100px">NILAI JABATAN</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 80px">PIR</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 100px"></th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 60px"></th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 60px"></th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 100px">PAY FOR<br>POSITION<br>(P1)<br>30%</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 100px">PAY FOR<br>PERFORMANCE<br>(P2)<br>70%</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 100px">PAY FOR<br>PEOPLE<br>(P3)</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 100px">Jumlah</th>
            <th class="td-border-all valign-top" style="padding:10px 0px; width: 80px"></th>
        </tr>
        <?php foreach($list_komite as $key => $val):?>
        <tr>
            <td class="td-border-all <?=(strlen($key) == 1) ? 'bold' : ''?>"><?=substr($key,-1)?></td>
            <td class="td-border-all <?=(strlen($key) == 1) ? 'bold' : ''?>"><?=$val?></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
        </tr>
        <?php endforeach;?>

        <!-- PANITIA -->
        <tr>
            <td class="" colspan="17">&nbsp;</td>
        </tr>
        <tr>
            <td class="td-border-all" colspan="17"><b>E. PANITIA</b></td>
        </tr>        
        <tr>
            <td class="td-border-all text-center">NO</td>
            <td class="td-border-all text-center">NAMA PEGAWAI</td>
            <td class="td-border-all text-center">TGL MASUK</td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all text-center">NAMA JABATAN</td>
            <td class="td-border-all text-center">KODE JABATAN</td>
            <td class="td-border-all text-center">NILAI JABATAN</td>
            <td class="td-border-all text-center">PIR</td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all text-center">PAY FOR<br>POSITION<br>(P1)<br>30%</td>
            <td class="td-border-all text-center">PAY FOR<br>PERFORMANCE<br>(P2)<br>70%</td>
            <td class="td-border-all text-center">PAY FOR<br>PEOPLE<br>(P3)</td>
            <td class="td-border-all text-center">Jumlah</td>
            <td class="td-border-all"></td>
        </tr>
        <?php foreach($list_panitia as $key => $val):?>
        <tr>
            <td class="td-border-all <?=(strlen($key) == 1) ? 'bold' : ''?>"><?=substr($key,-1)?></td>
            <td class="td-border-all <?=(strlen($key) == 1) ? 'bold' : ''?>"><?=$val?></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
            <td class="td-border-all"></td>
        </tr>
        <?php endforeach;?>

    </table>

    </body>
</html>