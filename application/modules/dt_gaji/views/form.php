<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-primary">
        <form id="form-filter" class="form-horizontal" action="<?=base_url()?>dt_gaji/<?=$action_filter?>" method="post" autocomplete="off"> 
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-2 control-label">Bulan - Tahun</label>
              <div class="col-sm-3">
                <select name="bulan" class="form-control select2 input-sm">
                  <?php foreach($list_month as $key => $val):?>
                    <option value="<?=$key?>" <?php if(@$bulan == $key) echo 'selected'?>><?=$val?></option>
                  <?php endforeach;?>
                </select>
              </div>
              <div class="col-sm-2">
                <select name="tahun" class="form-control select2 input-sm">
                  <?php for($t=$start_year; $t<=$end_year; $t++):?>
                    <option value="<?=$t?>" <?php if(@$tahun == $t) echo 'selected'?>><?=$t?></option>
                  <?php endfor;?>
                </select>
              </div>
              <div class="col-sm-8">
              </div>
            </div>
            <div class="form-group" style="margin-top: -10px">
              <div class="col-md-offset-2 col-sm-8">
                <input type="hidden" name="is_process" value="TRUE">
                <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-check-square-o"></i> Proses</button>
                <a class="btn btn-sm btn-default" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Batal</a>
              </div>
            </div>
          </div>
        </form>

        <?php if($is_process == 'TRUE'):?>
        <form id="form" class="form-horizontal" action="<?=base_url()?>dt_gaji/<?=$action?>" method="post" autocomplete="off"> 
          <input type="hidden" name="bulan" value="<?=$bulan?>">
          <input type="hidden" name="tahun" value="<?=$tahun?>">
          <hr style="margin-top: -10px">
          <div class="box-body" style="margin: -15px 0px">
            <input type="hidden" class="form-control input-sm" name="id_gaji_resume" id="id_gaji_resume" value="<?php if($gaji_resume != null){echo $gaji_resume['id_gaji_resume'];};?>">
            <div class="form-group">
              <label class="col-sm-2 control-label">Jasa Yang Dibagi</label>
              <div class="col-sm-3">
                <div class="input-group">
                  <span class="input-group-addon">Rp.</span>
                  <input type="text" name="jasa_dibagi_set" class="form-control input-sm" value="<?=digit($jasa['total_jasa'])?>" readonly="1">
                </div>
              </div>
              <?php if(@$jasa['id_jasa'] == ''):?>
              <div class="col-sm-7">
                <em style="color: red">Perhatian : Jasa belum diseting, silahkan klik <a href="<?=site_url('ms_jasa/form')?>">Tambah Jasa Rumah Sakit</a></em>
              </div>
              <?php else:?>
              <div class="col-sm-7 text-right">
                <b>P1 : PAY FOR POSITION | P2 : PAY FOR PERFORMANCE | P3 : PAY FOR PEOPLE</b>
              </div>
              <?php endif;?>
            </div>
          </div>

          <div style="width: 100%; height: 300px; overflow: auto;">
            <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
                <th style="vertical-align: middle;" class="text-center" rowspan="2" width="30">No.</th>
                <th style="vertical-align: middle;" class="text-center" rowspan="2">Nama</th>
                <th style="vertical-align: middle;" class="text-center" rowspan="2">Kelompok</th>
                <th style="vertical-align: middle;" class="text-center" rowspan="2">Pendidikan</th>
                <th style="vertical-align: middle;" class="text-center" rowspan="2">Tahun</th>
                <th style="vertical-align: middle;" class="text-center" rowspan="2">Bulan</th>                
                <th style="vertical-align: middle;" class="text-center" rowspan="2">Kode Jabatan</th>
                <th style="vertical-align: middle;" class="text-center" rowspan="2">Nilai Jabatan</th>
                <th style="vertical-align: middle;" class="text-center" rowspan="2">PIR</th>
                <th style="vertical-align: middle;" class="text-center" rowspan="2">Total Remun</th>
                <th style="vertical-align: middle;" class="text-center">Nilai P1</th>
                <th style="vertical-align: middle;" class="text-center">IKI IKU</th>
                <th style="vertical-align: middle;" class="text-center">P1</th>
                <th style="vertical-align: middle;" class="text-center">P2</th>
                <th style="vertical-align: middle;" class="text-center">P3</th>
                <th style="vertical-align: middle;" class="text-center" rowspan="2">Jumlah Diterima</th>
              </tr>
              <tr>
                <th style="vertical-align: middle;" class="text-center">%</th>
                <th style="vertical-align: middle;" class="text-center">%</th>
                <th style="vertical-align: middle;" class="text-center">30%</th>
                <th style="vertical-align: middle;" class="text-center">70%</th>
                <th style="vertical-align: middle;" class="text-center"></th>
              </tr>
            </thead>
            <tbody>
              <?php $i=1; foreach($all_pegawai as $row):?>
                <tr class="data-pegawai" data-id="<?=$row['id_pegawai']?>">
                  <td class="text-center"><?=($i++)?></td>                  
                  <td class="text-left">
                    <input type="hidden" name="id_gaji[]" value="<?=$row['id_gaji']?>">
                    <input type="hidden" name="id_pegawai[]" value="<?=$row['id_pegawai']?>">
                    <input type="hidden" name="id_jabatan[]" value="<?=$row['id_jabatan']?>">
                    <?=$row['gelar_depan']?> <?=$row['nama']?> <?=$row['gelar_belakang']?><br>
                    <em style="color: blue; font-size: 9px"><?=$row['pend_terakhir']?></em>
                  </td>                  
                  <td class="text-center">
                    <select name="kelompok_grade[]" class="form-control input-sm kelompok_grade" id="kelompok_grade_<?=$row['id_pegawai']?>" style="width: 80px">
                      <option value="">-----</option>
                      <?php foreach($all_kelompok_grade as $kl):?>
                        <option value="<?=$kl['kelompok']?>" <?php if(@$row['kelompok_grade'] == $kl['kelompok']) echo 'selected'?>><?=$kl['kelompok']?></option>
                      <?php endforeach;?>
                    </select>
                  </td>          
                  <td class="text-center">
                    <div id="box_jenjang_pend_<?=$row['id_pegawai']?>">
                    <select name="jenjang_pend[]" class="form-control input-sm jenjang_pend" id="jenjang_pend_<?=$row['id_pegawai']?>" style="width: 70px">
                      <option value="">-----</option>
                      <?php foreach($all_jenjang_pend as $jp):?>
                        <option value="<?=$jp['jenjang_pend']?>" <?php if(@$row['jenjang_pend'] == $jp['jenjang_pend']) echo 'selected'?>><?=$jp['jenjang_pend']?></option>
                      <?php endforeach;?>
                    </select>
                    </div>
                  </td>                  
                  <td class="text-center"><input type="text" class="form-control input-sm tahun_kerja" name="tahun_kerja[]" id="tahun_kerja_<?=$row['id_pegawai']?>" value="<?=@$row['tahun_kerja']?>" style="width: 40px"></td>                            
                  <td class="text-center"><input type="text" class="form-control input-sm bulan_kerja" name="bulan_kerja[]" id="bulan_kerja_<?=$row['id_pegawai']?>" value="<?=@$row['bulan_kerja']?>" style="width: 40px"></td>                  
                  <td class="text-center">
                    <select name="kd_jabatan_grade[]" class="form-control input-sm kd_jabatan_grade" id="kd_jabatan_grade_<?=$row['id_pegawai']?>" style="width: 80px">
                      <option value="">-----</option>
                      <?php foreach($all_nilai_grade as $ng):?>
                        <option value="<?=$ng['kd_jabatan_grade']?>" <?php if(@$row['kd_jabatan_grade'] == $ng['kd_jabatan_grade']) echo 'selected'?>><?=$ng['kd_jabatan_grade']?></option>
                      <?php endforeach;?>
                    </select>
                  </td>                  
                  <td class="text-center"><input type="text" class="form-control input-sm nilai_jabatan_grade" name="nilai_jabatan_grade[]" id="nilai_jabatan_grade_<?=$row['id_pegawai']?>" value="<?=@$row['nilai_jabatan_grade']?>" style="width: 50px"></td>                  
                  <td class="text-center"><input type="text" class="form-control input-sm pir" name="pir[]" id="pir_<?=$row['id_pegawai']?>" value="<?=@$row['pir']?>" style="width: 70px"></td>                  
                  <td class="text-center"><input type="text" class="form-control input-sm total_remun" name="total_remun[]" id="total_remun_<?=$row['id_pegawai']?>" value="<?=(@$row['total_remun'] !='' ? digit(@$row['total_remun']) : '')?>" style="width: 100px"></td>                  
                  <td class="text-center"><input type="text" class="form-control input-sm nilai_p1" name="nilai_p1[]" id="nilai_p1_<?=$row['id_pegawai']?>" value="<?=(@$row['nilai_p1'] !='' ? digit(@$row['nilai_p1']) : '100')?>" style="width: 40px"></td>                  
                  <td class="text-center"><input type="text" class="form-control input-sm nilai_p2" name="nilai_p2[]" id="nilai_p2_<?=$row['id_pegawai']?>" value="<?=(@$row['nilai_p2'] !='' ? digit(@$row['nilai_p2']) : '100')?>" style="width: 40px"></td>                  
                  <td class="text-center"><input type="text" class="form-control input-sm jumlah_p1" name="jumlah_p1[]" id="jumlah_p1_<?=$row['id_pegawai']?>" value="<?=(@$row['jumlah_p1'] !='' ? digit(@$row['jumlah_p1']) : '')?>" style="width: 100px"></td>                  
                  <td class="text-center"><input type="text" class="form-control input-sm jumlah_p2" name="jumlah_p2[]" id="jumlah_p2_<?=$row['id_pegawai']?>" value="<?=(@$row['jumlah_p2'] !='' ? digit(@$row['jumlah_p2']) : '')?>" style="width: 100px"></td>                  
                  <td class="text-center"><input type="text" class="form-control input-sm jumlah_p3" name="jumlah_p3[]" id="jumlah_p3_<?=$row['id_pegawai']?>" value="<?=(@$row['jumlah_p3'] !='' ? digit(@$row['jumlah_p3']) : '')?>" style="width: 100px"></td>                  
                  <td class="text-center"><input type="text" class="form-control input-sm jumlah_total" name="jumlah_total[]" id="jumlah_total_<?=$row['id_pegawai']?>" value="<?=(@$row['jumlah_total'] !='' ? digit(@$row['jumlah_total']) : '')?>" style="width: 100px"></td>                  
                </tr>
              <?php endforeach;?>              
            </tbody>
            </table>
          </div>

          <table class="table table-striped table-bordered table-condensed">
            <tbody>
            <tr>
              <td colspan="5" style="background-color: #eee"></td>
            </tr>
            <tr>
                <td width="15%" class="text-left"><b>TOTAL NILAI GRADE</b></td>    
                <td width="20%"></td>
                <td width="20%"></td>
                <td width="20%" class="text-center"><input type="text" class="form-control input-sm" name="total_nilai_jabatan_grade" id="total_nilai_jabatan_grade" readonly="1"></td>          
                <td width="20%"></td>
            </tr>
            <tr>
                <td class="text-left"><b>JASA YANG DIBAGI</b></td>    
                <td class="text-center">
                  <div class="input-group">
                    <span class="input-group-addon">Rp.</span>
                    <input type="text" class="form-control input-sm" name="jasa_dibagi" id="jasa_dibagi" value="<?=digit($jasa['total_jasa'])?>" readonly="1">
                  </div>
                </td>          
                <td class="text-center">
                  <div class="input-group">
                    <span class="input-group-addon">Rp.</span>
                    <input type="text" class="form-control input-sm" name="jasa_dibagi_f1" id="jasa_dibagi_f1" value="" readonly="1">
                  </div>
                </td>          
                <td class="text-center">
                  <div class="input-group">
                    <span class="input-group-addon">Rp.</span>
                    <input type="text" class="form-control input-sm" name="jasa_dibagi_f2" id="jasa_dibagi_f2" value="" readonly="1">
                  </div>
                </td>          
                <td></td>
            </tr>
            <tr>
                <td class="text-left"><b>PIR</b></td>    
                <td class="text-center">
                  <div class="input-group">
                    <span class="input-group-addon">Rp.</span>
                    <input type="text" class="form-control input-sm" name="nominal_pir" id="nominal_pir" value="<?=(@$gaji_resume['nominal_pir'] != '' ? digit($gaji_resume['nominal_pir']) : '')?>" readonly="1">
                  </div>
                </td>          
                <td></td>          
                <td><input type="text" class="form-control input-sm" name="nilai_pir" id="nilai_pir" value="<?=(@$gaji_resume['nilai_pir'] != '' ? digit($gaji_resume['nilai_pir']) : '')?>" readonly="1"></td>          
                <td></td>
            </tr>
            </tbody>
          </table>
          
          <hr>
          <div class="form-group">
            <div class="col-md-offset-2 col-sm-8">
              <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
              <a class="btn btn-sm btn-default" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Batal</a>
            </div>
          </div>
          <br>
        </div>
        </form>
        <?php endif;?>

      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<script>
  $(document).ready(function () {
    // form validator
    $('#form').validate();
    //
    $('.data-pegawai').each(function() {
        var i = $(this).attr('data-id');
        //
        $('#kelompok_grade_'+i).bind('change',function() {
            var id_pegawai = i;
            var kelompok = ($(this).val() != '' ? $(this).val() : '');
            $.get('<?=site_url("dt_gaji/ajax/get_jenjang_pend_html")?>?kelompok='+kelompok+'&id_pegawai='+i,null,function(data) {
              $('#box_jenjang_pend_'+i).html(data.html);
            },'json');
        });
        //
        $('#jenjang_pend_'+i+', #tahun_kerja_'+i+', #bulan_kerja_'+i).bind('change',function() {
            var jenjang_pend = ($('#jenjang_pend_'+i).val() != '' ? $('#jenjang_pend_'+i).val() : '');
            var tahun = ($('#tahun_kerja_'+i).val() != '' ? $('#tahun_kerja_'+i).val() : '');
            var bulan = ($('#bulan_kerja_'+i).val() != '' ? $('#bulan_kerja_'+i).val() : '');
            //
            $.get('<?=site_url("dt_gaji/ajax/get_nilai_grade")?>?jenjang_pend='+jenjang_pend+'&tahun='+tahun+'&bulan='+bulan,null,function(data) {
              $('#kd_jabatan_grade_'+i).val(data.kd_jabatan_grade);
              $('#nilai_jabatan_grade_'+i).val(data.nilai_jabatan_grade);
              __get_total_nilai_jabatan_grade();              
              __get_total_remun();
            },'json');            
        });                
        //
        $('#nilai_jabatan_grade_'+i).bind('keyup',function() {
          __get_total_nilai_jabatan_grade();              
          __get_total_remun();
        });
        //
        $('#nilai_p1_'+i+',#nilai_p2_'+i).bind('keyup',function() {
          __get_jumlah_p1(i);
          __get_jumlah_p2(i);
        });
        //
        $('#jumlah_p3_'+i).bind('keyup',function() {
          __get_jumlah_total(i);
        });
    });
    //
    function __get_total_remun() {
      $('.data-pegawai').each(function() {
          var i = $(this).attr('data-id');
          var nilai_jabatan_grade = ($('#nilai_jabatan_grade_'+i).val() != "" ? $('#nilai_jabatan_grade_'+i).val() : 0);
          var nilai_pir = ($('#nilai_pir').val() != "" ? $('#nilai_pir').val().replace(/[.]/g,"") : 0);
          var result = parseFloat(nilai_jabatan_grade) * parseFloat(nilai_pir);
          var result = Math.round(result);
          var result = number_format(result);
          $('#total_remun_'+i).val(result);
          __get_jumlah_p1(i);
          __get_jumlah_p2(i);
      });      
    }
    //
    function __get_jumlah_p1(i) {
      var prosen = '30'; // 30%
      var total_remun = ($('#total_remun_'+i).val() != "" ? $('#total_remun_'+i).val().replace(/[.]/g,"") : 0);
      var nilai_p1 = ($('#nilai_p1_'+i).val() != "" ? $('#nilai_p1_'+i).val().replace(/[.]/g,"") : 0);
      var result = parseFloat(total_remun) * (parseFloat(prosen) / 100) * parseFloat(nilai_p1);
      var result = Math.round(result);
      var result = number_format(result);
      $('#jumlah_p1_'+i).val(result);
      __get_jumlah_total(i);
    }
    //
    function __get_jumlah_p2(i) {
      var prosen = '70'; // 70%
      var total_remun = ($('#total_remun_'+i).val() != "" ? $('#total_remun_'+i).val().replace(/[.]/g,"") : 0);
      var nilai_p2 = ($('#nilai_p2_'+i).val() != "" ? $('#nilai_p2_'+i).val().replace(/[.]/g,"") : 0);
      var result = parseFloat(total_remun) * (parseFloat(prosen) / 100) * parseFloat(nilai_p2);
      var result = Math.round(result);
      var result = number_format(result);
      $('#jumlah_p2_'+i).val(result);
      __get_jumlah_total(i);
    }
    //
    function __get_jumlah_total(i) {
      var jumlah_p1 = ($('#jumlah_p1_'+i).val() != "" ? $('#jumlah_p1_'+i).val().replace(/[.]/g,"") : 0);
      var jumlah_p2 = ($('#jumlah_p2_'+i).val() != "" ? $('#jumlah_p2_'+i).val().replace(/[.]/g,"") : 0);
      var jumlah_p3 = ($('#jumlah_p3_'+i).val() != "" ? $('#jumlah_p3_'+i).val().replace(/[.]/g,"") : 0);
      var result = parseFloat(jumlah_p1) + parseFloat(jumlah_p2) + parseFloat(jumlah_p3);
      var result = Math.round(result);
      var result = number_format(result);
      $('#jumlah_total_'+i).val(result);
    }
    //
    __get_total_nilai_jabatan_grade();
    function __get_total_nilai_jabatan_grade() {
      var t = 0;
      $('.nilai_jabatan_grade').each(function() {
          var i = ($(this).val() != '' ? $(this).val() : 0);
          t += parseFloat(i);
      });
      var result = Math.round(t);
      var result = number_format(t);
      $('#total_nilai_jabatan_grade').val(result);
      __get_nominal_pir();
    }
    //
    __get_nominal_pir();
    function __get_nominal_pir() {
      var prosen_pir  = '1.5'; // 1.5%
      var jasa_dibagi = ($('#jasa_dibagi').val() != "") ? $('#jasa_dibagi').val().replace(/[.]/g,"") : 0;
      var result = parseFloat(prosen_pir) / 100 * parseFloat(jasa_dibagi); 
      var result = Math.round(result);
      var result = number_format(result);
      $('#nominal_pir').val(result);
      __get_jasa_dibagi_f1();
      __get_jasa_dibagi_f2();          
      __get_nilai_pir();
    }
    //
    function __get_nilai_pir() {
      var jasa_dibagi_f2 = ($('#jasa_dibagi_f2').val() != "" ? $('#jasa_dibagi_f2').val().replace(/[.]/g,"") : 0);
      var total_nilai_jabatan_grade = ($('#total_nilai_jabatan_grade').val() != "" ? $('#total_nilai_jabatan_grade').val().replace(/[.]/g,"") : 0);
      if(total_nilai_jabatan_grade != 0) {
        var result = parseFloat(jasa_dibagi_f2) / parseFloat(total_nilai_jabatan_grade);
      } else {
        var result = "0";
      }          
      var result = Math.round(result);
      var result = number_format(result);
      $('#nilai_pir').val(result);
      $('.pir').val(result);
    }
    //
    function __get_jasa_dibagi_f1() {
      var prosen_jasa_f1 = '65'; // 65%
      var jasa_dibagi = ($('#jasa_dibagi').val() != "") ? $('#jasa_dibagi').val().replace(/[.]/g,"") : 0;
      var nominal_pir = ($('#nominal_pir').val() != "") ? $('#nominal_pir').val().replace(/[.]/g,"") : 0;
      var result = (parseFloat(jasa_dibagi) - parseFloat(nominal_pir)) * parseFloat(prosen_jasa_f1) / 100;
      var result = Math.round(result);
      var result = number_format(result);
      $('#jasa_dibagi_f1').val(result);
    }
    //
    function __get_jasa_dibagi_f2() {
      var result = $('#jasa_dibagi_f1').val();
      $('#jasa_dibagi_f2').val(result); 
    }
    //
    function number_format(nStr) {
        nStr += '';
        x = nStr.split(',');
        x1 = x[0];
        x2 = x.length > 1 ? ',' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }
  })
</script>