<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dt_gaji extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_gaji_resume')
      ->get('dt_gaji_resume',$number,$offset)
      ->result_array();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_gaji_resume')
      ->get('dt_gaji_resume')
      ->num_rows();
  }
  
  function num_rows_total(){  
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_gaji_resume')
      ->get('dt_gaji_resume')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('dt_gaji_resume')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id_gaji_resume',$id)->get('dt_gaji_resume')->row_array();
  }

  public function get_grade($jenjang_pend, $tahun, $bulan)
  {
    $result = $this->db->where('jenjang_pend', $jenjang_pend)->get('ms_grade')->row_array();
    $tahun  = str_pad($tahun,'2','0',STR_PAD_LEFT);
    return @$result['gr_'.$tahun];
  }

  public function get_nilai_grade($kd_jabatan_grade, $jenjang_pend, $tahun, $bulan)
  {
    $result = $this->db
                ->like('jenjang_pend_str', $jenjang_pend)
                // ->where('kelompok', $jenjang_pend)
                ->get('ms_nilai_grade')->row_array();
    $tahun  = str_pad($tahun,'2','0',STR_PAD_LEFT);
    return @$result['gr_'.$tahun];
  }

  public function get_first()
  {
    return $this->db->order_by('id_gaji','asc')->get('dt_gaji')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_gaji','desc')->get('dt_gaji')->row();
  }

  public function get_all_pegawai($bulan=null, $tahun=null)
  {
      $sql = "SELECT 
                x.*,IF(LEFT(x.id_jabatan,9)='6423-3100',y.jabatan,'') as jabatan
              FROM 
              (
                SELECT 
                  a.id_grup,
                  a.id_pegawai, 
                  a.nama, 
                  a.gelar_depan, 
                  a.gelar_belakang, 
                  a.pend_terakhir, 
                  a.pin,
                  a.id_golongan,
                  IF(b.tahun_kerja IS NULL,a.masakerja_gol_thn,b.tahun_kerja) as tahun_kerja,
                  IF(b.bulan_kerja IS NULL,a.masakerja_gol_bln,b.bulan_kerja) as bulan_kerja,
                  b.id_gaji,
                  b.jenjang_pend,
                  b.kelompok_grade,
                  b.kd_jabatan_grade,
                  b.nilai_jabatan_grade,
                  b.pir,
                  b.total_remun,
                  b.nilai_p1,
                  b.nilai_p2,
                  b.jumlah_p1,
                  b.jumlah_p2,
                  b.jumlah_p3,
                  b.jumlah_total,
                  IF(c.id_direktur IS NOT NULL,'6423-3100-1000-0131',a.id_jabatan) as id_jabatan  
                FROM dt_pegawai a 
                LEFT JOIN 
                ( 
                  SELECT * FROM dt_gaji WHERE bulan='$bulan' AND tahun='$tahun' GROUP BY id_pegawai
                ) b ON a.id_pegawai=b.id_pegawai 
                LEFT JOIN 
                (
                  SELECT id_direktur FROM ap_profil
                ) c ON a.id_pegawai=c.id_direktur 
                WHERE a.is_deleted=0 AND a.is_active=1 AND a.is_dokter IN ('0','2')
              ) x 
              LEFT JOIN ms_jabatan y ON x.id_jabatan=y.id_jabatan 
              ORDER BY x.id_grup ASC,LEFT(x.pin,1) ASC,RIGHT(if(x.id_jabatan='','9999999999999',x.id_jabatan),4) ASC, x.id_golongan DESC
              ";
              // ORDER BY LEFT(x.pin,1) ASC, x.id_golongan DESC, LEFT(x.id_jabatan,14) ASC, x.nama ASC 
      return $this->db->query($sql)->result_array();
  }

  public function insert($data)
  {
    // gaji_resume
    $data_gaji_resume['tahun'] = $data['tahun'];
    $data_gaji_resume['bulan'] = $data['bulan'];
    $data_gaji_resume['total_nilai_jabatan_grade'] = str_replace('.','',$data['total_nilai_jabatan_grade']);
    $data_gaji_resume['jasa_dibagi'] = str_replace('.','',$data['jasa_dibagi']);
    $data_gaji_resume['jasa_dibagi_f1'] = str_replace('.','',$data['jasa_dibagi_f1']);
    $data_gaji_resume['jasa_dibagi_f2'] = str_replace('.','',$data['jasa_dibagi_f2']);
    $data_gaji_resume['nominal_pir'] = str_replace('.','',$data['nominal_pir']);
    $data_gaji_resume['nilai_pir'] = str_replace('.','',$data['nilai_pir']);
    $data_gaji_resume['created_by'] = $data['created_by'];
    $data_gaji_resume['is_active'] = $data['is_active'];
    $this->db->insert('dt_gaji_resume',$data_gaji_resume);
    //
    // gaji
    $data_gaji['tahun'] = $data['tahun'];
    $data_gaji['bulan'] = $data['bulan'];
    foreach($data['id_pegawai'] as $key => $val) {
      $data_gaji['tahun_kerja'] = @$data['tahun_kerja'][$key];
      $data_gaji['bulan_kerja'] = @$data['bulan_kerja'][$key];
      $data_gaji['id_pegawai'] = @$data['id_pegawai'][$key];
      $data_gaji['jenjang_pend'] = @$data['jenjang_pend'][$key];
      $data_gaji['kelompok_grade'] = @$data['kelompok_grade'][$key];
      $data_gaji['id_jabatan'] = @$data['id_jabatan'][$key];
      $data_gaji['kd_jabatan_grade'] = @$data['kd_jabatan_grade'][$key];
      $data_gaji['nilai_jabatan_grade'] = str_replace('.','',@$data['nilai_jabatan_grade'][$key]);
      $data_gaji['pir'] = str_replace('.','',@$data['pir'][$key]);
      $data_gaji['total_remun'] = str_replace('.','',@$data['total_remun'][$key]);
      $data_gaji['nilai_p1'] = str_replace('.','',@$data['nilai_p1'][$key]);
      $data_gaji['nilai_p2'] = str_replace('.','',@$data['nilai_p2'][$key]);
      $data_gaji['jumlah_p1'] = str_replace('.','',@$data['jumlah_p1'][$key]);
      $data_gaji['jumlah_p2'] = str_replace('.','',@$data['jumlah_p2'][$key]);
      $data_gaji['jumlah_p3'] = str_replace('.','',@$data['jumlah_p3'][$key]);
      $data_gaji['jumlah_total'] = str_replace('.','',@$data['jumlah_total'][$key]);
      $data_gaji['created_by'] = $data['created_by'];
      $data_gaji['is_active'] = $data['is_active'];
      $this->db->insert('dt_gaji',$data_gaji);
    }
  }

  public function update($id,$data)
  {
    // gaji_resume
    $data_gaji_resume['tahun'] = $data['tahun'];
    $data_gaji_resume['bulan'] = $data['bulan'];
    $data_gaji_resume['total_nilai_jabatan_grade'] = str_replace('.','',$data['total_nilai_jabatan_grade']);
    $data_gaji_resume['jasa_dibagi'] = str_replace('.','',$data['jasa_dibagi']);
    $data_gaji_resume['jasa_dibagi_f1'] = str_replace('.','',$data['jasa_dibagi_f1']);
    $data_gaji_resume['jasa_dibagi_f2'] = str_replace('.','',$data['jasa_dibagi_f2']);
    $data_gaji_resume['nominal_pir'] = str_replace('.','',$data['nominal_pir']);
    $data_gaji_resume['nilai_pir'] = str_replace('.','',$data['nilai_pir']);
    $data_gaji_resume['updated_by'] = $data['updated_by'];
    $data_gaji_resume['is_active'] = $data['is_active'];
    $this->db->where('id_gaji_resume', $id);
    $this->db->update('dt_gaji_resume',$data_gaji_resume);
    //
    // gaji
    $data_gaji['tahun'] = $data['tahun'];
    $data_gaji['bulan'] = $data['bulan'];
    foreach($data['id_pegawai'] as $key => $val) {
      $id_gaji = @$data['id_gaji'][$key];
      //
      $data_gaji['tahun_kerja'] = @$data['tahun_kerja'][$key];
      $data_gaji['bulan_kerja'] = @$data['bulan_kerja'][$key];
      $data_gaji['id_pegawai'] = @$data['id_pegawai'][$key];
      $data_gaji['jenjang_pend'] = @$data['jenjang_pend'][$key];
      $data_gaji['kelompok_grade'] = @$data['kelompok_grade'][$key];
      $data_gaji['id_jabatan'] = @$data['id_jabatan'][$key];
      $data_gaji['kd_jabatan_grade'] = @$data['kd_jabatan_grade'][$key];
      $data_gaji['nilai_jabatan_grade'] = str_replace('.','',@$data['nilai_jabatan_grade'][$key]);
      $data_gaji['pir'] = str_replace('.','',@$data['pir'][$key]);
      $data_gaji['total_remun'] = str_replace('.','',@$data['total_remun'][$key]);
      $data_gaji['nilai_p1'] = str_replace('.','',@$data['nilai_p1'][$key]);
      $data_gaji['nilai_p2'] = str_replace('.','',@$data['nilai_p2'][$key]);
      $data_gaji['jumlah_p1'] = str_replace('.','',@$data['jumlah_p1'][$key]);
      $data_gaji['jumlah_p2'] = str_replace('.','',@$data['jumlah_p2'][$key]);
      $data_gaji['jumlah_p3'] = str_replace('.','',@$data['jumlah_p3'][$key]);
      $data_gaji['jumlah_total'] = str_replace('.','',@$data['jumlah_total'][$key]);      
      //
      if($id_gaji == '') {
        $data_gaji['created_by'] = $data['updated_by'];
        $this->db->insert('dt_gaji',$data_gaji);
      } else {
        $data_gaji['updated_by'] = $data['updated_by'];
        $this->db->where('id_gaji', $id_gaji);
        $this->db->update('dt_gaji',$data_gaji);
      }      
    }
  }

  public function delete_temp($id)
  {
    $data = $this->get_by_id($id);
    //
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id_gaji_resume',$id)->update('dt_gaji_resume',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
    //
    $this->db->where(
      array(
        'tahun'=> $data['tahun'],
        'bulan'=> $data['bulan'],
      )
    )->update('dt_gaji',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_gaji',$id)->delete('dt_gaji');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE dt_gaji");
  }

  public function list_komite()
  {
    $data = array(
      'A'   => 'Komite Medis',
      'A1'  => 'Ketua',
      'A2'  => 'Sekretaris',
      'A3'  => 'Anggota',
      'B'   => 'Komite Keperawatan',
      'B1'  => 'Ketua',
      'B2'  => 'Sub komite',
      'B3'  => 'Sub komite',
      'B4'  => 'Sub komite',
      'C'   => 'Komite Mutu',
      'C1'  => 'Ketua',
      'C2'  => 'Sub komite',
      'C3'  => 'Sub komite',
      'C4'  => 'Sub komite',
      'C5'  => 'Sub komite',
      'D'   => 'SPI',
      'D1'  => 'Sekretaris'
    );
    return $data; 
  }

  public function list_panitia()
  {
    $data = array(
      'A'   => 'Panita BPJS',
      'A1'  => 'Anggota',
      'A2'  => 'Anggota',
      'A3'  => 'Anggota',
      'A4'  => 'Anggota',
      'A5'  => 'Anggota',
      'A6'  => 'Anggota',
      'A7'  => 'Anggota',
      'B'   => 'Panitia Pengadaan barang',
      'B1'  => 'Anggota',
      'B2'  => 'Anggota',
      'B3'  => 'Anggota',
      'B4'  => 'Anggota',
      'B5'  => 'Anggota',
      // 'B6'  => 'Anggota',
      // 'B7'  => 'Anggota',
      'C'   => 'Tim Remunerasi',
      'C1'  => 'Anggota',
      'C2'  => 'Anggota',
      'C3'  => 'Anggota',
      'C4'  => 'Anggota',
      'C5'  => 'Anggota',
      // 'C6'  => 'Anggota',
      // 'C7'  => 'Anggota',
    );
    return $data; 
  }

}
