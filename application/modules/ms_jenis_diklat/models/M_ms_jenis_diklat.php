<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ms_jenis_diklat extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_jenis_diklat')
      ->get('ms_jenis_diklat',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_jenis_diklat')
      ->get('ms_jenis_diklat')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->get('ms_jenis_diklat')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id_jenis_diklat',$id)->get('ms_jenis_diklat')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_jenis_diklat','asc')->get('ms_jenis_diklat')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_jenis_diklat','desc')->get('ms_jenis_diklat')->row();
  }

  public function insert($data)
  {
    $this->db->insert('ms_jenis_diklat',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_jenis_diklat',$id)->update('ms_jenis_diklat',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id_jenis_diklat',$id)->update('ms_jenis_diklat',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_jenis_diklat',$id)->delete('ms_jenis_diklat');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ms_jenis_diklat");
  }

}
