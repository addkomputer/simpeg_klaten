<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ms_instansi extends MY_Controller {

	var $access, $id_instansi;

  function __construct(){
		parent::__construct();

		$controller = 'ms_instansi';

    if($this->session->userdata('menu') != $controller){
      $this->session->unset_userdata('search');
      $this->session->set_userdata(array('menu' => 'ms_instansi'));
    }
    $this->load->model('ap_konfigurasi/m_ap_konfigurasi');
		$this->id_instansi = $this->session->userdata('id_instansi');
		$this->access = $this->m_ap_konfigurasi->get_permission($this->id_instansi, $controller);
		if ($this->access == null) {
			redirect(base_url().'ap_error/error_403');
		}
    $this->load->model('m_ms_instansi');
	}
	
	public function index()
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Daftar';

		if ($this->access->_read) {
			$search = null;
			
			if($this->session->userdata('search') != null){
				$search = $this->session->userdata('search');
			}
			
			$data['search'] = $search;
	
			$config['base_url'] = base_url().'ms_instansi/index/';
			$config['per_page'] = 10;
	
			$from = $this->uri->segment(3);
			
			$num_rows = $this->m_ms_instansi->num_rows($search);

			$config['total_rows'] = $num_rows;
			$this->pagination->initialize($config);

			$data['instansi'] = $this->m_ms_instansi->get_list($config['per_page'],$from,$search);

			$data['num_rows'] = $num_rows;
			$data['num_rows_total'] = $this->m_ms_instansi->num_rows_total();
			$data['search'] = $this->session->userdata('search');
			
			insert_log('read',$this->access->menu);
			$this->view('index',$data);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}
	
	public function form($id = null)
	{
		$data['access'] = $this->access;
		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Tambah Data';
				$data['action'] = 'insert';
				$data['instansi'] = null;
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Ubah Data';
				$data['action'] = 'update';
				$data['instansi'] = $this->m_ms_instansi->get_by_id($id);
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		}
	}

	public function search()
	{
		$data = $_POST;
		$this->session->set_userdata(array('search' => $data));
		redirect(base_url().$this->access->controller);
	}

	public function reset_search()
  {
    $this->session->unset_userdata('search');
    redirect(base_url().'ms_instansi/index');
  }

	public function insert()
	{
		$data = $_POST;
		if ($data != null) {
			$data['created_by'] = $this->session->userdata('nama');
			if(!isset($data['is_active'])){$data['is_active'] = 0;}
			$this->m_ms_instansi->insert($data);
			insert_log('insert',$this->access->menu);
			$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
			redirect(base_url().'ms_instansi/index');
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function update()
	{
		$data = $_POST;
		if ($data != null) {
			if($this->access->_update){
				$data['updated_by'] = $this->session->userdata('nama');
				if(!isset($data['is_active'])){$data['is_active'] = 0;}
				$id = $data['id_instansi'];
				$this->m_ms_instansi->update($id, $data);
				insert_log('update',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				redirect(base_url().'ms_instansi/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function delete()
	{
		$data = $_POST;
		if($data != null){
			if ($this->access->_delete) {
				$this->m_ms_instansi->delete_temp($data['id_instansi']);
				insert_log('delete',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				redirect(base_url().'ms_instansi/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}

}