<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ms_instansi extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      if ($search['instansi'] != '') {
        $this->db->like('instansi',$search['instansi']);
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('instansi')
      ->get('ms_instansi',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      if ($search['instansi'] != '') {
        $this->db->like('instansi',$search['instansi']);
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('instansi')
      ->get('ms_instansi')
      ->num_rows();
  }
  
  function num_rows_total(){
    return $this->db
      ->where('is_deleted','0')
      ->order_by('instansi')
      ->get('ms_instansi')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('ms_instansi')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id_instansi',$id)->get('ms_instansi')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_instansi','asc')->get('ms_instansi')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_instansi','desc')->get('ms_instansi')->row();
  }

  public function insert($data)
  {
    $this->db->insert('ms_instansi',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_instansi',$id)->update('ms_instansi',$data);
  }

  public function delete_temp($id)
  {
    $this->db->where('id_instansi',$id)->update('ms_instansi',array('is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_instansi',$id)->delete('ms_instansi');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ms_instansi");
  }

}
