<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-red"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="alert alert-success alert-dismissible" style="background-color:#00b894 !important;border-color:#55efc4">
        <h4><i class="icon fa fa-info"></i> Selamat Datang!</h4>
        Anda masuk sebagai <b><?=$this->session->userdata('nama')?></b> dari <b><?=$this->session->userdata('instansi')?></b><br>
      </div>
      <form id="form_search" action="<?=base_url().$this->access->controller;?>/search" method="post" autocomplete="off">
        <div class="row">
          <div class="col-md-7">
            <div class="form-group">
              <select name="id_instansi" class="form-control select2 input-sm" onchange="$('#form-index').submit()">
                <?php if($this->session->userdata('id_grup') == 3):?>
                  <?php foreach($instansi as $row): ?>
                    <?php if($this->session->userdata('id_instansi') == $row->id_instansi): ?>
                      <option value="<?=$row->id_instansi?>"><?=$row->instansi?></option>
                    <?php endif; ?>
                  <?php endforeach; ?>
                <?php else: ?>
                  <option value="">-- Semua Instansi --</option>
                  <?php foreach($instansi as $row): ?>
                    <option value="<?=$row->id_instansi?>" <?php if($search){if($search['id_instansi'] == $row->id_instansi){echo 'selected';}} ?>><?=$row->instansi?></option>
                  <?php endforeach;?>
                <?php endif; ?>
              </select>
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <select class="form-control select2" name="bulan" id="bulan">
                <option value="01" <?php if(@$search['bulan'] == "01"){echo 'selected';}?>>Januari</option>
                <option value="02" <?php if(@$search['bulan'] == "02"){echo 'selected';}?>>Februari</option>
                <option value="03" <?php if(@$search['bulan'] == "03"){echo 'selected';}?>>Maret</option>
                <option value="04" <?php if(@$search['bulan'] == "04"){echo 'selected';}?>>April</option>
                <option value="05" <?php if(@$search['bulan'] == "05"){echo 'selected';}?>>Mei</option>
                <option value="06" <?php if(@$search['bulan'] == "06"){echo 'selected';}?>>Juni</option>
                <option value="07" <?php if(@$search['bulan'] == "07"){echo 'selected';}?>>Juli</option>
                <option value="08" <?php if(@$search['bulan'] == "08"){echo 'selected';}?>>Agustus</option>
                <option value="09" <?php if(@$search['bulan'] == "09"){echo 'selected';}?>>September</option>
                <option value="10" <?php if(@$search['bulan'] == "10"){echo 'selected';}?>>Oktober</option>
                <option value="11" <?php if(@$search['bulan'] == "11"){echo 'selected';}?>>November</option>
                <option value="12" <?php if(@$search['bulan'] == "12"){echo 'selected';}?>>Desember</option>
              </select>
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <select class="form-control select2" name="tahun" id="tahun">
                <?php for ($i=2018; $i < 2099; $i++): ?>
                  <option value="<?=$i?>" <?php if(@$search['tahun'] == $i){echo 'selected';}?>><?=$i?></option>
                <?php endfor; ?>
              </select>
            </div>
          </div>
          <div class="col-md-1">
            <a class="btn btn-default btn-flat btn-block" href="<?=base_url().$this->access->controller?>/reset_search"><i class="fa fa-refresh"></i></a>
          </div>
        </div>
      </form>
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?=$total_pegawai?></h3>
              <p>Total Pegawai</p>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?=$total_hari?></h3>
              <p>Data Hari Ini</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?=$total_bulan?></h3>
              <p>Data Bulan Ini</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?=$total_tahun?></h3>
              <p>Data Tahun Ini</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
      </div>
      
      <div class="row">
        <div class="col-md-3">
          <!-- BAR CHART -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-bar-chart"></i> Rekap Bulan <?=@month_id($search['bulan'])?> <?=@$search['tahun']?></h3>
            </div>
            <div class="box-body" style="height:350px !important;">
              <div class="chart-responsive">
                <div class="chart-container" style="position: relative; height:50vh; width:95%">
                  <canvas id="rekapBulananChart"></canvas>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <div class="col-md-9">
          <!-- DONUT CHART -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-calendar"></i> Absensi Bulan <?=@month_id($search['bulan'])?> <?=@$search['tahun']?></h3>
            </div>
            <div class="box-body" style="height:350px !important;">
              <div class="chart-responsive">
                <div class="chart-container" style="position: relative; height:50vh; width:95%">
                  <canvas id="absensiBulananChart"></canvas>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->
<script>
  $(document).ready(function () {
    $('.select2').on('select2:select', function (e) {
      $('#form_search').submit();
    });
  })
  $(function () {
    //-------------
    //- JUMLAH PEGAWAI -
    //-------------
    var rekapBulananChartData = {
      labels  : ['P. Dinas', 'Tugas Belajar', 'Sakit', 'Cuti', 'Bencana', 'Ganti'],
      datasets: [
        {
          label               : 'Rekap Bulanan',
          fillColor           : 'rgba(210, 214, 222, 1)',
          strokeColor         : 'rgba(210, 214, 222, 1)',
          pointColor          : 'rgba(210, 214, 222, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : [<?=$rekap_bulanan->perjalanan_dinas?>, <?=$rekap_bulanan->tugas_belajar?>, <?=$rekap_bulanan->sakit?>, 
            <?=$rekap_bulanan->cuti?>, <?=$rekap_bulanan->bencana?>, <?=$rekap_bulanan->ganti?>]
        }
      ]
    }
    var rekapBulananChartCanvas                   = $('#rekapBulananChart').get(0).getContext('2d')
    var rekapBulananChart                         = new Chart(rekapBulananChartCanvas)
    var rekapBulananChartData                     = rekapBulananChartData
    rekapBulananChartData.datasets[0].fillColor   = '#badc58'
    rekapBulananChartData.datasets[0].strokeColor = '#6ab04c'
    rekapBulananChartData.datasets[0].pointColor  = '#0fb9b1'
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : false
    }

    barChartOptions.datasetFill = false
    rekapBulananChart.Bar(rekapBulananChartData, barChartOptions)



    //-------------
    //- ABSENSI BULANAN - 
    //-------------
    var absensiBulananChartData = {
      labels  : [
        <?php foreach ($absensi_bulan as $row) {
          echo "'".$row['tanggal']."', ";
        }?>
      ],
      datasets: [
        {
          label               : 'Absensi Bulanan',
          fillColor           : 'rgba(210, 214, 222, 1)',
          strokeColor         : 'rgba(210, 214, 222, 1)',
          pointColor          : 'rgba(210, 214, 222, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : [
              <?php foreach ($absensi_bulan as $row) {
                echo $row['jumlah'].", ";
              }?>
            ]
        }
      ]
    }
    var absensiBulananChartCanvas                   = $('#absensiBulananChart').get(0).getContext('2d')
    var absensiBulananChart                         = new Chart(absensiBulananChartCanvas)
    var absensiBulananChartData                     = absensiBulananChartData
    absensiBulananChartData.datasets[0].fillColor   = '#fd79a8'
    absensiBulananChartData.datasets[0].strokeColor = '#e84393'
    absensiBulananChartData.datasets[0].pointColor  = '#0fb9b1'
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : false
    }

    barChartOptions.datasetFill = false
    absensiBulananChart.Bar(absensiBulananChartData, barChartOptions)

  })
</script>