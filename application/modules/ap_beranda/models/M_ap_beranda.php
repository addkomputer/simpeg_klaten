<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ap_beranda extends CI_Model {

  private $suffix,$bulan,$tahun;

  function __construct(){
    parent::__construct();
    if ($this->session->userdata('search')) {
      $this->bulan = date('m');
      $this->tahun = date('Y');
      $this->suffix = $this->tahun;
      $this->tanggal = date('Y-m-d');
    }else{
      $this->bulan = date('m');
      $this->tahun = date('Y');
      $this->suffix = date('Y');
      $this->tanggal = date('Y-m-d');
    }
    if ($this->session->userdata('id_grup') == 3) {
      $this->id_instansi = $this->session->userdata('id_instansi');
    }else{
      $this->id_instansi = '';
    }
  }

  function total_pegawai($search){
    $where = '';
    if ($search['id_instansi'] != '') {
      $where .= "WHERE id_instansi='".$search['id_instansi']."' ";
    }
    $query = $this->db->query(
      "SELECT count(id_pegawai) as total FROM dt_pegawai $where"
    );

    return $query->row()->total;
  }

  function total_pns($search){
   $where = "WHERE id_status_pegawai = 2 ";
    if ($search['id_instansi'] != '') {
      $where .= "AND id_instansi='".$search['id_instansi']."' ";
    }
    $query = $this->db->query(
      "SELECT count(id_pegawai) as total FROM dt_pegawai $where"
    );

    return $query->row()->total;
  }

  function total_thl($search){
    $where = "WHERE id_status_pegawai = 5 ";
    if ($search['id_instansi'] != '') {
      $where .= "AND id_instansi='".$search['id_instansi']."' ";
    }
    $query = $this->db->query(
      "SELECT count(id_pegawai) as total FROM dt_pegawai $where"
    );

    return $query->row()->total;
  }

  function total_tahun($search){
    if ($this->db->table_exists('tb_log_kehadiran_'.$search['tahun'])){
      $where = "WHERE 1 = 1 ";
      if ($search['id_instansi'] != '') {
        $where .= "AND b.id_instansi='".$search['id_instansi']."' ";
      }
      $query = $this->db->query(
        "SELECT 
          count(a.id) as total
        FROM tb_log_kehadiran_".$search['tahun']." a
        JOIN dt_pegawai b ON a.id_pegawai = b.id_pegawai
        $where "
      );

      return $query->row()->total;
    }else{
      return 0;
    }
  }

  function total_bulan($search){
    if ($this->db->table_exists('tb_log_kehadiran_'.$search['tahun'])){
      $where = "WHERE a.bulan = '".$search['bulan']."' ";
      if ($search['id_instansi'] != '') {
        $where .= "AND b.id_instansi='".$search['id_instansi']."' ";
      }
      $query = $this->db->query(
        "SELECT 
          count(a.id) as total
        FROM tb_log_kehadiran_".$search['tahun']." a
        JOIN dt_pegawai b ON a.id_pegawai = b.id_pegawai
        $where "
      );

      return $query->row()->total;
    }else{
      return 0;
    }
  }

  function total_hari($search){
    if ($this->db->table_exists('tb_log_kehadiran_'.$search['tahun'])){
      $where = "WHERE a.tanggal = '".date('Y-m-d')."' ";
      if ($search['id_instansi'] != '') {
        $where .= "AND b.id_instansi='".$search['id_instansi']."' ";
      }
      $query = $this->db->query(
        "SELECT 
          count(a.id) as total
        FROM tb_log_kehadiran_".$search['tahun']." a
        JOIN dt_pegawai b ON a.id_pegawai = b.id_pegawai
        $where "
      );

      return $query->row()->total;
    }else{
      return 0;
    }
  }

  function absensi_bulan($search){
    $awal = $search['tahun']."-".$search['bulan']."-01";
    $akhir = date("Y-m-t", strtotime( $search['tahun']."-".$search['bulan']."-01"));

    $res = array();
    if ($this->db->table_exists('tb_log_kehadiran_'.$search['tahun'])){
      $where = "";
      if ($search['id_instansi'] != '') {
        $where .= "AND b.id_instansi='".$search['id_instansi']."' ";
      }
      for ($i=$awal; $i <= $akhir; $i++) { 
        $count = $this->db->query(
          "SELECT count(id) as total FROM tb_log_kehadiran_".$search['tahun']." a 
          JOIN dt_pegawai b ON a.id_pegawai = b.id_pegawai
          WHERE a.tanggal='".$i."' ".$where
        )->row()->total;
        $data = array(
          'tanggal' => date_to_id($i),
          'jumlah' => $count
        );
        array_push($res,$data);
      }
    }else{
      for ($i=$awal; $i <= $akhir; $i++) { 
        $data = array(
          'tanggal' => date_to_id($i),
          'jumlah' => 0
        );
        array_push($res,$data);
      }
    }

    return $res;
  }

  function rekap_bulanan($search){
    if ($this->db->table_exists('tb_log_kehadiran_'.$search['tahun'])){
      $where = "WHERE 1 = 1 AND a.bulan='".$search['bulan']."' ";
      if ($search['id_instansi'] != '') {
        $where .= "AND b.id_instansi = '".$search['id_instansi']."' ";
      }
      $query = $this->db->query(
        "SELECT 
          COUNT(CASE WHEN a.id_tipe LIKE '2%' THEN 1 END) AS perjalanan_dinas,
          COUNT(CASE WHEN a.id_tipe LIKE '3%' THEN 1 END) AS tugas_belajar,
          COUNT(CASE WHEN a.id_tipe LIKE '4%' THEN 1 END) AS sakit,
          COUNT(CASE WHEN a.id_tipe LIKE '5%' THEN 1 END) AS cuti,
          COUNT(CASE WHEN a.id_tipe LIKE '6%' THEN 1 END) AS bencana,
          COUNT(CASE WHEN a.id_tipe LIKE '7%' THEN 1 END) AS ganti
        FROM tb_log_kehadiran_".$search['tahun']." a
        JOIN dt_pegawai b ON a.id_pegawai = b.id_pegawai
        $where
        "
      );

      return $query->row();
    }else{
      return $res = array(
        'perjalanan_dinas' => 0,
        'tugas_belajar' => 0,
        'sakit' => 0,
        'cuti' => 0,
        'bencana' => 0,
        'ganti' => 0
      );
    }
  }
}
