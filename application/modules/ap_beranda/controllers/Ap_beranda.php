<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ap_beranda extends MY_Controller {

	var $access, $id_grup;

  function __construct(){
		parent::__construct();

		$controller = 'ap_beranda';

		if($this->session->userdata('menu')){
			if($this->session->userdata('menu') != $controller){
				$this->session->unset_userdata('search_term');
				$this->session->set_userdata(array('menu' => 'ap_beranda'));
			}
		}
    $this->load->model('ap_konfigurasi/m_ap_konfigurasi');
		$this->id_grup = $this->session->userdata('id_grup');
		$this->access = $this->m_ap_konfigurasi->get_permission($this->id_grup, $controller);
		if ($this->access == null) {
			redirect(base_url().'ap_error/error_403');
		}else{
			$this->load->model('m_ap_beranda');
			$this->load->model('ms_instansi/m_ms_instansi');
		}
	}

	public function index()
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Ringkasan';
		$data['instansi'] = $this->m_ms_instansi->get_all();
		insert_log('read',$this->access->menu);

		if($this->session->userdata('search') != null){
			$search = $this->session->userdata('search');
		}else{
			$id_instansi = '';
			if ($this->session->userdata('id_grup') == 3) {
				$id_instansi = $this->session->userdata('id_instansi');
			}
			$search = array(
				'bulan' => date('m'),
				'tahun' => date('Y'),
				'id_instansi' => $id_instansi
			);
		}

		$data['search'] = $search;

		$data['total_pegawai'] = $this->m_ap_beranda->total_pegawai($search);
		$data['total_pns'] = $this->m_ap_beranda->total_pns($search);
		$data['total_thl'] = $this->m_ap_beranda->total_thl($search);
		$data['total_tahun'] = $this->m_ap_beranda->total_tahun($search);
		$data['total_bulan'] = $this->m_ap_beranda->total_bulan($search);
		$data['total_hari'] = $this->m_ap_beranda->total_hari($search);
		$data['absensi_bulan'] = $this->m_ap_beranda->absensi_bulan($search);
		$data['rekap_bulanan'] = $this->m_ap_beranda->rekap_bulanan($search);

		
		// echo '<pre>' . var_export($data, true) . '</pre>';
		$this->view('index',$data);
	}

	public function search()
	{
		$search = $_POST;
		$this->session->set_userdata(array('search' => $search));
		redirect(base_url().$this->access->controller);
	}

	public function reset_search()
  {
    $this->session->unset_userdata('search');
    redirect(base_url().$this->access->controller);
  }

}
