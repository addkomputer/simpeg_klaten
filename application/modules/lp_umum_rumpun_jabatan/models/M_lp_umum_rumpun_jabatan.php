<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_lp_umum_rumpun_jabatan extends CI_Model {

  public function get_list($number,$offset,$search = null)
  {
    $where = "WHERE is_pegawai = 1";
    if ($search != null) {
      if ($search['id_ruang'] != '') {
        $where .= " AND a.id_ruang = ".$search['id_ruang'];
      }
      if ($search['id_rumpun_jabatan'] != '') {
        $where .= " AND substr(a.id_jabatan,17,1) = '".$search['id_rumpun_jabatan']."'";
      }
      if ($search['id_status_pegawai'] != '') {
        $where .= " AND a.id_status_pegawai = '".$search['id_status_pegawai']."'";
      }
    }

    return $this->db->query(
      "SELECT 
        a.gelar_depan,a.nama,a.gelar_belakang,a.nomor_induk,a.jenis_kelamin,
        a.tempat_lhr,a.tgl_lhr,
        b.status_pegawai,
        c.jabatan,
        d.ruang,
        e.golongan,e.pangkat,
        f.eselon,
        g.rumpun_jabatan
      FROM dt_pegawai a
        JOIN ms_status_pegawai b ON a.id_status_pegawai = b.id_status_pegawai
        LEFT JOIN ms_jabatan c ON a.id_jabatan = c.id_jabatan
        LEFT JOIN ms_ruang d ON a.id_ruang = d.id_ruang
        LEFT JOIN ms_golongan e ON a.id_golongan = e.id_golongan
        LEFT JOIN ms_eselon f ON right(a.id_jabatan,2) = f.id_eselon
        LEFT JOIN ms_rumpun_jabatan g ON substr(a.id_jabatan,17,1) = g.id_rumpun_jabatan
      $where
      ORDER BY 
        a.id_status_pegawai ASC, 
        a.id_golongan ASC
      LIMIT $offset,$number"
    )->result();
  }

  function num_rows($search = null){
    $where = "WHERE is_pegawai = 1";
    if ($search != null) {
      if ($search['id_ruang'] != '') {
        $where .= " AND a.id_ruang = ".$search['id_ruang'];
      }
      if ($search['id_rumpun_jabatan'] != '') {
        $where .= " AND substr(a.id_jabatan,17,1) = '".$search['id_rumpun_jabatan']."'";
      }
      if ($search['id_status_pegawai'] != '') {
        $where .= " AND a.id_status_pegawai = '".$search['id_status_pegawai']."'";
      }
    }
    
    return $this->db->query(
      "SELECT 
        a.gelar_depan,a.nama,a.gelar_belakang,a.nomor_induk,a.jenis_kelamin,
        a.tempat_lhr,a.tgl_lhr,
        b.status_pegawai,
        c.jabatan,
        d.ruang,
        e.golongan,e.pangkat,
        f.eselon,
        g.rumpun_jabatan
      FROM dt_pegawai a
        JOIN ms_status_pegawai b ON a.id_status_pegawai = b.id_status_pegawai
        LEFT JOIN ms_jabatan c ON a.id_jabatan = c.id_jabatan
        LEFT JOIN ms_ruang d ON a.id_ruang = d.id_ruang
        LEFT JOIN ms_golongan e ON a.id_golongan = e.id_golongan
        LEFT JOIN ms_eselon f ON right(a.id_jabatan,2) = f.id_eselon
        LEFT JOIN ms_rumpun_jabatan g ON substr(a.id_jabatan,17,1) = g.id_rumpun_jabatan
      $where
      ORDER BY 
        a.id_status_pegawai ASC, 
        a.id_golongan ASC"
    )->num_rows();
  }
  
  function get_print($search = null){
    $where = "WHERE is_pegawai = 1";
    if ($search != null) {
      if ($search['id_ruang'] != '') {
        $where .= " AND a.id_ruang = ".$search['id_ruang'];
      }
      if ($search['id_rumpun_jabatan'] != '') {
        $where .= " AND substr(a.id_jabatan,17,1) = '".$search['id_rumpun_jabatan']."'";
      }
      if ($search['id_status_pegawai'] != '') {
        $where .= " AND a.id_status_pegawai = '".$search['id_status_pegawai']."'";
      }
    }


    return $this->db->query(
      "SELECT 
        a.gelar_depan,a.nama,a.gelar_belakang,a.nomor_induk,a.jenis_kelamin,
        a.tempat_lhr,a.tgl_lhr,
        b.status_pegawai,
        c.jabatan,
        d.ruang,
        e.golongan,e.pangkat,
        f.eselon,
        g.rumpun_jabatan
      FROM dt_pegawai a
        JOIN ms_status_pegawai b ON a.id_status_pegawai = b.id_status_pegawai
        LEFT JOIN ms_jabatan c ON a.id_jabatan = c.id_jabatan
        LEFT JOIN ms_ruang d ON a.id_ruang = d.id_ruang
        LEFT JOIN ms_golongan e ON a.id_golongan = e.id_golongan
        LEFT JOIN ms_eselon f ON right(a.id_jabatan,2) = f.id_eselon
        LEFT JOIN ms_rumpun_jabatan g ON substr(a.id_jabatan,17,1) = g.id_rumpun_jabatan
      $where
      ORDER BY 
        a.id_status_pegawai ASC, 
        a.id_golongan ASC"
    )->result();
  }

  public function count_print($search)
  {
    $profil = $this->db->get('ap_profil')->row();
    $this->db->select("COUNT(CASE WHEN substr(a.id_jabatan,17,1) = '1' THEN 1 END) AS st");
    $this->db->select("COUNT(CASE WHEN substr(a.id_jabatan,17,1) = '2' THEN 1 END) AS ft");
    $this->db->select("COUNT(CASE WHEN substr(a.id_jabatan,17,1) = '3' THEN 1 END) AS fu");
    $this->db->select("COUNT(CASE WHEN substr(a.id_jabatan,17,1) = '4' THEN 1 END) AS pn");

    if ($search != null) {
      if ($search['id_rumpun_jabatan'] != '') {      
        $this->db->where('substr(a.id_jabatan,17,1)',$search['id_rumpun_jabatan']);
      }
      if ($search['id_ruang'] != '') {
        $this->db->where('a.id_ruang',$search['id_ruang']);
      }
      if ($search['id_status_pegawai'] != '') {
        $this->db->where('a.id_status_pegawai',$search['id_status_pegawai']);
      }
    }

    return $this->db
      ->where('a.is_deleted', 0)
      ->get('dt_pegawai a')
      ->row();
  }
  
}
