<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_lp_umum_ruang extends CI_Model {

  public function get_list($number,$offset,$search = null)
  {
    $where = "WHERE is_pegawai = 1";
    if ($search != null) {
      if ($search['id_ruang'] != '') {
        $where .= " AND a.id_ruang = ".$search['id_ruang'];
      }
      if ($search['id_jabatan'] != '') {
        $where .= " AND a.id_jabatan = '".$search['id_jabatan']."'";
      }
      if ($search['id_status_pegawai'] != '') {
        $where .= " AND a.id_status_pegawai = '".$search['id_status_pegawai']."'";
      }
    }

    return $this->db->query(
      "SELECT 
        a.gelar_depan,a.nama,a.gelar_belakang,a.nomor_induk,a.jenis_kelamin,
        a.tempat_lhr,a.tgl_lhr,
        b.status_pegawai,
        c.jabatan,
        d.ruang,
        e.golongan,e.pangkat,
        f.eselon
      FROM dt_pegawai a
        JOIN ms_status_pegawai b ON a.id_status_pegawai = b.id_status_pegawai
        LEFT JOIN ms_jabatan c ON a.id_jabatan = c.id_jabatan
        LEFT JOIN ms_ruang d ON a.id_ruang = d.id_ruang
        LEFT JOIN ms_golongan e ON a.id_golongan = e.id_golongan
        LEFT JOIN ms_eselon f ON right(a.id_jabatan,2) = f.id_eselon
      $where
      ORDER BY 
        a.id_status_pegawai ASC, 
        a.id_golongan ASC
      LIMIT $offset,$number"
    )->result();
  }

  function num_rows($search = null){
    $where = "WHERE is_pegawai = 1";
    if ($search != null) {
      if ($search['id_ruang'] != '') {
        $where .= " AND a.id_ruang = ".$search['id_ruang'];
      }
      if ($search['id_jabatan'] != '') {
        $where .= " AND a.id_jabatan = '".$search['id_jabatan']."'";
      }
      if ($search['id_status_pegawai'] != '') {
        $where .= " AND a.id_status_pegawai = '".$search['id_status_pegawai']."'";
      }
    }
    
    return $this->db->query(
      "SELECT 
        a.gelar_depan,a.nama,a.gelar_belakang,a.nomor_induk,a.jenis_kelamin,
        a.tempat_lhr,a.tgl_lhr,
        b.status_pegawai,
        c.jabatan,
        d.ruang,
        e.golongan,e.pangkat,
        f.eselon
      FROM dt_pegawai a
        JOIN ms_status_pegawai b ON a.id_status_pegawai = b.id_status_pegawai
        LEFT JOIN ms_jabatan c ON a.id_jabatan = c.id_jabatan
        LEFT JOIN ms_ruang d ON a.id_ruang = d.id_ruang
        LEFT JOIN ms_golongan e ON a.id_golongan = e.id_golongan
        LEFT JOIN ms_eselon f ON right(a.id_jabatan,2) = f.id_eselon
      $where
      ORDER BY 
        a.id_status_pegawai ASC, 
        a.id_golongan ASC"
    )->num_rows();
  }
  
  function get_print($search = null){
    $where = "WHERE is_pegawai = 1";
    if ($search != null) {
      if ($search['id_ruang'] != '') {
        $where .= " AND a.id_ruang = ".$search['id_ruang'];
      }
      if ($search['id_jabatan'] != '') {
        $where .= " AND a.id_jabatan = '".$search['id_jabatan']."'";
      }
      if ($search['id_status_pegawai'] != '') {
        $where .= " AND a.id_status_pegawai = '".$search['id_status_pegawai']."'";
      }
    }

    return $this->db->query(
      "SELECT 
        a.gelar_depan,a.nama,a.gelar_belakang,a.nomor_induk,a.jenis_kelamin,
        a.tempat_lhr,a.tgl_lhr,
        b.status_pegawai,
        c.jabatan,
        d.ruang,
        e.golongan,e.pangkat,
        f.eselon
      FROM dt_pegawai a
        JOIN ms_status_pegawai b ON a.id_status_pegawai = b.id_status_pegawai
        LEFT JOIN ms_jabatan c ON a.id_jabatan = c.id_jabatan
        LEFT JOIN ms_ruang d ON a.id_ruang = d.id_ruang
        LEFT JOIN ms_golongan e ON a.id_golongan = e.id_golongan
        LEFT JOIN ms_eselon f ON right(a.id_jabatan,2) = f.id_eselon
      $where
      ORDER BY 
        a.id_status_pegawai ASC, 
        a.id_golongan ASC"
    )->result();
  }

  public function count_print($search)
  {
    $profil = $this->db->get('ap_profil')->row();
    $this->db->select('a.status_pegawai, count(b.id_status_pegawai) as jumlah');

    if ($search != null) {
      if ($search['id_jabatan'] != '') {
        $this->db->where('b.id_jabatan',$search['id_jabatan']);
      }
      if ($search['id_ruang'] != '') {
        $this->db->where('b.id_ruang',$search['id_ruang']);
      }
      if ($search['id_status_pegawai'] != '') {
        $this->db->where('b.id_status_pegawai',$search['id_status_pegawai']);
      }
    }

    return $this->db
      ->where('b.id_unit_kerja', strval($profil->id_unit_kerja))
      ->where('b.id_kedudukan_pns',1)
      ->where('b.is_deleted', 0)
      ->join('dt_pegawai b','b.id_status_pegawai = a.id_status_pegawai','left')
      ->group_by('b.id_status_pegawai')
      ->order_by('a.id_status_pegawai')
      ->get('ms_status_pegawai a')
      ->result();
  }
  
}
