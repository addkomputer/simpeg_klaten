
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ap_pengguna extends MY_Controller {

	var $access, $id_grup;

  function __construct(){
		parent::__construct();

		$module_controller = 'ap_pengguna';

    if($this->session->userdata('menu') != $module_controller){
      $this->session->unset_userdata('search');
      $this->session->set_userdata(array('menu' => 'ap_pengguna'));
    }
    $this->load->model('ap_konfigurasi/m_ap_konfigurasi');
		$this->id_grup = $this->session->userdata('id_grup');
		$this->access = $this->m_ap_konfigurasi->get_permission($this->id_grup, $module_controller);
		if ($this->access == null) {
			redirect(base_url().'ap_error/error_403');
		}else{
			$this->load->model('m_ap_pengguna');
			$this->load->model('ap_grup/m_ap_grup');
			$this->load->model('ms_instansi/m_ms_instansi');
		}
	}
	
	public function index()
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Daftar';

		if ($this->access->_read) {
			$search = null;
			if($search = $_POST){
				$this->session->set_userdata(array('search' => $search));
			}else{
				if($this->session->userdata('search') != null){
					$search = $this->session->userdata('search');
				}
			}
			$data['search'] = $search;
	
	
			$config['base_url'] = base_url().'ap_pengguna/index/';
			$config['per_page'] = 10;
	
			$from = $this->uri->segment(3);
	
			if($search == null){
				$num_rows = $this->m_ap_pengguna->num_rows();
	
				$config['total_rows'] = $num_rows;
				$this->pagination->initialize($config);
	
				$data['pengguna'] = $this->m_ap_pengguna->get_list($config['per_page'],$from,$search = null);
			}else{
				$search = $this->session->userdata('search');
				$num_rows = $this->m_ap_pengguna->num_rows($search);
				$config['total_rows'] = $num_rows;
				$this->pagination->initialize($config);
	
				$data['pengguna'] = $this->m_ap_pengguna->get_list($config['per_page'],$from,$search);
			}
			insert_log('read',$this->access->menu);
			$data['num_rows'] = $num_rows;
			$data['num_rows_total'] = $this->m_ap_pengguna->num_rows_total();
			$this->view('index',$data);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}
	
	public function form($id = null)
	{
		$data['access'] = $this->access;
		$data['role'] = $this->m_ap_grup->get_all();
		$data['instansi'] = $this->m_ms_instansi->get_all();
		
		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Tambah Data';
				$data['action'] = 'insert';
				$data['pengguna'] = null;
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Ubah Data';
				$data['action'] = 'update';
				$data['pengguna'] = $this->m_ap_pengguna->get_by_id($id);
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		}
	}

	public function reset_search()
  {
    $this->session->unset_userdata('search');
    redirect(base_url().'ap_pengguna/index');
  }

	public function insert()
	{
		$data = $_POST;
		if ($data != null) {
			$data['created_by'] = $this->session->userdata('nama');
			if(!isset($data['is_active'])){$data['is_active'] = 0;}
			$data['user_pass'] = md5($data['user_pass']);
			unset($data['user_pass_repeat']);
			$this->m_ap_pengguna->insert($data);
			insert_log('insert',$this->access->menu);
			$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
			redirect(base_url().'ap_pengguna/index');
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function update()
	{
		$data = $_POST;
		if ($data != null) {
			if($this->access->_update){
				$data['updated_by'] = $this->session->userdata('nama');
				if(!isset($data['is_active'])){$data['is_active'] = 0;}
				$id = $data['id_pengguna'];
				unset($data['user_pass_repeat']);
				$this->m_ap_pengguna->update($id, $data);
				insert_log('update',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				redirect(base_url().'ap_pengguna/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function update_password()
	{
		$data = $_POST;
		if ($data != null) {
			if($this->access->_update){
				$data['updated_by'] = $this->session->userdata('menu');
				$data['user_pass'] = md5($data['user_pass']);
				$id = $data['id_pengguna'];
				unset($data['user_pass_repeat']);
				$this->m_ap_pengguna->update($id, $data);
				insert_log('update',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				redirect(base_url().'ap_pengguna/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function delete()
	{
		$data = $_POST;
		if($data != null){
			if ($this->access->_delete) {
				$this->m_ap_pengguna->delete_temp($data['id_pengguna']);
				insert_log('delete',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				redirect(base_url().'ap_pengguna/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}

}