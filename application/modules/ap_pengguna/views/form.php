<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-red"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">Pengaturan</li>
        <li class="active text-orange"><?=$access->menu?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-danger">
        <div class="box-header with-border">
          <i class="fa fa-table text-maroon"></i>
          <h3 class="box-title">Form <?=$access->menu?></h3>
        </div>
        <form id="form" class="form-horizontal" action="<?=base_url()?>ap_pengguna/<?=$action?>" method="post" autocomplete="off">
          <div class="box-body">
            <input type="hidden" class="form-control" name="id_pengguna" id="id_pengguna" value="<?php if($pengguna != null){echo $pengguna->id_pengguna;};?>">
            <div class="form-group">
              <label class="col-sm-2 control-label">Role</label>
              <div class="col-sm-4">
                <select class="form-control select2" name="id_grup" id="id_grup">
                  <option value="default">-- Pilih --</option>
                  <?php foreach ($role as $row): ?>
                    <option value="<?=$row->id_grup?>" <?php if($pengguna !=null){if($pengguna->id_grup == $row->id_grup){echo 'selected';}}?>><?=$row->id_grup.' - '.$row->grup?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Instansi</label>
              <div class="col-sm-5">
                <select name="id_instansi" class="form-control select2 input-sm">
                  <option value="">-- Semua Instansi --</option>
                  <?php foreach($instansi as $row): ?>
                    <option value="<?=$row->id_instansi?>" <?php if($pengguna !=null){if($pengguna->id_instansi == $row->id_instansi){echo 'selected';}}?>><?=$row->instansi?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Nama Lengkap</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" name="nama" id="nama" value="<?php if($pengguna != null){echo $pengguna->nama;};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Nama Pengguna</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" name="user_name" id="user_name" value="<?php if($pengguna != null){echo $pengguna->user_name;};?>" required>
              </div>
            </div>
            <?php if($action == 'insert'):?>
              <div class="form-group">
                <label class="col-sm-2 control-label">Password</label>
                <div class="col-sm-3">
                  <input type="password" class="form-control" name="user_pass" id="user_pass" value="" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Ulang Password</label>
                <div class="col-sm-3">
                  <input type="password" class="form-control" name="user_pass_repeat" data-match="#user_pass" data-match-error="Password tidak sama!" value="" required>
                  <div class="help-block with-errors"></div>
                </div>
              </div>
            <?php endif;?>
            <div class="form-group">
              <label class="col-sm-2 control-label">Aktif</label>
              <div class="col-sm-5">
                <input class="iCheckbox" type="checkbox" name="is_active" value="1" <?php if($pengguna != null){if($pengguna->is_active == 1){echo 'checked';}}else{echo 'checked';}?>>
              </div>
            </div>
            <hr>
            <div class="form-group">
              <div class="col-md-offset-2 col-sm-8">
                <button class="btn btn-flat btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button>
                <a class="btn btn-flat btn-default" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Batal</a>
              </div>
            </div>
          </div>
        </form>
      </div>
      <?php if($action == 'update'): ?>
        <div class="box box-danger">
          <form id="form_password" class="form-horizontal" action="<?=base_url()?>ap_pengguna/update_password" method="post">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-key text-maroon"></i> Ganti Password</h3>
            </div>
            <div class="box-body">
              <input type="hidden" class="form-control" name="id_pengguna" id="id_pengguna" value="<?php if($pengguna != null){echo $pengguna->id_pengguna;};?>">
              <div class="form-group">
                <label class="col-sm-2 control-label">Password</label>
                <div class="col-sm-3">
                  <input type="password" class="form-control" name="user_pass" id="user_pass" value="" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Ulang Password</label>
                <div class="col-sm-3">
                  <input type="password" class="form-control" name="user_pass_repeat" data-match="#user_pass" data-match-error="Password tidak sama!" value="" required>
                  <div class="help-block with-errors"></div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-offset-2 col-sm-8">
                  <button class="btn btn-flat btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button>
                  <a class="btn btn-flat btn-default" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Batal</a>
                </div>
              </div>
            </div>
          </form>
        </div>
      <?php endif;?>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<script>
  $(document).ready(function () {
    // form validator
    $('#form').validate({      
      rules : {
        id_grup : {
          valueNotEquals: "default"
        },
        user_pass_repeat : {
          equalTo : "#user_pass"
        }
      }
    });
    $('#form_password').validate({
      rules : {
        user_pass_repeat : {
          equalTo : "#user_pass"
        }
      }
    });
  })
</script>