<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ap_pengguna extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
    $this->db->select('a.*, b.grup, c.instansi');
    if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('a.is_deleted','0')
      ->join('ap_grup b','a.id_grup = b.id_grup')
      ->join('ms_instansi c','a.id_instansi = c.id_instansi','left')
      ->order_by('a.id_grup')
      ->order_by('id_pengguna','asc')
      ->get('ap_pengguna a',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('a.is_deleted','0')
      ->join('ap_grup b','a.id_grup = b.id_grup')
      ->join('ms_instansi c','a.id_instansi = c.id_instansi','left')
      ->order_by('id_pengguna')
      ->get('ap_pengguna a')
      ->num_rows();
  }
  
  function num_rows_total(){
    return $this->db
      ->where('a.is_deleted','0')
      ->join('ap_grup b','a.id_grup = b.id_grup')
      ->join('ms_instansi c','a.id_instansi = c.id_instansi','left')
      ->order_by('id_pengguna')
      ->get('ap_pengguna a')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('ap_pengguna')->result();
	}

  public function get_by_id($id)
  {
    return $this->db
      ->where('id_pengguna',$id)
      ->get('ap_pengguna')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_pengguna','asc')->get('ap_pengguna')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_pengguna','desc')->get('ap_pengguna')->row();
  }

  public function insert($data)
  {
    $this->db->insert('ap_pengguna',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_pengguna',$id)->update('ap_pengguna',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id_pengguna',$id)->update('ap_pengguna',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_pengguna',$id)->delete('ap_pengguna');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ap_pengguna");
  }

}
