<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ap_konfigurasi extends CI_Model {

  public function get_permission($id_grup, $controller)
  {
    $id_grup = $this->session->userdata('id_grup');
    $query = $this->db->query(
      "SELECT * 
      FROM
        ap_hak_akses a
      JOIN ap_menu b ON a.id_menu = b.id_menu
      WHERE
        a.id_grup = '$id_grup' AND
        b.controller = '$controller'"
    );
    return $query->row();
  }

	public function get_menu($induk = null)
  {
    $id_grup = $this->session->userdata('id_grup');

    $sql_where = '';
    $sql_where .= ($induk != '') ? "b.induk = '$induk'" : 'b.induk = ""';

    $query = $this->db->query(
      "SELECT * 
      FROM
        ap_hak_akses a
      JOIN ap_menu b ON a.id_menu = b.id_menu
      WHERE
        a.id_grup = '$id_grup' AND 
        $sql_where
      ORDER BY a.id_menu"
    );
    if ($query->num_rows() > 0) {
      $result = $query->result_array();
      foreach ($result as $key => $val) {
        $result[$key]['child'] = $this->get_menu($result[$key]['id_menu']);
      }
      return $result;
    }else{
      return array();
    }
  }

  public function get_menu_first($id_grup)
  {
    $query = $this->db->query(
      "SELECT * 
      FROM
        ap_hak_akses a
      JOIN ap_menu b ON a.id_menu = b.id_menu
      WHERE
        a.id_grup = '$id_grup' AND
        b.tipe = 1 AND
        b.url != '#'
      ORDER BY a.id_menu"
    );

    return $query->row();
  }

}
