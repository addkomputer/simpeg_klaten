<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dt_keluarga extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_pegawai')
      ->get('dt_keluarga',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_pegawai')
      ->get('dt_keluarga')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('dt_keluarga')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id_pegawai',$id)->get('dt_keluarga')->row();
  }

  public function get_by_id_pegawai($id)
  {
    return $this->db->where('id_pegawai',$id)->get('dt_keluarga')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_pegawai','asc')->get('dt_keluarga')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_pegawai','desc')->get('dt_keluarga')->row();
  }

  public function insert($data)
  {
    $this->db->insert('dt_keluarga',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_pegawai',$id)->update('dt_keluarga',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id_pegawai',$id)->update('dt_keluarga',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_pegawai',$id)->delete('dt_keluarga');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE dt_keluarga");
  }

}
