<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-primary">
        <form id="form" class="form-horizontal" action="<?=base_url()?>ms_jabatan/<?=$action?>" method="post" autocomplete="off"> 
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-2 control-label">ID</label>
              <div class="col-sm-3">
                <input type="text" class="form-control input-sm" name="id_jabatan" id="id_jabatan" value="<?php if($jabatan != null){echo $jabatan->id_jabatan;};?>">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Nama</label>
              <div class="col-sm-3">
                <input type="text" class="form-control input-sm" name="jabatan" id="jabatan" value="<?php if($jabatan != null){echo $jabatan->jabatan;};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Aktif</label>
              <div class="col-sm-5">
                <input class="iCheckbox" type="checkbox" name="is_active" value="1" <?php if($jabatan != null){if($jabatan->is_active == 1){echo 'checked';}}else{echo 'checked';}?>>
              </div>
            </div>
            <hr>
            <div class="form-group">
              <div class="col-md-offset-2 col-sm-8">
                <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
                <a class="btn btn-sm btn-default" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Batal</a>
              </div>
            </div>
          </div>
        </form>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<script>
  $(document).ready(function () {
    // form validator
    $('#form').validate();
  })
</script>