<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dt_log extends CI_Model {

	public function create_table($suffix)
  {
    $table = 'tb_log_'.$suffix;
    $this->db->query(
      "CREATE TABLE IF NOT EXISTS `$this->table_scanlog` (
        `SN` text NOT NULL,
        `ScanDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `PIN` text NOT NULL,
        `VerifyMode` int(11) NOT NULL,
        `IOMode` int(11) NOT NULL,
        `WorkCode` int(11) NOT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;"
    );
  }
}
