<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_tb_user extends CI_Model {

  public function get_list($number,$offset,$search = null)
  {
    $profil = $this->db->get('ap_profil')->row();
		if ($search != null) {
      if ($search['term'] != '') {
        $this->db->like('a.nama',$search['term']);
      }
      if ($search['id_ruang'] != '') {
        $this->db->where('a.id_ruang',$search['id_ruang']);
      }
      if ($search['id_jabatan'] != '') {
        $this->db->where('a.id_jabatan',$search['id_jabatan']);
      }
    }
    
    return $this->db
      ->join('ms_jabatan b', 'a.id_jabatan = b.id_jabatan','left')
      ->join('ms_status_pegawai e', 'a.id_status_pegawai = e.id_status_pegawai')
      ->join('ms_ruang f', 'a.id_ruang = f.id_ruang','left')
      ->where('a.is_deleted', 0)
      ->order_by('a.id_golongan','desc')
      ->order_by('a.id_pegawai')
      ->get('dt_pegawai a',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		$profil = $this->db->get('ap_profil')->row();

		if ($search != null) {
      if ($search['term'] != '') {
        $this->db->like('a.nama',$search['term']);
      }
      if ($search['id_ruang'] != '') {
        $this->db->where('a.id_ruang',$search['id_ruang']);
      }
      if ($search['id_jabatan'] != '') {
        $this->db->where('a.id_jabatan',$search['id_jabatan']);
      }
    }
    
    return $this->db
      ->join('ms_jabatan b', 'a.id_jabatan = b.id_jabatan','left')
      ->join('ms_status_pegawai e', 'a.id_status_pegawai = e.id_status_pegawai')
      ->join('ms_ruang f', 'a.id_ruang = f.id_ruang','left')
      ->where('a.is_deleted', 0)
      ->order_by('a.id_golongan','desc')
      ->order_by('a.id_pegawai')
      ->get('dt_pegawai a')
      ->num_rows();
  }
  
  function num_rows_all(){
    $profil = $this->db->get('ap_profil')->row();

    return $this->db
      ->join('ms_jabatan b', 'a.id_jabatan = b.id_jabatan','left')
      ->join('ms_status_pegawai e', 'a.id_status_pegawai = e.id_status_pegawai')
      ->join('ms_ruang f', 'a.id_ruang = f.id_ruang','left')
      ->where('a.is_deleted', 0)
      ->order_by('a.id_golongan','desc')
      ->order_by('a.id_pegawai')
      ->get('dt_pegawai a')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('dt_pegawai a')->result();
  }

  public function get_by_id($id)
  {
   
     return $this->db
      ->join('dt_pegawai b','a.pin=b.pin')
      ->join('tb_device c','a.device_sn=c.device_sn')
      ->join('tb_template d','a.pin=d.pin')
      ->where('a.pin',$id)
      ->get('tb_user a')->row();
  }

  public function num_row_by_id($id)
  {
   
     return $this->db
      ->where('a.pin',$id)
      ->get('tb_user a')->num_rows();
  }

  public function get_by_pin_sn($id, $sn)
  {
     return $this->db
      ->join('dt_pegawai b','a.pin=b.pin')
      ->join('tb_device c','a.device_sn=c.device_sn')
      ->join('tb_template d','a.pin=d.pin')
      ->where('a.pin',$id)
      ->where('a.device_sn',$sn)
      ->get('tb_user a')->row();
  }

  public function get_tmp($id)
  {
     return $this->db
      ->where('a.pin',$id)
      ->get('tb_template a')->result();
  }

  public function get_first()
  {
    return $this->db->order_by('id_user','asc')->get('tb_user')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_user','desc')->get('tb_user')->row();
  }

  public function insert($data)
  {
    $this->db->insert('tb_user',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_user',$id)->update('tb_user',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id_user',$id)->update('tb_user',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id, $sn)
  {
    $this->db->where('pin',$id)->where('device_sn',$sn)->delete('tb_user');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE tb_user");
  }

}
