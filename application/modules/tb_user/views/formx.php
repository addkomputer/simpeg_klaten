<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i>
        <?=$subtitle?>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-primary">
        
          <div class="box-body">
            <input type="hidden" class="form-control input-sm" name="id_user" id="id_user" value="<?php if($user != null){echo $user->id_user;};?>">
             <?php if($raw["Result"]): ?>
            <div class="form-group">
              <label class="col-sm-2 control-label">NIP</label>
              <div class="col-sm-3">
                <?=$user->nomor_induk?>
              </div>
            </div>
            <br>
            <div class="form-group">  
              <label class="col-sm-2 control-label">Nama</label>
              <div class="col-sm-3">
                      <?php
                          echo $user->gelar_depan.' '.$user->nama;
                          if ($user->gelar_belakang != ''){
                            echo ', '.$user->gelar_belakang;
                          }
                        ?>
              </div>
            </div>
            <br>
            <br>
            <div class="form-group">
              <label class="col-sm-2 control-label">Jumlah Lokasi</label>
              <div class="col-sm-3">
              <?=$jlokasi?> Lokasi
              </div>
            </div>
            <br>
            
            <hr>
            <div class="form-group">
              <div class="col-md-offset-2 col-sm-8">
                
                <a class="btn btn-sm btn-default" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Kembali</a>
              </div>
            </div>
          </div>
       
      </div>
      <hr>
      <div class="box box-primary" style="padding-top:-50px;">
      <div class="box-body table-responsive">
        
        <?php echo $this->session->flashdata('status'); ?>
       
        <table class="table table-striped table-bordered table-condensed">
          <thead>
            <tr>
              <th class="text-center" width="100">Device No</th>
              <th class="text-center">Lokasi</th>
              <th class="text-center" width="120">Dibuat</th>
              <th class="text-center" width="120">Status</th>
              <th class="text-center" width="120">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($device != null): ?>
              <?php $i=1;foreach ($device as $row): ?>
                <tr>
                  <td class="text-center"><?=$row->device_no?></td>
                  <td><?=$row->lokasi?></td>
                  <td class="text-center"><?=$row->created?></td>
                  <td class="text-center">
                    <?php if ($row->pin!=null): ?>
                      <i class="text-green"> Ada</i>  
                    <?php else: ?>
                      <i class="text-red"> Tidak Ada</i>
                    <?php endif; ?>
                  </td>
                  <td class="text-center">
                    <?php if ($row->pin!=null): ?>
                      
                    <a class="btn btn-sm btn-danger pull-left" href="<?=base_url()?>tb_user/formdel/<?=$user->pin?>/<?=$row->id_device?>"><i class="fa fa-trash"></i> Hapus</a>  
                    <?php else: ?>
                    <a class="btn btn-sm btn-primary pull-left" href="<?=base_url()?>tb_user/formplus/<?=$user->pin?>/<?=$row->id_device?>"><i class="fa fa-plus"></i> Tambah</a>
                    <?php endif; ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td class="text-center" colspan="99">Tidak ada data!</td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <span class="badge bg-blue">Jumlah Data : <?=$num_rows?></span>
       
      </div>
      <!-- /.box-footer-->
    </div>

     <?php else: ?>
            <div class="alert alert-danger">User tidak bisa diubah di alat, Silahkan Cek Kembali User Perangkat.  
              <a class="btn btn-sm btn-success" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Kembali</a>
            </div>
            <?php endif;?>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<script>
  $(document).ready(function () {
    // form validator
    $('#form').validate();
  })
</script>
