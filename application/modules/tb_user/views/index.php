<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
      <small><?=$subtitle?></small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">Data Kepegawaian</li>
      <li class="active text-orange"><?=$access->menu?></li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <i class="fa fa-table text-maroon"></i>
        <h3 class="box-title">Tabel <?=$access->menu?></h3>
        <div class="box-tools pull-right">
          <span class="badge bg-maroon"><i class="fa fa-bar-chart"></i> Jumlah Keseluruhan Data : <?=$num_rows_total?></span>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-md-1">
         
          </div>
          <form id="form_search" action="<?=base_url()?>tb_user/index/" method="post">
            <div class="col-md-3 col-md-offset-2">
              <div class="form-group">
                <select class="form-control select2" name="id_ruang" id="id_ruang">
                  <option value="">-- Semua Ruang --</option>
                  <?php foreach($ruang as $row): ?>
                    <option value="<?=$row->id_ruang?>" <?php if($search){if($search['id_ruang'] == $row->id_ruang){echo 'selected';}}?>><?=$row->ruang?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>
            <div class="col-md-3 ">
              <div class="form-group">
                <select class="form-control select2" name="id_jabatan" id="id_jabatan">
                  <option value="">-- Semua Jabatan --</option>
                  <?php foreach($jabatan as $row): ?>
                    <option value="<?=$row->id_jabatan?>" <?php if($search){if($search['id_jabatan'] == $row->id_jabatan){echo 'selected';}}?>><?=$row->jabatan?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="input-group">
                <input type="text" name="term" class="form-control pull-right" placeholder="Pencarian" value="<?php if($search){echo $search['term'];}?>">
                <div class="input-group-btn">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                  <a class="btn btn-default btn-flat" href="<?=base_url()?>tb_user/reset_search/"><i class="fa fa-refresh"></i></a>
                </div>
              </div>
            </div>
          </form>
        </div>
        <?php echo $this->session->flashdata('status'); ?>
        <table class="table table-striped table-bordered table-condensed">
          <thead>
            <tr>
              <th class="text-center" style="vertical-align:middle !important" width="30">No.</th>
              <th class="text-center" style="vertical-align:middle !important" width="60">Aksi</th>
              <th class="text-center" style="vertical-align:middle !important" width="60">PIN</th>
              <th class="text-center" style="vertical-align:middle !important" width="">Nama<br>Nomor Induk</th>
              <th class="text-center" style="vertical-align:middle !important" width="10">JK</th>
              <th class="text-center" style="vertical-align:middle !important" width="">Tempat Tanggal Lahir<br>Usia</th>
              <th class="text-center" style="vertical-align:middle !important" width="">Ruang</th>
              <th class="text-center" style="vertical-align:middle !important" width="">Jabatan</th>
              <th class="text-center" style="vertical-align:middle !important" width="10">Status</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($pegawai != null): ?>
              <?php $i=1;foreach ($pegawai as $row): ?>
                <tr>
                  <td class="text-center"><?=$this->uri->segment('4')+$i++?></td>
                  <td class="text-center">
                  <?php if($row->nomor_induk != 1): ?>
                      <a class="btn btn-primary" href="<?=base_url()?>tb_user/form/<?=$row->pin?>"><i class="fa fa-info"></i></a>
                    <?php endif;?>
                  </td>
                  <td class="text-center"><?=$row->pin?></td>
                  <td>
                    <b><?php 
                      echo $row->gelar_depan.' '.$row->nama;
                      if ($row->gelar_belakang != ''){
                        echo ', '.$row->gelar_belakang;
                      }
                    ?>
                    </b>
                    <br>
                    <?=$row->nomor_induk?>
                  </td>
                  <td class="text-center"><?php echo $jk = ($row->jenis_kelamin == '1') ? 'L' : 'P';?></td>
                  <td>
                    <?=$row->tempat_lhr.', '.date_to_id($row->tgl_lhr)?><br>
                    (<?php if($row->tgl_lhr != ''){echo hitung_umur($row->tgl_lhr);}?> Tahun)
                  </td>
                  <td><?=$row->ruang?></td>
                  <td><?=$row->jabatan?></td>
                  <td class="text-center"><?=$row->status_pegawai ?></td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td class="text-center" colspan="99">Tidak ada data!</td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <span class="badge bg-purple">Jumlah Data : <?=$num_rows?></span>
        <?php echo $this->pagination->create_links(); ?>
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- Modal Delete -->
<script type="text/javascript">
  function del(id) {
    $("#modal_delete").modal('show');
    $("#id_pegawai").val(id);
  }
  $('.select2').on('select2:select', function (e) {
    $('#form_search').submit();
  });
</script>