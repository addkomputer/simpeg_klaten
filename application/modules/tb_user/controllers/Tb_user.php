<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tb_user extends MY_Controller {

	var $access, $id_grup;

  function __construct(){
		parent::__construct();

		$controller = 'tb_user';

    if($this->session->userdata('menu') != $controller){
      $this->session->unset_userdata('search');
      $this->session->set_userdata(array('menu' => 'tb_user'));
    }
    $this->load->model('ap_konfigurasi/m_ap_konfigurasi');
		$this->id_grup = $this->session->userdata('id_grup');
		$this->access = $this->m_ap_konfigurasi->get_permission($this->id_grup, $controller);
		if ($this->access == null) {
			redirect(base_url().'ap_error/error_403');
		}
		$this->load->model('m_tb_user');
		$this->load->model('m_tb_device');
		$this->load->model('ms_ruang/m_ms_ruang');
		$this->load->model('ms_golongan/m_ms_golongan');
		$this->load->model('ms_kecamatan/m_ms_kecamatan');
		$this->load->model('ms_unit_kerja/m_ms_unit_kerja');
		$this->load->model('ms_unit_kerja/m_ms_unit_kerja');
		$this->load->model('ms_nama_instansi/m_ms_nama_instansi');
		$this->load->model('ms_rumpun_jabatan/m_ms_rumpun_jabatan');
		$this->load->model('ms_jabatan/m_ms_jabatan');
	}

	public function index()
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Daftar';
		$data['ruang'] = $this->m_ms_ruang->get_all();
		$data['jabatan'] = $this->m_ms_jabatan->get_all();

		if ($this->access->_read) {
			$search = null;
			if($search = $_POST){
				$this->session->set_userdata(array('search' => $search));
			}else{
				if($this->session->userdata('search') != null){
					$search = $this->session->userdata('search');
				}
			}
			$data['search'] = $search;

			$config['base_url'] = base_url().'tb_user/index/';
			$config['per_page'] = 10;

			$from = $this->uri->segment(3);

			if($search == null){
				$num_rows = $this->m_tb_user->num_rows();

				$config['total_rows'] = $num_rows;
				$this->pagination->initialize($config);

				$data['pegawai'] = $this->m_tb_user->get_list($config['per_page'],$from,$search = null);
			}else{
				$search = $this->session->userdata('search');
				$num_rows = $this->m_tb_user->num_rows($search);
				$config['total_rows'] = $num_rows;
				$this->pagination->initialize($config);

				$data['pegawai'] = $this->m_tb_user->get_list($config['per_page'],$from,$search);
			}

			$data['num_rows'] = $num_rows;
			$data['num_rows_total'] = $this->m_tb_user->num_rows_all();
			insert_log('read',$this->access->menu);
			$this->view('index',$data);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function form($id = null)
	{
		$data['access'] = $this->access;
		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Tambah Data';
				$data['action'] = 'insert';
				$data['user'] = null;
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Detail Pengguna Fingerprint';
				$data['user'] = $this->m_tb_user->get_by_id($id);


				$search = null;
				if($search = $_POST){
					$this->session->set_userdata(array('search' => $search));
				}else{
					if($this->session->userdata('search') != null){
						$search = $this->session->userdata('search');
					}
				}
				$data['search'] = $search;

				$config['base_url'] = base_url().'tb_device/index/';
				$config['per_page'] = 10;

				$from = $this->uri->segment(4);

				if($search == null){
					$num_rows = $this->m_tb_device->num_rows();

					$config['total_rows'] = $num_rows;
					$this->pagination->initialize($config);

					$data['device'] = $this->m_tb_device->get_listAll($data['user'] ->pin);
				}else{
					$search = $this->session->userdata('search');
					$num_rows = $this->m_tb_device->num_rows($search);
					$config['total_rows'] = $num_rows;
					$this->pagination->initialize($config);

					$data['device'] = $this->m_tb_device->get_listAll($data['user'] ->pin);
				}
				$data['num_rows'] = $num_rows;
				$data['num_rows_total'] = $this->m_tb_device->num_rows_total();
				$data['jlokasi'] = $this->m_tb_user->num_row_by_id($data['user'] ->pin);

				insert_log('read',$this->access->menu);
				$this->view('form', $data);
			}
		}
	}

		public function formdel($id = null,$sn = null)
	{
		$data['access'] = $this->access;
		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Tambah Data';
				$data['action'] = 'insert';
				$data['device'] = null;
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Info Alat Fingerprint';

				$data['device'] = $this->m_tb_device->get_by_id($sn);
				$data['user'] = $this->m_tb_user->get_by_id($id);

				$scanlog = webservice($data['device']->server_port,'localhost:8080/user/del','sn='.$data['device']->device_sn."&pin=".$data['user']->pin);
				$data['raw'] = json_decode($scanlog, true);
				// var_dump($data['raw']);

				$cek=$this->m_tb_user->get_by_pin_sn($data['user']->pin, $data['device']->device_sn);

				if ($data['raw']['Result']) {
					if ($cek !=null) {
						$this->m_tb_user->delete_permanent($data['user']->pin, $data['device']->device_sn);
						insert_log('delete',$this->access->menu);
					}
				}
				
				$search = null;
				if($search = $_POST){
					$this->session->set_userdata(array('search' => $search));
				}else{
					if($this->session->userdata('search') != null){
						$search = $this->session->userdata('search');
					}
				}
				$data['search'] = $search;

				$config['base_url'] = base_url().'tb_device/index/';
				$config['per_page'] = 10;

				$from = $this->uri->segment(5);

				if($search == null){
					$num_rows = $this->m_tb_device->num_rows();

					$config['total_rows'] = $num_rows;
					$this->pagination->initialize($config);

					$data['device'] = $this->m_tb_device->get_listAll($data['user'] ->pin);
				}else{
					$search = $this->session->userdata('search');
					$num_rows = $this->m_tb_device->num_rows($search);
					$config['total_rows'] = $num_rows;
					$this->pagination->initialize($config);

					$data['device'] = $this->m_tb_device->get_listAll($data['user'] ->pin);
				}
				$data['num_rows'] = $num_rows;
				$data['num_rows_total'] = $this->m_tb_device->num_rows_total();
				$data['jlokasi'] = $this->m_tb_user->num_row_by_id($data['user'] ->pin);

				insert_log('read',$this->access->menu);

			
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');

				$this->view('formx', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}

	public function formplus($id,$sn)
	{

				$data['access'] = $this->access;
				$data['subtitle'] = 'Detail Pengguna Fingerprint';

				$data['user'] = $this->m_tb_user->get_by_id($id);
				$dataGetTemp = $this->m_tb_user->get_tmp($id);
				$data['device'] = $this->m_tb_device->get_by_id($sn);

				$datas['created_by'] = $this->session->userdata('nama');
				$datas['pin'] = $data['user']->pin;
				$datas['nama'] = $data['user']->nama;
				$datas['pwd'] = $data['user']->pwd;
				$datas['rfid'] = $data['user']->rfid;
				$datas['privilege'] = $data['user']->privilege;
				$datas['device_sn'] = $data['device']->device_sn;
				$datas['is_active'] = 1;


				foreach ($dataGetTemp as $row) {
						$header = "[";
						$footer = "]";
						$content = "";
            $content = $content.'{"pin":"'.$row->pin.'","idx":"'.$row->finger_idx.'","alg_ver":"'.$row->alg_ver.'","template":"'.$row->template.'"}';
						$upTemp=$header.$content.$footer;
						$upTemp = str_replace("+","%2B",$upTemp);
						$parameter = 'sn='.$data['device']->device_sn.'&pin='.$data['user']->pin.'&nama='.$data['user']->nomor_induk.'&pwd='.$data['user']->pwd.'&rfid='.$data['user']->rfid.'&priv='.$data['user']->privilege.'&tmp='.$upTemp;
						$scanlog = webservice($data['device']->server_port,'localhost:8080/user/set',$parameter);
						$data['raw'] = json_decode($scanlog, true);
				}

				$cek=$this->m_tb_user->get_by_pin_sn($data['user']->pin, $data['device']->device_sn);

			
				if ($data['raw']['Result']) {
					if ($cek ==null) {
						$this->m_tb_user->insert($datas);
					}
				}

				

				$search = null;
				if($search = $_POST){
					$this->session->set_userdata(array('search' => $search));
				}else{
					if($this->session->userdata('search') != null){
						$search = $this->session->userdata('search');
					}
				}
				$data['search'] = $search;

				$config['base_url'] = base_url().'tb_device/index/';
				$config['per_page'] = 10;

				$from = $this->uri->segment(5);

				if($search == null){
					$num_rows = $this->m_tb_device->num_rows();

					$config['total_rows'] = $num_rows;
					$this->pagination->initialize($config);

					$data['device'] = $this->m_tb_device->get_listAll($data['user'] ->pin);
				}else{
					$search = $this->session->userdata('search');
					$num_rows = $this->m_tb_device->num_rows($search);
					$config['total_rows'] = $num_rows;
					$this->pagination->initialize($config);

					$data['device'] = $this->m_tb_device->get_listAll($data['user'] ->pin);
				}
				$data['num_rows'] = $num_rows;
				$data['num_rows_total'] = $this->m_tb_device->num_rows_total();
				$data['jlokasi'] = $this->m_tb_user->num_row_by_id($data['user'] ->pin);
				insert_log('read',$this->access->menu);

			

				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');

				$this->view('formx', $data);
	}

	public function reset_search()
  {
    $this->session->unset_userdata('search');
    redirect(base_url().'tb_user/index');
  }

	public function insert()
	{
		$data = $_POST;
		if ($data != null) {
			$data['created_by'] = $this->session->userdata('nama');
			if(!isset($data['is_active'])){$data['is_active'] = 0;}
			$this->m_tb_user->insert($data);
			insert_log('insert',$this->access->menu);
			$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
			redirect(base_url().'tb_user/index');
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function update()
	{
		$data = $_POST;
		if ($data != null) {
			if($this->access->_update){
				$data['updated_by'] = $this->session->userdata('nama');
				if(!isset($data['is_active'])){$data['is_active'] = 0;}
				$id = $data['id_user'];
				$this->m_tb_user->update($id, $data);
				insert_log('update',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				redirect(base_url().'tb_user/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function delete()
	{
		$data = $_POST;
		if($data != null){
			if ($this->access->_delete) {
				$this->m_tb_user->delete_temp($data['id_user']);
				insert_log('delete',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				redirect(base_url().'tb_user/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}

}
