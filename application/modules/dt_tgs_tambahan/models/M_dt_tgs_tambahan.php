<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dt_tgs_tambahan extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_pegawai')
      ->get('dt_tgs_tambahan',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_pegawai')
      ->get('dt_tgs_tambahan')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('dt_tgs_tambahan')->result();
  }
  
  public function get_by_id_pegawai($id)
  {
    return $this->db      
      ->where('a.id_pegawai',$id)
      ->order_by('a.id','desc')
      ->get('dt_tgs_tambahan a')
      ->result();
  }

  public function get_by_id($id)
  {
    return $this->db->where('id',$id)->get('dt_tgs_tambahan')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_pegawai','asc')->get('dt_tgs_tambahan')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_pegawai','desc')->get('dt_tgs_tambahan')->row();
  }

  public function insert($data)
  {
    $this->db->insert('dt_tgs_tambahan',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id',$id)->update('dt_tgs_tambahan',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id',$id)->update('dt_tgs_tambahan',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id',$id)->delete('dt_tgs_tambahan');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE dt_tgs_tambahan");
  }

}
