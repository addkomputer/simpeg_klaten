<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ms_jenis_instansi extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_jenis_instansi')
      ->get('ms_jenis_instansi',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_jenis_instansi')
      ->get('ms_jenis_instansi')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('ms_jenis_instansi')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id_jenis_instansi',$id)->get('ms_jenis_instansi')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_jenis_instansi','asc')->get('ms_jenis_instansi')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_jenis_instansi','desc')->get('ms_jenis_instansi')->row();
  }

  public function insert($data)
  {
    $this->db->insert('ms_jenis_instansi',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_jenis_instansi',$id)->update('ms_jenis_instansi',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id_jenis_instansi',$id)->update('ms_jenis_instansi',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_jenis_instansi',$id)->delete('ms_jenis_instansi');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ms_jenis_instansi");
  }

}
