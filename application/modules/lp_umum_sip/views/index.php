<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-<?=$access->icon?> text-light-blue"></i> Laporan Data Pegawai <?=$access->menu?>
      <small><?=$subtitle?></small>
    </h1>      
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box box-primary">
      <form id="form" class="form-horizontal" action="<?=base_url().$access->controller?>/search" method="post" autocomplete="off"> 
        <div class="box-body">
          <div class="row">
            <div class="col-md-2 col-md-offset-4">
              <select class="form-control input-sm select2" name="jenis" id="jenis">
                <option value="1" <?php if($search != null){if($search['jenis'] == '1'){echo 'selected';}}?>>Tanggal Terbit</option>
                <option value="2" <?php if($search != null){if($search['jenis'] == '2'){echo 'selected';}}?>>Tanggal Berlaku</option>
              </select>
            </div>
            <div class="col-md-2">
              <select class="form-control input-sm select2" name="bulan" id="bulan">
                <option value="01" <?php if($search != null){if($search['bulan'] == '01'){echo 'selected';}}?>>Januari</option>
                <option value="02" <?php if($search != null){if($search['bulan'] == '02'){echo 'selected';}}?>>Februari</option>
                <option value="03" <?php if($search != null){if($search['bulan'] == '03'){echo 'selected';}}?>>Maret</option>
                <option value="04" <?php if($search != null){if($search['bulan'] == '04'){echo 'selected';}}?>>April</option>
                <option value="05" <?php if($search != null){if($search['bulan'] == '05'){echo 'selected';}}?>>Mei</option>
                <option value="06" <?php if($search != null){if($search['bulan'] == '06'){echo 'selected';}}?>>Juni</option>
                <option value="07" <?php if($search != null){if($search['bulan'] == '07'){echo 'selected';}}?>>Juli</option>
                <option value="08" <?php if($search != null){if($search['bulan'] == '08'){echo 'selected';}}?>>Agustus</option>
                <option value="09" <?php if($search != null){if($search['bulan'] == '09'){echo 'selected';}}?>>September</option>
                <option value="10" <?php if($search != null){if($search['bulan'] == '10'){echo 'selected';}}?>>Oktober</option>
                <option value="11" <?php if($search != null){if($search['bulan'] == '11'){echo 'selected';}}?>>November</option>
                <option value="12" <?php if($search != null){if($search['bulan'] == '12'){echo 'selected';}}?>>Desember</option>
              </select>
            </div>
            <div class="col-md-2">
              <select class="form-contro input-sm select2" name="tahun" id="tahun">
                <?php for ($i=2000; $i < 2050; $i++): ?>
                  <option value="<?=$i?>" <?php if($search != null){if($search['tahun'] == $i){echo 'selected';}}?>><?=$i?></option>
                <?php endfor;?>
              </select>
            </div>
            <div class="col-md-2">
              <button class="btn btn-flat btn-primary" type="submit"><i class="fa fa-search"></i> </button> 
              <a class="btn btn-default btn-flat" href="<?=base_url().$access->controller?>/reset_search"><i class="fa fa-refresh"></i></a>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="box box-primary">
      <div class="box-body table-responsive">
        <a class="btn btn-success btn-flat pull-right" href="<?=base_url().$access->controller?>/cetak_pdf" target="_blank"><i class="fa fa-file-pdf-o"></i> Cetak PDF</a>
        <?php echo $this->session->flashdata('status'); ?>
        <br><br>
        <table class="table table-striped table-bordered table-condensed">
          <thead>
            <tr>
              <th class="text-center" width="30">No.</th>
              <th class="text-center" width="60">NIP/NPNP</th>
              <th class="text-center">Nama</th>
              <th class="text-center" width="10">JK</th>
              <th class="text-center" width="150">TTL</th>
              <th class="text-center" width="40">Usia</th>
              <th class="text-center" width="">Jabatan</th>
              <th class="text-center" width="20">Eselon</th>
              <th class="text-center" width="">Pangkat</th>
              <th class="text-center" width="10">Gol</th>
              <th class="text-center">Tanggal Terbit SIP</th>
              <th class="text-center">Tanggal Berlaku SIP</th>
              <th class="text-center" width="10">Status</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($pegawai != null): ?>
              <?php $i=1;foreach ($pegawai as $row): ?>
                <tr>
                  <td class="text-center"><?=$this->uri->segment('3')+$i++?></td>
                  <td><?=$row->nomor_induk?></td>
                  <td>
                    <?php 
                      echo $row->gelar_depan.' '.$row->nama;
                      if ($row->gelar_belakang != ''){
                        echo ', '.$row->gelar_belakang;
                      }
                    ?></td>
                  <td class="text-center"><?php echo $jk = ($row->jenis_kelamin == '1') ? 'L' : 'P';?></td>
                  <td><?=$row->tempat_lhr.', '.date_to_id($row->tgl_lhr)?></td>
                  <td class="text-center"><?php if($row->tgl_lhr != ''){echo hitung_umur($row->tgl_lhr);}?></td>
                  <td><?=$row->jabatan?></td>
                  <td class="text-center"><?php echo $eselon = ($row->eselon != '') ? $row->eselon : '-' ;?></td>
                  <td><?=$row->pangkat?></td>
                  <td class="text-center"><?=$row->golongan?></td>
                  <td class="text-center"><?=date_to_id($row->tgl_sip)?></td>
                  <td class="text-center"><?=date_to_id($row->tgl_sip_berlaku)?></td>
                  <td class="text-center"><?=$row->status_pegawai?></td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td class="text-center" colspan="99">Tidak ada data!</td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <span class="badge bg-blue">Jumlah Data : <?=$num_rows?></span>
        <?php echo $this->pagination->create_links(); ?>
      </div>
      <!-- /.box-footer-->
    </div>
  <!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
  $(document).ready(function () {
    // form validator
    $('#form').validate({
      rules : {
        'id_golongan' : {
          valueNotEquals : '0'
        }
      }
    });
  })
</script>