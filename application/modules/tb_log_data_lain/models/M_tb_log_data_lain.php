<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_tb_log_data_lain extends CI_Model {

  private $suffix,$bulan,$tahun,$id_ruang,$id_jabatan,$id_status_pegawai,$term;

  function __construct(){
    parent::__construct();
    if ($this->session->userdata('search')) {
      $this->bulan = $this->session->userdata('search')['bulan'];
      $this->tahun = $this->session->userdata('search')['tahun'];
      $this->id_ruang = $this->session->userdata('search')['id_ruang'];
      $this->id_jabatan = $this->session->userdata('search')['id_jabatan'];
      $this->id_status_pegawai = $this->session->userdata('search')['id_status_pegawai'];
      $this->term = $this->session->userdata('search')['term'];
      $this->suffix = $this->tahun.'_'.$this->bulan;
    }else{
      $this->bulan = date('m');
      $this->tahun = date('Y');
      $this->id_ruang = '';
      if ($this->session->userdata('id_grup') == 4) {
        $this->id_ruang = $this->session->userdata('id_ruang');
      }
      $this->id_jabatan = '';
      $this->term = '';
      $this->suffix = date('Y_m');
      $this->id_status_pegawai = '';
      $search = array(
        'bulan' => $this->bulan,
        'tahun' => $this->tahun,
        'id_ruang' => $this->id_ruang,
        'id_jabatan' => $this->id_jabatan,
        'id_status_pegawai' => $this->id_status_pegawai,
        'term' => $this->term
      );
      $this->session->set_userdata(array('search' => $search));
    }
    $this->create_table($this->suffix);
  }
  
  public function create_table($suffix)
  {
    $this->db->query(
      "CREATE TABLE IF NOT EXISTS `tb_log_data_lain_$suffix` (
        `id` bigint(20) NOT NULL AUTO_INCREMENT,
        `id_pegawai` varchar(50) NOT NULL DEFAULT '0',
        `nomor_induk` varchar(50) DEFAULT '0',
        `pin` varchar(50) DEFAULT '0',
        `id_ruang` int(11) DEFAULT '0',
        `tanggal` date NOT NULL,
        `id_tipe` char(2) DEFAULT NULL,
        `file_bukti` varchar(100) DEFAULT NULL,
        `keterangan` text,
        `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `created_by` varchar(50) NOT NULL DEFAULT 'System',
        `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
        `updated_by` varchar(50) DEFAULT NULL,
        `is_active` tinyint(1) NOT NULL DEFAULT '1',
        `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
        PRIMARY KEY (`id`)
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1;"
    );
  }

	public function get_list($number,$offset,$search = null)
  {
    if ($search != null) {
      if ($search['id_jabatan'] != '') {
        $this->db->where('b.id_jabatan',$search['id_jabatan']);
      }
      if ($search['id_ruang'] != '') {
        $this->db->where('b.id_ruang',$search['id_ruang']);
      }
      if ($search['id_status_pegawai'] != '') {
        $this->db->where('b.id_status_pegawai',$search['id_status_pegawai']);
      }
      if ($search['term'] != '') {
        $this->db->like('b.nama',$search['term']);
        $this->db->or_like('a.id_pegawai',$search['term']);
      }
    }
    
    $this->db->select('a.*, b.gelar_depan, b.nama, b.gelar_belakang, b.id_pegawai, c.jabatan, d.tipe, e.ruang, f.status_pegawai');

    return $this->db
      ->join('dt_pegawai b','a.id_pegawai = b.id_pegawai')
      ->join('ms_jabatan c', 'b.id_jabatan = c.id_jabatan','left')
      ->join('tb_log_tipe d','a.id_tipe = d.id_tipe')
      ->join('ms_ruang e', 'b.id_ruang = e.id_ruang','left')
      ->join('ms_status_pegawai f', 'b.id_status_pegawai = f.id_status_pegawai','left')
      ->get('tb_log_data_lain_'.$this->suffix.' a',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      if ($search['id_jabatan'] != '') {
        $this->db->where('b.id_jabatan',$search['id_jabatan']);
      }
      if ($search['id_ruang'] != '') {
        $this->db->where('b.id_ruang',$search['id_ruang']);
      }
      if ($search['id_status_pegawai'] != '') {
        $this->db->where('b.id_status_pegawai',$search['id_status_pegawai']);
      }
      if ($search['term'] != '') {
        $this->db->like('b.nama',$search['term']);
        $this->db->or_like('a.id_pegawai',$search['term']);
      }
    }
    
    $this->db->select('a.*, b.gelar_depan, b.nama, b.gelar_belakang, b.id_pegawai, c.jabatan, d.tipe');

    return $this->db
      ->join('dt_pegawai b','a.id_pegawai = b.id_pegawai')
      ->join('ms_jabatan c', 'b.id_jabatan = c.id_jabatan','left')
      ->join('tb_log_tipe d','a.id_tipe = d.id_tipe')
      ->get('tb_log_data_lain_'.$this->suffix.' a')
      ->num_rows();
  }
  
  function num_rows_total($search = null){  
    if ($this->session->userdata('id_grup') == 4) {
      if ($search != null) {
        if ($search['id_ruang'] != '') {
          $this->db->where('b.id_ruang',$search['id_ruang']);
        }
     }
    }
    return $this->db
      ->join('dt_pegawai b','a.id_pegawai = b.id_pegawai')
      ->order_by('id')
      ->get('tb_log_data_lain_'.$this->suffix.' a')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_active','1')
			->get('tb_log_data_lain_'.$this->suffix)->result();
  }

  public function get_by_id($id)
  {
    return $this->db->where('id',$id)->get('tb_log_data_lain_'.$this->suffix)->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id','asc')->get('tb_log_data_lain_'.$this->suffix)->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id','desc')->get('tb_log_data_lain_'.$this->suffix)->row();
  }

  public function insert($data)
  {
    //get pegawai
    $pegawai = $this->db->where('id_pegawai', $data['id_pegawai'])->get('dt_pegawai')->row_array();
    if($pegawai != null){
      $d = array(
        'id_pegawai' => $pegawai['id_pegawai'],
        'nomor_induk' => $pegawai['nomor_induk'],
        'pin' => $pegawai['pin'],
        'id_ruang' => $pegawai['id_ruang'],
        'tanggal' => $data['tanggal'],
        'id_tipe' => '6',
        'file_bukti' => $data['file_bukti'],
        'keterangan' => $data['keterangan']
      );
      $cek = $this->db
        ->where('id_pegawai',$data['id_pegawai'])
        ->where('tanggal',$data['tanggal'])
        ->get('tb_log_data_lain_'.$this->suffix)
        ->row();
      if($cek == null){
        $this->db->insert('tb_log_data_lain_'.$this->suffix, $d);
      }else{
        $this->db
          ->where('id_pegawai',$data['id_pegawai'])
          ->where('tanggal',$data['tanggal'])
          ->update('tb_log_data_lain_'.$this->suffix, $d);
      }

      $log = array(
        'id_pegawai' => $pegawai['id_pegawai'],
        'nomor_induk' => $pegawai['nomor_induk'],
        'pin' => $pegawai['pin'],
        'tanggal' => $data['tanggal'],
        'id_tipe' => '6',
        'ket_umum' => $data['keterangan']
      );
      $cek_log = $this->db
        ->where('id_pegawai',$data['id_pegawai'])
        ->where('tanggal',$data['tanggal'])
        ->get('tb_log_kehadiran_'.$this->suffix)
        ->row();
       if ($cek_log == NULL) {
        //insert
        $this->db->insert('tb_log_kehadiran_'.$this->suffix,$log);
      }else{
        //update
        $this->db
          ->where('id_pegawai', $data['id_pegawai'])
          ->where('tanggal', $data['tanggal'])
          ->update('tb_log_kehadiran_'.$this->suffix,$log);
      }
    }
  }

  public function update($id,$data)
  {
    $this->db->where('id',$id)->update('tb_log_data_lain_'.$this->suffix,$data);
    $log = array(
      'id_tipe' => '6',
      'ket_umum' => $data['keterangan']
    );
    $data_lain = $this->db->where('id',$id)->get('tb_log_data_lain_'.$this->suffix)->row_array();
    $this->db
      ->where('id_pegawai',$data_lain['id_pegawai'])
      ->where('tanggal',$data_lain['tanggal'])
      ->update('tb_log_kehadiran_'.$this->suffix,$log);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id',$id)->update('tb_log_data_lain_'.$this->suffix,array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $data = $this->db->where('id',$id)->get('tb_log_data_lain_'.$this->suffix)->row();
    $this->db
      ->where('id_pegawai',$data->id_pegawai)
      ->where('tanggal',$data->tanggal)
      ->update('tb_log_kehadiran_'.$this->suffix,array('ket_umum' => '','id_tipe' => -1));
    $this->db->where('id',$id)->delete('tb_log_data_lain_'.$this->suffix);
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE tb_log_data_lain_2018_10");
  }

}
