<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masuk extends MX_Controller {

	function __construct(){
    parent::__construct();
		$this->load->model('ap_profil/m_ap_profil');
		$this->load->model('ap_aktivitas/m_ap_aktivitas');
		$this->load->model('ap_konfigurasi/m_ap_konfigurasi');
		$this->load->model('ms_instansi/m_ms_instansi');
		$this->load->model('m_ap_otentikasi');
  }

	public function index()
	{
		$data['profil'] = $this->m_ap_profil->get_first();
		$this->load->view('masuk', $data);
	}

	public function action()
	{
		$data = $_POST;
		$data['user_pass'] = md5($data['user_pass']);

		$result = $this->m_ap_otentikasi->action($data);
		if($result->num_rows() >= 1){
			foreach ($result->result() as $sess) {
				$sess_data['id_pengguna'] = $sess->id_pengguna;
				$sess_data['id_grup'] = $sess->id_grup;
				$sess_data['nama'] = $sess->nama;
				$sess_data['id_instansi'] = $sess->id_instansi;
				$sess_data['instansi'] = $this->m_ms_instansi->get_by_id($sess->id_instansi)->instansi;
				
				$this->load->library('user_agent');
				if ($this->agent->is_browser()){
					$agent = $this->agent->browser().' '.$this->agent->version();
				}elseif ($this->agent->is_robot()){
					$agent = $this->agent->robot();
				}elseif ($this->agent->is_mobile()){
					$agent = $this->agent->mobile();
				}else{
					$agent = 'Unidentified';
				}

				$data_log = array(
					"id_pengguna" => $sess->id_pengguna,
					"nama" => $sess->nama,
					"tipe" => 'signin',
					"ip" => $this->input->ip_address(),
					"user_agent" => $agent,
        	"platform" => $this->agent->platform(),
					"deskripsi" => 'Masuk ke sistem'
				);
				$this->m_ap_aktivitas->insert($data_log);
			}

			$sess_data['logged'] = true;
			$this->session->set_userdata($sess_data);
			$menu = $this->m_ap_konfigurasi->get_menu_first($sess->id_grup);
			redirect(base_url().$menu->controller);
		}else{
			$this->session->set_flashdata('status', '<div class="alert alert-danger alert-dismissable fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Maaf Anda gagal masuk!</div>');
			redirect(base_url().'ap_otentikasi/masuk');
		}
	}

}
