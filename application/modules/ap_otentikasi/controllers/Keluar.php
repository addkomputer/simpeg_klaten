<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keluar extends MX_Controller {

  public function action()
	{
		$this->load->model('ap_aktivitas/m_ap_aktivitas');

		$this->load->library('user_agent');
		if ($this->agent->is_browser()){
			$agent = $this->agent->browser().' '.$this->agent->version();
		}elseif ($this->agent->is_robot()){
			$agent = $this->agent->robot();
		}elseif ($this->agent->is_mobile()){
			$agent = $this->agent->mobile();
		}else{
			$agent = 'Unidentified';
		}

		$data_log = array(
			"id_pengguna" => $this->session->userdata('id_pengguna'),
			"nama" => $this->session->userdata('nama'),
			"tipe" => 'signout',
			"ip" => $this->input->ip_address(),
			"user_agent" => $agent,
			"platform" => $this->agent->platform(),
			"deskripsi" => 'Keluar dari sistem'
		);

		$this->m_ap_aktivitas->insert($data_log);
		$this->session->sess_destroy();
		redirect(base_url().'ap_otentikasi/masuk');
	}

}
