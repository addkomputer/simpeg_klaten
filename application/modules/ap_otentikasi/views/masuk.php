<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=$profil->nama_aplikasi?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?=base_url();?>bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=base_url();?>bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?=base_url();?>bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url();?>dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?=base_url();?>plugins/iCheck/square/blue.css">
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?=base_url();?>img/<?=$profil->logo?>" type="image/x-icon">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
      body {
        /* background-image: url('<?=base_url();?>img/bg.jpg') !important; */
        background: linear-gradient(0deg,rgba(221, 75, 57, 0.8),rgba(221, 75, 57, 0.8)), url('<?=base_url();?>img/bg.jpg') !important;
      }
      .error{
        color:#FF0000;
      },
      .valid{
        color:green;
      }
    </style>
  </head>
  <body class="hold-transition login-page" style="height:0;">
    <div class="login-box">
      <div class="login-box-body">
        <div class="text-center">
          <img src="<?=base_url()?>img/<?=$profil->logo?>" width="80">
          <h5 style="margin-bottom:-10px; font-weight:bold;"><?=$profil->pemda?><br></h5>
          <h4 class="text-center">
            <?=$profil->nama_instansi?><br>
            <strong class="text-red"><?=$profil->nama_aplikasi?></strong><br>
          </h4>
        </div>
        <br>
        <form id="form" action="<?=base_url();?>ap_otentikasi/masuk/action" method="post" autocomplete="on">
          <!-- <h2>Masuk</h2> -->
          <div class="form-group has-feedback">
            <!-- <label>Nomor Induk</label> -->
            <input type="text" class="form-control" name="user_name" placeholder="Nama Pengguna" required>
            <span class="fa fa-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <!-- <label>Kata Sandi</label> -->
            <input type="password" class="form-control" name="user_pass" placeholder="Kata Sandi" required>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-sign-in"></i> Masuk</button>
            </div>
          </div>
          <div class="row">
            <hr>
            <div class="col-md-12 text-center">
              Copyright &copy <?php if(date('Y') == $profil->tahun){echo date('Y');}else{echo $profil->tahun.' - '.date('Y');}?> <br>
            </div>
          </div>
        </form>
        <?php echo $this->session->flashdata('status'); ?>
      </div>
    </div>
    <!-- /.login-box -->

    <!-- jQuery 3 -->
    <script src="<?=base_url();?>bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?=base_url();?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="<?=base_url();?>plugins/iCheck/icheck.min.js"></script>
    <!-- Jquery Validation -->
    <script src="<?=base_url()?>bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
    <script>

      $(document).ready(function () {
        $.validator.addMethod("valueNotEquals", function(value, element, arg){
          return arg !== value;
        }, "Value must not equal arg.");
        jQuery.extend(jQuery.validator.messages, {
          required: "Wajib diisi.",
          remote: "Please fix this field.",
          email: "Please enter a valid email address.",
          url: "Please enter a valid URL.",
          date: "Please enter a valid date.",
          dateISO: "Please enter a valid date (ISO).",
          number: "Harap masukkan angka yang benar.",
          digits: "Please enter only digits.",
          creditcard: "Please enter a valid credit card number.",
          equalTo: "Please enter the same value again.",
          accept: "Please enter a value with a valid extension.",
          maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
          minlength: jQuery.validator.format("Please enter at least {0} characters."),
          rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
          range: jQuery.validator.format("Please enter a value between {0} and {1}."),
          max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
          min: jQuery.validator.format("Please enter a value greater than or equal to {0}."),
          valueNotEquals: "Harap pilih salah satu!"
        });
        $('#form').validate();
        // fade out alert
        setInterval(function () {
          $('.alert').fadeOut();
        }, 1500);
      });
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' /* optional */
        });
      });
    </script>
  </body>
</html>
