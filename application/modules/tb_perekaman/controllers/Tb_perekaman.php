<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tb_perekaman extends MY_Controller {

	var $access, $id_grup;

  function __construct(){
		parent::__construct();

		$controller = 'tb_perekaman';

    if($this->session->userdata('menu') != $controller){
      $this->session->unset_userdata('search');
      $this->session->set_userdata(array('menu' => 'tb_perekaman'));
    }
    $this->load->model('ap_konfigurasi/m_ap_konfigurasi');
		$this->id_grup = $this->session->userdata('id_grup');
		$this->access = $this->m_ap_konfigurasi->get_permission($this->id_grup, $controller);
		if ($this->access == null) {
			redirect(base_url().'ap_error/error_403');
		}
		$this->load->model('m_tb_perekaman');
		$this->load->model('m_tb_device');
		$this->load->model('m_dt_pns');
	}

	public function index()
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Daftar';

		if ($this->access->_read) {
			$search = null;
			if($search = $_POST){
				$this->session->set_userdata(array('search' => $search));
			}else{
				if($this->session->userdata('search') != null){
					$search = $this->session->userdata('search');
				}
			}
			$data['search'] = $search;

			$config['base_url'] = base_url().'tb_perekaman/index/';
			$config['per_page'] = 10;

			$from = $this->uri->segment(3);
			
			if($search == null){
				$num_rows = $this->m_dt_pns->num_rows();

				$config['total_rows'] = $num_rows;
				$this->pagination->initialize($config);

				$data['pegawai'] = $this->m_dt_pns->get_list($config['per_page'],$from,$search = null);
			}else{
				$search = $this->session->userdata('search');
				$num_rows = $this->m_dt_pns->num_rows($search);
				$config['total_rows'] = $num_rows;
				$this->pagination->initialize($config);

				$data['pegawai'] = $this->m_dt_pns->get_list($config['per_page'],$from,$search);
			}
			
			$data['num_rows'] = $num_rows;
			$data['num_rows_total'] = $this->m_dt_pns->num_rows_all();
			insert_log('read',$this->access->menu);
			$this->view('index',$data);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function form($id = null)
	{
		$data['access'] = $this->access;
		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Tambah Data';
				$data['action'] = 'insert';
				$data['device'] = null;
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Ubah Data Perekaman';
				$data['action'] = 'update';
				$data['pegawai'] = $this->m_dt_pns->get_by_id($id);
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}


	public function reset_search()
  {
    $this->session->unset_userdata('search');
    redirect(base_url().'tb_perekaman/index');
  }

	public function insert()
	{
		$data = $_POST;
		if ($data != null) {
			$data['created_by'] = $this->session->userdata('nama');
			if(!isset($data['is_active'])){$data['is_active'] = 0;}
			$this->m_tb_perekaman->insert($data);
			insert_log('insert',$this->access->menu);
			$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
			redirect(base_url().'tb_perekaman/index');
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function update()
	{
		$data = $_POST;
		if ($data != null) {
			if($this->access->_update){
				$data['updated_by'] = $this->session->userdata('nama');
				if(!isset($data['is_active'])){$data['is_active'] = 0;}
				$id = $data['id_pegawai'];
				$this->m_dt_pns->update($id, $data);
				insert_log('update',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				redirect(base_url().'tb_perekaman/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function delete()
	{
		$data = $_POST;
		if($data != null){
			if ($this->access->_delete) {
				$this->m_tb_perekaman->delete_temp($data['id_user']);
				insert_log('delete',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				redirect(base_url().'tb_perekaman/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}

}
