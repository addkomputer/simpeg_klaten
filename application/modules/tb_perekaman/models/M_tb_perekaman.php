<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_tb_perekaman extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like("server_IP",$val);
					$this->db->or_like("lokasi",$val);
        }
      }
    }

    return $this->db
      ->join('dt_pegawai b','a.nama=b.nomor_induk')
      ->join('tb_device c','a.device_sn=c.device_sn')
      ->where('a.is_deleted','0')
      ->order_by('id_user')
      ->get('tb_user a',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
					$this->db->like("server_IP",$val);
					$this->db->or_like("lokasi",$val);
        }
      }
    }

    return $this->db->query("select nomor_induk, dt_pegawai.nama, lokasi 
      from tb_user 
      inner join dt_pegawai on tb_user.nama=dt_pegawai.nomor_induk
      inner join tb_device on tb_device.device_sn=tb_user.device_sn 
      order by dt_pegawai.nomor_induk")
      ->num_rows();
  }

  function num_rows_total(){
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_user')
      ->get('tb_user')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('tb_user')->result();
	}

  public function get_by_id($id)
  {
     return $this->db
      ->join('dt_pegawai b','a.nama=b.nomor_induk')
      ->join('tb_device c','a.device_sn=c.device_sn')
      ->join('tb_template d','a.pin=d.pin')
      ->where('a.pin',$id)
      ->get('tb_user a')->row();
  }

  public function get_tmp($id)
  {
     return $this->db
      ->where('a.pin',$id)
      ->get('tb_template a')->result();
  }

  public function get_first()
  {
    return $this->db->order_by('id_user','asc')->get('tb_user')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_user','desc')->get('tb_user')->row();
  }

  public function insert($data)
  {
    $this->db->insert('tb_user',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_user',$id)->update('tb_user',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id_user',$id)->update('tb_user',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_user',$id)->delete('tb_user');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE tb_user");
  }

}
