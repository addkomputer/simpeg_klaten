<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_tb_device extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like("server_IP",$val);
					$this->db->or_like("lokasi",$val);
					$this->db->or_like("device_sn",$val);
        }
      }
    }

    return $this->db
      // ->where('is_deleted','0')
      ->order_by('device_no')
      ->get('tb_device',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
					$this->db->like("server_IP",$val);
          $this->db->or_like("lokasi",$val);
          $this->db->or_like("device_sn",$val);
        }
      }
    }

    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_device')
      ->get('tb_device')
      ->num_rows();
  }

  function num_rows_total(){
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_device')
      ->get('tb_device')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
      ->where('is_active','1')
      ->order_by('device_no')
			->get('tb_device')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id_device',$id)->get('tb_device')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_device','asc')->get('tb_device')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_device','desc')->get('tb_device')->row();
  }

  public function insert($data)
  {
    $this->db->insert('tb_device',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_device',$id)->update('tb_device',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id_device',$id)->update('tb_device',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_device',$id)->delete('tb_device');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE tb_device");
  }

}
