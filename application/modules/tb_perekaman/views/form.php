<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-primary">
        <?php
                        $id_status_pegawai = '';
                        switch ($pegawai->id_status_pegawai) {
                          case 1:
                            $id_status_pegawai = 'CPNS';
                            break;
                          case 2:
                            $id_status_pegawai = 'PNS';
                            break;
                          case 3:
                            $id_status_pegawai = 'BLUD';
                            break;
                          case 4:
                            $id_status_pegawai = 'P3K';
                            break;
                          case 5:
                            $id_status_pegawai = 'THL';
                            break;
                        }
                      ?>
        <form id="form" class="form-horizontal" action="<?=base_url()?>tb_perekaman/<?=$action?>" method="post" autocomplete="off">
          <div class="box-body">
            <input type="hidden" class="form-control input-sm" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai != null){echo $pegawai->id_pegawai;};?>">
            <div class="form-group">
              <label class="col-sm-2 control-label">PIN</label>
              <div class="col-sm-1">
                <input type="number" class="form-control input-sm" name="pin" value="<?php if($pegawai != null){echo $pegawai->pin;};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Nomor Induk</label>
              <div class="col-sm-3">
                        <?=$pegawai->nomor_induk?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Nama</label>
              <div class="col-sm-3">
                    <?php
                      echo $pegawai->gelar_depan.' '.$pegawai->nama;
                      if ($pegawai->gelar_belakang != ''){
                        echo ', '.$pegawai->gelar_belakang;
                      }
                    ?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Jenis Kelamin</label>
              <div class="col-sm-3">
                      <?php echo $jk = ($pegawai->jenis_kelamin == '1') ? 'Laki-laki' : 'Perempuan';?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">TTL</label>
              <div class="col-sm-3">
                      <?=$pegawai->tempat_lhr.', '.date_to_id($pegawai->tgl_lhr)?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Usia</label>
              <div class="col-sm-3">
                      <?php if($pegawai->tgl_lhr != ''){echo hitung_umur($pegawai->tgl_lhr);}?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Jabatan</label>
              <div class="col-sm-3">
                      <?=$pegawai->jabatan?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Status</label>
              <div class="col-sm-3">
                      <?= $id_status_pegawai ?>
              </div>
            </div>

            <hr>
            <div class="form-group">
              <div class="col-md-offset-2 col-sm-8">
                <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
                <a class="btn btn-sm btn-default" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Batal</a>
              </div>
            </div>
          </div>
        </form>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<script>
  $(document).ready(function () {
    // form validator
    $('#form').validate();
  })
</script>
