<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_tb_log_kehadiran extends CI_Model {

  private $suffix,$tanggal,$term,$id_ruang,$id_jabatan;

  function __construct(){
    parent::__construct();
    if ($this->session->userdata('search')) {
      $this->tanggal = $this->session->userdata('search')['tanggal'];
      $this->term = $this->session->userdata('search')['term'];
      if ($this->session->userdata('id_grup') == 4) {
        $this->id_ruang = $this->session->userdata('id_ruang');
      }else{
        $this->id_ruang = $this->session->userdata('search')['id_ruang'];
      }
      $this->id_jabatan = $this->session->userdata('search')['id_jabatan'];
      $this->id_tipe = $this->session->userdata('search')['id_tipe'];
      $this->suffix = substr($this->tanggal,6,4).'_'.substr($this->tanggal,3,2);
    }else{
      $this->tanggal = date('d-m-Y');
      $this->suffix = date('Y_m');
      if ($this->session->userdata('id_grup') == 4) {
        $this->id_ruang = $this->session->userdata('id_ruang');
      }else{
        $this->id_ruang = '';
      }
      $this->term = '';
      $this->id_jabatan = '';
      $this->id_tipe = '';
      $search = array(
        'tanggal' => $this->tanggal,
        'term' => $this->term,
        'suffix' => $this->suffix,
        'id_ruang' => $this->id_ruang,
        'id_jabatan' => $this->id_jabatan,
        'id_tipe' => $this->id_tipe
      );
      $this->session->set_userdata(array('search' => $search));
    }
    $this->create_table($this->suffix);
  }

  public function create_table()
	{
		$this->db->query(
			"CREATE TABLE IF NOT EXISTS `tb_log_kehadiran_$this->suffix` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `id_pegawai` varchar(50) DEFAULT '0',
        `nomor_induk` varchar(50) DEFAULT '0',
        `pin` varchar(50) DEFAULT '0',
        `id_tipe` int(1) DEFAULT '-1',
        `id_shift` int(11) DEFAULT '7',
        `is_tukar` tinyint(1) DEFAULT '0',
        `kode` varchar(50) DEFAULT 'P3',
        `tanggal` date DEFAULT NULL,
        `jam_datang` time DEFAULT NULL,
        `jam_batas_datang` time DEFAULT NULL,
        `is_datang` tinyint(1) NOT NULL DEFAULT '0',
        `jam_datang_pegawai` time DEFAULT NULL,
        `terlambat_datang` int(11) DEFAULT '0',
        `ket_datang` text,
        `is_pulang` tinyint(1) NOT NULL DEFAULT '0',
        `jam_pulang` time DEFAULT NULL,
        `jam_batas_pulang` time DEFAULT NULL,
        `jam_pulang_pegawai` time DEFAULT NULL,
        `mendahului_pulang` int(11) DEFAULT '0',
        `ket_pulang` text,
        `ket_umum` text,
        PRIMARY KEY (`id`)
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;"
		);
	}

	public function get_list($number,$offset,$search = null)
  {
    $where = "WHERE ";
    if ($search != null) {
      $where .= "a.tanggal = '".date_to_id($this->tanggal)."'";
      if ($search['term'] != '') {
        $where .= " AND c.nama LIKE '%".$search['term']."%'";
      }
      if($search['id_ruang'] != ''){
        $where .= " AND c.id_ruang = '".$search['id_ruang']."'";
      }
      if($search['id_jabatan'] != ''){
        $where .= " AND c.id_jabatan = '".$search['id_jabatan']."'";
      }
      if($search['id_tipe'] != ''){
        $where .= " AND a.id_tipe LIKE '".$search['id_tipe']."%'";
      }
    }else{
      $where .= "a.tanggal = '".$this->tanggal."'";
    }
    
    $query = $this->db->query(
      "SELECT 
        a.pin,a.tanggal,a.kode,
        a.is_datang,a.jam_datang,a.jam_datang_pegawai,a.ket_datang,
        a.is_pulang,a.jam_pulang,a.jam_pulang_pegawai,a.ket_pulang,a.ket_umum,
        b.id_tipe,b.tipe,
        c.pin,c.id_pegawai,c.nomor_induk,c.gelar_depan,c.nama,c.gelar_belakang,
        d.status_pegawai,
        e.jabatan,
        f.ruang
      FROM tb_log_kehadiran_$this->suffix a
        JOIN tb_log_tipe b ON a.id_tipe=b.id_tipe 
        LEFT JOIN dt_pegawai c ON a.pin = c.pin
        LEFT JOIN ms_status_pegawai d ON c.id_status_pegawai=d.id_status_pegawai 
        LEFT JOIN ms_jabatan e ON c.id_jabatan = e.id_jabatan
        LEFT JOIN ms_ruang f ON c.id_ruang=f.id_ruang 
      $where
      ORDER BY
        id_golongan DESC
      LIMIT $offset,$number"
    );

    return $query->result();
  }

  function num_rows($search = null){
		$where = "WHERE ";
    if ($search != null) {
      $where .= "a.tanggal = '".date_to_id($this->tanggal)."'";
      if ($search['term'] != '') {
        $where .= " AND c.nama LIKE '%".$search['term']."%'";
      }
      if($search['id_ruang'] != ''){
        $where .= " AND c.id_ruang = '".$search['id_ruang']."'";
      }
      if($search['id_jabatan'] != ''){
        $where .= " AND c.id_jabatan = '".$search['id_jabatan']."'";
      }
      if($search['id_tipe'] != ''){
        $where .= " AND a.id_tipe LIKE '".$search['id_tipe']."%'";
      }
    }else{
      $where .= "a.tanggal = '".$this->tanggal."'";
    }
    
    $query = $this->db->query(
      "SELECT 
        a.pin,a.tanggal,a.jam_datang_pegawai,a.ket_datang,
        a.jam_pulang_pegawai,a.ket_pulang,a.ket_umum,
        b.id_tipe,b.tipe,
        c.id_pegawai,c.nomor_induk,c.gelar_depan,c.nama,c.gelar_belakang,
        d.status_pegawai,
        e.jabatan,
        f.ruang
      FROM tb_log_kehadiran_$this->suffix a
        JOIN tb_log_tipe b ON a.id_tipe=b.id_tipe 
        LEFT JOIN dt_pegawai c ON a.pin = c.pin
        LEFT JOIN ms_status_pegawai d ON c.id_status_pegawai=d.id_status_pegawai 
        LEFT JOIN ms_jabatan e ON c.id_jabatan = e.id_jabatan
        LEFT JOIN ms_ruang f ON c.id_ruang=f.id_ruang 
      $where
      ORDER BY
        id_golongan DESC"
    );

    return $query->num_rows();
  }
  
  function num_rows_total(){  
    $where = "WHERE ";
    if ($this->session->userdata('search') != null) {
      $where .= "a.tanggal = '".date_to_id($this->tanggal)."'";
    }else{
      $where .= "a.tanggal = '".$this->tanggal."'";
    }
    
    $query = $this->db->query(
      "SELECT 
        a.pin,a.tanggal,a.jam_datang_pegawai,a.ket_datang,
        a.jam_pulang_pegawai,a.ket_pulang,a.ket_umum,
        b.id_tipe,b.tipe,
        c.id_pegawai,c.nomor_induk,c.gelar_depan,c.nama,c.gelar_belakang,
        d.status_pegawai,
        e.jabatan,
        f.ruang
      FROM tb_log_kehadiran_$this->suffix a
        JOIN tb_log_tipe b ON a.id_tipe=b.id_tipe 
        LEFT JOIN dt_pegawai c ON a.pin = c.pin
        LEFT JOIN ms_status_pegawai d ON c.id_status_pegawai=d.id_status_pegawai 
        LEFT JOIN ms_jabatan e ON c.id_jabatan = e.id_jabatan
        LEFT JOIN ms_ruang f ON c.id_ruang=f.id_ruang 
      $where
      ORDER BY
        jam_datang_pegawai DESC"
    );

    return $query->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_active','1')
			->get('tb_log_kehadiran_2018_10')->result();
  }
  
  public function detail($tanggal,$id_pegawai)
  {
    $data = $this->db
      ->join('ms_golongan b','a.id_golongan = b.id_golongan')
      ->join('ms_ruang c','a.id_ruang = c.id_ruang')
      ->join('ms_status_pegawai d','a.id_status_pegawai = d.id_status_pegawai')
      ->where('a.id_pegawai',$id_pegawai)
      ->get('dt_pegawai a')->row();
    $data->detail = $this->db
      ->join('tb_device b','a.SN = b.device_sn')
      ->like('a.ScanDate',$tanggal)
      ->where('a.pin',$data->pin)
      ->get('tb_scanlog_'.$this->suffix.' a')->result();
    return $data;
  }

  function count(){
    $tanggal = date_to_id($this->tanggal);
    $where = "a.tanggal = '$tanggal'";
    if($this->id_ruang != ''){
      $where .= " AND b.id_ruang = $this->id_ruang";
    }
    $query = $this->db->query(
      "SELECT 
        COUNT(id) AS semua,
        COUNT(CASE WHEN a.id_tipe LIKE '-1%' THEN 1 END) AS tanpa_keterangan,
        COUNT(CASE WHEN a.id_tipe LIKE '0%' THEN 1 END) AS libur,
        COUNT(CASE WHEN a.id_tipe LIKE '1%' THEN 1 END) AS masuk,
        COUNT(CASE WHEN a.id_tipe = '21' THEN 1 END) AS dinas_dalam,
        COUNT(CASE WHEN a.id_tipe = '22' THEN 1 END) AS dinas_luar,
        COUNT(CASE WHEN a.id_tipe LIKE '3%' THEN 1 END) AS sakit,
        COUNT(CASE WHEN a.id_tipe LIKE '4%' THEN 1 END) AS cuti,
        COUNT(CASE WHEN a.id_tipe LIKE '5%' THEN 1 END) AS izin,
        COUNT(CASE WHEN a.id_tipe LIKE '6%' THEN 1 END) AS data_lain
      FROM tb_log_kehadiran_$this->suffix a
      RIGHT JOIN dt_pegawai b ON a.pin = b.pin
      WHERE
        $where
      "
    );

    return $query->row();
  }

  public function get_by_id($id)
  {
    return $this->db->where('id',$id)->get('tb_log_kehadiran_2018_10')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id','asc')->get('tb_log_kehadiran_2018_10')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id','desc')->get('tb_log_kehadiran_2018_10')->row();
  }

  public function insert($data)
  {
    $this->db->insert('tb_log_kehadiran_2018_10',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id',$id)->update('tb_log_kehadiran_2018_10',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id',$id)->update('tb_log_kehadiran_2018_10',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id',$id)->delete('tb_log_kehadiran_2018_10');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE tb_log_kehadiran_2018_10");
  }

}
