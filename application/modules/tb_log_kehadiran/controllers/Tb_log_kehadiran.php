<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tb_log_kehadiran extends MY_Controller {

	var $access, $id_grup;

  function __construct(){
		parent::__construct();

		$controller = 'tb_log_kehadiran';

    if($this->session->userdata('menu') != $controller){
      $this->session->unset_userdata('search');
      $this->session->set_userdata(array('menu' => 'tb_log_kehadiran'));
    }
    $this->load->model('ap_konfigurasi/m_ap_konfigurasi');
		$this->id_grup = $this->session->userdata('id_grup');
		$this->access = $this->m_ap_konfigurasi->get_permission($this->id_grup, $controller);
		if ($this->access == null) {
			redirect(base_url().'ap_error/error_403');
		}
		$this->load->model('m_tb_log_kehadiran');
		$this->load->model('ms_ruang/m_ms_ruang');
		$this->load->model('ms_jabatan/m_ms_jabatan');
	}
	
	public function index()
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Daftar';
		$data['ruang'] = $this->m_ms_ruang->get_all();
		$data['jabatan'] = $this->m_ms_jabatan->get_all();

		if ($this->access->_read) {
			$search = null;
			if($search = $_POST){
				if (!$this->session->userdata('search')['tanggal']) {
					$search['tanggal'] = date('d-m-Y');
				}else{
					$search['tanggal'] = $this->session->userdata('search')['tanggal'];
				}
				$this->session->set_userdata(array('search' => $search));
			}else{
				if($this->session->userdata('search') != null){
					$search = $this->session->userdata('search');
				}
			}
			
			$data['search'] = $search;
	
			$config['base_url'] = base_url().'tb_log_kehadiran/index/';
			$config['per_page'] = 10;
	
			$from = $this->uri->segment(3);
			if ($from == '') {
				$from = 0;
			}
	
			if($search == null){
				$num_rows = $this->m_tb_log_kehadiran->num_rows();
	
				$config['total_rows'] = $num_rows;
				$this->pagination->initialize($config);
	
				$data['main'] = $this->m_tb_log_kehadiran->get_list($config['per_page'],$from,$search = null);
			}else{
				$search = $this->session->userdata('search');
				$num_rows = $this->m_tb_log_kehadiran->num_rows($search);
				$config['total_rows'] = $num_rows;
				$this->pagination->initialize($config);
	
				$data['main'] = $this->m_tb_log_kehadiran->get_list($config['per_page'],$from,$search);
			}
			
			$data['num_rows'] = $num_rows;
			$data['num_rows_total'] = $this->m_tb_log_kehadiran->num_rows_total();
			$data['count'] = $this->m_tb_log_kehadiran->count();
			// echo '<pre>' . var_export($data['main'], true) . '</pre>';exit();
			insert_log('read',$this->access->menu);
			$this->view('index',$data);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function search()
	{
		$search = $_POST;
		if (!isset($_POST['id_tipe'])) {
			$search['id_tipe'] = '';
		};
		$this->session->set_userdata(array('search' => $search));
		redirect(base_url().$this->access->controller);
	}

	public function search_tanggal($tanggal)
	{
		$search['tanggal'] = $tanggal;
		if (!$this->session->userdata('search')['term']) {
			$search['term'] = '';
			$search['id_ruang'] = '';
			if ($this->session->userdata('id_grup') == 4) {
				$search['id_ruang'] = $this->session->userdata('id_ruang');
			}
			$search['id_jabatan'] = '';
			$search['id_tipe'] = '';
		}else{
			$search['term'] = $this->session->userdata('search')['term'];
			$search['id_ruang'] = $this->session->userdata('search')['id_ruang'];
			$search['id_jabatan'] = $this->session->userdata('search')['id_jabatan'];
			$search['id_tipe'] = $this->session->userdata('search')['id_tipe'];
		}
		$this->session->set_userdata(array('search' => $search));
		redirect(base_url().$this->access->controller);
	}
	
	public function form($id = null)
	{
		$data['access'] = $this->access;
		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Tambah Data';
				$data['action'] = 'insert';
				$data['grup'] = null;
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Ubah Data';
				$data['action'] = 'update';
				$data['grup'] = $this->m_tb_log_kehadiran->get_by_id($id);
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		}
	}

	public function detail($tanggal,$id_pegawai)
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Detail Kehadiran';
		$data['main'] = $this->m_tb_log_kehadiran->detail($tanggal,$id_pegawai);
		$this->view('detail',$data);
	}

	public function reset_search()
  {
    $this->session->unset_userdata('search');
    redirect(base_url().'tb_log_kehadiran/index');
  }

	public function insert()
	{
		$data = $_POST;
		if ($data != null) {
			$data['created_by'] = $this->session->userdata('nama');
			if(!isset($data['is_active'])){$data['is_active'] = 0;}
			$this->m_tb_log_kehadiran->insert($data);
			insert_log('insert',$this->access->menu);
			$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
			redirect(base_url().'tb_log_kehadiran/index');
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function update()
	{
		$data = $_POST;
		if ($data != null) {
			if($this->access->_update){
				$data['updated_by'] = $this->session->userdata('nama');
				if(!isset($data['is_active'])){$data['is_active'] = 0;}
				$id = $data['id_grup'];
				$this->m_tb_log_kehadiran->update($id, $data);
				insert_log('update',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				redirect(base_url().'tb_log_kehadiran/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function delete()
	{
		$data = $_POST;
		if($data != null){
			if ($this->access->_delete) {
				$this->m_tb_log_kehadiran->delete_temp($data['id_grup']);
				insert_log('delete',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				redirect(base_url().'tb_log_kehadiran/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}

}