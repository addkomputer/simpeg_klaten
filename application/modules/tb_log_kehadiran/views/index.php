<style>
  .tooltip-inner {
    white-space:pre;
    max-width: none;
  }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
      <small><?=$subtitle?></small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">Kehadiran Saya</li>
      <li class="active text-orange"><?=$access->menu?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <i class="fa fa-table text-maroon"></i>
        <h3 class="box-title">Tabel <?=$access->menu?></h3>
        <div class="box-tools pull-right">
          <span class="badge bg-maroon"><i class="fa fa-bar-chart"></i> Jumlah Keseluruhan Data : <?=$num_rows_total?></span>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <form id="form_search" action="<?=base_url()?>tb_log_kehadiran/search" method="post" autocomplete="off">
            <div class="col-md-2">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <input type="text" class="form-control datepicker" id="tanggal" name="tanggal" placeholder="dd-mm-yyyy" value="<?php if($search != null){echo $search['tanggal'];}else{echo date('d-m-Y');}?>">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <select name="id_ruang" id="id_ruang" class="form-control select2">
                  <?php if($this->session->userdata('id_grup') == 4): ?>
                    <?php foreach($ruang as $row): ?>
                      <?php if($row->id_ruang == $this->session->userdata('id_ruang')): ?>
                        <option value="<?=$row->id_ruang?>" <?php if($search){if($row->id_ruang == $search['id_ruang']){echo 'selected';}}?>><?=$row->ruang?></option>
                      <?php endif;?>
                    <?php endforeach;?>
                  <?php else: ?>
                    <option value="">-- Semua Ruang --</option>
                    <?php foreach($ruang as $row): ?>
                      <option value="<?=$row->id_ruang?>" <?php if($search){if($row->id_ruang == $search['id_ruang']){echo 'selected';}}?>><?=$row->ruang?></option>
                    <?php endforeach;?>
                  <?php endif;?>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <select name="id_jabatan" id="id_jabatan" class="form-control select2">
                  <option value="">-- Semua Jabatan --</option>
                  <?php foreach($jabatan as $row): ?>
                    <option value="<?=$row->id_jabatan?>" <?php if($search){if($row->id_jabatan == $search['id_jabatan']){echo 'selected';}}?>><?=$row->jabatan?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>
            <div class="col-md-4">  
              <div class="input-group">
                <input type="text" name="term" class="form-control pull-right" placeholder="Pencarian Nama" value="<?php if($search != null){echo $search['term'];}?>">
                <div class="input-group-btn">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                  <a class="btn btn-default btn-flat" href="<?=base_url()?>tb_log_kehadiran/reset_search"><i class="fa fa-refresh"></i></a>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 text-center">
                <div class="btn-group" data-toggle="buttons">
                  <label class="btn btn-default btn-flat <?php if($search){if($search['id_tipe'] == ''){echo 'active';}}else{echo 'active';}?>">
                    <i class="fa fa-list-alt"></i>
                    <input type="radio" name="id_tipe" value="" autocomplete="off"> Semua
                    <span class="badge bg-green"><?=$count->semua?></span>
                  </label>
                  <label class="btn btn-default btn-flat <?php if($search){if($search['id_tipe'] == '-1'){echo 'active';}}?>">
                    <i class="fa fa-times-rectangle"></i>
                    <input type="radio" name="id_tipe" value="-1" autocomplete="off"> Tanpa Keterangan 
                    <span class="badge bg-green"><?=$count->tanpa_keterangan?></span>
                  </label>
                  <label class="btn btn-default btn-flat <?php if($search){if($search['id_tipe'] == '0'){echo 'active';}}?>">
                    <i class="fa fa-times-rectangle"></i>
                    <input type="radio" name="id_tipe" value="0" autocomplete="off"> Libur  
                    <span class="badge bg-green"><?=$count->libur?></span>
                  </label>
                  <label class="btn btn-default btn-flat <?php if($search){if($search['id_tipe'] == '1'){echo 'active';}}?>">
                    <i class="fa fa-user"></i>
                    <input type="radio" name="id_tipe" value="1" autocomplete="off"> Masuk 
                    <span class="badge bg-green"><?=$count->masuk?></span>
                  </label>
                  <label class="btn btn-default btn-flat <?php if($search){if($search['id_tipe'] == '2'){echo 'active';}}?>">
                    <i class="fa fa-files-o"></i>
                    <input type="radio" name="id_tipe" value="21" autocomplete="off"> DD/DL
                    <span class="badge bg-green"><?=$count->dinas_dalam?></span>
                  </label>
                  <label class="btn btn-default btn-flat <?php if($search){if($search['id_tipe'] == '3'){echo 'active';}}?>">
                    <i class="fa fa-plus"></i>
                    <input type="radio" name="id_tipe" value="3" autocomplete="off"> Sakit 
                    <span class="badge bg-green"><?=$count->sakit?></span>
                  </label>
                  <label class="btn btn-default btn-flat <?php if($search){if($search['id_tipe'] == '4'){echo 'active';}}?>">
                    <i class="fa fa-ban"></i>
                    <input type="radio" name="id_tipe" value="4" autocomplete="off"> Cuti 
                    <span class="badge bg-green"><?=$count->cuti?></span>
                  </label>
                  <label class="btn btn-default btn-flat <?php if($search){if($search['id_tipe'] == '5'){echo 'active';}}?>">
                    <i class="fa fa-calendar-minus-o"></i>
                    <input type="radio" name="id_tipe" value="5" autocomplete="off"> Izin 
                    <span class="badge bg-green"><?=$count->izin?></span>
                  </label>
                  <label class="btn btn-default btn-flat <?php if($search){if($search['id_tipe'] == '6'){echo 'active';}}?>">
                    <i class="fa fa-file"></i>
                    <input type="radio" name="id_tipe" value="6" autocomplete="off"> Data Lain  
                    <span class="badge bg-green"><?=$count->data_lain?></span>
                  </label>
                </div>
              </div>
            </div>
          </form>
        </div>
        <br>
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-condensed">
            <thead>
              <tr>
                <th class="text-center" style="vertical-align:middle" width="20">No.</th>
                <th class="text-center" style="vertical-align:middle" width="20">Aksi</th>
                <th class="text-center" style="vertical-align:middle" width="20">Status <br> PIN</th>
                <th class="text-center" style="vertical-align:middle">Nama Pegawai <br> Nomor Induk</th>
                <th class="text-center" style="vertical-align:middle" width="200">Jabatan</th>
                <th class="text-center" style="vertical-align:middle" width="200">Ruang</th>
                <th class="text-center" style="vertical-align:middle" width="90">Tanggal</th>
                <th class="text-center" style="vertical-align:middle" width="50">Kode</th>
                <th class="text-center" style="vertical-align:middle" width="90">Jam Datang Shift</th>
                <th class="text-center" style="vertical-align:middle" width="100">Jam Datang Pegawai <br>Ket. Datang</th>
                <th class="text-center" style="vertical-align:middle" width="90">Jam Pulang Shift</th>
                <th class="text-center" style="vertical-align:middle" width="100">Jam Pulang Pegawai <br>Ket. Pulang</th>
                <th class="text-center" style="vertical-align:middle" width="100">Keterangan</th>
              </tr>
            </thead>
            <tbody>
              <?php if ($main != null): ?>
                <?php $i=1;foreach ($main as $row): ?>
                  <tr>
                    <td class="text-center"><?=$this->uri->segment('3')+$i++?></td>
                    <td class="text-center">                      
                      <a class="btn btn-xs btn-flat btn-success" href="<?=base_url()?>tb_log_kehadiran/detail/<?=$row->tanggal.'/'.$row->id_pegawai?>" style="padding-right:0.250em;"><i class="fa fa-list"></i></a> 
                    </td>
                    <td class="text-center">
                      <?=$row->status_pegawai?><br><?=$row->pin?>
                    </td>
                    <td>
                      <strong><?php 
                        echo $row->gelar_depan.' '.$row->nama;
                        if ($row->gelar_belakang != ''){
                          echo ', '.$row->gelar_belakang;
                        }
                      ?></strong><br>
                      <?=$row->nomor_induk?><br>
                      <?=$row->status_pegawai?>
                    </td>
                    <td>
                      <?php if($row->jabatan == ''){echo '-';}else{echo $row->jabatan;}?>
                    </td>
                    <td>
                      <?php if($row->ruang == ''){echo '-';}else{echo $row->ruang;}?></dd>
                    </td>
                    <td class="text-center"><?=date_to_id($row->tanggal)?></td>
                    <td class="text-center">
                      <?=$row->kode?>
                    </td>
                    <td class="text-center"><?=$row->jam_datang?></td>
                    <td class="text-center text-green">
                      <?php if($row->is_datang == 1):?>  
                        <b><?=$row->jam_datang_pegawai?></b><br>
                        <small class="text-blue"><?=$row->ket_datang?></small>
                      <?php else: ?>
                        <b>-</b><br>
                        <small class="text-red">Belum absen datang</small>
                      <?php endif; ?>
                    </td>
                    <td class="text-center"><?=$row->jam_pulang?></td>
                    <td class="text-center text-red">
                      <?php if($row->is_pulang == 1):?>  
                        <b><?=$row->jam_pulang_pegawai?></b><br>
                        <small class="text-blue"><?=$row->ket_pulang?></small>
                      <?php else: ?>
                        <b>-</b><br>
                        <small class="text-red">Belum absen pulang</small>
                      <?php endif; ?>
                    </td>                  
                    <?php 
                      $col_tipe = '';
                      switch (substr($row->id_tipe,0,1)) {
                        case '0':
                          $col_tipe = 'text-red';
                          break;
                        
                        case '1':
                          $col_tipe = 'text-green';
                          break;
                        
                        case '2':
                          $col_tipe = 'text-orange';
                          break;

                        case '3':
                          $col_tipe = 'text-teal';
                          break;
                        
                        case '4':
                          $col_tipe = 'text-maroon';
                          break;

                        case '5':
                          $col_tipe = 'text-purple';
                          break;

                        case '6':
                          $col_tipe = 'text-navy';
                          break;
                      }
                    ?>
                    <td class="text-center <?=$col_tipe?>">
                      <?php if($row->id_tipe != '1'){
                        echo $row->tipe;
                      }?><br>
                      <?=$row->ket_umum?>
                    </td>
                  </tr>
                <?php endforeach; ?>
              <?php else: ?>
                <tr>
                  <td class="text-center" colspan="99">Tidak ada data!</td>
                </tr>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <span class="badge bg-purple">Jumlah Data : <?=$num_rows?></span>
        <?php echo $this->pagination->create_links(); ?>
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
  $(document).ready(function () {
    $('#tanggal').datepicker()
    .on('changeDate', function(e) {
      // alert(e.format());
      window.location.replace("<?=base_url().$this->access->controller.'/search_tanggal/'?>"+e.format());
    });
    $('.select2').on('select2:select', function (e) {
      $('#form_search').submit();
    });
    $("input[name='id_tipe']").change(function(){
      $('#form_search').submit();
    });
    $('[data-toggle="tooltip"]').tooltip();
  })
  function del(id) {
    $("#modal_delete").modal('show');
    $("#id").val(id);
  }
</script>