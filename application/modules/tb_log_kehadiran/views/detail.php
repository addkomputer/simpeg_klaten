<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">Kehadiran Saya</li>
        <li class="active text-orange"><?=$access->menu?></li>
      </ol> 
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-primary">
        <div class="box-header with-border">
          <i class="fa fa-table text-maroon"></i>
          <h3 class="box-title">Tabel <?=$access->menu?></h3>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <table class="table table-striped table-condensed">
                <tr>
                  <td width="100">Nama</td>
                  <td width="10">:</td>
                  <td>
                    <?php 
                      echo $main->gelar_depan.' '.$main->nama;
                      if ($main->gelar_belakang != ''){
                        echo ', '.$main->gelar_belakang;
                      }
                    ?>
                  </td>
                </tr>
                <tr>
                  <td>Nomor Induk</td>
                  <td>:</td>
                  <td>
                    <?=$main->nomor_induk?>
                  </td>
                </tr>
                <tr>
                  <td>TTL</td>
                  <td>:</td>
                  <td>
                    <?=$main->tempat_lhr?>, <?=date_to_id($main->tgl_lhr)?>
                  </td>
                </tr>
              </table>
            </div>
            <div class="col-md-6">
              <table class="table table-striped table-condensed">
                <tr>
                  <td width="100">Golongan</td>
                  <td width="10">:</td>
                  <td>
                    <?=$main->golongan.' - '.$main->pangkat?>
                  </td>
                </tr>
                <tr>
                  <td>Ruang</td>
                  <td>:</td>
                  <td>
                    <?=$main->ruang?>
                  </td>
                </tr>
                <tr>
                  <td>Status</td>
                  <td>:</td>
                  <td>
                    <?=$main->status_pegawai?>
                  </td>
                </tr>
              </table>
            </div>
            <div class="col-md-12">
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-condensed">
                  <thead>
                    <tr>
                      <th class="text-center" width="50">No</th>
                      <th class="text-center">Mesin SN</th>
                      <th class="text-center">Lokasi</th>
                      <th class="text-center">ScanDate</th>
                      <th class="text-center">Tipe</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i=1;foreach ($main->detail as $row): ?>
                      <tr>
                        <td class="text-center"><?=$i++?></td>
                        <td class="text-center"><?=$row->device_sn?></td>
                        <td class="text-center"><?=$row->lokasi?></td>
                        <td class="text-center"><?=$row->ScanDate?></td>
                        <td class="text-center">
                          <?php if($row->IOMode == 0):?>
                            Masuk
                          <?php else: ?>
                            Keluar
                          <?php endif; ?>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<script>
  $(document).ready(function () {
    // form validator
    $('#form').validate();
  })
</script>