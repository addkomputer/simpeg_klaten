<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ms_jasa extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_jasa')
      ->get('ms_jasa',$number,$offset)
      ->result_array();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_jasa')
      ->get('ms_jasa')
      ->num_rows();
  }
  
  function num_rows_total(){  
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_jasa')
      ->get('ms_jasa')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('ms_jasa')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id_jasa',$id)->get('ms_jasa')->row_array();
  }

  public function get_by_filter($tahun, $bulan)
  {
    return $this->db
      ->where('tahun', $tahun)
      ->where('bulan', $bulan)
      ->get('ms_jasa')->row_array();
  }

  public function get_first()
  {
    return $this->db->order_by('id_jasa','asc')->get('ms_jasa')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_jasa','desc')->get('ms_jasa')->row();
  }

  public function insert($data)
  {
    $this->db->insert('ms_jasa',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_jasa',$id)->update('ms_jasa',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id_jasa',$id)->update('ms_jasa',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_jasa',$id)->delete('ms_jasa');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ms_jasa");
  }

}
