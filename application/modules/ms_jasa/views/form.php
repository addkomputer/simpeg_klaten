<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-primary">
        <form id="form" class="form-horizontal" action="<?=base_url()?>ms_jasa/<?=$action?>" method="post" autocomplete="off"> 
          <div class="box-body">
            <input type="hidden" class="form-control input-sm" name="id_jasa" id="id_jasa" value="<?php if($jasa != null){echo $jasa['id_jasa'];};?>">
            <div class="form-group">
              <label class="col-sm-2 control-label">Bulan - Tahun</label>
              <div class="col-sm-3">
                <select name="bulan" class="form-control select2 input-sm">
                  <?php foreach($list_month as $key => $val):?>
                    <option value="<?=$key?>" <?php if(@$jasa['bulan'] == $key) echo 'selected'?>><?=$val?></option>
                  <?php endforeach;?>
                </select>
              </div>
              <div class="col-sm-2">
                <select name="tahun" class="form-control select2 input-sm">
                  <?php for($t=$start_year; $t<=$end_year; $t++):?>
                    <option value="<?=$t?>" <?php if(@$jasa['tahun'] == $t) echo 'selected'?>><?=$t?></option>
                  <?php endfor;?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Total Jasa Karyawan (Rp.)</label>
              <div class="col-sm-3">
                <div class="input-group">
                  <span class="input-group-addon">Rp.</span>
                  <input type="text" class="form-control input-sm" name="total_jasa" id="total_jasa" value="<?php if($jasa != null){echo digit($jasa['total_jasa']);};?>" required>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Total Jasa Dokter (Rp.)</label>
              <div class="col-sm-3">
                <div class="input-group">
                  <span class="input-group-addon">Rp.</span>
                  <input type="text" class="form-control input-sm" name="total_jasa_dokter" id="total_jasa_dokter" value="<?php if($jasa != null){echo digit($jasa['total_jasa_dokter']);};?>" required>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Aktif</label>
              <div class="col-sm-5">
                <input class="iCheckbox" type="checkbox" name="is_active" value="1" <?php if($jasa != null){if($jasa['is_active'] == 1){echo 'checked';}}else{echo 'checked';}?>>
              </div>
            </div>
            <hr>
            <div class="form-group">
              <div class="col-md-offset-2 col-sm-8">
                <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
                <a class="btn btn-sm btn-default" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Batal</a>
              </div>
            </div>
          </div>
        </form>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<script>
  $(document).ready(function () {
    // form validator
    $('#form').validate();
    //
    $('#total_jasa,#total_jasa_dokter').bind('blur',function() {
        var id = $(this).attr('id');
        var val = $(this).val();
        var result = number_format(val);
        $('#'+id).val(result);
    });
    //
    function number_format(nStr) {
        nStr += '';
        x = nStr.split(',');
        x1 = x[0];
        x2 = x.length > 1 ? ',' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }
  })
</script>