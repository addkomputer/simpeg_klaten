<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?=base_url();?>img/no_user.png" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?=$this->session->userdata('nama');?></p>
        <a href="<?=base_url();?>#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <?php foreach ($sidenav as $lvl1): ?>
        <?php if($lvl1['tipe'] == 2): ?>
          <li class="header"><?=$lvl1['menu']?></li>
        <?php else:?>
          <li class="
            <?php 
              if(count($lvl1['child']) > 0){
                echo 'treeview ';
                foreach ($lvl1['child'] as $lvl2) {
                  echo active_menu($lvl2['controller']);
                  if($lvl2['child'] > 0){
                    foreach ($lvl2['child'] as $lvl3) {
                      echo active_menu($lvl3['controller']);
                    }
                  }
                };
              }else{
                echo active_menu($lvl1['controller']);
              }
            ?>
          ">
            <a href="<?php echo base_url();if(count($lvl1['child']) > 0){echo '#';}else{echo $lvl1['controller'].'/'.$lvl1['url'];}?>">
              <i class="fa fa-<?=$lvl1['icon']?>"></i> <span><?=$lvl1['menu']?></span>
              <?php if(count($lvl1['child']) > 0):?>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              <?php endif;?>
            </a>
            <?php if(count($lvl1['child']) > 0):?>
              <ul class="treeview-menu">
                <?php foreach ($lvl1['child'] as $lvl2): ?>  
                  <li class="<?php if(count($lvl2['child']) > 0){echo 'treeview ';foreach ($lvl2['child'] as $lvl3) {echo active_menu($lvl3['controller']);};}else{echo active_menu($lvl2['controller']);}?>">
                    <a href="<?php echo base_url();if(count($lvl2['child']) > 0){echo '#';}else{echo $lvl2['controller'].'/'.$lvl2['url'];}?>">
                      <i class="fa fa-<?=$lvl2['icon']?>"></i> <span><?=$lvl2['menu']?></span>
                      <?php if(count($lvl2['child']) > 0):?>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      <?php endif;?>
                    </a>
                    <?php if(count($lvl2['child']) > 0):?>
                      <ul class="treeview-menu">
                        <?php foreach ($lvl2['child'] as $lvl3): ?>
                          <li class="<?php if(count($lvl3['child']) > 0){echo 'treeview ';foreach ($lvl3['child'] as $lvl4) {echo active_menu($lvl4['controller']);};}else{echo active_menu($lvl3['controller']);}?>">
                            <a href="<?php echo base_url();if(count($lvl3['child']) > 0){echo '#';}else{echo $lvl3['controller'].'/'.$lvl3['url'];}?>"><i class="fa fa-circle-o"></i> <?=$lvl3['menu']?></a>
                          </li>
                        <?php endforeach;?>
                      </ul>
                    <?php endif;?>
                  </li>
                <?php endforeach;?>
              </ul>
            <?php endif;?>
          </li>
        <?php endif;?>
      <?php endforeach;?>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>

<!-- =============================================== -->