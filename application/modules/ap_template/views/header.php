<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?=@$profil->nama_aplikasi?></title>
  <!-- Favicon -->
  <link rel="shortcut icon" href="<?=base_url();?>img/<?=@$profil->logo?>" type="image/x-icon">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?=base_url();?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url();?>bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url();?>bower_components/Ionicons/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?=base_url();?>bower_components/select2/dist/css/select2.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?=base_url();?>bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?=base_url();?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Bootstrap wysihtml5 -->
  <link rel="stylesheet" href="<?=base_url()?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?=base_url();?>/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url();?>dist/css/AdminLTE.css">
  <!-- Custom style -->
  <link rel="stylesheet" href="<?=base_url();?>dist/css/custom.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?=base_url();?>plugins/iCheck/square/red.css">
  <!-- timer css -->
  <link rel="stylesheet" href="<?=base_url();?>bower_components/jquery/dist/jquery-clockpicker.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?=base_url();?>dist/css/skins/_all-skins.min.css">
  <!-- jQuery 3 -->
  <script src="<?=base_url();?>bower_components/jquery/dist/jquery.min.js"></script>
  <!-- timer -->
  <script src="<?=base_url();?>bower_components/jquery/dist/jquery-clockpicker.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="<?=base_url();?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- bootstrap wysihtml5 -->
  <script src="<?=base_url();?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
  <!-- SlimScroll -->
  <script src="<?=base_url();?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <!-- Jquery Validation -->
  <script src="<?=base_url()?>bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
  <!-- FastClick -->
  <script src="<?=base_url();?>bower_components/fastclick/lib/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="<?=base_url();?>dist/js/adminlte.min.js"></script>
  <!-- Select2 -->
  <script src="<?=base_url();?>bower_components/select2/dist/js/select2.full.min.js"></script>
  <!-- iCheck -->
  <script src="<?=base_url();?>plugins/iCheck/icheck.min.js"></script>
  <!-- date-range-picker -->
  <script src="<?=base_url();?>bower_components/moment/min/moment.min.js"></script>
  <script src="<?=base_url();?>bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
  <!-- bootstrap datepicker -->
  <script src="<?=base_url();?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="<?=base_url();?>bower_components/bootstrap-datepicker/js/locales/bootstrap-datepicker.id.js"></script>
  <!-- bootstrap time picker -->
  <script src="<?=base_url();?>plugins/timepicker/bootstrap-timepicker.min.js"></script>
  <!-- ChartJs -->
  <script src="<?=base_url();?>bower_components/chart.js/Chart.js"></script>
  <!-- jQuery Chain -->
  <script src="<?=base_url();?>plugins/jquery_chained/jquery.chained.min.js"></script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <script>
    $(document).ready(function () {
      // Admin LTE demo
      $('.sidebar-menu').tree();
      //select2
      $('.select2').select2({
        width : '100%'
      });  
      // fade out alert
      setInterval(function () {
        $('.callout').fadeOut();
      }, 1500);
      //checkbox
      $('.iCheckbox').iCheck({
        checkboxClass: 'icheckbox_square-red',
        radioClass: 'iradio_square-red',
        increaseArea: '20%' /* optional */
      });
      //datepicker
      $('.datepicker').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        clearBtn: true
      })
      //timepicker
      $('.timepicker').timepicker({
        showInputs: true,
        showMeridian: false,
        showSeconds: true
      })
      $.validator.addMethod("valueNotEquals", function(value, element, arg){
        return arg !== value;
      }, "Value must not equal arg.");
      jQuery.extend(jQuery.validator.messages, {
        required: "Wajib diisi!",
        remote: "Please fix this field.",
        email: "Please enter a valid email address.",
        url: "Please enter a valid URL.",
        date: "Please enter a valid date.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Harap masukkan angka yang benar!",
        digits: "Please enter only digits.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Masukan tidak sama!",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
        minlength: jQuery.validator.format("Please enter at least {0} characters."),
        rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
        min: jQuery.validator.format("Please enter a value greater than or equal to {0}."),
        valueNotEquals: "Harap pilih salah satu!"
      });
    })
  </script>
  <style>
    .error{
      color:#FF0000;
    },
    .valid{
      color:green;
    }
    @media only screen and (max-width: 600px) {
      .navbar-brand {
        display: none;
      }
    }
    th{
      vertical-align: middle !important;
    }
    .nav-tabs-custom > .nav-tabs > li.active {
      border-top-color : #dd4b39;
    }
  </style>
</head>
<body class="hold-transition skin-red sidebar-mini fixed">
<!-- Site wrapper -->
<div class="wrapper">
