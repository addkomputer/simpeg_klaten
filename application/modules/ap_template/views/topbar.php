<header class="main-header">
    <!-- Logo -->
    <a href="<?=base_url();?>app_dashboard" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">
        <img class="logo-img" src="<?=base_url()?>img/<?=$profil->logo?>" alt="" srcset="" width="25">
      </span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">
        <img class="logo-img" src="<?=base_url()?>img/<?=$profil->logo?>" alt="" srcset="" width="25">
        <?=$profil->singkatan?>
      </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="<?=base_url();?>#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <span class="navbar-brand"><?=$profil->nama_aplikasi?></span>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="<?=base_url();?>#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?=base_url();?>img/no_user.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?=$this->session->userdata('nama');?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?=base_url();?>img/no_user.png" class="img-circle" alt="User Image">

                <p>
                  <?=$this->session->userdata('nama');?> 
                  <!-- - Web Developer -->
                  <!-- <small>Member since Nov. 2012</small> -->
                </p>
              </li>
              <!-- Menu Body -->
              <!-- <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="<?=base_url();?>#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="<?=base_url();?>#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="<?=base_url();?>#">Friends</a>
                  </div>
                </div>
              </li> -->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?=base_url();?>ap_ganti_password/form" class="btn btn-default btn-flat"><i class="fa fa-user"></i> Ganti Kata Sandi</a>
                </div>
                <div class="pull-right">
                  <a href="<?=base_url();?>ap_otentikasi/keluar/action" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Keluar</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>