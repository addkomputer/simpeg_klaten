
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> <span class="text-orange"><?=$profil->versi?></span> <span>Page rendered in {elapsed_time} seconds.</span>
    </div>
    <strong>
      Copyright &copy; <?php if(date('Y') == $profil->tahun){echo date('Y');}else{echo $profil->tahun.' - '.date('Y');}?> 
      <a class="text-green" href="<?=base_url();?>"><?=$profil->nama_instansi?></a>.</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->
</body>
</html>