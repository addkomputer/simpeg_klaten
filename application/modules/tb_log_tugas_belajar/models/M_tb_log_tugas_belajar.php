<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_tb_log_tugas_belajar extends CI_Model {

  function __construct(){
    parent::__construct();
  }

  public function create_table_kehadiran($suffix)
  {
    $this->db->query(
      "CREATE TABLE IF NOT EXISTS `tb_log_kehadiran_$suffix` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `bulan` varchar(2) NOT NULL,
        `id_pegawai` varchar(50) DEFAULT '0',
        `nomor_induk` varchar(50) DEFAULT '0',
        `pin` varchar(50) DEFAULT '0',
        `id_instansi` int(11) DEFAULT '0',
        `id_tipe` int(1) DEFAULT '-1',
        `id_shift` int(11) DEFAULT '7',
        `is_tukar` tinyint(1) DEFAULT '7',
        `kode` varchar(50) DEFAULT 'P3',
        `tanggal` date DEFAULT NULL,
        `jam_datang` time DEFAULT NULL,
        `jam_batas_datang` time DEFAULT NULL,
        `is_datang` tinyint(1) NOT NULL DEFAULT '0',
        `jam_datang_pegawai` time DEFAULT NULL,
        `terlambat_datang` int(11) DEFAULT '0',
        `ket_datang` text,
        `is_pulang` tinyint(1) NOT NULL DEFAULT '0',
        `jam_pulang` time DEFAULT NULL,
        `jam_batas_pulang` time DEFAULT NULL,
        `jam_pulang_pegawai` time DEFAULT NULL,
        `mendahului_pulang` int(11) DEFAULT '0',
        `ket_pulang` text,
        `file_bukti` varchar(100) DEFAULT NULL,
        `keterangan` text,
        PRIMARY KEY (`id`)
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;"
    );
  }
  
  public function create_table($suffix)
  {
    $this->db->query(
      "CREATE TABLE IF NOT EXISTS `tb_log_tugas_belajar_$suffix` (
        `id` bigint(20) NOT NULL AUTO_INCREMENT,
        `bulan` varchar(2) NOT NULL,
        `id_pegawai` varchar(50) NOT NULL DEFAULT '0',
        `nomor_induk` varchar(50) DEFAULT '0',
        `pin` varchar(50) DEFAULT '0',
        `id_instansi` int(11) DEFAULT '0',
        `tanggal` date NOT NULL,
        `id_tipe` char(2) DEFAULT NULL,
        `file_bukti` varchar(100) DEFAULT NULL,
        `keterangan` text,
        `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `created_by` varchar(50) NOT NULL DEFAULT 'System',
        `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
        `updated_by` varchar(50) DEFAULT NULL,
        `is_active` tinyint(1) NOT NULL DEFAULT '1',
        `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
        PRIMARY KEY (`id`)
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1;"
    );
  }

	public function get_list($number,$offset,$search)
  {
    $tanggal = date_to_id($search['tanggal']);
    $suffix = substr($tanggal,0,4);
    if ($this->db->table_exists('tb_log_tugas_belajar_'.$suffix)){
      $where = "WHERE a.tanggal = '$tanggal' ";
      if ($search['id_instansi'] != '') {
        $where .= "AND b.id_instansi = '".$search['id_instansi']."' ";
      }
      if ($search['id_jabatan'] != '') {
        $where .= "AND b.id_jabatan = '".$search['id_jabatan']."' ";
      }
      if ($search['id_status_pegawai'] != '') {
        $where .= "AND b.id_status_pegawai = '".$search['id_status_pegawai']."' ";
      }
      if ($search['id_tipe'] != '') {
        $where .= "AND a.id_tipe = '".$search['id_tipe']."' ";
      }
      if ($search['term'] != '') {
        $where .= "AND b.nama LIKE '%".$search['term']."%' ";
      }
      $query = $this->db->query(
        "SELECT a.*,
          b.gelar_depan,b.nama,b.gelar_belakang,
          c.instansi,
          d.jabatan,
          e.tipe,
          f.status_pegawai
        FROM tb_log_tugas_belajar_$suffix a
        JOIN dt_pegawai b ON a.id_pegawai = b.id_pegawai
        LEFT JOIN ms_instansi c ON b.id_instansi = c.id_instansi
        LEFT JOIN ms_jabatan d ON b.id_jabatan = d.id_jabatan
        JOIN tb_log_tipe e ON a.id_tipe = e.id_tipe
        JOIN ms_status_pegawai f ON b.id_status_pegawai = f.id_status_pegawai
          $where
        LIMIT $offset,$number"
      );
      return $query->result();
    }else{
      return null;
    }
  }

  function num_rows($search){
    $tanggal = date_to_id($search['tanggal']);
    $suffix = substr($tanggal,0,4);
    if ($this->db->table_exists('tb_log_tugas_belajar_'.$suffix)){
      $where = "WHERE a.tanggal = '$tanggal' ";
      if ($search['id_instansi'] != '') {
        $where .= "AND b.id_instansi = '".$search['id_instansi']."' ";
      }
      if ($search['id_jabatan'] != '') {
        $where .= "AND b.id_jabatan = '".$search['id_jabatan']."' ";
      }
      if ($search['id_status_pegawai'] != '') {
        $where .= "AND b.id_status_pegawai = '".$search['id_status_pegawai']."' ";
      }
      if ($search['id_tipe'] != '') {
        $where .= "AND a.id_tipe = '".$search['id_tipe']."' ";
      }
      if ($search['term'] != '') {
        $where .= "AND b.nama LIKE '%".$search['term']."%' ";
      }
      $query = $this->db->query(
        "SELECT a.*,
          b.gelar_depan,b.nama,b.gelar_belakang,
          c.instansi,
          d.jabatan,
          e.tipe,
          f.status_pegawai
        FROM tb_log_tugas_belajar_$suffix a
        JOIN dt_pegawai b ON a.id_pegawai = b.id_pegawai
        LEFT JOIN ms_instansi c ON b.id_instansi = c.id_instansi
        LEFT JOIN ms_jabatan d ON b.id_jabatan = d.id_jabatan
        JOIN tb_log_tipe e ON a.id_tipe = e.id_tipe
        JOIN ms_status_pegawai f ON b.id_status_pegawai = f.id_status_pegawai
          $where"
      );
      return $query->num_rows();
    }else{
      return null;
    }
  }
  
  function num_rows_total($search){  
    $tanggal = date_to_id($search['tanggal']);
    $suffix = substr($tanggal,0,4);
    if ($this->db->table_exists('tb_log_tugas_belajar_'.$suffix)){
      $query = $this->db->query(
        "SELECT a.*,
          b.gelar_depan,b.nama,b.gelar_belakang,
          c.instansi,
          d.jabatan,
          e.tipe,
          f.status_pegawai
        FROM tb_log_tugas_belajar_$suffix a
        JOIN dt_pegawai b ON a.id_pegawai = b.id_pegawai
        LEFT JOIN ms_instansi c ON b.id_instansi = c.id_instansi
        LEFT JOIN ms_jabatan d ON b.id_jabatan = d.id_jabatan
        JOIN tb_log_tipe e ON a.id_tipe = e.id_tipe
        JOIN ms_status_pegawai f ON b.id_status_pegawai = f.id_status_pegawai"
      );
      return $query->num_rows();
    }else{
      return null;
    }
	}

	public function get_all()
	{
		return $this->db
			->where('is_active','1')
			->get('tb_log_tugas_belajar_'.$this->suffix)->result();
  }

  public function get_by_id($id,$tanggal)
  {
    $suffix = substr($tanggal,0,4);
    return $this->db->where('id',$id)->get('tb_log_tugas_belajar_'.$suffix)->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id','asc')->get('tb_log_tugas_belajar_'.$this->suffix)->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id','desc')->get('tb_log_tugas_belajar_'.$this->suffix)->row();
  }

  public function insert($data)
  {
    $suffix = substr($data['tanggal'],0,4);
    $bulan = substr($data['tanggal'],5,2);
    $this->create_table_kehadiran($suffix);
    $this->create_table($suffix);
    //get pegawai
    $pegawai = $this->db->where('id_pegawai', $data['id_pegawai'])->get('dt_pegawai')->row_array();
    if($pegawai != null){
      $d = array(
        'bulan' => $bulan,
        'id_pegawai' => $pegawai['id_pegawai'],
        'nomor_induk' => $pegawai['nomor_induk'],
        'pin' => $pegawai['pin'],
        'id_instansi' => $pegawai['id_instansi'],
        'tanggal' => $data['tanggal'],
        'id_tipe' => $data['id_tipe'],
        'file_bukti' => $data['file_bukti'],
        'keterangan' => $data['keterangan']
      );
      $cek = $this->db
        ->where('id_pegawai',$data['id_pegawai'])
        ->where('tanggal',$data['tanggal'])
        ->get('tb_log_tugas_belajar_'.$suffix)
        ->row();
      if($cek == null){
        $this->db->insert('tb_log_tugas_belajar_'.$suffix, $d);
      }else{
        $this->db
          ->where('id_pegawai',$data['id_pegawai'])
          ->where('tanggal',$data['tanggal'])
          ->update('tb_log_tugas_belajar_'.$suffix, $d);
      }

      $log = array(
        'bulan' => $bulan,
        'id_pegawai' => $pegawai['id_pegawai'],
        'nomor_induk' => $pegawai['nomor_induk'],
        'pin' => $pegawai['pin'],
        'tanggal' => $data['tanggal'],
        'id_tipe' => $data['id_tipe'],
        'file_bukti' => $data['file_bukti'],
        'keterangan' => $data['keterangan']
      );
      $cek_log = $this->db
        ->where('id_pegawai',$data['id_pegawai'])
        ->where('tanggal',$data['tanggal'])
        ->get('tb_log_kehadiran_'.$suffix)
        ->row();
       if ($cek_log == NULL) {
        //insert
        $this->db->insert('tb_log_kehadiran_'.$suffix,$log);
      }else{
        //update
        $this->db
          ->where('id_pegawai', $data['id_pegawai'])
          ->where('tanggal', $data['tanggal'])
          ->update('tb_log_kehadiran_'.$suffix,$log);
      }
    }
  }

  public function update($id,$data)
  {
    $suffix = substr($data['tanggal'],0,4);
    $this->db->where('id',$id)->update('tb_log_tugas_belajar_'.$suffix,$data);
    $log = array(
      'id_tipe' => $data['id_tipe'],
      'file_bukti' => $data['file_bukti'],
      'keterangan' => $data['keterangan']
    );
    $tugas_belajar = $this->db->where('id',$id)->get('tb_log_tugas_belajar_'.$suffix)->row_array();
    $this->db
      ->where('id_pegawai',$tugas_belajar['id_pegawai'])
      ->where('tanggal',$tugas_belajar['tanggal'])
      ->update('tb_log_kehadiran_'.$suffix,$log);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id',$id)->update('tb_log_tugas_belajar_'.$this->suffix,array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id,$tanggal)
  {
    $suffix = substr($tanggal,0,4);
    $data = $this->db->where('id',$id)->get('tb_log_tugas_belajar_'.$suffix)->row();
    $this->db
      ->where('id_pegawai',$data->id_pegawai)
      ->where('tanggal',$data->tanggal)
      ->update('tb_log_kehadiran_'.$suffix,array('keterangan' => '','id_tipe' => -1));
    $this->db->where('id',$id)->delete('tb_log_tugas_belajar_'.$suffix);
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE tb_log_tugas_belajar_2018_10");
  }

}
