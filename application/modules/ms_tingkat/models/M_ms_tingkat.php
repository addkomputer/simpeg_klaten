<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ms_tingkat extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_tingkat')
      ->get('ms_tingkat',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_tingkat')
      ->get('ms_tingkat')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('ms_tingkat')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id_tingkat',$id)->get('ms_tingkat')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_tingkat','asc')->get('ms_tingkat')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_tingkat','desc')->get('ms_tingkat')->row();
  }

  public function insert($data)
  {
    $this->db->insert('ms_tingkat',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_tingkat',$id)->update('ms_tingkat',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id_tingkat',$id)->update('ms_tingkat',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_tingkat',$id)->delete('ms_tingkat');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ms_tingkat");
  }

}
