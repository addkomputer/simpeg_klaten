<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_rk_log_kehadiran extends CI_Model {

  private $tanggal, $suffix;

  function __construct(){
		parent::__construct();
    
    if ($this->session->userdata('search')) {
      $this->tanggal = $this->session->userdata('search')['tanggal'];
      $this->suffix = substr($this->tanggal,6,4);
    }else{
      $this->tanggal = date('Y-m-d');
      $this->suffix = date('Y');
    }
  }
  
	public function get_list($number,$offset,$search = null)
  {
    $suffix = substr(date_to_id($search['tanggal']),0,4);
    if ($this->db->table_exists('tb_log_kehadiran_'.$suffix))
    {
      $where = "WHERE ";
      if ($search != null) {
        $where .= "a.tanggal = '".date_to_id($search['tanggal'])."'";
        if ($search['id_jabatan'] != '') {
          $where .= "AND c.id_jabatan = '".$search['id_jabatan']."'";
        }
        if ($search['id_instansi'] != '') {
          $where .= "AND c.id_instansi = '".$search['id_instansi']."'";
        }
      }else{
        $where .= "a.tanggal = '".$this->tanggal."'";
      }
      
      $query = $this->db->query(
        "SELECT 
          a.*,
          b.tipe,
          c.gelar_depan,c.nama,c.gelar_belakang,
          d.status_pegawai,
          e.jabatan,
          f.instansi
        FROM tb_log_kehadiran_$this->suffix a 
          JOIN tb_log_tipe b ON a.id_tipe=b.id_tipe 
          JOIN dt_pegawai c ON a.pin = c.pin
          JOIN ms_status_pegawai d ON c.id_status_pegawai = d.id_status_pegawai
          LEFT JOIN ms_jabatan e ON c.id_jabatan = e.id_jabatan
          LEFT JOIN ms_instansi f ON c.id_instansi = f.id_instansi
        $where
        ORDER BY
          c.id_golongan DESC
        LIMIT $offset,$number"
      );

      return $query->result();
    }else{
      return null;
    }
  }

  function num_rows($search = null){
    $suffix = substr(date_to_id($search['tanggal']),0,4);
    if ($this->db->table_exists('tb_log_kehadiran_'.$suffix))
    {
      $where = "WHERE ";
      if ($search != null) {
        $where .= "a.tanggal = '".date_to_id($search['tanggal'])."'";
        if ($search['id_jabatan'] != '') {
          $where .= "AND c.id_jabatan = '".$search['id_jabatan']."'";
        }
        if ($search['id_instansi'] != '') {
          $where .= "AND c.id_instansi = '".$search['id_instansi']."'";
        }
      }else{
        $where .= "a.tanggal = '".$this->tanggal."'";
      }
      
      $query = $this->db->query(
        "SELECT 
          a.*,
          b.tipe,
          c.gelar_depan,c.nama,c.gelar_belakang,
          d.status_pegawai,
          e.jabatan,
          f.instansi
        FROM tb_log_kehadiran_$this->suffix a 
          JOIN tb_log_tipe b ON a.id_tipe=b.id_tipe 
          JOIN dt_pegawai c ON a.pin = c.pin
          JOIN ms_status_pegawai d ON c.id_status_pegawai = d.id_status_pegawai
          LEFT JOIN ms_jabatan e ON c.id_jabatan = e.id_jabatan
          LEFT JOIN ms_instansi f ON c.id_instansi = f.id_instansi
        $where
        ORDER BY
          c.id_golongan DESC"
      );

      return $query->num_rows();
    }else{
      return 0;
    }
  }
  
  function num_rows_total(){
    if ($this->db->table_exists('tb_log_kehadiran_'.$this->suffix))
    {
      return $this->db
      ->order_by('id')
      ->get('tb_log_kehadiran_'.$this->suffix)
      ->num_rows();
    }else{
      return 0;
    }  
    
  }

  public function get_print($search = null)
  {
    $suffix = substr(date_to_id($search['tanggal']),0,4);
    if ($this->db->table_exists('tb_log_kehadiran_'.$suffix)){

      $where = "WHERE ";
      if ($search != null) {
        $where .= "a.tanggal = '".date_to_id($search['tanggal'])."'";
        if ($search['id_jabatan'] != '') {
          $where .= "AND c.id_jabatan = '".$search['id_jabatan']."'";
        }
        if ($search['id_instansi'] != '') {
          $where .= "AND c.id_instansi = '".$search['id_instansi']."'";
        }
      }else{
        $where .= "a.tanggal = '".$this->tanggal."'";
      }
      
      $query = $this->db->query(
        "SELECT 
          a.*,
          b.tipe,
          c.gelar_depan,c.nama,c.gelar_belakang,
          d.status_pegawai,
          e.jabatan,
          f.instansi
        FROM tb_log_kehadiran_$this->suffix a 
          JOIN tb_log_tipe b ON a.id_tipe=b.id_tipe 
          JOIN dt_pegawai c ON a.pin = c.pin
          JOIN ms_status_pegawai d ON c.id_status_pegawai = d.id_status_pegawai
          LEFT JOIN ms_jabatan e ON c.id_jabatan = e.id_jabatan
          LEFT JOIN ms_instansi f ON c.id_instansi = f.id_instansi
        $where
        ORDER BY
          c.id_golongan DESC"
      );
  
      return $query->result();
    }else{
      return null;
    }
  }
  
  public function count_category($search)
  {
    $where = "WHERE ";
    if ($search != null) {
      $where .= "b.tanggal = '".date_to_id($search['tanggal'])."'";
      if ($search['id_jabatan'] != '') {
        $where .= "AND c.id_jabatan = '".$search['id_jabatan']."'";
      }
    }else{
      $where .= "a.tanggal = '".$this->tanggal."'";
    }
    
    $query = $this->db->query(
      "SELECT a.id_tipe,a.tipe, count(d.id_tipe) AS total
      FROM tb_log_tipe a 
      LEFT JOIN (
          SELECT b.id_tipe FROM tb_log_kehadiran_$this->suffix b
          LEFT JOIN dt_pegawai c ON b.nomor_induk = c.nomor_induk 
          $where
        ) d on d.id_tipe = a.id_tipe
      GROUP BY 
        a.id_tipe"
    );

    return $query->result();
  }

  public function count_print($search)
  {
    $suffix = substr(date_to_id($search['tanggal']),0,4);
    if ($this->db->table_exists('tb_log_kehadiran_'.$suffix)){
      $tanggal = date_to_id($this->tanggal);
      $where = "WHERE ";
      if ($search != null) {
        $where .= "b.tanggal = '".date_to_id($search['tanggal'])."'";
        if ($search['id_jabatan'] != '') {
          $where .= "AND c.id_jabatan = '".$search['id_jabatan']."'";
        }
        if ($search['id_instansi'] != '') {
          $where .= "AND c.id_instansi = '".$search['id_instansi']."'";
        }
      }else{
        $where .= "a.tanggal = '".$this->tanggal."'";
      }
  
      $query = $this->db->query(
        "SELECT tipe, COUNT(d.id) as total FROM tb_log_tipe a
          LEFT JOIN (
            SELECT b.id, b.id_tipe FROM tb_log_kehadiran_$suffix b 
            JOIN dt_pegawai c ON b.id_pegawai = c.id_pegawai
            $where
          ) AS d ON d.id_tipe = a.id_tipe
          GROUP BY a.id_tipe
        "
      );
  
      return $query->result();
    }else{
      return null;
    }
  }

	public function get_all()
	{
		return $this->db
			->where('is_active','1')
			->get("tb_log_kehadiran_$this->suffix")->result();
  }
  
  public function detail($tanggal,$nomor_induk)
  {
    $data = $this->db->where('nomor_induk',$nomor_induk)->get('dt_pegawai')->row();
    $data->detail = $this->db
      ->join('tb_device b','a.SN = b.device_sn')
      ->like('a.ScanDate',$tanggal)
      ->where('a.nomor_induk',$nomor_induk)
      ->get('tb_scanlog_'.$this->suffix.' a')->result();
    return $data;
  }

  public function get_by_id($id)
  {
    return $this->db->where('id',$id)->get("tb_log_kehadiran_$this->suffix")->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id','asc')->get("tb_log_kehadiran_$this->suffix")->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id','desc')->get("tb_log_kehadiran_$this->suffix")->row();
  }

  public function insert($data)
  {
    $this->db->insert("tb_log_kehadiran_$this->suffix",$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id',$id)->update("tb_log_kehadiran_$this->suffix",$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id',$id)->update("tb_log_kehadiran_$this->suffix",array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id',$id)->delete("tb_log_kehadiran_$this->suffix");
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE tb_log_kehadiran_$this->suffix");
  }

}
