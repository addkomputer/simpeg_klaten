<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-<?=$access->icon?> text-red"></i> <?=$access->menu?>
      <small><?=$subtitle?></small>
    </h1>      
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box box-danger">
      <div class="box-header with-border">
        <i class="fa fa-table text-maroon"></i>
        <h3 class="box-title">Tabel <?=$access->menu?></h3>
        <div class="box-tools pull-right">
          <span class="badge bg-maroon"><i class="fa fa-bar-chart"></i> Jumlah Keseluruhan Data : <?=$num_rows_total?></span>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-md-2">
            <a class="btn btn-success btn-flat" href="<?=base_url().$access->controller?>/cetak_pdf" target="_blank"><i class="fa fa-file-pdf-o"></i> Cetak PDF</a>
          </div>
          <form id="form_search" action="<?=base_url().$access->controller?>/search" method="post" autocomplete="off">
            <div class="col-md-2">
              <div class="input-group">
                <input type="text" class="form-control datepicker" id="tanggal" name="tanggal" placeholder="dd-mm-yyyy" value="<?php if($search != null){echo $search['tanggal'];}else{echo date('d-m-Y');}?>">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
              </div>
            </div>
            <div class="col-md-4">
              <select name="id_instansi" class="form-control select2 input-sm" onchange="$('#form-index').submit()">
                <?php if($this->session->userdata('id_grup') == 3): ?>
                  <?php foreach($instansi as $row): ?>
                    <?php if($row->id_instansi == $this->session->userdata('id_instansi')): ?>
                      <option value="<?=$row->id_instansi?>" <?php if($search){if($row->id_instansi == $search['id_instansi']){echo 'selected';}}?>><?=$row->instansi?></option>
                    <?php endif;?>
                  <?php endforeach;?>
                <?php else: ?>
                  <option value="">-- Semua Instansi --</option>
                  <?php foreach($instansi as $row): ?>
                    <option value="<?=$row->id_instansi?>" <?php if($search){if($row->id_instansi == $search['id_instansi']){echo 'selected';}}?>><?=$row->instansi?></option>
                  <?php endforeach;?>
                <?php endif;?>
              </select>
            </div>
            <div class="col-md-4">
              <div class="input-group pull-right">
                <select name="id_jabatan" class="form-control select2 input-sm" onchange="$('#form-index').submit()">
                  <option value="">-- Semua Jabatan --</option>
                  <?php foreach($jabatan as $row): ?>
                    <option value="<?=$row->id_jabatan?>" <?php if($search){if($search['id_jabatan'] == $row->id_jabatan){echo 'selected';}} ?>><?=$row->jabatan?></option>
                  <?php endforeach;?>
                </select>
                <div class="input-group-btn">
                  <!-- <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button> -->
                  <a class="btn btn-default" href="<?=base_url().$this->access->controller?>/reset_search"><i class="fa fa-refresh"></i></a>
                </div>
              </div>
            </div>
          </form>
        </div>
        <br>
        <?php echo $this->session->flashdata('status'); ?>
        <table class="table table-striped table-bordered table-condensed">
          <thead>
            <tr>
              <th class="text-center" width="30">No.</th>
              <th class="text-center" width="20">Status Pegawai</th>
              <th class="text-center">Nama Pegawai</th>
              <th class="text-center">Instansi</th>
              <th class="text-center">Jabatan</th>
              <th class="text-center" width="80">Tanggal</th>
              <th class="text-center" width="80">Tipe</th>
              <th class="text-center" width="150">Keterangan</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($main != null): ?>
              <?php $i=1;foreach ($main as $row): ?>
                <tr>
                  <td class="text-center"><?=$this->uri->segment('3')+$i++?></td>
                  <td class="text-center"><?=$row->status_pegawai?></td>
                  <td>
                    <b>
                    <?php 
                      echo $row->gelar_depan.' '.$row->nama;
                      if ($row->gelar_belakang != ''){
                        echo ', '.$row->gelar_belakang;
                      }
                    ?>
                    </b>
                    <br>
                    <?=$row->nomor_induk?>
                  </td>
                  <td><?=$row->instansi?></td>
                  <td><?=$row->jabatan?></td>
                  <td class="text-center"><?=date_to_id($row->tanggal)?></td>
                  <?php 
                    $col_tipe = '';
                    switch (substr($row->id_tipe,0,1)) {
                      case '0':
                        $col_tipe = 'text-red';
                        break;
                      
                      case '1':
                        $col_tipe = 'text-green';
                        break;
                      
                      case '2':
                        $col_tipe = 'text-orange';
                        break;

                      case '3':
                        $col_tipe = 'text-teal';
                        break;
                      
                      case '4':
                        $col_tipe = 'text-maroon';
                        break;

                      case '5':
                        $col_tipe = 'text-purple';
                        break;

                      case '6':
                        $col_tipe = 'text-navy';
                        break;
                    }
                  ?>
                  <td class="text-center <?=$col_tipe?>">
                    <?php if($row->id_tipe != '1'){
                      echo $row->tipe;
                    }?>
                  </td>
                  <td><?=$row->keterangan?></td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td class="text-center" colspan="99">Tidak ada data!</td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <span class="badge bg-blue">Jumlah Data : <?=$num_rows?></span>
        <?php echo $this->pagination->create_links(); ?>
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
  $(document).ready(function () {
    $('#tanggal').datepicker()
    .on('changeDate', function(e) {
      // $('#form_search').submit();
      window.location.replace("<?=base_url().$this->access->controller.'/search_tanggal/'?>"+e.format());
    });
    $('.select2').on('select2:select', function (e) {
      $('#form_search').submit();
    });
  })
  function del(id) {
    $("#modal_delete").modal('show');
    $("#id").val(id);
  }
</script>