<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dt_file_riwayat_jabatan extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_pegawai')
      ->get('dt_file_riwayat_jabatan',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_pegawai')
      ->get('dt_file_riwayat_jabatan')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('dt_file_riwayat_jabatan')->result();
  }
  
  public function get_by_id_pegawai($id)
  {
    return $this->db
      ->join('ms_golongan b','a.id_gol = b.id_golongan')
      ->where('a.id_pegawai',$id)
      ->order_by('a.id','desc')
      ->get('dt_file_riwayat_jabatan a')
      ->result();
  }

  public function get_by_id($id)
  {
    return $this->db->where('id_pegawai',$id)->get('dt_file_riwayat_jabatan')->row();
  }

  public function get_by_id_riwayat($id)
  {
    return $this->db->where('id_riwayat',$id)->get('dt_file_riwayat_jabatan')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_pegawai','asc')->get('dt_file_riwayat_jabatan')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_pegawai','desc')->get('dt_file_riwayat_jabatan')->row();
  }

  public function insert($data)
  {
    $this->db->insert('dt_file_riwayat_jabatan',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id',$id)->update('dt_file_riwayat_jabatan',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id',$id)->update('dt_file_riwayat_jabatan',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $query = $this->db->where('id',$id)->get('dt_file_riwayat_jabatan')->row();
    $file = $query->file;
    unlink(base_url().'berkas/riwayat_jabatan/'.$file);
    $this->db->where('id',$id)->delete('dt_file_riwayat_jabatan');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE dt_file_riwayat_jabatan");
  }

}
