      </tbody>
    </table>
    <br><br>
    <table id="count_table">
      <tbody>
        <tr>
          <td class="text-left" width="100">Islam</td>
          <td>:</td>
          <td class="text-right" width="40"><?=$total->islam?></td>
        </tr>
        <tr>
          <td class="text-left">Protestan</td>
          <td>:</td>
          <td class="text-right"><?=$total->protestan?></td>
        </tr>
        <tr>
          <td class="text-left">Katholik</td>
          <td>:</td>
          <td class="text-right"><?=$total->katholik?></td>
        </tr>
        <tr>
          <td class="text-left">Hindu</td>
          <td>:</td>
          <td class="text-right"><?=$total->hindu?></td>
        </tr>
        <tr>
          <td class="text-left">Budha</td>
          <td>:</td>
          <td class="text-right"><?=$total->budha?></td>
        </tr>
        <tr>
          <td class="text-left">Kosong</td>
          <td>:</td>
          <td class="text-right"><?=$total->kosong?></td>
        </tr>
        <tr>
          <th class="text-left">Total</th>
          <th>:</th>
          <th class="text-right"><?=$total->islam+$total->protestan+$total->katholik+$total->hindu+$total->budha+$total->kosong?></th>
        </tr>
      </tbody>
    </table>
    <br><br>
    <?php 
      $nama_direktur = '';
      if ($direktur->gelar_depan != '') {
        $nama_direktur .= $direktur->gelar_depan.' ';
      }
      $nama_direktur .= $direktur->nama;
      if ($direktur->gelar_belakang != '') {
        $nama_direktur .= ', '.$direktur->gelar_belakang;
      }
    ?>
    <table id="sign_table">
      <tbody>
        <tr>
          <td class="text-center">
            Mengetahui, <br>
            Direktur
          </td>
        </tr>
        <tr>
          <td><br><br><br><br></td>
        </tr>
        <tr>
          <td class="text-center">
            <u><?=$nama_direktur?></u><br>
            NIP. <?=$direktur->nomor_induk?>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>