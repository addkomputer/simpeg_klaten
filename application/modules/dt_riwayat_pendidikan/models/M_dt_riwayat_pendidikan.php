<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dt_riwayat_pendidikan extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_pegawai')
      ->get('dt_riwayat_pendidikan',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_pegawai')
      ->get('dt_riwayat_pendidikan')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('dt_riwayat_pendidikan')->result();
  }
  
  public function get_by_id_pegawai($id)
  {
    $query = $this->db
      ->where('a.id_pegawai',$id)
      ->order_by('a.id','desc')
      ->get('dt_riwayat_pendidikan a');

    $return = array();

    foreach ($query->result() as $row)
    {
      $return[$row->id] = $row;
      $return[$row->id]->file = $this->get_file($row->id); // Get the categories sub categories
    }

    return $return;
  }

  public function get_file($id_riwayat)
  {
    $this->db->where('id_riwayat', $id_riwayat);
    $query = $this->db->get('dt_file_riwayat_pendidikan');
    return $query->result();
  }

  public function get_by_id($id)
  {
    $query = $this->db->where('id',$id)->get('dt_riwayat_pendidikan')->row();
    $query->file = $this->db->where('id_riwayat',$query->id)->get('dt_file_riwayat_pendidikan')->result();
    return $query;
  }

  public function get_first()
  {
    return $this->db->order_by('id_pegawai','asc')->get('dt_riwayat_pendidikan')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_pegawai','desc')->get('dt_riwayat_pendidikan')->row();
  }

  public function insert($data)
  {
    $this->db->insert('dt_riwayat_pendidikan',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id',$id)->update('dt_riwayat_pendidikan',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id',$id)->update('dt_riwayat_pendidikan',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id',$id)->delete('dt_riwayat_pendidikan');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE dt_riwayat_pendidikan");
  }

}
