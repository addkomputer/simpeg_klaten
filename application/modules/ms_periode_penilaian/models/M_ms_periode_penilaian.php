<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ms_periode_penilaian extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_periode_penilaian')
      ->get('ms_periode_penilaian',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_periode_penilaian')
      ->get('ms_periode_penilaian')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('ms_periode_penilaian')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id_periode_penilaian',$id)->get('ms_periode_penilaian')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_periode_penilaian','asc')->get('ms_periode_penilaian')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_periode_penilaian','desc')->get('ms_periode_penilaian')->row();
  }

  public function insert($data)
  {
    $this->db->insert('ms_periode_penilaian',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_periode_penilaian',$id)->update('ms_periode_penilaian',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id_periode_penilaian',$id)->update('ms_periode_penilaian',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_periode_penilaian',$id)->delete('ms_periode_penilaian');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ms_periode_penilaian");
  }

}
