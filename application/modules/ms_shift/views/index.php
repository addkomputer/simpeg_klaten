<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
      <small><?=$subtitle?></small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">Administrasi Kehadiran</li>
      <li class="active text-orange"><?=$access->menu?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <i class="fa fa-table text-maroon"></i>
        <h3 class="box-title">Tabel <?=$access->menu?></h3>
        <div class="box-tools pull-right">
          <span class="badge bg-maroon"><i class="fa fa-bar-chart"></i> Jumlah Keseluruhan Data : <?=$num_rows_total?></span>
        </div>
      </div>
      <div class="box-body">
        <div class="alert alert-danger" role="alert">
          <strong>Peringatan!</strong> Hati-hati dalam merubah atau menghapus shift, karena akan mempengaruhi data kehadiran pegawai!
        </div>
        <div class="row">
          <div class="col-md-2">
            <a class="btn btn-flat btn-primary pull-left" href="<?=base_url()?>ms_shift/form"><i class="fa fa-plus"></i> Tambah</a>
          </div>
          <div class="col-md-4 col-md-offset-6">
            <form action="<?=base_url()?>ms_shift/index" method="post" autocomplete="off">
            <div class="input-group" >
              <input type="text" name="shift" class="form-control pull-right" placeholder="Pencarian" value="<?php if($search != null){echo $search['shift'];}?>">
              <div class="input-group-btn">
                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                <a class="btn btn-default btn-flat" href="<?=base_url()?>ms_shift/reset_search"><i class="fa fa-refresh"></i></a>
              </div>
            </div>
          </form>
          </div>
        </div>
        <br>
        <?php echo $this->session->flashdata('status'); ?>
        <table class="table table-striped table-bordered table-condensed">
          <thead>
            <tr>
              <th class="text-center" width="50">No</th>
              <th class="text-center" width="60">Aksi</th>
              <th class="text-center" width="50">Kode</th>
              <th class="text-center">Nama</th>
              <th class="text-center" width="150">Jam Datang</th>
              <th class="text-center" width="150">Jam Batas Datang</th>
              <th class="text-center" width="150">Jam Pulang</th>
              <th class="text-center" width="150">Jam Batas Pulang</th>
              <th class="text-center" width="50">Aktif</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($shift != null): ?>
              <?php $i=1;foreach ($shift as $row): ?>
                <tr>
                  <td class="text-center"><?= $this->uri->segment(3)+($i++);?></td>
                  <td class="text-center">
                    <a class="btn btn-xs btn-flat btn-warning" href="<?=base_url()?>ms_shift/form/<?=$row->id_shift?>" style="padding-right:0.250em;"><i class="fa fa-pencil"></i></a> 
                    <a class="btn btn-xs btn-flat btn-danger" href="#" onclick="del('<?=$row->id_shift?>')"><i class="fa fa-trash"></i></a>
                  </td>
                  <td class="text-center"><?=$row->kode?></td>
                  <td><?=$row->shift?></td>
                  <td class="text-center text-green"><?=$row->jam_datang?></td>
                  <td class="text-center text-green"><?=$row->jam_batas_datang?></td>
                  <td class="text-center text-red"><?=$row->jam_pulang?></td>
                  <td class="text-center text-red"><?=$row->jam_batas_pulang?></td>
                  <td class="text-center">
                    <?php if ($row->is_active == 1): ?>
                      <i class="fa fa-check text-green"></i>
                    <?php else: ?>
                      <i class="fa fa-close text-red"></i>
                    <?php endif; ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td class="text-center" colspan="99">Tidak ada data!</td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <span class="badge bg-purple" style="margin-top:6px;">Jumlah Data : <?=$num_rows?></span>
        <?php echo $this->pagination->create_links(); ?>
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- Modal Delete -->
<div id="modal_delete" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>ms_shift/delete" method="post" autocomplete="off">
        <input type="hidden" name="id_shift" id="delete_id_shift">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-flat btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
  function del(id) {
    $("#modal_delete").modal('show');
    $("#delete_id_shift").val(id);
  }
</script>