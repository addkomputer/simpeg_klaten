<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">Administrasi Kehadiran</li>
        <li class="active text-orange"><?=$access->menu?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-primary">
        <div class="box-header with-border">
          <i class="fa fa-table text-maroon"></i>
          <h3 class="box-title">Form <?=$access->menu?></h3>
        </div>
        <form id="form" class="form-horizontal" action="<?=base_url()?>ms_shift/<?=$action?>" method="post" autocomplete="off">
          <div class="box-body">
            <input type="hidden" class="form-control" name="id_shift" id="id_shift" value="<?php if($shift != null){echo $shift->id_shift;};?>" readonly required>
            <div class="form-group">
              <label class="col-sm-2 control-label">Kode</label>
              <div class="col-sm-1">
                <input type="text" class="form-control" name="kode" id="kode" value="<?php if($shift != null){echo $shift->kode;};?>" required <?php if($shift){if($shift->kode == 'REG'){echo 'readonly';}} ?>>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Nama</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="shift" id="shift" value="<?php if($shift != null){echo $shift->shift;};?>" required <?php if($shift){if($shift->kode == 'REG'){echo 'readonly';}} ?>>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Jam Datang</label>
              <div class="col-sm-2">
                <input type="text" class="form-control timepicker" name="jam_datang" id="jam_datang" value="<?php if($shift != null){echo $shift->jam_datang;};?>" required>
              </div>
              <label class="col-sm-2 control-label" style="width:max-content !important;">Jam Batas Datang</label>
              <div class="col-sm-2">
                <input type="text" class="form-control timepicker" name="jam_batas_datang" id="jam_batas_datang" value="<?php if($shift != null){echo $shift->jam_batas_datang;};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Jam Pulang</label>
              <div class="col-sm-2">
                <input type="text" class="form-control timepicker" name="jam_pulang" id="jam_pulang" value="<?php if($shift != null){echo $shift->jam_pulang;};?>" required>
              </div>
              <label class="col-sm-2 control-label" style="width:max-content !important;">Jam Batas Pulang</label>
              <div class="col-sm-2">
                <input type="text" class="form-control timepicker" name="jam_batas_pulang" id="jam_batas_pulang" value="<?php if($shift != null){echo $shift->jam_batas_pulang;};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Aktif</label>
              <div class="col-sm-5">
                <input class="iCheckbox" type="checkbox" name="is_active" value="1" <?php if($shift != null){if($shift->is_active == 1){echo 'checked';}}else{echo 'checked';}?>>
              </div>
            </div>
            <hr>
            <div class="form-group">
              <div class="col-md-offset-2 col-sm-8">
                <button class="btn btn-flat btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button>
                <a class="btn btn-flat btn-default" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Batal</a>
              </div>
            </div>
          </div>
        </form>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<script>
  $(document).ready(function () {
    // form validator
    $('#form').validate();
  })
</script>