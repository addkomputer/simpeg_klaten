<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ms_shift extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_shift')
      ->get('ms_shift',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('shift')
      ->get('ms_shift')
      ->num_rows();
  }
  
  function num_rows_total(){
    return $this->db
      ->where('is_deleted','0')
      ->order_by('shift')
      ->get('ms_shift')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('ms_shift')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id_shift',$id)->get('ms_shift')->row();
  }

  public function get_by_kode($kode)
  {
    return $this->db->where('kode',$kode)->get('ms_shift')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_shift','asc')->get('ms_shift')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_shift','desc')->get('ms_shift')->row();
  }

  public function insert($data)
  {
    $this->db->insert('ms_shift',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_shift',$id)->update('ms_shift',$data);
  }

  public function delete_temp($id)
  {
    $this->db->where('id_shift',$id)->update('ms_shift',array('is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_shift',$id)->delete('ms_shift');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ms_shift");
  }

}
