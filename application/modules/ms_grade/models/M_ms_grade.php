<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ms_grade extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_grade')
      ->get('ms_grade',$number,$offset)
      ->result_array();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_grade')
      ->get('ms_grade')
      ->num_rows();
  }
  
  function num_rows_total(){  
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_grade')
      ->get('ms_grade')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('ms_grade')->result();
	}

  public function get_all_kelompok()
  {
    return $this->db      
      ->where('is_deleted','0')
      ->where('is_active','1')
      ->group_by('kelompok')
      ->get('ms_grade')->result_array();
  }

  public function get_all_jenjang_pend($kelompok=null)
  {
    if($kelompok != '') $this->db->where('kelompok', $kelompok);
      $result = $this->db      
      ->where('is_deleted','0')
      ->where('is_active','1')
      ->group_by('jenjang_pend')
      ->get('ms_grade')->result_array();
      return $result;
  }

  public function get_by_id($id)
  {
    return $this->db->where('id_grade',$id)->get('ms_grade')->row_array();
  }

  public function get_first()
  {
    return $this->db->order_by('id_grade','asc')->get('ms_grade')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_grade','desc')->get('ms_grade')->row();
  }

  public function insert($data)
  {
    $this->db->insert('ms_grade',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_grade',$id)->update('ms_grade',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id_grade',$id)->update('ms_grade',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_grade',$id)->delete('ms_grade');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ms_grade");
  }

}
