<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Rekap Data Pegawai Berdasarkan Kedudukan</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
  <table id="kop">
    <tbody>
      <tr>
        <td class="text-center logo"><img src="<?=base_url()?>img/kebumen.png" height="80"></td>
        <td class="text-center">
          <span id="kop_1">PEMERINTAH KABUPATEN KEBUMEN </span><br>
          <span id="kop_2">RUMAH SAKIT UMUM DAERAH KEBUMEN dr. SOEDIRMAN</span><br>
          <span id="kop_3">
            Jalan Lingkar Selatan, Muktisari, Kebumen, Kabupaten Kebumen, Jawa Tengah 54351 <br>
            Telp/Fax : 0287-3873318, 0287-381101, 0287-385274; WA : 0877-3427-026; Email : rsud@kebumenkab.go.id
          </span>
        </td>
        <td class="text-center logo"><img src="<?=base_url()?>img/logo.png" height="80"></td>
      </tr>
    </tbody>
  </table>
  <div class="garis"></div>
  <h3 class="text-center"><?=$judul?></h3>
  <table id="filter_table">
    <tbody>
      <tr>
        <td width="120">Kedudukan </td>
        <td>:</td>
        <td>
          <?php if($search['id_kedudukan_pns'] != null): ?>
            <?php 
              switch ($search['id_kedudukan_pns']) {
                case '1':
                  echo 'Pegawai Aktif';
                  break;
                
                case '2':
                  echo 'Pensiun (Batas Usia Pensiun)';
                  break;

                case '3':
                  echo 'Diberhentikan karena Hukuman Disiplin';
                  break;
                
                case '4':
                  echo 'Meninggal Dunia/Janda/Duda';
                  break;
                
                case '5':
                  echo 'Berhenti Atas Permintaan Sendiri (APS';
                  break;
                
                case '6':
                  echo 'Mutasi keluar';
                  break;

                case '7':
                  echo 'engundurkan Diri dari PNS';
                  break;

                case '8':
                  echo 'Mengundurkan Diri dari CPNS';
                  break;

                case '9':
                  echo 'Cuti diluar Tanggungan Negara';
                  break;

                case '10':
                  echo 'Tidak Diketahui';
                  break;
              }
            ?>
          <?php else: ?>
            Semua
          <?php endif;?>  
        </td>
      </tr>
      <tr>
        <td>Ruang</td>
        <td>:</td>
        <td>
          <?php if($search['ruang'] != null): ?>
            <?=$search['ruang']->ruang?>
          <?php else: ?>
            Semua
          <?php endif;?>
        </td>
      </tr>
      <tr>
        <td>Jabatan</td>
        <td>:</td>
        <td>
          <?php if($search['jabatan'] != null): ?>
            <?=$search['jabatan']->jabatan?>
          <?php else: ?>
            Semua
          <?php endif;?>
        </td>
      </tr>
      <tr>
        <td>Status Pegawai</td>
        <td>:</td>
        <td>
          <?php if($search['status_pegawai'] != null): ?>
            <?=$search['status_pegawai']->status_pegawai?>
          <?php else: ?>
            Semua
          <?php endif;?>
        </td>
      </tr>
    </tbody>
  </table>
  <br>
  <table id="body_table">
    <thead>
      <tr>
        <th class="text-center" width="30">No.</th>
        <th class="text-center" width="60">NIP/NPNP</th>
        <th class="text-center" >Nama</th>
        <th class="text-center" width="10">JK</th>
        <th class="text-center" width="150">TTL</th>
        <th class="text-center" width="40">Usia</th>
        <th class="text-center" width="">Ruang</th>
        <th class="text-center" width="">Jabatan</th>
        <th class="text-center" width="20">Eselon</th>
        <th class="text-center" width="">Pangkat</th>
        <th class="text-center" width="10">Gol</th>
        <th class="text-center" width="10">Status</th>
      </tr>
    </thead>
    <tbody>
      <?php if($pegawai != null): ?>
        <?php foreach($pegawai as $row): ?>
          <tr>
            <?php 
              $nama = $row->gelar_depan.' '.$row->nama;
              if ($row->gelar_belakang != ''){
                $nama .= ', '.$row->gelar_belakang;
              }
              if ($row->jenis_kelamin == 1) {
                $jk = "L";
              }else{
                $jk = "P";
              }
              if($row->tgl_lhr != ''){$umur = hitung_umur($row->tgl_lhr);}else{$umur = '-';};
              $eselon = ($row->eselon != '') ? $row->eselon : '-' ; 
            ?>
            <td class="text-center"><?=$i++?></td>
            <td class="text-left"><?=$row->nomor_induk?></td>
            <td class="text-left"><?=$nama?></td>
            <td class="text-center"><?=$jk?></td>
            <td class="text-left"><?=$row->tempat_lhr.', '.date_to_id($row->tgl_lhr)?></td>
            <td class="text-center"><?=$umur?></td>
            <td class="text-left"><?=$row->ruang?></td>
            <td class="text-left"><?=$row->jabatan?></td>
            <td class="text-center"><?=$eselon?></td>
            <td class="text-left"><?=$row->pangkat?></td>
            <td class="text-center"><?=$row->golongan?></td>
            <td class="text-center"><?=$row->status_pegawai?></td>
          </tr>
        <?php endforeach;?>
      <?php endif;?>
    </tbody>
  </table>
  <br><br>
  <table id="count_table">
      <tbody>
        <tr>
          <td class="text-left" width="300">Pegawai Aktif </td>
          <td width="10">:</td>
          <td class="text-right" width="50"><?=$total->kedudukan_1?></td>
        </tr>
        <tr>
          <td class="text-left" width="300">Pensiun (Batas Usia Pensiun)</td>
          <td width="10">:</td>
          <td class="text-right" width="50"><?=$total->kedudukan_2?></td>
        </tr>
        <tr>
          <td class="text-left" width="300">Diberhentikan karena Hukuman Disiplin</td>
          <td width="10">:</td>
          <td class="text-right" width="50"><?=$total->kedudukan_3?></td>
        </tr>
        <tr>
          <td class="text-left" width="300">Meninggal Dunia/Janda/Duda</td>
          <td width="10">:</td>
          <td class="text-right" width="50"><?=$total->kedudukan_4?></td>
        </tr>
        <tr>
          <td class="text-left" width="300">Berhenti Atas Permintaan Sendiri (APS)</td>
          <td width="10">:</td>
          <td class="text-right" width="50"><?=$total->kedudukan_5?></td>
        </tr>
        <tr>
          <td class="text-left" width="300">Mutasi keluar</td>
          <td width="10">:</td>
          <td class="text-right" width="50"><?=$total->kedudukan_6?></td>
        </tr>
        <tr>
          <td class="text-left" width="300">Mengundurkan Diri dari PNS</td>
          <td width="10">:</td>
          <td class="text-right" width="50"><?=$total->kedudukan_7?></td>
        </tr>
        <tr>
          <td class="text-left" width="300">Mengundurkan Diri dari CPNS</td>
          <td width="10">:</td>
          <td class="text-right" width="50"><?=$total->kedudukan_8?></td>
        </tr>
        <tr>
          <td class="text-left" width="300">Cuti diluar Tanggungan Negara</td>
          <td width="10">:</td>
          <td class="text-right" width="50"><?=$total->kedudukan_9?></td>
        </tr>
        <tr>
          <td class="text-left" width="300">Tidak Diketahui</td>
          <td width="10">:</td>
          <td class="text-right" width="50"><?=$total->kedudukan_10?></td>
        </tr>
        <tr>
          <th class="text-left" width="300">Total</th>
          <th>:</th>
          <th class="text-right" width="50">
            <?=
              $total->kedudukan_1+$total->kedudukan_2+$total->kedudukan_3+$total->kedudukan_4+
              $total->kedudukan_5+$total->kedudukan_6+$total->kedudukan_7+$total->kedudukan_8+
              $total->kedudukan_9+$total->kedudukan_10
            ?>
          </th>
        </tr> -->
      </tbody>
    </table>
    <br><br>
    <?php 
      if($direktur){
        $nama_direktur = '';
        if ($direktur->gelar_depan != '') {
          $nama_direktur .= $direktur->gelar_depan.' ';
        }
        $nama_direktur .= $direktur->nama;
        if ($direktur->gelar_belakang != '') {
          $nama_direktur .= ', '.$direktur->gelar_belakang;
        }
      }
    ?>
    <table id="sign_table">
      <tbody>
        <tr>
          <td class="text-center">
            <?=$profil->kop_ttd_direktur?>
          </td>
        </tr>
        <tr>
          <td><br><br><br><br></td>
        </tr>
        <tr>
          <td class="text-center">
            <u><?=$nama_direktur?></u><br>
            NIP. <?=$direktur->nomor_induk?>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>