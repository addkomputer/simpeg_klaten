<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Rekap Data Pegawai Berdasarkan Jenis Kelamin</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
  <table id="kop">
    <tbody>
      <tr>
        <td class="text-center logo"><img src="<?=base_url()?>img/kebumen.png" height="80"></td>
        <td class="text-center">
          <span id="kop_1">PEMERINTAH KABUPATEN KEBUMEN </span><br>
          <span id="kop_2">RUMAH SAKIT UMUM DAERAH KEBUMEN dr. SOEDIRMAN</span><br>
          <span id="kop_3">
            Jalan Lingkar Selatan, Muktisari, Kebumen, Kabupaten Kebumen, Jawa Tengah 54351 <br>
            Telp/Fax : 0287-3873318, 0287-381101, 0287-385274; WA : 0877-3427-026; Email : rsud@kebumenkab.go.id
          </span>
        </td>
        <td class="text-center logo"><img src="<?=base_url()?>img/logo.png" height="80"></td>
      </tr>
    </tbody>
  </table>
  <div class="garis"></div>
  <h3 class="text-center"><?=$judul?></h3>
  <table id="filter_table">
    <tbody>
      <tr>
        <td width="120">Kedudukan </td>
        <td>:</td>
        <td>
          <?php if($search['id_kedudukan_pns'] != null): ?>
            <?php 
              switch ($search['id_kedudukan_pns']) {
                case '1':
                  echo 'Pegawai Aktif';
                  break;
                
                case '2':
                  echo 'Pensiun (Batas Usia Pensiun)';
                  break;

                case '3':
                  echo 'Diberhentikan karena Hukuman Disiplin';
                  break;
                
                case '4':
                  echo 'Meninggal Dunia/Janda/Duda';
                  break;
                
                case '5':
                  echo 'Berhenti Atas Permintaan Sendiri (APS';
                  break;
                
                case '6':
                  echo 'Mutasi keluar';
                  break;

                case '7':
                  echo 'engundurkan Diri dari PNS';
                  break;

                case '8':
                  echo 'Mengundurkan Diri dari CPNS';
                  break;

                case '9':
                  echo 'Cuti diluar Tanggungan Negara';
                  break;

                case '10':
                  echo 'Tidak Diketahui';
                  break;
              }
            ?>
          <?php else: ?>
            Semua
          <?php endif;?>  
        </td>
      </tr>
      <tr>
        <td>Ruang</td>
        <td>:</td>
        <td>
          <?php if($search['ruang'] != null): ?>
            <?=$search['ruang']->ruang?>
          <?php else: ?>
            Semua
          <?php endif;?>
        </td>
      </tr>
      <tr>
        <td>Jabatan</td>
        <td>:</td>
        <td>
          <?php if($search['jabatan'] != null): ?>
            <?=$search['jabatan']->jabatan?>
          <?php else: ?>
            Semua
          <?php endif;?>
        </td>
      </tr>
      <tr>
        <td>Status Pegawai</td>
        <td>:</td>
        <td>
          <?php if($search['status_pegawai'] != null): ?>
            <?=$search['status_pegawai']->status_pegawai?>
          <?php else: ?>
            Semua
          <?php endif;?>
        </td>
      </tr>
    </tbody>
  </table>
  <br>