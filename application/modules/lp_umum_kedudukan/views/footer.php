      </tbody>
    </table>
    <br><br>
    <table id="count_table">
      <tbody>
        <tr>
          <td class="text-left" width="300">Pegawai Aktif </td>
          <td width="10">:</td>
          <td class="text-right" width="50"><?=$total->kedudukan_1?></td>
        </tr>
        <tr>
          <td class="text-left" width="300">Pensiun (Batas Usia Pensiun)</td>
          <td width="10">:</td>
          <td class="text-right" width="50"><?=$total->kedudukan_2?></td>
        </tr>
        <tr>
          <td class="text-left" width="300">Diberhentikan karena Hukuman Disiplin</td>
          <td width="10">:</td>
          <td class="text-right" width="50"><?=$total->kedudukan_3?></td>
        </tr>
        <tr>
          <td class="text-left" width="300">Meninggal Dunia/Janda/Duda</td>
          <td width="10">:</td>
          <td class="text-right" width="50"><?=$total->kedudukan_4?></td>
        </tr>
        <tr>
          <td class="text-left" width="300">Berhenti Atas Permintaan Sendiri (APS)</td>
          <td width="10">:</td>
          <td class="text-right" width="50"><?=$total->kedudukan_5?></td>
        </tr>
        <tr>
          <td class="text-left" width="300">Mutasi keluar</td>
          <td width="10">:</td>
          <td class="text-right" width="50"><?=$total->kedudukan_6?></td>
        </tr>
        <tr>
          <td class="text-left" width="300">Mengundurkan Diri dari PNS</td>
          <td width="10">:</td>
          <td class="text-right" width="50"><?=$total->kedudukan_7?></td>
        </tr>
        <tr>
          <td class="text-left" width="300">Mengundurkan Diri dari CPNS</td>
          <td width="10">:</td>
          <td class="text-right" width="50"><?=$total->kedudukan_8?></td>
        </tr>
        <tr>
          <td class="text-left" width="300">Cuti diluar Tanggungan Negara</td>
          <td width="10">:</td>
          <td class="text-right" width="50"><?=$total->kedudukan_9?></td>
        </tr>
        <tr>
          <td class="text-left" width="300">Tidak Diketahui</td>
          <td width="10">:</td>
          <td class="text-right" width="50"><?=$total->kedudukan_10?></td>
        </tr>
        <tr>
          <th class="text-left" width="300">Total</th>
          <th>:</th>
          <th class="text-right" width="50">
            <?=
              $total->kedudukan_1+$total->kedudukan_2+$total->kedudukan_3+$total->kedudukan_4+
              $total->kedudukan_5+$total->kedudukan_6+$total->kedudukan_7+$total->kedudukan_8+
              $total->kedudukan_9+$total->kedudukan_10
            ?>
          </th>
        </tr> -->
      </tbody>
    </table>
    <br><br>
    <?php 
      if($direktur){
        $nama_direktur = '';
        if ($direktur->gelar_depan != '') {
          $nama_direktur .= $direktur->gelar_depan.' ';
        }
        $nama_direktur .= $direktur->nama;
        if ($direktur->gelar_belakang != '') {
          $nama_direktur .= ', '.$direktur->gelar_belakang;
        }
      }
    ?>
    <table id="sign_table">
      <tbody>
        <tr>
          <td class="text-center">
            <?=$profil->kop_ttd_direktur?>
          </td>
        </tr>
        <tr>
          <td><br><br><br><br></td>
        </tr>
        <tr>
          <td class="text-center">
            <u><?=$nama_direktur?></u><br>
            NIP. <?=$direktur->nomor_induk?>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>