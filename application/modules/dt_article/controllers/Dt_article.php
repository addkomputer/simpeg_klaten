<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dt_article extends MY_Controller {

	var $access, $id_article;

  function __construct(){
		parent::__construct();

		$controller = 'dt_article';

    if($this->session->userdata('menu') != $controller){
      $this->session->unset_userdata('search');
      $this->session->set_userdata(array('menu' => 'dt_article'));
    }
    $this->load->model('ap_konfigurasi/m_ap_konfigurasi');
		$this->id_article = $this->session->userdata('id_article');
		$this->access = $this->m_ap_konfigurasi->get_permission($this->id_article, $controller);
		if ($this->access == null) {
			redirect(base_url().'ap_error/error_403');
		}
    $this->load->model('m_dt_article');
    $this->load->model('ap_pengguna/m_ap_pengguna');
    $this->load->model('ms_instansi/m_ms_instansi');
	}
	
	public function index()
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Daftar';
		$data['instansi'] = $this->m_ms_instansi->get_all();

		if ($this->access->_read) {
			$search = null;
			
			if($this->session->userdata('search') != null){
				$search = $this->session->userdata('search');
			}else{
				$id_instansi = '';
				if ($this->session->userdata('id_grup') == 3) {
					$id_instansi = $this->session->userdata('id_instansi');
				}
				$search = array(
					'tanggal' => '',
					'id_instansi' => $id_instansi,
					'term' => ''
				);
			}
			
			$data['search'] = $search;
	
			$config['base_url'] = base_url().'dt_article/index/';
			$config['per_page'] = 10;
	
			$from = $this->uri->segment(3);
			if ($from == null) {
				$from = 0;
			}
			
			$num_rows = $this->m_dt_article->num_rows($search);

			$config['total_rows'] = $num_rows;
			$this->pagination->initialize($config);

			$data['article'] = $this->m_dt_article->get_list($config['per_page'],$from,$search);

			$data['num_rows'] = $num_rows;
			$data['num_rows_total'] = $this->m_dt_article->num_rows_total();
			$data['search'] = $this->session->userdata('search');
			
			insert_log('read',$this->access->menu);
			$this->view('index',$data);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}
	
	public function form($id = null)
	{
		$data['access'] = $this->access;
		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Tambah Data';
				$data['action'] = 'insert';
				$data['article'] = null;
				$data['pegawai'] = $this->m_dt_article->get_pegawai();
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Ubah Data';
				$data['action'] = 'update';
				$data['article'] = $this->m_dt_article->get_by_id($id);
				$data['pegawai'] = $this->m_dt_article->get_article_pegawai($id);
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		}
	}

	public function search()
	{
		$data = $_POST;
		$this->session->set_userdata(array('search' => $data));
		redirect(base_url().$this->access->controller);
	}

	public function search_tanggal($tanggal)
	{
		$search['tanggal'] = $tanggal;
		if (!$this->session->userdata('search')['id_jabatan']) {
			$search['id_instansi'] = '';
			if ($this->session->userdata('id_grup') == 3) {
				$search['id_instansi']	= $this->session->userdata('id_instansi');
			}
			$search['term'] = '';
		}else{
			$search['id_instansi'] = $this->session->userdata('search')['id_instansi'];
			$search['term'] = $this->session->userdata('search')['term'];
		}
		$this->session->set_userdata(array('search' => $search));
		redirect(base_url().$this->access->controller);
	}

	public function reset_search()
  {
    $this->session->unset_userdata('search');
    redirect(base_url().'dt_article/index');
  }

	public function insert()
	{
		$data = $_POST;
		if ($data != null) {
			$data['created_by'] = $this->session->userdata('nama');
			if(!isset($data['is_active'])){$data['is_active'] = 0;}
			$data['post_date'] = date_to_id($data['post_date']);
			unset($data['_wysihtml5_mode']);
			$this->m_dt_article->insert($data);
			insert_log('insert',$this->access->menu);
			$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
			redirect(base_url().'dt_article/index');
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function update()
	{
		$data = $_POST;
		if ($data != null) {
			if($this->access->_update){
				$data['updated_by'] = $this->session->userdata('nama');
				if(!isset($data['is_active'])){$data['is_active'] = 0;}
				$id = $data['id_article'];
				$data['post_date'] = date_to_id($data['post_date']);
				unset($data['_wysihtml5_mode']);
				$this->m_dt_article->update($id, $data);
				insert_log('update',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				redirect(base_url().'dt_article/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function delete()
	{
		$data = $_POST;
		if($data != null){
			if ($this->access->_delete) {
				$this->m_dt_article->delete_temp($data['id_article']);
				insert_log('delete',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				redirect(base_url().'dt_article/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}

}