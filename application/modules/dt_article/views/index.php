<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-<?=$access->icon?> text-red"></i> <?=$access->menu?>
      <small><?=$subtitle?></small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">Master Kepegawaian</li>
      <li class="active text-orange"><?=$access->menu?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box box-danger">
      <div class="box-header with-border">
        <i class="fa fa-table text-maroon"></i>
        <h3 class="box-title">Tabel <?=$access->menu?></h3>
        <div class="box-tools pull-right">
          <span class="badge bg-maroon"><i class="fa fa-bar-chart"></i> Jumlah Keseluruhan Data : <?=$num_rows_total?></span>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <form id="form-index" action="<?=base_url()?>dt_article/search" method="post">
            <div class="col-md-1">
              <a class="btn btn-primary btn-flat" href="<?=base_url()?>dt_article/form"><i class="fa fa-plus"></i> Tambah</a>
            </div>
            <div class="col-md-2 col-md-offset-2">
              <div class="input-group">
                <input type="text" class="form-control datepicker" id="tanggal" name="tanggal" placeholder="dd-mm-yyyy" value="<?php if($search != null){echo $search['tanggal'];}else{echo '';}?>">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <select name="id_instansi" class="form-control select2 input-sm" onchange="$('#form-index').submit()">
                  <?php if($this->session->userdata('id_grup') == 3):?>
                    <?php foreach($instansi as $row): ?>
                      <?php if($this->session->userdata('id_instansi') == $row->id_instansi): ?>
                        <option value="<?=$row->id_instansi?>"><?=$row->instansi?></option>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <option value="">-- Semua Instansi --</option>
                    <?php foreach($instansi as $row): ?>
                      <option value="<?=$row->id_instansi?>" <?php if($search){if($search['id_instansi'] == $row->id_instansi){echo 'selected';}} ?>><?=$row->instansi?></option>
                    <?php endforeach;?>
                  <?php endif; ?>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="input-group pull-right">
                <input type="text" name="term" class="form-control pull-right" placeholder="Pencarian" value="<?php if($search != null){echo $search['term'];}?>">
                <div class="input-group-btn">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                  <a class="btn btn-default btn-flat" href="<?=base_url()?>dt_article/reset_search"><i class="fa fa-refresh"></i></a>
                </div>
              </div>
            </div>
          </form>
        </div>
        <?php echo $this->session->flashdata('status'); ?>
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
                <th class="text-center" width="30">No.</th>
                <th class="text-center" width="60">Aksi</th>
                <th class="text-center">Judul</th>
                <th class="text-center" width="150">Pengirim</th>
                <th class="text-center" width="">Instansi</th>
                <th class="text-center" width="120">Tanggal</th>
                <th class="text-center" width="60">Waktu</th>
                <th class="text-center" width="30">Jml</th>
                <th class="text-center" width="30">Status</th>
              </tr>
            </thead>
            <tbody>
              <?php if ($article != null): ?>
                <?php $i=1;foreach ($article as $row): ?>
                  <tr>
                    <td class="text-center"><?=$this->uri->segment('3')+$i++?></td>
                    <td class="text-center">
                      <?php if($row->id_article != 0): ?>
                        <a class="btn btn-xs btn-warning btn-flat" href="<?=base_url()?>dt_article/form/<?=$row->id_article?>" style="padding-right:0.250em;"><i class="fa fa-pencil"></i></a> 
                        <a class="btn btn-xs btn-danger btn-flat" href="#" onclick="del(<?=$row->id_article?>)"><i class="fa fa-trash"></i></a>
                      <?php endif;?>
                    </td>
                    <td><?=$row->article?></td>
                    <td><?=$row->author?></td>
                    <td><?=$row->instansi?></td>
                    <td class="text-center"><?=date_id($row->post_date)?></td>
                    <td class="text-center"><?=$row->post_time?></td>
                    <td class="text-center"><?=$row->jumlah?></td>
                    <td class="text-center">
                      <?php if ($row->is_active == 1): ?>
                        <i class="fa fa-check text-green"></i>
                      <?php else: ?>
                        <i class="fa fa-close text-red"></i>
                      <?php endif; ?>
                    </td>
                  </tr>
                <?php endforeach; ?>
              <?php else: ?>
                <tr>
                  <td class="text-center" colspan="99">Tidak ada data!</td>
                </tr>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <span class="badge bg-purple">Jumlah Data : <?=$num_rows?></span>
        <?php echo $this->pagination->create_links(); ?>
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- Modal Delete -->
<div id="modal_delete" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>dt_article/delete" method="post">
        <input type="hidden" name="id_article" id="id_article">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-danger btn-flat"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
  $(document).ready(function () {
    $('#tanggal').datepicker()
    .on('changeDate', function(e) {
      // $('#form_search').submit();
      window.location.replace("<?=base_url().$this->access->controller.'/search_tanggal/'?>"+e.format());
    });
    $('.select2').on('select2:select', function (e) {
      $('#form_search').submit();
    });
    // $('.select2').on('select2:select', function (e) {
    //   $('#form_search').submit();
    // });
  })
  function del(id) {
    $("#modal_delete").modal('show');
    $("#id_article").val(id);
  }
</script>