<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-red"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">Master Kepegawaian</li>
        <li class="active text-orange"><?=$access->menu?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-danger">
        <div class="box-header with-border">
          <i class="fa fa-table text-maroon"></i>
          <h3 class="box-title">Form <?=$access->menu?></h3>
        </div>
        <form id="form" class="form-horizontal" action="<?=base_url()?>dt_article/<?=$action?>" method="post" autocomplete="off"> 
          <div class="box-body">
            <input type="hidden" class="form-control" name="id_article" id="id_article" value="<?php if($article != null){echo $article->id_article;}else{echo uniqid();};?>">
            <div class="form-group">
              <label class="col-sm-2 control-label">Judul</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" name="article" id="article" value="<?php if($article != null){echo $article->article;};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Isi</label>
              <div class="col-sm-8">
                <textarea class="form-control" name="content" id="content"><?php if($article != null){echo $article->content;};?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Pengirim</label>
              <div class="col-sm-3">
                <input type="hidden" class="form-control" name="id_author" id="id_author" value="<?php if($article != null){echo $article->id_author;}else{echo $this->session->userdata('id_pengguna');}?>" required readonly>
                <input type="hidden" class="form-control" name="id_instansi" id="id_instansi" value="<?php if($article != null){echo $article->id_instansi;}else{echo $this->session->userdata('id_instansi');}?>" required readonly>
                <input type="text" class="form-control" name="author" id="author" value="<?php if($article != null){echo $article->author;}else{echo $this->session->userdata('nama');}?>" required readonly>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Tanggal Kirim</label>
              <div class="col-sm-2">
                <div class="input-group">
                  <input type="text" class="form-control datepicker" name="post_date" id="post_date" value="<?php if($article != null){echo date_to_id($article->post_date);}else{echo date('d-m-Y');}?>" required>
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
              </div>
              <label class="col-sm-2 control-label">Waktu Kirim</label>
              <div class="col-sm-2">
                <div class="input-group">
                  <input type="text" class="form-control timepicker" name="post_time" id="post_time" value="<?php if($article != null){echo $article->post_time;}else{echo date('H:i:s');}?>" required>
                  <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Aktif</label>
              <div class="col-sm-5">
                <input class="iCheckbox" type="checkbox" name="is_active" value="1" <?php if($article != null){if($article->is_active == 1){echo 'checked';}}else{echo 'checked';}?>>
              </div>
            </div>
            <hr>
            <h4>Daftar Pegawai Penerima</h4>
            <div class="table-responsive" style="max-height:500px;">
              <table class="table table-bordered table-striped table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" width="30">No</th>
                    <th class="text-center" width="20">#</th>
                    <th class="text-center">Nama</th>
                    <th class="text-center">Nomor Induk</th>
                    <th class="text-center">Instansi</th>
                    <th class="text-center">Jabatan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i=1;foreach($pegawai as $row):?>
                    <tr>
                      <td><?=$i++?></td>
                      <td class="text-center">
                        <input class="iCheckbox" type="checkbox" name="id_pegawai[]" value="<?=$row->id_pegawai?>" <?php if(@$row->id_article != null){echo 'checked';};?>>
                      </td>
                      <td>
                        <?php 
                          echo $row->gelar_depan.' '.$row->nama;
                          if ($row->gelar_belakang != ''){
                            echo ', '.$row->gelar_belakang;
                          }
                        ?>
                      </td>
                      <td><?=$row->nomor_induk?></td>
                      <td><?=$row->instansi?></td>
                      <td><?=$row->jabatan?></td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
            <div class="form-group">
              <div class="col-md-offset-2 col-sm-8">
                <button class="btn btn-success btn-flat" type="submit"><i class="fa fa-save"></i> Simpan</button>
                <a class="btn btn-default btn-flat" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Batal</a>
              </div>
            </div>
          </div>
        </form>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<script>
  $(document).ready(function () {
    // form validator
    $('#form').validate();
    $('#content').wysihtml5();
  })
</script>