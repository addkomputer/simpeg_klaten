<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dt_article extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
    $where = "WHERE 1 = 1 ";
    if ($this->session->userdata('id_grup') == 3) {
      $where .= "AND a.id_author='".$this->session->userdata('id_pengguna')." '";
    }
    if ($search != null) {
      if ($search['tanggal'] != '') {
        $where .= "AND a.post_date ='".date_to_id($search['tanggal'])."' ";
      }
      if ($search['id_instansi'] != '') {
        $where .= "AND a.id_instansi ='".$search['id_instansi']."' ";
      }
      if ($search['term'] != '') {
        $where .= "AND a.article LIKE'".$search['term']."%' ";
      }
    }
    $query = $this->db->query(
      "SELECT 
        a.*,
        COUNT(b.id_pegawai) as jumlah,
        c.instansi
      FROM dt_article a
      LEFT JOIN dt_article_pegawai b ON b.id_article = a.id_article
      LEFT JOIN ms_instansi c ON a.id_instansi = c.id_instansi
      $where
      GROUP BY a.id_article
      ORDER BY a.post_date ASC, a.post_time ASC
      LIMIT $offset,$number"
    );
    
    return $query->result();
  }

  public function get_pegawai()
  {
    $where = "WHERE a.is_deleted = 0 ";
    if ($this->session->userdata('id_grup') == 3) {
      $where = "AND a.id_instansi = '".$this->session->userdata('id_instansi')."' ";
    }
    $query = $this->db->query(
      "SELECT 
        a.*,
        b.instansi,
        c.jabatan
      FROM dt_pegawai a
      LEFT JOIN ms_instansi b ON a.id_instansi = b.id_instansi
      LEFT JOIN ms_jabatan c ON a.id_jabatan = c.id_jabatan
      $where
      ORDER BY a.id_instansi ASC"
    );
    
    return $query->result();
  }

  public function get_article_pegawai($id_article)
  {
    $query = $this->db->query(
      "SELECT 
        a.id_pegawai,a.gelar_depan,a.nama,a.gelar_belakang,a.nomor_induk,
        c.id_article,
        d.jabatan,
        e.instansi
      FROM dt_pegawai a
      LEFT JOIN (
        SELECT * 
        FROM dt_article_pegawai b 
        WHERE b.id_article = '$id_article'
      ) AS c ON c.id_pegawai = a.id_pegawai
      LEFT JOIN ms_jabatan d ON a.id_jabatan = d.id_jabatan
      LEFT JOIN ms_instansi e ON a.id_instansi = e.id_instansi
      WHERE a.is_deleted = 0
      ORDER BY a.id_instansi ASC"
    );

    return $query->result();
  }

  function num_rows($search = null){
		$where = "WHERE 1 = 1 ";
    if ($this->session->userdata('id_grup') == 3) {
      $where .= "AND a.id_author='".$this->session->userdata('id_pengguna')." '";
    }
    if ($search != null) {
      if ($search['tanggal'] != '') {
        $where .= "AND a.post_date ='".date_to_id($search['tanggal'])."' ";
      }
      if ($search['id_instansi'] != '') {
        $where .= "AND a.id_instansi ='".$search['id_instansi']."' ";
      }
      if ($search['term'] != '') {
        $where .= "AND a.article LIKE'".$search['term']."%' ";
      }
    }
    $query = $this->db->query(
      "SELECT 
        a.*,
        COUNT(b.id_pegawai) as jumlah,
        c.instansi
      FROM dt_article a
      LEFT JOIN dt_article_pegawai b ON b.id_article = a.id_article
      LEFT JOIN ms_instansi c ON a.id_instansi = c.id_instansi
      $where
      GROUP BY a.id_article
      ORDER BY a.post_date ASC, a.post_time ASC"
    );
    
    return $query->num_rows();
  }
  
  function num_rows_total(){
    return $this->db
      ->where('is_deleted','0')
      ->order_by('article')
      ->get('dt_article')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('dt_article')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id_article',$id)->get('dt_article')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_article','asc')->get('dt_article')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_article','desc')->get('dt_article')->row();
  }

  public function insert($data)
  {
    $d_1 = array(
      'id_article' => $data['id_article'],
      'article' => $data['article'],
      'content' => $data['content'],
      'id_author' => $data['id_author'],
      'author' => $data['author'],
      'id_instansi' => $data['id_instansi'],
      'post_date' => $data['post_date'],
      'post_time' => $data['post_time']
    );
    $this->db->insert('dt_article',$d_1);
    foreach ($data['id_pegawai'] as $key => $val) {
      $d_2 = array(
        'id_article' => $data['id_article'],
        'id_pegawai' => $val,
        'created_by' => $data['created_by']
      );
      $this->db->insert('dt_article_pegawai', $d_2);
    }
  }

  public function update($id,$data)
  {
    $d_1 = array(
      'article' => $data['article'],
      'content' => $data['content'],
      'id_author' => $data['id_author'],
      'author' => $data['author'],
      'id_instansi' => $data['id_instansi'],
      'post_date' => $data['post_date'],
      'post_time' => $data['post_time']
    );
    $this->db->where('id_article',$data['id_article'])->update('dt_article',$d_1);
    $this->db->where('id_article',$data['id_article'])->delete('dt_article_pegawai');
    foreach ($data['id_pegawai'] as $key => $val) {
      $d_2 = array(
        'id_article' => $data['id_article'],
        'id_pegawai' => $val,
        'created_by' => $data['updated_by']
      );
      $this->db->insert('dt_article_pegawai', $d_2);
    }
  }

  public function delete_temp($id)
  {
    $this->db->where('id_article',$id)->update('dt_article',array('is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_article',$id)->delete('dt_article');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE dt_article");
  }

}
