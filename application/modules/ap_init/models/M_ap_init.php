<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ap_init extends CI_Model {

  private $table_scanlog,$table_log_kehadiran,$table_sakit;

  public function __construct() {
    parent::__construct();
    
		$this->table_scanlog = 'tb_scanlog_'.date('Y');
		$this->table_log_kehadiran = 'tb_log_kehadiran_'.date('Y');
    $this->table_mapping_shift = 'dt_mapping_shift_'.date('Y');
    
    // $this->create_scanlog();
    $this->create_log_kehadiran();
    // $this->create_mapping_shift();
  }
  
  public function create_scanlog()
	{
		$this->db->query(
			"CREATE TABLE IF NOT EXISTS `$this->table_scanlog` (
				`SN` text NULL,
				`ScanDate` timestamp NULL,
				`PIN` text NULL,
				`nomor_induk` VARCHAR(50) NULL,
				`VerifyMode` int(11) NULL,
				`IOMode` int(11) NULL,
				`WorkCode` int(11) NULL
			) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;"
		);
	}

	public function create_log_kehadiran()
	{
		$this->db->query(
      "CREATE TABLE IF NOT EXISTS `$this->table_log_kehadiran` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `bulan` varchar(2) NOT NULL,
        `id_pegawai` varchar(50) DEFAULT '0',
        `nomor_induk` varchar(50) DEFAULT '0',
        `pin` varchar(50) DEFAULT '0',
        `id_instansi` int(11) DEFAULT '0',
        `id_tipe` int(1) DEFAULT '-1',
        `id_shift` int(11) DEFAULT '7',
        `is_tukar` tinyint(1) DEFAULT '0',
        `kode` varchar(50) DEFAULT 'P3',
        `tanggal` date DEFAULT NULL,
        `jam_datang` time DEFAULT NULL,
        `jam_batas_datang` time DEFAULT NULL,
        `is_datang` tinyint(1) NOT NULL DEFAULT '0',
        `jam_datang_pegawai` time DEFAULT NULL,
        `terlambat_datang` int(11) DEFAULT '0',
        `ket_datang` text,
        `is_pulang` tinyint(1) NOT NULL DEFAULT '0',
        `jam_pulang` time DEFAULT NULL,
        `jam_batas_pulang` time DEFAULT NULL,
        `jam_pulang_pegawai` time DEFAULT NULL,
        `mendahului_pulang` int(11) DEFAULT '0',
        `ket_pulang` text,
        `file_bukti` varchar(100) DEFAULT NULL,
        `keterangan` text,
        PRIMARY KEY (`id`)
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;"
    );
	}

	public function create_mapping_shift()
  {
    $this->db->query(
      "CREATE TABLE IF NOT EXISTS `$this->table_mapping_shift` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `id_pegawai` varchar(50) NULL DEFAULT '0',
        `nomor_induk` varchar(50) NULL DEFAULT '0',
        `pin` varchar(50) NULL DEFAULT '0',
				`id_ruang` int(11) NULL DEFAULT '0',
        `id_shift` int(11) NULL DEFAULT '8',
				`kode` varchar(50) NULL DEFAULT 'P3',
        `tanggal` date NULL DEFAULT '0000-00-00',
        `jam_datang` time NULL DEFAULT '00:00:00',
        `jam_batas_datang` time NULL DEFAULT '00:00:00',
        `jam_pulang` time NULL DEFAULT '00:00:00',
        `jam_batas_pulang` time NULL DEFAULT '00:00:00',
        `is_over24` tinyint(1) NULL DEFAULT '0',
        `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
        `created_by` varchar(50) NULL DEFAULT 'System',
        `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
        `updated_by` varchar(50) DEFAULT NULL,
        `is_active` tinyint(1) NULL DEFAULT '1',
        `is_deleted` tinyint(1) NULL DEFAULT '0',
        PRIMARY KEY (`id`)
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1;"
    );
  }
  
}
