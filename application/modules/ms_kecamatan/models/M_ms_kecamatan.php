<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ms_kecamatan extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_kecamatan')
      ->get('ms_kecamatan',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_kecamatan')
      ->get('ms_kecamatan')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('ms_kecamatan')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id_kecamatan',$id)->get('ms_kecamatan')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_kecamatan','asc')->get('ms_kecamatan')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_kecamatan','desc')->get('ms_kecamatan')->row();
  }

  public function insert($data)
  {
    $this->db->insert('ms_kecamatan',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_kecamatan',$id)->update('ms_kecamatan',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id_kecamatan',$id)->update('ms_kecamatan',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_kecamatan',$id)->delete('ms_kecamatan');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ms_kecamatan");
  }

}
