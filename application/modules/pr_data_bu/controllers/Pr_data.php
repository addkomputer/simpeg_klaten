<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pr_data extends MY_Controller {

	var $access, $id_grup;

  function __construct(){
		parent::__construct();

		$controller = 'pr_data';

    if($this->session->userdata('menu') != $controller){
      $this->session->unset_userdata('search');
      $this->session->set_userdata(array('menu' => 'pr_data'));
    }
    $this->load->model('ap_konfigurasi/m_ap_konfigurasi');
		$this->id_grup = $this->session->userdata('id_grup');
		$this->access = $this->m_ap_konfigurasi->get_permission($this->id_grup, $controller);
		if ($this->access == null) {
			redirect(base_url().'ap_error/error_403');
		}
		$this->load->model('m_pr_data');
		$this->load->model('ms_ruang/m_ms_ruang');
		$this->load->model('ms_golongan/m_ms_golongan');
		$this->load->model('ms_kecamatan/m_ms_kecamatan');
		$this->load->model('ms_unit_kerja/m_ms_unit_kerja');
		$this->load->model('ms_unit_kerja/m_ms_unit_kerja');
		$this->load->model('ms_nama_instansi/m_ms_nama_instansi');
		$this->load->model('ms_rumpun_jabatan/m_ms_rumpun_jabatan');
		$this->load->model('ms_jabatan/m_ms_jabatan');	
		$this->load->model('ms_tingkat/m_ms_tingkat');
		$this->load->model('ms_periode_penilaian/m_ms_periode_penilaian');
		$this->load->model('dt_anak/m_dt_anak');
		$this->load->model('ap_profil/m_ap_profil');
		$this->load->model('dt_tingkat/m_dt_tingkat');
		$this->load->model('dt_atasan/m_dt_atasan');
		$this->load->model('dt_keluarga/m_dt_keluarga');
		$this->load->model('dt_riwayat_pangkat/m_dt_riwayat_pangkat');
		$this->load->model('dt_riwayat_pendidikan/m_dt_riwayat_pendidikan');
		$this->load->model('dt_riwayat_jabatan/m_dt_riwayat_jabatan');
		$this->load->model('ms_jenis_diklat/m_ms_jenis_diklat');
		$this->load->model('dt_riwayat_diklat/m_dt_riwayat_diklat');
		$this->load->model('dt_riwayat_diklat_baru/m_dt_riwayat_diklat_baru');
		$this->load->model('dt_riwayat_usulan_diklat/m_dt_riwayat_usulan_diklat');
		$this->load->model('dt_riwayat_penghargaan/m_dt_riwayat_penghargaan');
		$this->load->model('ms_hukuman_disiplin/m_ms_hukuman_disiplin');
		$this->load->model('dt_riwayat_hukuman/m_dt_riwayat_hukuman');
		$this->load->model('dt_riwayat_ppk/m_dt_riwayat_ppk');
		$this->load->model('dt_tgs_pokok/m_dt_tgs_pokok');
		$this->load->model('dt_tgs_tambahan/m_dt_tgs_tambahan');
		$this->load->model('dt_tgs_kreatifitas/m_dt_tgs_kreatifitas');
		$this->load->model('dt_perilaku_kerja/m_dt_perilaku_kerja');
		$this->load->model('dt_proses_penilaian/m_dt_proses_penilaian');
		$this->load->model('dt_perilaku_kerja_tubel/m_dt_perilaku_kerja_tubel');
		$this->load->model('dt_perilaku_kerja_manual/m_dt_perilaku_kerja_manual');
	}

	public function reset_search()
  {
		$this->session->unset_userdata('search');
		redirect(base_url().'pr_data');
	}

	public function data_pokok()
	{
		$id = $this->session->userdata('id_pegawai');
		$data['access'] = $this->access;
		$data['ruang'] = $this->m_ms_ruang->get_all();
		$data['golongan'] = $this->m_ms_golongan->get_all();
		$data['kecamatan'] = $this->m_ms_kecamatan->get_all();
		$data['unit_kerja'] = $this->m_ms_unit_kerja->get_all();
		$data['nama_instansi'] = $this->m_ms_nama_instansi->get_all();
		$data['rumpun_jabatan'] = $this->m_ms_rumpun_jabatan->get_all();
		$data['jabatan_list'] = $this->m_ms_jabatan->get_all();
		$data['tingkat_list'] = $this->m_ms_tingkat->get_all();
		$data['periode_penilaian'] = $this->m_ms_periode_penilaian->get_all();
		
		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Tambah Data';
				$data['action'] = 'insert';
				$data['pegawai'] = null;
				$data['jabatan'] = null;
				$data['tingkat'] = null;
				$data['anak'] = null;
				$data['atasan'] = null;
				$data['keluarga'] = null;
				$data['id_pegawai'] = uniqid();
				
				$this->view('data_pokok', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Data Pokok';
				$data['action'] = 'update';
				$data['pegawai'] = $this->m_pr_data->get_by_id($id);
				$data['jabatan'] = $this->m_pr_data->get_jabatan_by_id($id);
				$data['tingkat'] = $this->m_pr_data->get_tingkat_by_id($id);
				$data['anak'] = $this->m_dt_anak->get_by_id_pegawai($id);
				$data['atasan'] = $this->m_dt_atasan->get_by_id($id);
				$data['keluarga'] = $this->m_dt_keluarga->get_by_id($id);
				// echo '<pre>' . var_export($data['jabatan'], true) . '</pre>';
				$this->view('data_pokok', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}

	public function data_pokok_action($action)
	{
		$data = $_POST;
		if ($data != null) {
			$profil = $this->m_ap_profil->get_first();
			switch ($action) {
				case 'insert':
					if ($this->access->_create) {
						$data['tgl_lhr'] = id_to_date($data['tgl_lhr']);
						$data['tmt_cpns'] = id_to_date($data['tmt_cpns']);
						$data['tmt_pns'] = id_to_date($data['tmt_pns']);
						$data['tgl_karpeg'] = id_to_date($data['tgl_karpeg']);
						$data['tgl_karpeg'] = id_to_date($data['tgl_karpeg']);
						$data['tmt_gol'] = id_to_date($data['tmt_gol']);
						$data['id_unit_kerja'] = $profil->id_unit_kerja;
						$data['created_by'] = $this->session->userdata('nama');
						
						$this->m_pr_data->insert($data);
						$this->m_ap_id->increment(2);
						insert_log('insert',$this->access->menu);
						$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
						redirect(base_url().'pr_data/data_pokok/');
					}else{
						redirect(base_url().'ap_error/error_403');
					}
					break;

				case 'update':
					if ($this->access->_update) {
						$id = $data['id_pegawai'];
						$data['tgl_lhr'] = id_to_date($data['tgl_lhr']);
						$data['status_pegawai'] = '2';
						$data['nama'] = strtoupper($data['nama']);
						$data['tmt_cpns'] = id_to_date($data['tmt_cpns']);
						$data['tmt_pns'] = id_to_date($data['tmt_pns']);
						$data['tgl_karpeg'] = id_to_date($data['tgl_karpeg']);
						$data['tgl_karpeg'] = id_to_date($data['tgl_karpeg']);
						$data['tmt_gol'] = id_to_date($data['tmt_gol']);
						$data['id_unit_kerja'] = $profil->id_unit_kerja;
						$data['updated_by'] = $this->session->userdata('nama');
						insert_log('update',$this->access->menu);
						$this->m_pr_data->update($id,$data);
						$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
						redirect(base_url().'pr_data/data_pokok/');
					}else{
						redirect(base_url().'ap_error/error_403');
					}
					break;

				case 'delete':
					if ($this->access->_delete) {
						$this->m_pr_data->delete_temp($data['id_pegawai']);
						insert_log('delete',$this->access->menu);
						$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
						redirect(base_url().'pr_data/data_pokok/');
					}else{
						redirect(base_url().'ap_error/error_403');
					}
					break;
			}
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function posisi_jabatan()
	{
		$id = $this->session->userdata('id_pegawai');
		$data['access'] = $this->access;
		$data['golongan'] = $this->m_ms_golongan->get_all();
		$data['kecamatan'] = $this->m_ms_kecamatan->get_all();
		$data['unit_kerja'] = $this->m_ms_unit_kerja->get_all();
		$data['nama_instansi'] = $this->m_ms_nama_instansi->get_all();
		$data['rumpun_jabatan'] = $this->m_ms_rumpun_jabatan->get_all();
		$data['jabatan_list'] = $this->m_ms_jabatan->get_all();
		$data['tingkat_list'] = $this->m_ms_tingkat->get_all();
		$data['periode_penilaian'] = $this->m_ms_periode_penilaian->get_all();

		if ($this->access->_update) {
			$data['subtitle'] = 'Posisi dan Jabatan';
			$data['action'] = 'update';
			$data['pegawai'] = $this->m_pr_data->get_by_id($id);
			$data['jabatan'] = $this->m_pr_data->get_jabatan_by_id($id);
			$data['tingkat'] = $this->m_pr_data->get_tingkat_by_id($id);
			$data['anak'] = $this->m_dt_anak->get_by_id_pegawai($id);
			$data['atasan'] = $this->m_dt_atasan->get_by_id($id);
			$data['keluarga'] = $this->m_dt_keluarga->get_by_id($id);
			$this->view('posisi_jabatan', $data);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function posisi_jabatan_action($action)
	{
		$data = $_POST;
		if ($data != null) {
			switch ($action) {
				case 'update':
					if($this->access->_update){
						$pegawai = array(
							'id_jabatan' => $data['id_jabatan'],
							'tmt_jabatan' => date_to_id($data['tmt_jabatan']),
							'tmt_pangkat' => date_to_id($data['tmt_pangkat'])
						);
						$this->m_pr_data->update($data['id_pegawai'], $pegawai);
						$tingkat = array(
							'id_pegawai' => $data['id_pegawai'],
							'id_tingkat' => $data['id_tingkat']
						);
						if($this->m_dt_tingkat->get_by_id($data['id_pegawai'])){
							$this->m_dt_tingkat->update($data['id_pegawai'],$tingkat);
						}else{
							$this->m_dt_tingkat->insert($tingkat);
						};
						$atasan = array(
							'id_pegawai' => $data['id_pegawai'],
							'nip_atasan' => $data['nip_atasan'],
							'plh_plt_pp' => $data['plh_plt_pp'],
							'id_jab_plh_plt_pp' => $data['id_jab_plh_plt_pp'],
							'nip_app' => $data['nip_app'],
							'plh_plt_app' => $data['plh_plt_app'],
							'id_jab_plh_plt_app' => $data['id_jab_plh_plt_app'],
							'id_periode' => $data['id_periode']
						);
						if($this->m_dt_atasan->get_by_id($data['id_pegawai'])){
							$this->m_dt_atasan->update($data['id_pegawai'],$atasan);
						}else{
							$this->m_dt_atasan->insert($atasan);
						};
						insert_log('update',$this->access->menu);
						$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
						redirect(base_url().'pr_data/posisi_jabatan/');
					}else{
						redirect(base_url().'ap_error/error_403');
					}
					break;
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function keluarga()
	{
		$id = $this->session->userdata('id_pegawai');
		$data['access'] = $this->access;
		$data['golongan'] = $this->m_ms_golongan->get_all();
		$data['kecamatan'] = $this->m_ms_kecamatan->get_all();
		$data['unit_kerja'] = $this->m_ms_unit_kerja->get_all();
		$data['nama_instansi'] = $this->m_ms_nama_instansi->get_all();
		$data['rumpun_jabatan'] = $this->m_ms_rumpun_jabatan->get_all();
		$data['jabatan_list'] = $this->m_ms_jabatan->get_all();
		$data['tingkat_list'] = $this->m_ms_tingkat->get_all();
		$data['periode_penilaian'] = $this->m_ms_periode_penilaian->get_all();
		

		if ($this->access->_update) {
			$data['subtitle'] = 'Keluarga';
			$data['action'] = 'update';
			$data['pegawai'] = $this->m_pr_data->get_by_id($id);
			$data['jabatan'] = $this->m_pr_data->get_jabatan_by_id($id);
			$data['tingkat'] = $this->m_pr_data->get_tingkat_by_id($id);
			$data['anak'] = $this->m_dt_anak->get_by_id_pegawai($id);
			$data['atasan'] = $this->m_dt_atasan->get_by_id($id);
			$data['keluarga'] = $this->m_dt_keluarga->get_by_id($id);
			$this->view('keluarga', $data);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function keluarga_action($action)
	{
		$data = $_POST;
		if ($data != null) {
			switch ($action) {
				case 'update':
					if($this->access->_update){
						$pegawai = array(
							'status_perkawinan' => $data['status_perkawinan']
						);
						$this->m_pr_data->update($data['id_pegawai'],$pegawai);
						$keluarga = array(
							'id_pegawai' => $data['id_pegawai'],
							'nama_pasangan' => $data['nama_pasangan'],
							'nip_pasangan' => $data['nip_pasangan'],
							'pekerjaan_pasangan' => $data['pekerjaan_pasangan'],
							'seri_karis' => $data['seri_karis'],
							'tgl_karis' => $data['tgl_karis'],
							'tgl_karis' => date_to_id($data['tgl_karis']),
							'jml_anak' => $data['jml_anak']
						);
						if($this->m_dt_keluarga->get_by_id($data['id_pegawai'])){
							$this->m_dt_keluarga->update($data['id_pegawai'],$keluarga);
						}else{
							$this->m_dt_keluarga->insert($keluarga);
						};
						foreach ($data['id'] as $key => $val) {
							if($data['nama'][$key] != '' || $data['nama'][$key] != null){
								$anak = array(
									'id_pegawai' => $data['id_pegawai'],
									'nama' => $data['nama'][$key],
									'tgl_lahir' => date_to_id($data['tgl_lahir'][$key])
								);
								echo $val;
								if($val == '' || $val == null){
									//insert
									$this->m_dt_anak->insert($anak);
								}else{
									//update
									$this->m_dt_anak->update($val,$anak);
								}
							}else{
								$this->m_dt_anak->delete_permanent($val);
							}
						}
						insert_log('update',$this->access->menu);
						$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
						redirect(base_url().'pr_data/keluarga/');
					}else{
						redirect(base_url().'ap_error/error_403');
					}
					break;
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function riwayat_pangkat($id = null)
	{
		$id_pegawai = $this->session->userdata('id_pegawai');
		$data['access'] = $this->access;
		$data['golongan'] = $this->m_ms_golongan->get_all();
		$data['riwayat'] = $this->m_dt_riwayat_pangkat->get_by_id_pegawai($id_pegawai);
		$data['pegawai'] = $this->m_pr_data->get_by_id($id_pegawai);

		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Riwayat Pangkat';
				$data['action'] = 'insert';
				$data['pangkat'] = null;
				$this->view('riwayat_pangkat', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Riwayat Pangkat';
				$data['action'] = 'update';
				$data['pangkat'] = $this->m_dt_riwayat_pangkat->get_by_id($id_pegawai);
				$this->view('riwayat_pangkat', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}

	public function riwayat_pangkat_action($action)
	{
		$data = $_POST;
		$data['tmt_riwayat_gol'] = date_to_id($data['tmt_riwayat_gol']);
		$data['tgl_sk'] = date_to_id($data['tgl_sk']);
		switch ($action) {
			case 'insert':
				$this->m_dt_riwayat_pangkat->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_riwayat_pangkat->update($data['id'],$data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_riwayat_pangkat->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().'pr_data/riwayat_pangkat/');
	}

	public function riwayat_pendidikan()
	{
		$id_pegawai = $this->session->userdata('id_pegawai');
		$data['access'] = $this->access;
		$data['golongan'] = $this->m_ms_golongan->get_all();
		$data['riwayat'] = $this->m_dt_riwayat_pendidikan->get_by_id_pegawai($id_pegawai);
		$data['pegawai'] = $this->m_pr_data->get_by_id($id_pegawai);

		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Riwayat Pendidikan';
				$data['action'] = 'insert';
				$data['pendidikan'] = null;
				$this->view('riwayat_pendidikan', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Riwayat Pendidikan';
				$data['action'] = 'update';
				$data['pendidikan'] = $this->m_dt_riwayat_pendidikan->get_by_id($id_pegawai);
				$this->view('riwayat_pendidikan', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}

	public function riwayat_pendidikan_action($action)
	{
		$data = $_POST;
		$data['tgl_lulus'] = date_to_id($data['tgl_lulus']);
		switch ($action) {
			case 'insert':
				$this->m_dt_riwayat_pendidikan->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_riwayat_pendidikan->update($data['id'],$data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_riwayat_pendidikan->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().'pr_data/riwayat_pendidikan/');
	}

	public function riwayat_jabatan($id = null)
	{
		$id_pegawai = $this->session->userdata('id_pegawai');
		$data['access'] = $this->access;
		$data['riwayat'] = $this->m_dt_riwayat_jabatan->get_by_id_pegawai($id_pegawai);
		$data['pegawai'] = $this->m_pr_data->get_by_id($id_pegawai);

		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Riwayat Jabatan';
				$data['action'] = 'insert';
				$data['jabatan'] = null;
				$this->view('riwayat_jabatan', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Riwayat Jabatan';
				$data['action'] = 'update';
				$data['jabatan'] = $this->m_dt_riwayat_jabatan->get_by_id($id_pegawai);
				$this->view('riwayat_jabatan', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}

	public function riwayat_jabatan_action($action)
	{
		$data = $_POST;
		$data['tmt_jabatan'] = date_to_id($data['tmt_jabatan']);
		$data['tgl_sk'] = date_to_id($data['tgl_sk']);
		switch ($action) {
			case 'insert':
				$this->m_dt_riwayat_jabatan->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_riwayat_jabatan->update($data['id'],$data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_riwayat_jabatan->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().'pr_data/riwayat_jabatan/');
	}

	public function riwayat_diklat($jenis=null,$id = null)
	{
		$id_pegawai = $this->session->userdata('id_pegawai');
		$data['access'] = $this->access;
		$data['subtitle'] = 'Riwayat Diklat';
		$data['pegawai'] = $this->m_pr_data->get_by_id($id_pegawai);
		$data['jenis_diklat'] = $this->m_ms_jenis_diklat->get_all();
		$data['riwayat_diklat'] = $this->m_dt_riwayat_diklat->get_by_id_pegawai($id_pegawai);
		$data['riwayat_diklat_baru'] = $this->m_dt_riwayat_diklat_baru->get_by_id_pegawai($id_pegawai);
		$data['riwayat_usulan_diklat'] = $this->m_dt_riwayat_usulan_diklat->get_by_id_pegawai($id_pegawai);

		//action list
		$data['action_diklat'] = 'insert';
		$data['action_diklat_baru'] = 'insert';
		$data['action_usulan_diklat'] = 'insert';

		//list row edit
		$data['diklat'] = null;
		$data['diklat_baru'] = null;
		$data['usulan_diklat'] = null;

		switch ($jenis) {
			case 1:
				$data['action_diklat'] = 'update';
				$data['diklat'] = $this->m_dt_riwayat_diklat->get_by_id($id);
				break;
			case 2:
				$data['action_usulan_diklat'] = 'update';
				$data['usulan_diklat'] = $this->m_dt_riwayat_usulan_diklat->get_by_id($id);
				break;
			case 3:
				$data['action_diklat_baru'] = 'update';
				$data['diklat_baru'] = $this->m_dt_riwayat_diklat_baru->get_by_id($id);
				break;
		}

		$this->view('riwayat_diklat', $data);
	}

	public function riwayat_diklat_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_riwayat_diklat->insert($data);
				$this->session->set_flashdata('status_diklat', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_riwayat_diklat->update($data['id'],$data);
				$this->session->set_flashdata('status_diklat', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_riwayat_diklat->delete_permanent($data['id']);
				$this->session->set_flashdata('status_diklat', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().'pr_data/riwayat_diklat/');
	}

	public function riwayat_usulan_diklat_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_riwayat_usulan_diklat->insert($data);
				$this->session->set_flashdata('status_usulan', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_riwayat_usulan_diklat->update($data['id'],$data);
				$this->session->set_flashdata('status_usulan', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_riwayat_usulan_diklat->delete_permanent($data['id']);
				$this->session->set_flashdata('status_usulan', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().'pr_data/riwayat_diklat/');
	}

	public function riwayat_diklat_baru_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_riwayat_diklat_baru->insert($data);
				$this->session->set_flashdata('status_baru', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_riwayat_diklat_baru->update($data['id'],$data);
				$this->session->set_flashdata('status_baru', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_riwayat_diklat_baru->delete_permanent($data['id']);
				$this->session->set_flashdata('status_baru', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().'pr_data/riwayat_diklat/');
	}

	public function riwayat_penghargaan($id = null)
	{
		$id_pegawai = $this->session->userdata('id_pegawai');
		$data['access'] = $this->access;
		$data['riwayat'] = $this->m_dt_riwayat_penghargaan->get_by_id_pegawai($id_pegawai);
		$data['pegawai'] = $this->m_pr_data->get_by_id($id_pegawai);

		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Riwayat Jabatan';
				$data['action'] = 'insert';
				$data['penghargaan'] = null;
				$this->view('riwayat_penghargaan', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Riwayat Jabatan';
				$data['action'] = 'update';
				$data['penghargaan'] = $this->m_dt_riwayat_penghargaan->get_by_id($id_pegawai);
				$this->view('riwayat_penghargaan', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}

	public function riwayat_penghargaan_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_riwayat_penghargaan->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_riwayat_penghargaan->update($data['id'],$data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_riwayat_penghargaan->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().'pr_data/riwayat_penghargaan/');
	}

	public function riwayat_hukuman($id = null)
	{
		$id_pegawai = $this->session->userdata('id_pegawai');
		$data['access'] = $this->access;
		$data['riwayat'] = $this->m_dt_riwayat_hukuman->get_by_id_pegawai($id_pegawai);
		$data['pegawai'] = $this->m_pr_data->get_by_id($id_pegawai);
		$data['hukuman_disiplin'] = $this->m_ms_hukuman_disiplin->get_all();
		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Riwayat Jabatan';
				$data['action'] = 'insert';
				$data['hukuman'] = null;
				$this->view('riwayat_hukuman', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Riwayat Jabatan';
				$data['action'] = 'update';
				$data['hukuman'] = $this->m_dt_riwayat_hukuman->get_by_id($id_pegawai);
				$this->view('riwayat_hukuman', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}

	public function riwayat_hukuman_action($action)
	{
		$data = $_POST;
		$data['tmt_hukuman'] = date_to_id($data['tmt_hukuman']);
		switch ($action) {
			case 'insert':
				$this->m_dt_riwayat_hukuman->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_riwayat_hukuman->update($data['id'],$data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_riwayat_hukuman->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().'pr_data/riwayat_hukuman/');
	}

	public function riwayat_ppk($id = null)
	{
		$id_pegawai = $this->session->userdata('id_pegawai');
		$data['access'] = $this->access;
		$data['riwayat'] = $this->m_dt_riwayat_ppk->get_by_id_pegawai($id_pegawai);
		$data['pegawai'] = $this->m_pr_data->get_by_id($id_pegawai);

		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Riwayat PPK';
				$data['action'] = 'insert';
				$data['ppk'] = null;
				$this->view('riwayat_ppk', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Riwayat PPK';
				$data['action'] = 'update';
				$data['ppk'] = $this->m_dt_riwayat_ppk->get_by_id($id);
				$this->view('riwayat_ppk', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}

	public function riwayat_ppk_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_riwayat_ppk->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_riwayat_ppk->update($data['id'],$data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_riwayat_ppk->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().'pr_data/riwayat_ppk/');
	}

	public function skp($id = null)
	{
		$id_pegawai = $this->session->userdata('id_pegawai');
		$data['access'] = $this->access;
		$data['riwayat'] = $this->m_dt_tgs_pokok->get_by_id_pegawai($id_pegawai);
		$data['pegawai'] = $this->m_pr_data->get_by_id($id_pegawai);

		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Sasaran Kerja Pegawai';
				$data['action'] = 'insert';
				$data['skp'] = null;
				$this->view('skp', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Sasaran Kerja Pegawai';
				$data['action'] = 'update';
				$data['skp'] = $this->m_dt_tgs_pokok->get_by_id($id);
				$this->view('skp', $data);
			} else {
				redirect(base_url().'ap_error/error_403');
			}
		}
	}

	public function skp_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_tgs_pokok->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_tgs_pokok->update($data['id'],$data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_tgs_pokok->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().'pr_data/skp/');
	}

	public function realisasi($tipe = null,$id = null)
	{
		$id_pegawai = $this->session->userdata('id_pegawai');
		$data['access'] = $this->access;
		$data['subtitle'] = 'Sasaran Kerja Pegawai';
		$data['riwayat_tgs_pokok'] = $this->m_dt_tgs_pokok->get_by_id_pegawai($id_pegawai);
		$data['riwayat_tgs_tambahan'] = $this->m_dt_tgs_tambahan->get_by_id_pegawai($id_pegawai);
		$data['riwayat_tgs_kreatifitas'] = $this->m_dt_tgs_kreatifitas->get_by_id_pegawai($id_pegawai);
		$data['pegawai'] = $this->m_pr_data->get_by_id($id_pegawai);

		//action list
		$data['action_tgs_pokok'] = '';
		$data['action_tgs_tambahan'] = 'insert';
		$data['action_tgs_kreatifitas'] = 'insert';

		// list row edit
		$data['tgs_pokok'] = null;
		$data['tgs_tambahan'] = null;
		$data['tgs_kreatifitas'] = null;

		switch ($tipe) {
			case 1:
				$data['action_tgs_pokok'] = 'update';
				$data['tgs_pokok'] = $this->m_dt_tgs_pokok->get_by_id($id);
				break;
			case 2:
				$data['action_tgs_tambahan'] = 'update';
				$data['tgs_tambahan'] = $this->m_dt_tgs_tambahan->get_by_id($id);
				break;
			case 3:
				$data['action_tgs_kreatifitas'] = 'update';
				$data['tgs_kreatifitas'] = $this->m_dt_tgs_kreatifitas->get_by_id($id);
				break;
		}

		$this->view('realisasi', $data);
	}

	public function realisasi_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_tgs_pokok->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_tgs_pokok->update($data['id'],$data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_tgs_pokok->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().'pr_data/realisasi/');
	}

	public function tgs_tambahan_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_tgs_tambahan->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_tgs_tambahan->update($data['id'],$data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_tgs_tambahan->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().'pr_data/realisasi/');
	}

	public function tgs_kreatifitas_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_tgs_kreatifitas->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_tgs_kreatifitas->update($data['id'],$data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_tgs_kreatifitas->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().'pr_data/realisasi/');
	}

	public function perilaku_kerja($tipe = null,$id = null)
	{
		$id_pegawai = $this->session->userdata('id_pegawai');
		$data['access'] = $this->access;
		$data['subtitle'] = 'Perilaku';
		$data['riwayat_perilaku_kerja'] = $this->m_dt_perilaku_kerja->get_by_id_pegawai($id_pegawai);
		$data['riwayat_proses_penilaian'] = $this->m_dt_proses_penilaian->get_by_id_pegawai($id_pegawai);
		$data['pegawai'] = $this->m_pr_data->get_by_id($id_pegawai);

		//action list
		$data['action_perilaku_kerja'] = 'insert';
		$data['action_proses_penilaian'] = 'update';

		//list row edit
		$data['perilaku_kerja'] = null;
		$data['proses_penilaian'] = null;

		switch ($tipe) {
			case 1:
				$data['action_perilaku_kerja'] = 'update';
				$data['perilaku_kerja'] = $this->m_dt_perilaku_kerja->get_by_id($id);
				break;
			case 2:
				$data['action_proses_penilaian'] = 'update';
				$data['proses_penilaian'] = $this->m_dt_proses_penilaian->get_by_id($id);
				break;
		}

		$this->view('perilaku_kerja', $data);
	}

	public function perilaku_kerja_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_perilaku_kerja->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$this->m_dt_perilaku_kerja->update($data['id'],$data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_perilaku_kerja->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().'pr_data/perilaku_kerja/');
	}

	public function proses_penilaian_action($action)
	{
		$data = $_POST;
		$data['tgl_keberatan'] = date_to_id($data['tgl_keberatan']);
		$data['tgl_tanggapan'] = date_to_id($data['tgl_tanggapan']);
		$data['tgl_kep_penilaian'] = date_to_id($data['tgl_kep_penilaian']);
		$data['tgl_rekomendasi'] = date_to_id($data['tgl_rekomendasi']);
		switch ($action) {
			case 'insert':
				$this->m_dt_proses_penilaian->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$exist = $this->m_dt_proses_penilaian->get_by_id_pegawai($data['id_pegawai']);
				if ($exist != null) {
					$this->m_dt_proses_penilaian->update($data['id_pegawai'],$data);
				}else{
					$this->m_dt_proses_penilaian->insert($data);
				}
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_proses_penilaian->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().'pr_data/perilaku_kerja/');
	}

	public function tugas_belajar($tipe = null,$id = null)
	{
		$id_pegawai = $this->session->userdata('id_pegawai');
		$data['access'] = $this->access;
		$data['subtitle'] = 'Perilaku';
		$data['riwayat_perilaku_kerja_tubel'] = $this->m_dt_perilaku_kerja_tubel->get_by_id_pegawai($id_pegawai);
		$data['riwayat_proses_penilaian'] = $this->m_dt_proses_penilaian->get_by_id_pegawai($id_pegawai);
		$data['pegawai'] = $this->m_pr_data->get_by_id($id_pegawai);

		//action list
		$data['action_perilaku_kerja_tubel'] = 'update';
		$data['action_proses_penilaian'] = 'update';

		//list row edit
		$data['perilaku_kerja_tubel'] = null;
		$data['proses_penilaian'] = null;

		switch ($tipe) {
			case 1:
				$data['action_perilaku_kerja_tubel'] = 'update';
				$data['perilaku_kerja_tubel'] = $this->m_dt_perilaku_kerja_tubel->get_by_id($id);
				break;
			case 2:
				$data['action_proses_penilaian'] = 'update';
				$data['proses_penilaian'] = $this->m_dt_proses_penilaian->get_by_id($id);
				break;
		}

		$this->view('tugas_belajar', $data);
	}

	public function perilaku_kerja_tubel_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_perilaku_kerja_tubel->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$exist = $this->m_dt_perilaku_kerja_tubel->get_by_id_pegawai($data['id_pegawai']);
				if ($exist != null) {
					$this->m_dt_perilaku_kerja_tubel->update($data['id_pegawai'],$data);
				}else{
					$this->m_dt_perilaku_kerja_tubel->insert($data);
				}
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_perilaku_kerja_tubel->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().'pr_data/tugas_belajar/');
	}

	public function proses_penilaian_tubel_action($action)
	{
		$data = $_POST;
		$data['tgl_keberatan'] = date_to_id($data['tgl_keberatan']);
		$data['tgl_tanggapan'] = date_to_id($data['tgl_tanggapan']);
		$data['tgl_kep_penilaian'] = date_to_id($data['tgl_kep_penilaian']);
		$data['tgl_rekomendasi'] = date_to_id($data['tgl_rekomendasi']);
		switch ($action) {
			case 'insert':
				$this->m_dt_proses_penilaian->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$exist = $this->m_dt_proses_penilaian->get_by_id_pegawai($data['id_pegawai']);
				if ($exist != null) {
					$this->m_dt_proses_penilaian->update($data['id_pegawai'],$data);
				}else{
					$this->m_dt_proses_penilaian->insert($data);
				}
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_proses_penilaian->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().'pr_data/perilaku_kerja/');
	}

	public function penilaian_gabung($tipe = null,$id = null)
	{
		$id_pegawai = $this->session->userdata('id_pegawai');
		$data['access'] = $this->access;
		$data['subtitle'] = 'Perilaku';
		$data['riwayat_perilaku_kerja_manual'] = $this->m_dt_perilaku_kerja_manual->get_by_id_pegawai($id_pegawai);
		$data['riwayat_proses_penilaian'] = $this->m_dt_proses_penilaian->get_by_id_pegawai($id_pegawai);
		$data['pegawai'] = $this->m_pr_data->get_by_id($id_pegawai);

		//action list
		$data['action_perilaku_kerja_manual'] = 'update';
		$data['action_proses_penilaian'] = 'update';

		//list row edit
		$data['perilaku_kerja_manual'] = null;
		$data['proses_penilaian'] = null;

		switch ($tipe) {
			case 1:
				$data['action_perilaku_kerja_manual'] = 'update';
				$data['perilaku_kerja_manual'] = $this->m_dt_perilaku_kerja_tubel->get_by_id($id);
				break;
			case 2:
				$data['action_proses_penilaian'] = 'update';
				$data['proses_penilaian'] = $this->m_dt_proses_penilaian->get_by_id($id);
				break;
		}

		$this->view('penilaian_gabung', $data);
	}

	public function perilaku_kerja_manual_action($action)
	{
		$data = $_POST;
		switch ($action) {
			case 'insert':
				$this->m_dt_perilaku_kerja_manual->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$exist = $this->m_dt_perilaku_kerja_manual->get_by_id_pegawai($data['id_pegawai']);
				if ($exist != null) {
					$this->m_dt_perilaku_kerja_manual->update($data['id_pegawai'],$data);
				}else{
					$this->m_dt_perilaku_kerja_manual->insert($data);
				}
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_perilaku_kerja_manual->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().'pr_data/penilaian_gabung/');
	}

	public function proses_penilaian_manual_action($action)
	{
		$data = $_POST;
		$data['tgl_keberatan'] = date_to_id($data['tgl_keberatan']);
		$data['tgl_tanggapan'] = date_to_id($data['tgl_tanggapan']);
		$data['tgl_kep_penilaian'] = date_to_id($data['tgl_kep_penilaian']);
		$data['tgl_rekomendasi'] = date_to_id($data['tgl_rekomendasi']);
		switch ($action) {
			case 'insert':
				$this->m_dt_proses_penilaian->insert($data);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
				break;

			case 'update':
				$exist = $this->m_dt_proses_penilaian->get_by_id_pegawai($data['id_pegawai']);
				if ($exist != null) {
					$this->m_dt_proses_penilaian->update($data['id_pegawai'],$data);
				}else{
					$this->m_dt_proses_penilaian->insert($data);
				}
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				break;

			case 'delete':
				$this->m_dt_proses_penilaian->delete_permanent($data['id']);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				break;
		}
		insert_log('update',$this->access->menu);
		redirect(base_url().'pr_data/penilaian_gabung/');
	}

	public function delete()
	{
		$data = $_POST;
		$this->m_pr_data->delete_temp($data['id_pegawai']);
		redirect(base_url().$this->access->controller);
	}

}
