<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <?php $this->load->view('pr_data/sidebar'); ?>
        </div>
        <div class="col-md-9">
          <?php $this->load->view('pr_data/header'); ?>
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="update"><a href="<?=base_url().$access->controller?>/data_pokok">Data Pokok</a></li>
              <li class="update"><a href="<?=base_url().$access->controller?>/posisi_jabatan">Posisi dan Jabatan</a></li>
              <li class="active"><a href="<?=base_url().$access->controller?>/keluarga">Keluarga</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                <?php echo $this->session->flashdata('status'); ?>
                <h4>Keluarga</h4>
                <form id="form_keluarga" class="form-horizontal" method="post" action="<?=base_url()?>pr_data/keluarga_action/<?=$action?>" autocomplete="off">
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Status Perkawinan</label>
                    <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
                    <div class="col-sm-2">
                      <select class="form-control select2" name="status_perkawinan" id="status_perkawinan">
                        <option value="0" <?php if($pegawai){if($pegawai->status_perkawinan == '0'){echo 'selected';}}?>> -- Pilih -- </option>
                        <option value="1" <?php if($pegawai){if($pegawai->status_perkawinan == '1'){echo 'selected';}}?>> Belum Kawin </option>
                        <option value="2" <?php if($pegawai){if($pegawai->status_perkawinan == '2'){echo 'selected';}}?>> Kawin </option>
                        <option value="D" <?php if($pegawai){if($pegawai->status_perkawinan == 'D'){echo 'selected';}}?>> Duda </option>
                        <option value="J" <?php if($pegawai){if($pegawai->status_perkawinan == 'J'){echo 'selected';}}?>> Janda </option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Nama Pasangan</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control input-sm" name="nama_pasangan" id="nama_pasangan" value="<?php if($keluarga){echo $keluarga->nama_pasangan;}?>" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">NIP Pasangan</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control input-sm" name="nip_pasangan" id="nip_pasangan" value="<?php if($keluarga){echo $keluarga->nip_pasangan;}?>" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Pekerjaan</label>
                    <div class="col-sm-3">
                      <select class="form-control select2" name="pekerjaan_pasangan" id="pekerjaan_pasangan">
                        <option value="0" <?php if($keluarga){if($keluarga->pekerjaan_pasangan == '0'){echo 'selected';}}?>> -- Pilih -- </option>
                        <option value="1" <?php if($keluarga){if($keluarga->pekerjaan_pasangan == '1'){echo 'selected';}}?>> PNS </option>
                        <option value="2" <?php if($keluarga){if($keluarga->pekerjaan_pasangan == '2'){echo 'selected';}}?>> TNI / POLRI </option>
                        <option value="3" <?php if($keluarga){if($keluarga->pekerjaan_pasangan == '3'){echo 'selected';}}?>> Pengusaha / Wira Swasta </option>
                        <option value="4" <?php if($keluarga){if($keluarga->pekerjaan_pasangan == '4'){echo 'selected';}}?>> Petani/Nelayan </option>
                        <option value="5" <?php if($keluarga){if($keluarga->pekerjaan_pasangan == '5'){echo 'selected';}}?>> Karyawan Swasta </option>
                        <option value="6" <?php if($keluarga){if($keluarga->pekerjaan_pasangan == '6'){echo 'selected';}}?>> Mengurus Rumah Tangga </option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">No. Seri Karis / Karsu</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control input-sm" name="seri_karis" id="seri_karis" value="<?php if($keluarga){echo $keluarga->seri_karis;}?>" required>
                    </div>
                    <label class="col-md-2 control-label" style="width:max-content !important;">Tanggal</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control input-sm datepicker" name="tgl_karis" id="tgl_karis" value="<?php if($keluarga){if($keluarga->tgl_karis != ''){echo date_to_id($keluarga->tgl_karis);}}?>" placeholder="dd-mm-yyyy" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Jumlah Anak</label>
                    <div class="col-md-1">
                      <input type="text" class="form-control input-sm" name="jml_anak" id="jml_anak" value="<?php if($keluarga){echo $keluarga->jml_anak;}?>" required>
                    </div>
                  </div>
                  <h4>Data Anak (Yang Mendapat Tunjangan)</h4>
                  <div class="form-group">
                    <input type="hidden" name="id[]" id="id[]" value="<?php if($anak){echo @$anak[0]['id'];}?>">
                    <label class="col-md-2 control-label">Nama Anak</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control input-sm" name="nama[]" id="nama[]" value="<?php if($anak){echo @$anak[0]['nama'];}?>">
                    </div>
                    <label class="col-md-2 control-label" style="width:max-content !important;">Tanggal Lahir</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control input-sm datepicker" name="tgl_lahir[]" id="tgl_lahir[]" value="<?php if($anak){echo date_to_id(@$anak[0]['tgl_lahir']);}?>" placeholder="dd-mm-yyyy">
                    </div>
                  </div>
                  <div class="form-group">
                    <input type="hidden" name="id[]" id="id[]" value="<?php if($anak){echo @$anak[1]['id'];}?>">
                    <label class="col-md-2 control-label">Nama Anak</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control input-sm" name="nama[]" id="nama[]" value="<?php if($anak){echo @$anak[1]['nama'];}?>">
                    </div>
                    <label class="col-md-2 control-label" style="width:max-content !important;">Tanggal Lahir</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control input-sm datepicker" name="tgl_lahir[]" id="tgl_lahir[]" value="<?php if($anak){echo date_to_id(@$anak[1]['tgl_lahir']);}?>" placeholder="dd-mm-yyyy">
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <div class="col-md-offset-2 col-sm-8">
                      <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
        </div>
      </div>
    </section>
  </div>
<!-- /.content-wrapper -->
<!-- /script -->
<script>
  $(document).ready(function () {
    // form validator
    $('#form_data_pegawai').validate({
      rules:{
        no_hp : {
          number : true
        },
        agama : {
          valueNotEquals : '0'
        },
        jenis_kelamin : {
          valueNotEquals : '0'
        },
        nik : {
          number : true
        },
        nip_baru : {
          number : true
        },
        nip_lama : {
          number : true
        },
        kode_pos_rumah : {
          number : true
        },
        thn_lulus_pend : {
          number : true
        },
        tingkat_pend_cpns : {
          valueNotEquals : '0'
        },
        tingkat_pend_akhir : {
          valueNotEquals : '0'
        },
        id_golongan : {
          valueNotEquals : '0'
        },
        masakerja_gol_thn : {
          number : true
        },
        masakerja_gol_bln : {
          number : true
        },
        npwp : {
          number : true
        },
        id_kedudukan_pns : {
          valueNotEquals : '0'
        }
      },
      submitHandler: function(form) {
        $(form).ajaxSubmit();
      }
    }); 
    //status pu pns
    <?php if($action == 'insert'): ?>
      $('.no_registrasi_pupns').hide();
      $('.update').hide();
    <?php else:?>
      if ($('#status_pupns_2015').val() == 0) {
        $('.no_registrasi_pupns').hide();
      }else{
        $('.no_registrasi_pupns').show();
      }
    <?php endif;?>
    $('#status_pupns_2015').on('change', function() {
      if(this.value == 0){
        $('.no_registrasi_pupns').hide();
      }else{
        $('.no_registrasi_pupns').show();
      }
    });
  })
</script>