<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
      <small><?=$subtitle?></small>
      <div class="label bg-orange pull-right"><i class="fa fa-database"></i> Jumlah Keseluruhan Data : <?=$num_rows_total?></div>
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box box-primary">
      <div class="box-body table-responsive">
        <a class="btn btn-sm btn-primary pull-left" href="<?=base_url()?>dt_pns/data_pokok/"><i class="fa fa-plus"></i> Tambah</a>
        <div>
          <form action="<?=base_url()?>dt_pns/index/" method="post">
            <div class="input-group input-group-sm pull-right" style="width: 250px;">
              <input type="text" name="term" class="form-control pull-right" placeholder="Pencarian" value="">
              <div class="input-group-btn">
                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                <a class="btn btn-default" href="<?=base_url()?>dt_pns/reset_search/"><i class="fa fa-refresh"></i></a>
              </div>
            </div>
          </form>
        </div>
        <br><br>
        <?php echo $this->session->flashdata('status'); ?>
        <?php if ($this->session->userdata('search')): ?>
          <i class="search_result">Hasil pencarian dengan kata kunci: <b><?=$search['term'];?></b></i><br><br>
        <?php endif; ?>
        <table class="table table-striped table-bordered table-condensed">
          <thead>
            <tr>
              <th class="text-center" width="30">No.</th>
              <th class="text-center" width="60">Aksi</th>
              <th class="text-center" width="60">NIP</th>
              <th class="text-center" >Nama</th>
              <th class="text-center" width="10">JK</th>
              <th class="text-center" width="150">TTL</th>
              <th class="text-center" width="40">Usia</th>
              <th class="text-center" width="">Jabatan</th>
              <th class="text-center" width="20">Eselon</th>
              <th class="text-center" width="">Pangkat</th>
              <th class="text-center" width="10">Gol</th>
              <th class="text-center" width="10">Status</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($pegawai != null): ?>
              <?php $i=1;foreach ($pegawai as $row): ?>
                <tr>
                  <?php 
                    $status_pegawai = '';
		              	switch ($row->status_pegawai) {
                      case 1:
                        $status_pegawai = 'CPNS';
                        break;
                      case 2:
                        $status_pegawai = 'PNS';
                        break;
                      case 3:
                        $status_pegawai = 'BLUD';
                        break;
                      case 4:
                        $status_pegawai = 'P3K';
                        break;
                      case 5:
                        $status_pegawai = 'THL';
                        break;
                    }
                  ?>
                  <td class="text-center"><?=$this->uri->segment('4')+$i++?></td>
                  <td class="text-center">
                    <a class="text-orange" href="<?=base_url()?>dt_pns/data_pokok/<?=$row->id_pegawai?>" style="padding-right:0.5em;"><i class="fa fa-pencil"></i></a> 
                    <a class="text-red" href="#" onclick="del('<?=$row->id_pegawai?>')"><i class="fa fa-trash"></i></a>
                  </td>
                  <td><?=$row->nomor_induk?></td>
                  <td>
                    <?php 
                      echo $row->gelar_depan.' '.$row->nama;
                      if ($row->gelar_belakang != ''){
                        echo ', '.$row->gelar_belakang;
                      }
                    ?></td>
                  <td class="text-center"><?php echo $jk = ($row->jenis_kelamin == '1') ? 'L' : 'P';?></td>
                  <td><?=$row->tempat_lhr.', '.date_to_id($row->tgl_lhr)?></td>
                  <td class="text-center"><?php if($row->tgl_lhr != ''){echo hitung_umur($row->tgl_lhr);}?></td>
                  <td><?=$row->jabatan?></td>
                  <td class="text-center"><?php echo $eselon = ($row->eselon != '') ? $row->eselon : '-' ;?></td>
                  <td><?=$row->pangkat?></td>
                  <td class="text-center"><?=$row->golongan?></td>
                  <td class="text-center"><?= $status_pegawai ?></td>
                </tr>
              <?php endforeach; ?>
            <?php else: ?>
              <tr>
                <td class="text-center" colspan="99">Tidak ada data!</td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <span class="badge bg-green">Jumlah Data : <?=$num_rows?></span>
        <?php echo $this->pagination->create_links(); ?>
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- Modal Delete -->
<div id="modal_delete" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>dt_pns/delete" method="post">
        <input type="hidden" name="id_pegawai" id="id_pegawai">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
  function del(id) {
    $("#modal_delete").modal('show');
    $("#id_pegawai").val(id);
  }
</script>