<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <?php $this->load->view('pr_data/sidebar'); ?>
        </div>
        <div class="col-md-9">
          <?php $this->load->view('pr_data/header'); ?>
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title text-orange">Riwayat Diklat</h3> 
            </div>
            <div class="box-body">
              <?php echo $this->session->flashdata('status_diklat'); ?>
              <form id="form_diklat" class="form-horizontal" action="<?=base_url().$access->controller?>/riwayat_diklat_action/<?=$action_diklat?>" method="post" autocomplete="off"> 
                <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
                <input type="hidden" name="id" id="id" value="<?php if($diklat){echo $diklat->id;}?>">
                <div class="form-group">
                  <label class="col-md-2 control-label">Nama Diklat</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control input-sm" name="nama_diklat" id="nama_diklat" value="<?php if($diklat){echo $diklat->nama_diklat;}?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Tahun Penyelenggaraan</label>
                  <div class="col-md-2">
                    <input type="text" class="form-control input-sm" name="thn_diklat" id="thn_diklat" value="<?php if($diklat){echo $diklat->thn_diklat;}?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Lama Diklat</label>
                  <div class="col-md-1">
                    <input type="text" class="form-control input-sm" name="lama_bln" id="lama_bln" value="<?php if($diklat){echo $diklat->lama_bln;}else{echo '0';}?>">
                  </div>
                  <label class="col-md-2 control-label" style="width:max-content !important;">Bulan</label>
                  <div class="col-md-1">
                    <input type="text" class="form-control input-sm" name="lama_hari" id="lama_hari" value="<?php if($diklat){echo $diklat->lama_hari;}else{echo '0';}?>">
                  </div>
                  <label class="col-md-2 control-label" style="width:max-content !important;">Hari</label>
                  <div class="col-md-1">
                    <input type="text" class="form-control input-sm" name="lama_jam" id="lama_jam" value="<?php if($diklat){echo $diklat->lama_jam;}else{echo '0';}?>">
                  </div>
                  <label class="col-md-2 control-label" style="width:max-content !important;">Jam</label>
                </div>
                <hr>
                <div class="form-group">
                  <div class="col-md-offset-2 col-sm-8">
                    <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
                  </div>
                </div>
                <hr>
              </form>
              <table class="table table-striped table-bordered table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" width="30">No.</th>
                    <th class="text-center" width="60">Aksi</th>
                    <th class="text-center" width="">Nama Diklat</th>
                    <th class="text-center" width="80">Tahun</th>
                    <th class="text-center" width="200">Lama Diklat</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if ($riwayat_diklat != null): ?>
                    <?php $i=1;foreach ($riwayat_diklat as $row): ?>
                      <tr>
                        <td class="text-center"><?=$i++?></td>
                        <td class="text-center">
                          <?php if($row->id != 0): ?>
                            <a class="text-orange" href="<?=base_url()?>pr_data/riwayat_diklat/1/<?=$row->id?>" style="padding-right:0.250em;"><i class="fa fa-pencil"></i></a> 
                            <a class="text-red" href="#" onclick="del_diklat(<?=$row->id?>)"><i class="fa fa-trash"></i></a>
                          <?php endif;?>
                        </td>
                        <td><?=$row->nama_diklat?></td>
                        <td class="text-center"><?=$row->thn_diklat?></td>
                        <td class="text-center"><?=$row->lama_bln.' bulan '.$row->lama_hari.' hari '.$row->lama_jam.' jam'?></td>
                      </tr>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <tr>
                      <td class="text-center" colspan="99">Data tidak ada!</td>
                    </tr>
                  <?php endif; ?>
                </tbody>
              </table>
            </div>
          </div>
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title text-orange">Usulan Diklat</h3> 
            </div>
            <div class="box-body">
              <?php echo $this->session->flashdata('status_usulan'); ?>
              <form id="form_usulan_diklat" class="form-horizontal" action="<?=base_url().$access->controller?>/riwayat_usulan_diklat_action/<?=$action_usulan_diklat?>" method="post" autocomplete="off"> 
                <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
                <input type="hidden" name="id" id="id" value="<?php if($usulan_diklat){echo $usulan_diklat->id;}?>">
                <div class="form-group">
                  <label class="col-md-2 control-label">Nama Diklat</label>
                  <div class="col-md-5">
                    <select class="form-control select2" name="id_nama_diklat" id="id_nama_diklat">
                      <option value="0">-- Pilih --</option>
                      <?php foreach($jenis_diklat as $row): ?>
                        <option value="<?=$row->id_diklat?>" <?php if($usulan_diklat){if($usulan_diklat->id_nama_diklat == $row->id_diklat){echo 'selected';}}?>><?=$row->nama_diklat?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Tahun</label>
                  <div class="col-md-2">
                    <input type="text" class="form-control input-sm" name="thn" id="thn" value="<?php if($usulan_diklat){echo $usulan_diklat->thn;}?>" required>
                  </div>
                </div>
                <hr>
                <div class="form-group">
                  <div class="col-md-offset-2 col-sm-8">
                    <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
                  </div>
                </div>
                <hr>
              </form>
              <table class="table table-striped table-bordered table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" width="30">No.</th>
                    <th class="text-center" width="60">Aksi</th>
                    <th class="text-center" width="">Nama Diklat</th>
                    <th class="text-center" width="80">Tahun</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if ($riwayat_usulan_diklat != null): ?>
                    <?php $i=1;foreach ($riwayat_usulan_diklat as $row): ?>
                      <tr>
                        <td class="text-center"><?=$i++?></td>
                        <td class="text-center">
                          <?php if($row->id != 0): ?>
                            <a class="text-orange" href="<?=base_url()?>pr_data/riwayat_diklat/2 /<?=$row->id?>" style="padding-right:0.250em;"><i class="fa fa-pencil"></i></a> 
                            <a class="text-red" href="#" onclick="del_usulan_diklat(<?=$row->id?>)"><i class="fa fa-trash"></i></a>
                          <?php endif;?>
                        </td>
                        <td><?=$row->nama_diklat?></td>
                        <td class="text-center"><?=$row->thn?></td>
                      </tr>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <tr>
                      <td class="text-center" colspan="99">Data tidak ada!</td>
                    </tr>
                  <?php endif; ?>
                </tbody>
              </table>
            </div>
          </div>
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title text-orange">Usulan Diklat Baru yang belum ada di database</h3> 
            </div>
            <div class="box-body">
              <?php echo $this->session->flashdata('status_baru'); ?>
              <form id="form_diklat_baru" class="form-horizontal" action="<?=base_url().$access->controller?>/riwayat_diklat_baru_action/<?=$action_diklat_baru?>" method="post" autocomplete="off"> 
                <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
                <input type="hidden" name="id" id="id" value="<?php if($diklat_baru){echo $diklat_baru->id;}?>">
                <div class="form-group">
                  <label class="col-md-2 control-label">Nama Diklat</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control input-sm" name="nama_diklat" id="nama_diklat" value="<?php if($diklat_baru){echo $diklat_baru->nama_diklat;}?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Tahun Penyelenggaraan</label>
                  <div class="col-md-2">
                    <input type="text" class="form-control input-sm" name="thn_diklat" id="thn_diklat" value="<?php if($diklat_baru){echo $diklat_baru->thn_diklat;}?>" required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label">Lama Diklat</label>
                  <div class="col-md-1">
                    <input type="text" class="form-control input-sm" name="lama_bln" id="lama_bln" value="<?php if($diklat_baru){echo $diklat_baru->lama_bln;}else{echo '0';}?>">
                  </div>
                  <label class="col-md-2 control-label" style="width:max-content !important;">Bulan</label>
                  <div class="col-md-1">
                    <input type="text" class="form-control input-sm" name="lama_hari" id="lama_hari" value="<?php if($diklat_baru){echo $diklat_baru->lama_hari;}else{echo '0';}?>">
                  </div>
                  <label class="col-md-2 control-label" style="width:max-content !important;">Hari</label>
                  <div class="col-md-1">
                    <input type="text" class="form-control input-sm" name="lama_jam" id="lama_jam" value="<?php if($diklat_baru){echo $diklat_baru->lama_jam;}else{echo '0';}?>">
                  </div>
                  <label class="col-md-2 control-label" style="width:max-content !important;">Jam</label>
                </div>
                <hr>
                <div class="form-group">
                  <div class="col-md-offset-2 col-sm-8">
                    <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
                  </div>
                </div>
                <hr>
              </form>
              <table class="table table-striped table-bordered table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" width="30">No.</th>
                    <th class="text-center" width="60">Aksi</th>
                    <th class="text-center" width="">Nama Diklat</th>
                    <th class="text-center" width="80">Tahun</th>
                    <th class="text-center" width="200">Lama Diklat</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if ($riwayat_diklat_baru != null): ?>
                    <?php $i=1;foreach ($riwayat_diklat_baru as $row): ?>
                      <tr>
                        <td class="text-center"><?=$i++?></td>
                        <td class="text-center">
                          <?php if($row->id != 0): ?>
                            <a class="text-orange" href="<?=base_url()?>pr_data/riwayat_diklat/3/<?=$row->id?>" style="padding-right:0.250em;"><i class="fa fa-pencil"></i></a> 
                            <a class="text-red" href="#" onclick="del_diklat_baru(<?=$row->id?>)"><i class="fa fa-trash"></i></a>
                          <?php endif;?>
                        </td>
                        <td><?=$row->nama_diklat?></td>
                        <td class="text-center"><?=$row->thn_diklat?></td>
                        <td class="text-center"><?=$row->lama_bln.' bulan '.$row->lama_hari.' hari '.$row->lama_jam.' jam'?></td>
                      </tr>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <tr>
                      <td class="text-center" colspan="99">Data tidak ada!</td>
                    </tr>
                  <?php endif; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
<!-- /.content-wrapper -->
<!-- Modal Delete -->
<div id="modal_delete_diklat" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>pr_data/riwayat_diklat_action/delete" method="post">
        <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
        <input type="hidden" name="id" id="id_delete_diklat">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Modal Delete -->
<div id="modal_delete_usulan_diklat" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>pr_data/riwayat_usulan_diklat_action/delete" method="post">
        <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
        <input type="hidden" name="id" id="id_delete_usulan_diklat">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Modal Delete -->
<div id="modal_delete_diklat_baru" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>pr_data/riwayat_diklat_baru_action/delete" method="post">
        <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
        <input type="hidden" name="id" id="id_delete_diklat_baru">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /script -->
<script>
  $(document).ready(function () {
    $('#form_diklat').validate({
      rules : {
        lama_bln : {
          number : true
        },
        lama_hari : {
          number : true
        },
        lama_jam : {
          number : true
        },
        thn_diklat : {
          number : true
        }
      }
    })
    $('#form_usulan_diklat').validate({
      rules : {
        id_nama_diklat : {
          valueNotEquals : '0'
        },
        thn : {
          number : true
        }
      }
    })
  })
  $('#form_diklat_baru').validate({
      rules : {
        lama_bln : {
          number : true
        },
        lama_hari : {
          number : true
        },
        lama_jam : {
          number : true
        },
        thn_diklat : {
          number : true
        }
      }
    })
  function del_diklat(id) {
    $("#modal_delete_diklat").modal('show');
    $("#id_delete_diklat").val(id);
  }
  function del_usulan_diklat(id) {
    $("#modal_delete_usulan_diklat").modal('show');
    $("#id_delete_usulan_diklat").val(id);
  }
  function del_diklat_baru(id) {
    $("#modal_delete_diklat_baru").modal('show');
    $("#id_delete_diklat_baru").val(id);
  }
</script>