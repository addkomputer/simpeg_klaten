<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <?php $this->load->view('pr_data/sidebar'); ?>
        </div>
        <div class="col-md-9">
          <?php $this->load->view('pr_data/header'); ?>
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="update"><a href="<?=base_url().$access->controller?>/data_pokok">Data Pokok</a></li>
              <li class="active"><a href="<?=base_url().$access->controller?>/posisi_jabatan">Posisi dan Jabatan</a></li>
              <li class="update"><a href="<?=base_url().$access->controller?>/keluarga">Keluarga</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                <?php echo $this->session->flashdata('status'); ?>
                <h4>Lokasi</h4>
                <form id="form_jabatan" class="form-horizontal" method="post" action="<?=base_url()?>pr_data/posisi_jabatan_action/<?=$action?>" autocomplete="off">
                  <input type="hidden" name="id_pegawai" id="id_pegawai" value="<?php if($pegawai){echo $pegawai->id_pegawai;}?>">
                  <div class="form-group">
                    <label class="col-md-2 control-label">Kecamatan</label>
                    <div class="col-md-2">
                      <select class="form-control select2" name="id_kecamatan" id="id_kecamatan" disabled>
                        <?php foreach($kecamatan as $row): ?>
                          <option value="<?=$row->id_kecamatan?>" <?php if($profil->id_kecamatan == $row->id_kecamatan){echo 'selected';}?>><?=$row->kecamatan?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Unit Kerja</label>
                    <div class="col-sm-4">
                      <select class="form-control select2" name="id_unit_kerja" id="id_unit_kerja" disabled>
                        <?php foreach($unit_kerja as $row): ?>
                          <option value="<?=$row->id_unit_kerja?>" <?php if($profil->id_unit_kerja == $row->id_unit_kerja){echo 'selected';}?>><?=$row->unit_kerja?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Instansi</label>
                    <div class="col-sm-4">
                      <select class="form-control select2" name="id_instansi" id="id_instansi" disabled>
                        <?php foreach($nama_instansi as $row): ?>
                          <option value="<?=$row->id_instansi?>" <?php if($profil->id_instansi == $row->id_instansi){echo 'selected';}?>><?=$row->nama_instansi?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>
                  <h4>Jabatan</h4>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Rumpun Jabatan</label>
                    <div class="col-sm-3">
                      <select class="form-control select2" name="id_rumpun_jabatan" id="id_rumpun_jabatan">
                        <option value="0" <?php if($jabatan){if($jabatan->id_rumpun_jabatan == '0'){echo 'selected';}}else{echo 'selected';}?>>-- Pilih --</option>
                        <?php foreach($rumpun_jabatan as $row): ?>
                          <option value="<?=$row->id_rumpun_jabatan?>" <?php if($jabatan){if($jabatan->id_rumpun_jabatan == $row->id_rumpun_jabatan){echo 'selected';}}?>><?=$row->rumpun_jabatan?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Jabatan</label>
                    <div class="col-sm-5">
                      <select class="form-control select2" name="id_jabatan" id="id_jabatan">
                        <option value="0" <?php if($jabatan){if($jabatan->id_jabatan == '0'){echo 'selected';}}else{echo 'selected';}?>>-- Pilih --</option>
                        <?php foreach($jabatan_list as $row): ?>
                          <option value="<?=$row->id_jabatan?>" <?php if($jabatan){if($jabatan->id_jabatan == $row->id_jabatan){echo 'selected';}}?>><?=$row->jabatan?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">TMT Jabatan</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control input-sm datepicker" name="tmt_jabatan" id="tmt_jabatan" value="<?php if($pegawai){echo date_to_id($pegawai->tmt_jabatan);}?>" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">TMT Pangkat</label>
                    <div class="col-md-2">
                      <input type="text" class="form-control input-sm datepicker" name="tmt_pangkat" id="tmt_pangkat" value="<?php if($pegawai){echo date_to_id($pegawai->tmt_pangkat);}?>" required>
                    </div>
                    <?php if($jabatan->id_rumpun_jabatan == 2): ?>
                      <label class="col-md-2 control-label" style="width:max-content !important;">Usulan PAK Berikutnya</label>
                      <div class="col-md-2">
                        <input type="text" class="form-control input-sm" name="" id="" value="<?php if($pegawai){if($pegawai->tmt_pangkat != null){echo date('d-m-Y', strtotime("+30 months", strtotime($pegawai->tmt_pangkat)));}}?>" readonly>
                      </div>
                      <label class="col-md-2 control-label" style="width:max-content !important;">Pembebasan</label>
                      <div class="col-md-2">
                        <input type="text" class="form-control input-sm" name="" id="" value="<?php if($pegawai){if($pegawai->tmt_pangkat != null){echo date('d-m-Y', strtotime("+60 months", strtotime($pegawai->tmt_pangkat)));}}?>" readonly>
                      </div>
                    <?php endif;?>
                    <?php if($jabatan->id_rumpun_jabatan == 3): ?>
                      <label class="col-md-2 control-label" style="width:max-content !important;">Naik Pangkat Berikutnya</label>
                      <div class="col-md-2">
                        <input type="text" class="form-control input-sm" name="" id="" value="<?php if($pegawai){if($pegawai->tmt_pangkat != null){echo date('d-m-Y', strtotime("+48 months", strtotime($pegawai->tmt_pangkat)));}}?>" readonly>
                      </div>
                    <?php endif;?>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Tingkat</label>
                    <div class="col-md-3">
                      <select class="form-control select2" name="id_tingkat" id="id_tingkat">
                        <option value="0" <?php if($tingkat){if($tingkat->id_tingkat == '0'){echo 'selected';}}else{echo 'selected';}?>>-- Pilih --</option>
                        <?php foreach($tingkat_list as $row): ?>
                          <option value="<?=$row->id_tingkat?>" <?php if($tingkat){if($tingkat->id_tingkat == $row->id_tingkat){echo 'selected';}}?>><?=$row->tingkat?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">NIP Pejabat Penilai</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control input-sm" name="nip_atasan" id="nip_atasan" value="<?php if($atasan){echo $atasan->nip_atasan;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Jabatan Rangkap</label>
                    <div class="col-md-2">
                      <select class="form-control select2" name="plh_plt_pp" id="plh_plt_pp">
                        <option value="0" <?php if($atasan){if($atasan->plh_plt_pp == 0){echo 'selected';}}?>> -- Pilih -- </option>
                        <option value="1" <?php if($atasan){if($atasan->plh_plt_pp == 1){echo 'selected';}}?>> Plt. </option>
                        <option value="2" <?php if($atasan){if($atasan->plh_plt_pp == 2){echo 'selected';}}?>> Plh. </option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Jabatan</label>
                    <div class="col-sm-5">
                      <select class="form-control select2" name="id_jab_plh_plt_pp" id="id_jab_plh_plt_pp">
                        <option value="0">-- Pilih --</option>
                        <?php foreach($jabatan_list as $row): ?>
                          <option value="<?=$row->id_jabatan?>" <?php if($atasan){if($atasan->id_jab_plh_plt_pp == $row->id_jabatan){echo 'selected';}}?>><?=$row->jabatan?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">NIP Atasan Pejabat Penilai</label>
                    <div class="col-md-3">
                      <input type="text" class="form-control input-sm" name="nip_app" id="nip_app" value="<?php if($atasan){echo $atasan->nip_app;}?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-2 control-label">Jabatan Rangkap</label>
                    <div class="col-md-2">
                      <select class="form-control select2" name="plh_plt_app" id="plh_plt_app">
                        <option value="0" <?php if($atasan){if($atasan->plh_plt_app == 0){echo 'selected';}}?>> -- Pilih -- </option>
                        <option value="1" <?php if($atasan){if($atasan->plh_plt_app == 1){echo 'selected';}}?>> Plt. </option>
                        <option value="2" <?php if($atasan){if($atasan->plh_plt_app == 2){echo 'selected';}}?>> Plh. </option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Jabatan</label>
                    <div class="col-sm-5">
                      <select class="form-control select2" name="id_jab_plh_plt_app" id="id_jab_plh_plt_app">
                        <option value="0">-- Pilih --</option>
                        <?php foreach($jabatan_list as $row): ?>
                          <option value="<?=$row->id_jabatan?>" <?php if($atasan){if($atasan->id_jab_plh_plt_app == $row->id_jabatan){echo 'selected';}}?>><?=$row->jabatan?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Periode Penilaian</label>
                    <div class="col-sm-5">
                      <select class="form-control select2" name="id_periode" id="id_periode">
                        <option value="0" <?php if($atasan){if($atasan->id_periode == 0){echo 'selected';}}?>>-- Pilih --</option>
                        <?php foreach($periode_penilaian as $row): ?>
                          <option value="<?=$row->id?>" <?php if($atasan){if($atasan->id_periode == $row->id){echo 'selected';}}?>><?=$row->periode_penilaian?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>
                  <hr>
                  <div class="form-group">
                    <div class="col-md-offset-2 col-sm-8">
                      <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
        </div>
      </div>
    </section>
  </div>
<!-- /.content-wrapper -->
<!-- /script -->
<script>
  $(document).ready(function () {
    // form validator
    $('#form_jabatan').validate({
      submitHandler: function(form) {
        $(form).ajaxSubmit();
      }
    });
  })
</script>