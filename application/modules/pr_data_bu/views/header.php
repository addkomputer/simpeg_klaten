<div class="box box-primary">
  <div class="box-body">
    <a class="btn btn-app btn-default" href="<?= base_url().$access->controller;?>/data_pokok">
      <i class="fa fa-database"></i> Data Utama
    </a>
    <a class="btn btn-app btn-default update" href="<?= base_url().$access->controller;?>/riwayat_pangkat">
      <i class="fa fa-user"></i> Pangkat / Gol
    </a>
    <a class="btn btn-app btn-default update" href="<?= base_url().$access->controller;?>/riwayat_pendidikan">
      <i class="fa fa-graduation-cap"></i> Pendidikan
    </a>
    <a class="btn btn-app btn-default update" href="<?= base_url().$access->controller;?>/riwayat_jabatan">
      <i class="fa fa-id-card-o"></i> Jabatan
    </a>
    <a class="btn btn-app btn-default update" href="<?= base_url().$access->controller;?>/riwayat_diklat">
      <i class="fa fa-book"></i> Diklat
    </a>
    <a class="btn btn-app btn-default update" href="<?= base_url().$access->controller;?>/riwayat_penghargaan">
      <i class="fa fa-trophy"></i> Penghargaan
    </a>
    <a class="btn btn-app btn-default update" href="<?= base_url().$access->controller;?>/riwayat_hukuman">
      <i class="fa fa-thumbs-down"></i> Huk. Disiplin
    </a>
    <a class="btn btn-app btn-default update" href="<?= base_url().$access->controller;?>/riwayat_ppk">
      <i class="fa fa-address-book-o"></i> PPK
    </a>
  </div>
</div>