  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">Administrasi Kehadiran</li>
        <li class="active text-orange"><?=$access->menu?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <i class="fa fa-table text-maroon"></i>
          <h3 class="box-title">Tabel <?=$access->menu?></h3>
          <div class="box-tools pull-right">
            <span class="badge bg-maroon"><i class="fa fa-bar-chart"></i> Jumlah Keseluruhan Data : <?=$num_rows_total?></span>
          </div>
        </div>
        <div class="box-body">
          <form id="form_search" action="<?=base_url().$this->access->controller;?>/search" method="post" autocomplete="off">
            <div class="row">
              <div class="col-md-1">
                <a class="btn btn-primary btn-flat" href="<?=base_url().$this->access->controller?>/form"><i class="fa fa-plus"></i> Tambah</a>
              </div>
              <div class="col-md-2 col-md-offset-1">
                <div class="form-group">
                  <select name="bulan" class="form-control select2 input-sm">
                    <option value="01" <?php if($search){if($search['bulan'] == '01'){echo 'selected';}}else{if(date('m') == '01'){echo 'selected';}} ?>>Januari</option>
                    <option value="02" <?php if($search){if($search['bulan'] == '02'){echo 'selected';}}else{if(date('m') == '02'){echo 'selected';}} ?>>Februari</option>
                    <option value="03" <?php if($search){if($search['bulan'] == '03'){echo 'selected';}}else{if(date('m') == '03'){echo 'selected';}} ?>>Maret</option>
                    <option value="04" <?php if($search){if($search['bulan'] == '04'){echo 'selected';}}else{if(date('m') == '04'){echo 'selected';}} ?>>April</option>
                    <option value="05" <?php if($search){if($search['bulan'] == '05'){echo 'selected';}}else{if(date('m') == '05'){echo 'selected';}} ?>>Mei</option>
                    <option value="06" <?php if($search){if($search['bulan'] == '06'){echo 'selected';}}else{if(date('m') == '06'){echo 'selected';}} ?>>Juni</option>
                    <option value="07" <?php if($search){if($search['bulan'] == '07'){echo 'selected';}}else{if(date('m') == '07'){echo 'selected';}} ?>>Juli</option>
                    <option value="08" <?php if($search){if($search['bulan'] == '08'){echo 'selected';}}else{if(date('m') == '08'){echo 'selected';}} ?>>Agustus</option>
                    <option value="09" <?php if($search){if($search['bulan'] == '09'){echo 'selected';}}else{if(date('m') == '09'){echo 'selected';}} ?>>September</option>
                    <option value="10" <?php if($search){if($search['bulan'] == '10'){echo 'selected';}}else{if(date('m') == '10'){echo 'selected';}} ?>>Oktober</option>
                    <option value="11" <?php if($search){if($search['bulan'] == '11'){echo 'selected';}}else{if(date('m') == '11'){echo 'selected';}} ?>>November</option>
                    <option value="12" <?php if($search){if($search['bulan'] == '12'){echo 'selected';}}else{if(date('m') == '12'){echo 'selected';}} ?>>Desember</option>
                  </select>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <select name="tahun" class="form-control input-sm select2">
                    <?php for($i=2018; $i <= date('Y'); $i++):?>
                      <option value="<?=$i?>" <?php if($search){if($search['tahun'] == $i){echo 'selected';}}else{if(date('Y') == $i){echo 'selected';}} ?>><?=$i?></option>
                    <?php endfor; ?>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <select name="id_ruang" class="form-control select2 input-sm" onchange="$('#form-index').submit()">
                    <?php if($this->session->userdata('id_grup') == 4):?>
                      <?php foreach($ruang as $row): ?>
                        <?php if($this->session->userdata('id_ruang') == $row->id_ruang): ?>
                          <option value="<?=$row->id_ruang?>"><?=$row->ruang?></option>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    <?php else: ?>
                      <option value="">-- Semua Ruang --</option>
                      <?php foreach($ruang as $row): ?>
                        <option value="<?=$row->id_ruang?>" <?php if($search){if($search['id_ruang'] == $row->id_ruang){echo 'selected';}} ?>><?=$row->ruang?></option>
                      <?php endforeach;?>
                    <?php endif; ?>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="input-group pull-right">
                  <input type="text" name="term" class="form-control pull-right" placeholder="Pencarian" value="<?php if($search != null){echo $search['term'];}?>">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                    <a class="btn btn-default btn-flat" href="<?=base_url().$this->access->controller?>/reset_search"><i class="fa fa-refresh"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <?php echo $this->session->flashdata('status'); ?>
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-condensed">
              <thead>
                <tr>
                  <th class="text-center" width="30">No.</th>
                  <!-- <th class="text-center" width="60">Aksi</th> -->
                  <th class="text-center">Ruang</th>
                  <th class="text-center">Pegawai 1</th>
                  <th class="text-center">Shift 1</th>
                  <th class="text-center">Pegawai 2</th>
                  <th class="text-center">Shift 2</th>
                </tr>
              </thead>
              <tbody>
                <?php if ($main != null): ?>
                  <?php $i=1;foreach ($main as $row): ?>
                    <tr>
                      <td class="text-center"><?=$this->uri->segment('3')+$i++?></td>
                      <!-- <td class="text-center">                      
                        <a class="btn btn-xs btn-flat btn-warning" href="<?=base_url()?>tb_log_sakit/form/<?=$row->id?>" style="padding-right:0.250em;"><i class="fa fa-pencil"></i></a> 
                        <a class="btn btn-xs btn-flat btn-danger" href="#" onclick="del(<?=$row->id?>)"><i class="fa fa-trash"></i></a>
                      </td> -->
                      <td><?=$row->ruang?></td>
                      <td>
                        <?php
                          $nama_1 = $row->gelar_depan_1.' '.$row->nama_1;
                          if ($row->gelar_belakang_1 != ''){
                            $nama_1 .= ', '.$row->gelar_belakang_1;
                          }
                          echo $nama_1;
                        ?><br>
                        <?=$row->nomor_induk_1?> - <?=$row->pin_1?>
                      </td>
                      <td class="text-center">
                        <?=date_to_id($row->tanggal_1)?><br>
                        <?=$row->kode_1?> (<?=$row->jam_1?>)
                      </td>
                      <td>
                        <?php
                          $nama_2 = $row->gelar_depan_2.' '.$row->nama_2;
                          if ($row->gelar_belakang_2 != ''){
                            $nama_2 .= ', '.$row->gelar_belakang_2;
                          }
                          echo $nama_2;
                        ?>
                        <br>
                        <?=$row->nomor_induk_2?> - <?=$row->pin_2?>
                      </td>
                      <td class="text-center">
                        <?=date_to_id($row->tanggal_2)?><br>
                        <?=$row->kode_2?> (<?=$row->jam_2?>)
                      </td>
                    </tr>
                  <?php endforeach; ?>
                <?php else: ?>
                  <tr>
                    <td class="text-center" colspan="99">Tidak ada data!</td>
                  </tr>
                <?php endif; ?>
              </tbody>
            </table>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
          <span class="badge bg-purple">Jumlah Data : <?=$num_rows?></span>
          <?php echo $this->pagination->create_links(); ?>
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- Modal Delete -->
  <div id="modal_delete" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <form action="<?=base_url().$access->controller?>/delete" method="post">
          <input type="hidden" name="id" id="id">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Hapus Data</h4>
          </div>
          <div class="modal-body">
            <p>Anda yakin ingin menghapus data ini?</p>
            <b class="cl-danger">Peringatan!</b>
            <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-flat btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
            <button type="submit" class="btn btn-flat btn-danger"><i class="fa fa-trash"></i> Hapus</button>
          </div>
        </form>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <!-- /.content-wrapper -->
  <script type="text/javascript">
    $(document).ready(function () {
      $('.select2').on('select2:select', function (e) {
        $('#form_search').submit();
      });
    })
    function del(id) {
      $("#modal_delete").modal('show');
      $("#id").val(id);
    }
  </script>