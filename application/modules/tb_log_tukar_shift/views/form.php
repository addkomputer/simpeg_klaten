<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">Administrasi Kehadiran</li>
        <li class="active text-orange"><?=$access->menu?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="box box-primary">
        <div class="box-header with-border">
          <i class="fa fa-table text-maroon"></i>
          <h3 class="box-title">Form <?=$access->menu?></h3>
        </div>
        <form id="form" class="form-horizontal" action="<?=base_url().$this->access->controller?>/<?=$action?>" method="post" enctype="multipart/form-data" autocomplete="off"> 
          <div class="box-body">
            <div class="alert alert-info">
              <p>Follow the steps to continue to payment.</p>
            </div>
            <input type="hidden" class="form-control" name="id" id="id" value="<?php if($main != null){echo $main->id;};?>">
            <div class="form-group">
              <label class="col-sm-2 control-label">Ruang</label>
              <div class="col-sm-5">
                <select class="form-control select2" name="id_ruang" id="id_ruang" <?php if($action == 'update'){echo 'disabled';}?>>
                  <?php if($this->session->userdata('id_grup') == 4): ?>
                    <?php foreach($ruang as $row): ?>
                      <?php if($this->session->userdata('id_ruang') == $row->id_ruang):?>
                        <option value="<?=$row->id_ruang?>" <?php if($main){if($main->id_ruang == $row->id_ruang){echo 'selected';}} ?>><?=$row->ruang?></option>
                      <?php endif; ?>
                    <?php endforeach;?>
                  <?php else: ?>
                    <option value="">-- Pilih Ruang --</option>
                    <?php foreach($ruang as $row): ?>
                      <option value="<?=$row->id_ruang?>" <?php if($main){if($main->id_ruang == $row->id_ruang){echo 'selected';}} ?>><?=$row->ruang?></option>
                    <?php endforeach;?>
                  <?php endif; ?>
                </select>
              </div>
            </div>
            <h4>Pegawai 1</h4>
            <div class="form-group">
              <label class="col-sm-2 control-label">Nama Pegawai</label>
              <div class="col-sm-6">
                <select class="form-control select2" name="id_pegawai_1" id="id_pegawai_1">
                  <option value="">-- Pilih Pegawai --</option>
                  <?php foreach($pegawai as $row):?>
                    <?php 
                      $nama_1 = $row->gelar_depan.' '.$row->nama;
                      if ($row->gelar_belakang != ''){
                        $nama_1 .= ', '.$row->gelar_belakang;
                      }
                    ?>
                    <option value="<?=$row->id_pegawai?>" data-chained="<?=$row->id_ruang?>"><?=$nama_1." - ".$row->pin." - ".$row->nomor_induk?></option>
                  <?php endforeach; ?>
                </select>
                <input type="hidden" id="pin_1" name="pin_1" value="">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Tanggal</label>
              <div class="col-sm-2">
                <div class="input-group">
                  <input class="form-control" type="text" id="tanggal_1" name="tanggal_1" value="" required>
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Kode Shift</label>
              <div class="col-md-1">
                <input class="form-control" type="text" id="kode_1" name="kode_1" value="" required readonly>
              </div>
              <label class="col-sm-1 control-label">Jam Shift</label>
              <div class="col-md-3">
                <input class="form-control" type="text" id="jam_1" name="jam_1" value="" required readonly>
              </div>
            </div>
            <h4>Pegawai 2</h4>
            <div class="form-group">
              <label class="col-sm-2 control-label">Nama Pegawai</label>
              <div class="col-sm-6">
                <select class="form-control select2" name="id_pegawai_2" id="id_pegawai_2">
                  <option value="">-- Pilih Pegawai --</option>
                  <?php foreach($pegawai as $row):?>
                    <?php 
                      $nama_2 = $row->gelar_depan.' '.$row->nama;
                      if ($row->gelar_belakang != ''){
                        $nama_2 .= ', '.$row->gelar_belakang;
                      }
                    ?>
                    <option value="<?=$row->id_pegawai?>" data-chained="<?=$row->id_ruang?>"><?=$nama_2." - ".$row->pin." - ".$row->nomor_induk?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <input type="hidden" id="pin_2" name="pin_2" value="">
            <div class="form-group">
              <label class="col-sm-2 control-label">Tanggal</label>
              <div class="col-sm-2">
                <div class="input-group">
                  <input class="form-control" type="text" id="tanggal_2" name="tanggal_2" value="" required>
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Kode Shift</label>
              <div class="col-md-1">
                <input class="form-control" type="text" id="kode_2" name="kode_2" value="" required readonly>
              </div>
              <label class="col-sm-1 control-label">Jam Shift</label>
              <div class="col-md-3">
                <input class="form-control" type="text" id="jam_2" name="jam_2" value="" required readonly>
              </div>
            </div>
            <h4>Bukti & Keterangan</h4>
            <div class="form-group">
              <label class="col-sm-2 control-label">File Bukti</label>
              <div class="col-sm-10">
                <input type="file" name="file_bukti" <?php if($action == 'insert'){echo 'required';} ?>/>
                <?php if($action == 'update'): ?>
                  <input type="hidden" name="old_file_bukti" value="<?php if($main){echo $main->file_bukti;}?>">
                  <?php if($main->file_bukti != ''): ?>
                    File : <a href="<?=base_url()?>berkas/bukti/<?=$main->file_bukti?>" target="_blank"><i class="fa fa-download"></i> <?=$main->file_bukti?></a>
                  <?php endif;?>
                <?php endif;?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Keterangan</label>
              <div class="col-sm-5">
                <textarea name="keterangan" id="keterangan" class="form-control"><?php if($main){echo $main->keterangan;}?></textarea>
              </div>
            </div>
            <hr>
            <div class="form-group">
              <div class="col-md-offset-2 col-sm-8">
                <button class="btn btn-flat btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button>
                <a class="btn btn-flat btn-default" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Batal</a>
              </div>
            </div>
          </div>
        </form>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<style>
  .datepicker table tr td.new, .datepicker .next, .datepicker .prev{
    height: 0;
    line-height: 0;
    visibility: hidden;
}
</style>
<script>
  $(document).ready(function () {
    //initial tanggal
    $('#tanggal_1').attr('readonly', true);
    $('#tanggal_2').attr('readonly', true);
    //chained
    $("#id_pegawai_1").chained("#id_ruang");
    $("#id_pegawai_2").chained("#id_ruang");
    // form validator
    $('#form').validate({
      rules : {
        id_ruang : {
          valueNotEquals : ''
        },
        id_pegawai_1 : {
          valueNotEquals : ''
        },
        id_pegawai_2 : {
          valueNotEquals : ''
        }
      }
    });
    //change id_pegawai
    $('#id_pegawai_1').on('change', function() {
      var id_pegawai_1 = $("#id_pegawai_1 option:selected").val();
      if(id_pegawai_1 != ''){
        $('#tanggal_1').attr('readonly', false);
        $('#tanggal_1').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy',
          clearBtn: true
        })
      }else{
        $('#tanggal_1').attr('readonly', true);
        $('#tanggal_1').val("");
        $('#kode_1').val("");
        $('#jam_1').val("");
        $('#tanggal_1').datepicker("destroy");
      }
    })
    $('#id_pegawai_2').on('change', function() {
      var id_pegawai_2 = $("#id_pegawai_2 option:selected").val();
      if(id_pegawai_2 != ''){
        $('#tanggal_2').attr('readonly', false);
        $('#tanggal_2').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy',
          clearBtn: true
        })
      }else{
        $('#tanggal_2').attr('readonly', true);
        $('#tanggal_2').val("");
        $('#kode_2').val("");
        $('#jam_2').val("");
        $('#tanggal_2').datepicker("destroy");
      }
    })
    //change tanggal 1
    $('#tanggal_1').datepicker()
    .on('changeDate', function(e) {
      var tanggal = e.format();
      var id_pegawai = $("#id_pegawai_1").val();
      //get shift pegawai 1 
      $.ajax({
        type: 'post',
        url: '<?=base_url($access->controller)?>/get_shift',
        data: 'id_pegawai='+id_pegawai+'&tanggal='+tanggal,
        dataType: 'json',
        success: function (data) {
          $("#pin_1").val(data.pin);
          $("#kode_1").val(data.kode);
          $("#jam_1").val(data.jam_datang+' - '+data.jam_pulang);
        }
      });
    });

    $('#tanggal_2').datepicker()
    .on('changeDate', function(e) {
      var tanggal = e.format();
      var id_pegawai = $("#id_pegawai_2").val();
      //get shift pegawai 1 
      $.ajax({
        type: 'post',
        url: '<?=base_url($access->controller)?>/get_shift',
        data: 'id_pegawai='+id_pegawai+'&tanggal='+tanggal,
        dataType: 'json',
        success: function (data) {
          $("#pin_2").val(data.pin);
          $("#kode_2").val(data.kode);
          $("#jam_2").val(data.jam_datang+' - '+data.jam_pulang);
        }
      });
    });
  })
</script>