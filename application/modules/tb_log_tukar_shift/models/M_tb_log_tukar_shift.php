<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_tb_log_tukar_shift extends CI_Model {

  private $suffix,$bulan,$tahun,$id_ruang,$id_jabatan,$id_status_pegawai,$term;

  function __construct(){
    parent::__construct();
    if ($this->session->userdata('search')) {
      $this->bulan = $this->session->userdata('search')['bulan'];
      $this->tahun = $this->session->userdata('search')['tahun'];
      $this->id_ruang = $this->session->userdata('search')['id_ruang'];
      $this->term = $this->session->userdata('search')['term'];
      $this->suffix = $this->tahun.'_'.$this->bulan;
    }else{
      $this->bulan = date('m');
      $this->tahun = date('Y');
      $this->id_ruang = '';
      if ($this->session->userdata('id_grup') == 4) {
        $this->id_ruang = $this->session->userdata('id_ruang');
      }
      $this->term = '';
      $this->suffix = date('Y_m');
      $this->id_status_pegawai = '';
      $search = array(
        'bulan' => $this->bulan,
        'tahun' => $this->tahun,
        'id_ruang' => $this->id_ruang,
        'term' => $this->term
      );
      $this->session->set_userdata(array('search' => $search));
    }
    $this->create_table($this->suffix);
  }
  
  public function create_table($suffix)
  {
    $this->db->query(
      "CREATE TABLE IF NOT EXISTS `tb_log_tukar_shift_$this->suffix` (
        `id` bigint(20) NOT NULL AUTO_INCREMENT,
        `id_ruang` int(11) DEFAULT '0',
        `tanggal` date DEFAULT NULL,
        `id_pegawai_1` varchar(50) DEFAULT '0',
        `pin_1` varchar(50) DEFAULT '0',
        `kode_1` varchar(50) DEFAULT '0',
        `tanggal_1` date DEFAULT NULL,
        `jam_1` varchar(50) DEFAULT '0',
        `id_pegawai_2` varchar(50) DEFAULT '0',
        `pin_2` varchar(50) DEFAULT '0',
        `kode_2` varchar(50) DEFAULT '0',
        `tanggal_2` date DEFAULT NULL,
        `jam_2` varchar(50) DEFAULT NULL,
        `file_bukti` varchar(255) DEFAULT NULL,
        `keterangan` text,
        `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `created_by` varchar(50) NOT NULL DEFAULT 'System',
        `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
        `updated_by` varchar(50) DEFAULT NULL,
        `is_active` tinyint(1) NOT NULL DEFAULT '1',
        `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
        PRIMARY KEY (`id`)
      ) ENGINE=MyISAM DEFAULT CHARSET=latin1"
    );
  }

  public function get_pegawai()
  {
    return $this->db->query(
      "SELECT 
        a.*,
        b.gelar_depan,b.nama,b.gelar_belakang,b.id_ruang 
      FROM dt_mapping_shift_$this->suffix a
      JOIN dt_pegawai b ON a.pin = b.pin
      GROUP BY 
        a.pin"
    )->result();
  }

	public function get_list($number,$offset,$search = null)
  {
    $where = "WHERE 1 = 1 ";
    if ($search != null) {
      if ($search['id_ruang'] != '') {
        $where .= "AND a.id_ruang = ".$search['id_ruang']." ";
      }
      if ($search['term'] != '') {
        $where .= "AND (c.nama LIKE '%".$search['term']."%' OR d.nama LIKE '%".$search['term']."%')";
      }
    }

    $query = $this->db->query(
      "SELECT 
        a.*,
        b.ruang,
        c.nomor_induk as nomor_induk_1, c.gelar_depan as gelar_depan_1, c.nama as nama_1, c.gelar_belakang as gelar_belakang_1,
        d.nomor_induk as nomor_induk_2, d.gelar_depan as gelar_depan_2, d.nama as nama_2, d.gelar_belakang as gelar_belakang_2
      FROM tb_log_tukar_shift_$this->suffix a
      JOIN ms_ruang b ON a.id_ruang = b.id_ruang
      JOIN dt_pegawai c ON a.id_pegawai_1 = c.id_pegawai
      JOIN dt_pegawai d ON a.id_pegawai_2 = d.id_pegawai
        $where"
    )->result();

    return $query;
  }

  function num_rows($search = null){
		$where = "WHERE 1 = 1 ";
    if ($search != null) {
      if ($search['id_ruang'] != '') {
        $where .= "AND a.id_ruang = ".$search['id_ruang']." ";
      }
      if ($search['term'] != '') {
        $where .= "AND (c.nama LIKE '%".$search['term']."%' OR d.nama LIKE '%".$search['term']."%')";
      }
    }

    $query = $this->db->query(
      "SELECT 
        a.*,
        b.ruang,
        c.nomor_induk as nomor_induk_1, c.gelar_depan as gelar_depan_1, c.nama as nama_1, c.gelar_belakang as gelar_belakang_1,
        d.nomor_induk as nomor_induk_2, d.gelar_depan as gelar_depan_2, d.nama as nama_2, d.gelar_belakang as gelar_belakang_2
      FROM tb_log_tukar_shift_$this->suffix a
      JOIN ms_ruang b ON a.id_ruang = b.id_ruang
      JOIN dt_pegawai c ON a.id_pegawai_1 = c.id_pegawai
      JOIN dt_pegawai d ON a.id_pegawai_2 = d.id_pegawai
        $where"
    )->num_rows();

    return $query;
  }
  
  function num_rows_total($search = null){  
    $query = $this->db->query(
      "SELECT 
        a.*,
        b.ruang,
        c.nomor_induk as nomor_induk_1, c.gelar_depan as gelar_depan_1, c.nama as nama_1, c.gelar_belakang as gelar_belakang_1,
        d.nomor_induk as nomor_induk_2, d.gelar_depan as gelar_depan_2, d.nama as nama_2, d.gelar_belakang as gelar_belakang_2
      FROM tb_log_tukar_shift_$this->suffix a
      JOIN ms_ruang b ON a.id_ruang = b.id_ruang
      JOIN dt_pegawai c ON a.id_pegawai_1 = c.id_pegawai
      JOIN dt_pegawai d ON a.id_pegawai_2 = d.id_pegawai"
    )->num_rows();

    return $query;
	}

	public function get_all()
	{
		return $this->db
			->where('is_active','1')
			->get('tb_log_tukar_shift_'.$this->suffix)->result();
  }

  public function get_by_id($id)
  {
    return $this->db->where('id',$id)->get('tb_log_tukar_shift_'.$this->suffix)->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id','asc')->get('tb_log_tukar_shift_'.$this->suffix)->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id','desc')->get('tb_log_tukar_shift_'.$this->suffix)->row();
  }

  public function insert($data)
  {
    $suffix = substr($data['tanggal'],0,4)."_".substr($data['tanggal'],5,2);
    $table_tukar_shift = "tb_log_tukar_shift_".$suffix;
    $table_mapping_shift = "dt_mapping_shift_".$suffix;
    $table_kehadiran = "tb_log_kehadiran_".$suffix;
    //pegawai 1 <- pegawai 2 
    $shift_1 = $this->db->where('kode',$data['kode_2'])->get('ms_shift')->row_array();
    $shift_1['kode_awal'] = $data['kode_1'];
    $shift_1['is_tukar'] = 1;
    //update data mapping shift
    unset($shift_1['shift']);
    $this->db
      ->where('id_pegawai',$data['id_pegawai_1'])
      ->where('tanggal',$data['tanggal_1'])
      ->update($table_mapping_shift,$shift_1);
    //update kehadiran
    $d_kehadiran_1 = array(
      'kode' => $shift_1['kode'],
      'jam_datang' => $shift_1['jam_datang'],
      'jam_batas_datang' => $shift_1['jam_batas_datang'],
      'jam_pulang' => $shift_1['jam_pulang'],
      'jam_batas_pulang' => $shift_1['jam_batas_pulang'],
      'is_tukar' => 1
    );
    $this->db
      ->where('id_pegawai',$data['id_pegawai_1'])
      ->where('tanggal',$data['tanggal_1'])
      ->update($table_kehadiran,$d_kehadiran_1);

    //pegawai 2 <- pegawai 1
    $shift_2 = $this->db->where('kode',$data['kode_1'])->get('ms_shift')->row_array();
    $shift_2['kode_awal'] = $data['kode_2'];
    $shift_2['is_tukar'] = 1;
    //update data mapping shift
    unset($shift_2['shift']);
    $this->db
      ->where('id_pegawai',$data['id_pegawai_2'])
      ->where('tanggal',$data['tanggal_2'])
      ->update($table_mapping_shift,$shift_2);
    //update kehadiran
    $d_kehadiran_2 = array(
      'kode' => $shift_2['kode'],
      'jam_datang' => $shift_2['jam_datang'],
      'jam_batas_datang' => $shift_2['jam_batas_datang'],
      'jam_pulang' => $shift_2['jam_pulang'],
      'jam_batas_pulang' => $shift_2['jam_batas_pulang'],
      'is_tukar' => 1
    );
    $this->db
      ->where('id_pegawai',$data['id_pegawai_2'])
      ->where('tanggal',$data['tanggal_2'])
      ->update($table_kehadiran,$d_kehadiran_2);

    $this->db->insert($table_tukar_shift,$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id',$id)->update('tb_log_tukar_shift_'.$this->suffix,$data);
    $log = array(
      'id_tipe' => '3',
      'ket_umum' => $data['keterangan']
    );
    $tukar_shift = $this->db->where('id',$id)->get('tb_log_tukar_shift_'.$this->suffix)->row_array();
    $this->db
      ->where('id_pegawai',$tukar_shift['id_pegawai'])
      ->where('tanggal',$tukar_shift['tanggal'])
      ->update('tb_log_kehadiran_'.$this->suffix,$log);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id',$id)->update('tb_log_tukar_shift_'.$this->suffix,array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $data = $this->db->where('id',$id)->get('tb_log_tukar_shift_'.$this->suffix)->row();
    $this->db
      ->where('id_pegawai',$data->id_pegawai)
      ->where('tanggal',$data->tanggal)
      ->update('tb_log_kehadiran_'.$this->suffix,array('ket_umum' => '','id_tipe' => -1));
    $this->db->where('id',$id)->delete('tb_log_tukar_shift_'.$this->suffix);
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE tb_log_tukar_shift_2018_10");
  }

  public function get_shift($data)
  {
    $suffix = substr($data['tanggal'],0,4)."_".substr($data['tanggal'],5,2);
    $table_mapping_shift = "dt_mapping_shift_".$suffix;

    $query = $this->db->query(
      "SELECT * FROM $table_mapping_shift WHERE tanggal='".$data['tanggal']."' AND id_pegawai='".$data['id_pegawai']."'"
    )->row();

    return $query;
  }

}
