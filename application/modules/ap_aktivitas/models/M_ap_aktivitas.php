<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ap_aktivitas extends CI_Model {

	public function get_list($number,$offset = 0,$search = null)
  {    
    if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          if ($key == 'created') {
            $this->db->like($key,$val);
          }else{
            $this->db->where($key,$val);
          }
        }
      }
    }else{
      $this->db->like('created',date('Y-m-d'));
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_aktivitas','DESC')
      ->get('ap_aktivitas',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          if ($key == 'created') {
            $this->db->like($key,$val);
          }else{
            $this->db->where($key,$val);
          }
        }
      }
    }else{
      $this->db->like('created',date('Y-m-d'));
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_aktivitas','DESC')
      ->get('ap_aktivitas')
      ->num_rows();
  }
  
  function num_rows_total(){
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_aktivitas','DESC')
      ->get('ap_aktivitas')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('ap_aktivitas')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id_aktivitas',$id)->get('ap_aktivitas')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_aktivitas','asc')->get('ap_aktivitas')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_aktivitas','desc')->get('ap_aktivitas')->row();
  }

  public function insert($data)
  {
    $this->db->insert('ap_aktivitas',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_aktivitas',$id)->update('ap_aktivitas',$data);
  }

  public function delete_temp($id)
  {
    $this->db->where('id_aktivitas',$id)->update('ap_aktivitas',array('is_deleted' => '1'));
  }

  public function delete_permanent()
  {
    $this->db->where('id_aktivitas',$id)->delete('ap_aktivitas');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ap_aktivitas");
  }

}
