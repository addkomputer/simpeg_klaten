<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ap_aktivitas extends MY_Controller {

	var $access, $aktivitas_id;

  function __construct(){
		parent::__construct();

		$controller = 'ap_aktivitas';

    if($this->session->userdata('menu') != $controller){
      $this->session->unset_userdata('search');
      $this->session->set_userdata(array('menu' => 'ap_aktivitas'));
    }
    $this->load->model('ap_konfigurasi/m_ap_konfigurasi');
		$this->aktivitas_id = $this->session->userdata('aktivitas_id');
		$this->access = $this->m_ap_konfigurasi->get_permission($this->aktivitas_id, $controller);
		if ($this->access == null) {
			redirect(base_url().'ap_error/error_403');
		}
		$this->load->model('m_ap_aktivitas');
		$this->load->model('ap_pengguna/m_ap_pengguna');
	}
	
	public function index()
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Daftar';
		$data['user'] = $this->m_ap_pengguna->get_all();

		if ($this->access->_read) {
			$search = null;
			if($search = $_POST){
				$search['created'] = id_to_date($search['created']);
				$this->session->set_userdata(array('search' => $search));
			}else{
				if($this->session->userdata('search') != null){
					$search = $this->session->userdata('search');
				}
			}
			$data['search'] = $search;

			$config['base_url'] = base_url().'ap_aktivitas/index/';
			$config['per_page'] = 10;
	
			$from = $this->uri->segment(3);
			if ($from == '') $from = 0;
	
			if($search == null){
				$num_rows = $this->m_ap_aktivitas->num_rows();
	
				$config['total_rows'] = $num_rows;
				$this->pagination->initialize($config);
	
				$data['aktivitas'] = $this->m_ap_aktivitas->get_list($config['per_page'],$from,$search = null);
			}else{
				$search = $this->session->userdata('search');
				$num_rows = $this->m_ap_aktivitas->num_rows($search);
				$config['total_rows'] = $num_rows;
				$this->pagination->initialize($config);
	
				$data['aktivitas'] = $this->m_ap_aktivitas->get_list($config['per_page'],$from,$search);
			}
			$data['num_rows'] = $num_rows;
			$data['num_rows_total'] = $this->m_ap_aktivitas->num_rows_total();
			$this->view('index',$data);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}
	
	public function form($id = null)
	{
		$data['access'] = $this->access;
		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Tambah Data';
				$data['action'] = 'insert';
				$data['aktivitas'] = null;
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Ubah Data';
				$data['action'] = 'update';
				$data['aktivitas'] = $this->m_ap_aktivitas->get_by_id($id);
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		}
	}

	public function reset_search()
  {
    $this->session->unset_userdata('search');
    redirect(base_url().'ap_aktivitas/index');
  }

	public function insert()
	{
		$data = $_POST;
		if ($data != null) {
			$data['created_by'] = $this->session->userdata('user_fullname');
			if(!isset($data['is_active'])){$data['is_active'] = 0;}
			$this->m_ap_aktivitas->insert($data);
			insert_aktivitas('insert',$this->access->menu_name);
			$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
			redirect(base_url().'ap_aktivitas/index');
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function update()
	{
		$data = $_POST;
		if ($data != null) {
			if($this->access->_update){
				$data['updated_by'] = $this->session->userdata('user_fullname');
				if(!isset($data['is_active'])){$data['is_active'] = 0;}
				$id = $data['aktivitas_id'];
				$this->m_ap_aktivitas->update($id, $data);
				insert_aktivitas('update',$this->access->menu_name);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				redirect(base_url().'ap_aktivitas/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function delete()
	{
		$data = $_POST;
		if($data != null){
			if ($this->access->_delete) {
				$this->m_ap_aktivitas->delete_temp($data['aktivitas_id']);
				insert_aktivitas('delete',$this->access->menu_name);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				redirect(base_url().'ap_aktivitas/index');
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}

}