<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-<?=$access->icon?> text-red"></i> <?=$access->menu?>
      <small><?=$subtitle?></small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">Pengaturan</li>
      <li class="active text-orange"><?=$access->menu?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box box-danger">
        <div class="box-header with-border">
          <i class="fa fa-table text-maroon"></i>
          <h3 class="box-title">Tabel <?=$access->menu?></h3>
          <div class="box-tools pull-right">
            <span class="badge bg-maroon"><i class="fa fa-bar-chart"></i> Jumlah Keseluruhan Data : <?=$num_rows_total?></span>
          </div>
        </div>
      <div class="box-body">
        <form id="form" class="form-horizontal" action="<?=base_url().$access->controller?>" method="post" autocomplete="off">
          <div class="form-group">
            <label class="col-sm-2 control-label"><i class="fa fa-user"></i> Pengguna</label>
            <div class="col-sm-5">
              <select class="form-control select2" name="id_pegawai" id="id_pegawai">
                <option value="">-- Semua Pengguna --</option>
                <?php foreach($user as $row): ?>
                  <option value="<?=$row->id_pegawai?>" <?php if($search != null){if($search['id_pegawai'] == $row->id_pegawai){echo 'selected';}}?>><?=$row->nama.' - '.$row->nomor_induk?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <label class="col-sm-2 control-label"><i class="fa fa-calendar"></i> Tanggal</label>
            <div class="col-sm-2">
              <input class="form-control datepicker" id="created" name="created" tipe="text" value="<?php if($search != null){echo id_to_date($search['created']);}else{echo date('d-m-Y');}?>">
            </div>
          </div>
        </form>
        <hr>
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
                <th class="text-center" width="30">No.</th>
                <th class="text-center" width="150">Stempel Waktu</th>
                <th class="text-center" width="70">Tipe</th>
                <th class="text-center" width="80">IP Address</th>
                <th class="text-center" width="200">User Agent</th>
                <th class="text-center" width="100">Platform</th>
                <th class="text-center" width="150">Nama</th>
                <th class="text-center">Deskripsi</th>
              </tr>
            </thead>
            <tbody>
              <?php if ($aktivitas != null): ?>
                <?php $i=1;foreach ($aktivitas as $row): ?>
                  <?php 
                    switch ($row->tipe) {
                      case 'signin':
                        $cl = 'text-info';
                        $ic = 'sign-in';
                        break;

                      case 'signout':
                        $cl = 'text-orange';
                        $ic = 'sign-out';
                        break;

                      case 'read':
                        $cl = 'text-green';
                        $ic = 'eye';
                        break;

                      case 'insert':
                        $cl = 'text-blue';
                        $ic = 'plus';
                        break;
                      
                      case 'update':
                        $cl = 'text-yellow';
                        $ic = 'pencil';
                        break;

                      case 'delete':
                        $cl = 'text-red';
                        $ic = 'trash';
                        break;
                    }
                  ?>
                  <?php 
                    $platform_logo = '';
                    if (strpos($row->platform, 'Windows') !== false) {
                      $platform_logo = 'windows';
                    }else if (strpos($row->platform, 'Mac') !== false) {
                      $platform_logo = 'apple';
                    }else if (strpos($row->platform, 'Linux') !== false) {
                      $platform_logo = 'linux';
                    }else if (strpos($row->platform, 'Android') !== false) {
                      $platform_logo = 'android';
                    }
                  ?>
                  <?php 
                    $user_agent_logo = '';
                    if (strpos($row->user_agent, 'Chrome') !== false) {
                      $user_agent_logo = 'chrome';
                    }else if (strpos($row->user_agent, 'Firefox') !== false) {
                      $user_agent_logo = 'firefox';
                    }else if (strpos($row->user_agent, 'Opera') !== false) {
                      $user_agent_logo = 'opera';
                    }else if (strpos($row->user_agent, 'Safari') !== false) {
                      $user_agent_logo = 'safari';
                    }
                  ?>
                  <tr class="<?=$cl?>">
                    <td class="text-center"><?=$this->uri->segment('3')+$i++?></td>
                    <td class="text-center"><?=$row->created?></td>
                    <td class="text-center"><i class="fa fa-<?=$ic?>"></i> <?=$row->tipe?></td>
                    <td class="text-center"><?=$row->ip?></td>
                    <td><i class="fa fa-<?=$user_agent_logo?>"></i> <?=$row->user_agent?></td>
                    <td class="text-center"><i class="fa fa-<?=$platform_logo?>"></i> <?=$row->platform?></td>
                    <td><?=$row->nama?></td>
                    <td><?=$row->deskripsi?></td>
                  </tr>
                <?php endforeach; ?>
              <?php else: ?>
                <tr>
                  <td class="text-center" colspan="99">Tidak ada data!</td>
                </tr>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix"> 
        <?php echo $this->pagination->create_links(); ?>        
        <span class="badge bg-purple" style="margin-top:6px;">Jumlah Data : <?=$num_rows?></span>
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script tipe="text/javascript">
  $('#id_pegawai').on('change', function() {
    var data = $(".select2 option:selected").val();
    $("#form").submit();
  })
  //datepicker event
  $('#created').datepicker({
    autoclose: true,
    format: 'dd-mm-yyyy'
  }).on('changeDate', function(e) {
    $("#form").submit();
  });
</script>