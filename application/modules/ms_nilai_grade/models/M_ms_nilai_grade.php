<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ms_nilai_grade extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_nilai_grade')
      ->get('ms_nilai_grade',$number,$offset)
      ->result_array();
  }

  function num_rows($search = null){
		if ($search != null) {
      foreach ($search as $key => $val) {
        if($val != ''){
          $this->db->like($key,$val);
        }
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_nilai_grade')
      ->get('ms_nilai_grade')
      ->num_rows();
  }
  
  function num_rows_total(){  
    return $this->db
      ->where('is_deleted','0')
      ->order_by('id_nilai_grade')
      ->get('ms_nilai_grade')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('ms_nilai_grade')->result_array();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id_nilai_grade',$id)->get('ms_nilai_grade')->row_array();
  }

  public function get_first()
  {
    return $this->db->order_by('id_nilai_grade','asc')->get('ms_nilai_grade')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_nilai_grade','desc')->get('ms_nilai_grade')->row();
  }

  public function insert($data)
  {
    $this->db->insert('ms_nilai_grade',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_nilai_grade',$id)->update('ms_nilai_grade',$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id_nilai_grade',$id)->update('ms_nilai_grade',array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_nilai_grade',$id)->delete('ms_nilai_grade');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ms_nilai_grade");
  }

}
