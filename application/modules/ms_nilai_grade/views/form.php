<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-primary">
        <form id="form" class="form-horizontal" action="<?=base_url()?>ms_nilai_grade/<?=$action?>" method="post" autocomplete="off"> 
          <div class="box-body">
            <input type="hidden" class="form-control input-sm" name="id_nilai_grade" id="id_nilai_grade" value="<?php if($nilai_grade != null){echo $nilai_grade['id_nilai_grade'];};?>">
            <div class="form-group">
              <label class="col-sm-2 control-label">Kelompok</label>
              <div class="col-sm-4">
                <input type="text" class="form-control input-sm" name="kelompok" id="kelompok" value="<?php if($nilai_grade != null){echo $nilai_grade['kelompok'];};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Min - Max</label>
              <div class="col-sm-2">
                <input type="text" class="form-control input-sm" name="min" id="min" value="<?php if($nilai_grade != null){echo $nilai_grade['min'];};?>" required>
              </div>
              <div class="col-sm-2">
                <input type="text" class="form-control input-sm" name="max" id="max" value="<?php if($nilai_grade != null){echo $nilai_grade['max'];};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Jabatan</label>
              <div class="col-sm-3">
                <input type="text" class="form-control input-sm" name="kd_jabatan_grade" id="kd_jabatan_grade" value="<?php if($nilai_grade != null){echo $nilai_grade['kd_jabatan_grade'];};?>" required>
              </div>
            </div>
            <?php for($gr=0;$gr<=40;$gr++):?>
            <?php $gr_idx = str_pad($gr,'2','0',STR_PAD_LEFT)?>
            <div class="form-group">
              <label class="col-sm-2 control-label"><?=$gr?></label>
              <div class="col-sm-2">
                <input type="text" class="form-control input-sm" name="gr_<?=$gr_idx?>" id="gr_<?=$gr_idx?>" value="<?php if($nilai_grade != null){echo $nilai_grade['gr_'.$gr_idx];};?>">
              </div>
            </div>
            <?php endfor;?>
            <div class="form-group">
              <label class="col-sm-2 control-label">Aktif</label>
              <div class="col-sm-5">
                <input class="iCheckbox" type="checkbox" name="is_active" value="1" <?php if($nilai_grade != null){if($nilai_grade['is_active'] == 1){echo 'checked';}}else{echo 'checked';}?>>
              </div>
            </div>
            <hr>
            <div class="form-group">
              <div class="col-md-offset-2 col-sm-8">
                <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
                <a class="btn btn-sm btn-default" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Batal</a>
              </div>
            </div>
          </div>
        </form>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<script>
  $(document).ready(function () {
    // form validator
    $('#form').validate();
  })
</script>