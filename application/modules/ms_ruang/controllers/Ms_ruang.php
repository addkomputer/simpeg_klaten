<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ms_ruang extends MY_Controller {

	var $access, $id_grup;

  function __construct(){
		parent::__construct();

		$controller = 'ms_ruang';

    if($this->session->userdata('menu') != $controller){
      $this->session->unset_userdata('search');
      $this->session->set_userdata(array('menu' => 'ms_ruang'));
    }
    $this->load->model('ap_konfigurasi/m_ap_konfigurasi');
		$this->id_grup = $this->session->userdata('id_grup');
		$this->access = $this->m_ap_konfigurasi->get_permission($this->id_grup, $controller);
		if ($this->access == null) {
			redirect(base_url().'ap_error/error_403');
		}
		$this->load->model('m_ms_ruang');
	}
	
	public function index()
	{
		$data['access'] = $this->access;
		$data['subtitle'] = 'Daftar';

		if ($this->access->_read) {
			$config['base_url'] = base_url().$this->access->controller;
			$config['per_page'] = 10;
			
			$search = null;
			if($this->session->userdata('search') != null){
				$search = $this->session->userdata('search');
			}
			
			$from = $this->uri->segment(3);
			if($from = ''){
				$from = 0;
			}
	
			$num_rows = $this->m_ms_ruang->num_rows();

			$config['total_rows'] = $num_rows;
			$this->pagination->initialize($config);

			$data['ruang'] = $this->m_ms_ruang->get_list($config['per_page'],$from,$search);
			
			$data['search'] = $this->session->userdata('search');
			$data['num_rows'] = $num_rows;
			$data['num_rows_total'] = $this->m_ms_ruang->num_rows_total();

			insert_log('read',$this->access->menu);
			$this->view('index',$data);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function search()
	{
		$data = $_POST;
		$this->session->set_userdata(array('search' => $data));
		redirect(base_url().$this->access->controller);
	}

	public function reset_search()
  {
    $this->session->unset_userdata('search');
    redirect(base_url().$this->access->controller);
  }
	
	public function form($id = null)
	{
		$data['title'] = $this->access->menu;
		$data['ruang_list'] = $this->m_ms_ruang->get_all();
		$data['access'] = $this->access;
		if ($id == null) {
			if ($this->access->_create) {
				$data['subtitle'] = 'Tambah Data';
				$data['action'] = 'insert';
				$data['ruang'] = null;
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		} else {
			if ($this->access->_update) {
				$data['subtitle'] = 'Ubah Data';
				$data['action'] = 'update';
				$data['ruang'] = $this->m_ms_ruang->get_by_id($id);
				$this->view('form', $data);
			} else {
				redirect(base_url().'ap_error/error_403');				
			}
		}
	}

	public function insert()
	{
		$data = $_POST;
		if ($data != null) {
			$data['created_by'] = $this->session->userdata('nama');
			if(!isset($data['is_active'])){$data['is_active'] = 0;}
			unset($data['id_ruang_old']);
			$this->m_ms_ruang->insert($data);
			insert_log('insert',$this->access->menu);
			$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil ditambah!</div>');
			redirect(base_url().$this->access->controller);
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function update()
	{
		$data = $_POST;
		if ($data != null) {
			if($this->access->_update){
				$data['updated_by'] = $this->session->userdata('nama');
				if(!isset($data['is_active'])){$data['is_active'] = 0;}
				$id = $data['id_ruang_old'];
				unset($data['id_ruang_old']);
				$this->m_ms_ruang->update($id, $data);
				insert_log('update',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil diubah!</div>');
				redirect(base_url().$this->access->controller);
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		} else {
			redirect(base_url().'ap_error/error_403');
		}
	}

	public function delete()
	{
		$data = $_POST;
		if($data != null){
			if ($this->access->_delete) {
				$this->m_ms_ruang->delete_permanent($data['id_ruang']);
				insert_log('delete',$this->access->menu);
				$this->session->set_flashdata('status', '<div class="callout callout-success callout-dismissable fade in">Data berhasil dihapus!</div>');
				redirect(base_url().$this->access->controller);
			}else{
				redirect(base_url().'ap_error/error_403');
			}
		}else{
			redirect(base_url().'ap_error/error_403');
		}
	}

}