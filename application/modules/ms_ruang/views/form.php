<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
        <small><?=$subtitle?></small>
      </h1>
      <ol class="breadcrumb">
        <li class="active">Master Kepegawaian</li>
        <li class="active text-orange"><?=$access->menu?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box box-primary">
        <div class="box-header with-border">
          <i class="fa fa-table text-maroon"></i>
          <h3 class="box-title">Form <?=$access->menu?></h3>
        </div>
        <form id="form" class="form-horizontal" action="<?=base_url()?>ms_ruang/<?=$action?>" method="post" autocomplete="off">
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-2 control-label">Id</label>
              <div class="col-sm-2">
                <input type="text" class="form-control" name="id_ruang" id="id_ruang" value="<?php if($ruang != null){echo $ruang->id_ruang;};?>" readonly required>
                <input type="hidden" class="form-control" name="id_ruang_old" id="id_ruang_old" value="<?php if($ruang != null){echo $ruang->id_ruang;};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Nama</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="ruang" id="ruang" value="<?php if($ruang != null){echo $ruang->ruang;};?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Aktif</label>
              <div class="col-sm-5">
                <input class="iCheckbox" type="checkbox" name="is_active" value="1" <?php if($ruang != null){if($ruang->is_active == 1){echo 'checked';}}else{echo 'checked';}?>>
              </div>
            </div>
            <hr>
            <div class="form-group">
              <div class="col-md-offset-2 col-sm-8">
                <button class="btn btn-success btn-flat" type="submit"><i class="fa fa-save"></i> Simpan</button>
                <a class="btn btn-default btn-flat" href="<?=base_url().$access->controller?>/index"><i class="fa fa-close"></i> Batal</a>
              </div>
            </div>
          </div>
        </form>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->

<!-- /script -->
<script>
  $(document).ready(function () {
    // form validator
    $('#form').validate();
  })
</script>