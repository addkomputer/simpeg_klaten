<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-<?=$access->icon?> text-light-blue"></i> <?=$access->menu?>
      <small><?=$subtitle?></small>
    </h1>
    <ol class="breadcrumb">
      <li class="active">Master Kepegawaian</li>
      <li class="active text-orange"><?=$access->menu?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <i class="fa fa-table text-maroon"></i>
        <h3 class="box-title">Tabel <?=$access->menu?></h3>
        <div class="box-tools pull-right">
          <span class="badge bg-maroon"><i class="fa fa-bar-chart"></i> Jumlah Keseluruhan Data : <?=$num_rows_total?></span>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-md-1">
            <?php if($access->_create):?>
              <a class="btn btn-primary btn-flat pull-left" href="<?=base_url()?>ms_ruang/form"><i class="fa fa-plus"></i> Tambah</a>
            <?php endif; ?>
          </div>
          <div class="col-md-4 col-md-offset-7">
             <form action="<?=base_url()?>ms_ruang/search" method="post" autocomplete="off">
              <div class="input-group pull-right" style="width: 300px;">
                <input type="text" name="ruang" class="form-control pull-right" placeholder="Pencarian" value="<?php if($search != null){echo $search['ruang'];}?>">
                <div class="input-group-btn">
                  <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-search"></i></button>
                  <a class="btn btn-default btn-flat" href="<?=base_url()?>ms_ruang/reset_search"><i class="fa fa-refresh"></i></a>
                </div>
              </div>
            </form>
          </div>
        </div>
        <br>
        <?php echo $this->session->flashdata('status'); ?>
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
                <th class="text-center" width="50">No</th>
                <th class="text-center" width="60">Aksi</th>
                <!-- <th class="text-center" width="60">Induk</th> -->
                <th class="text-center">Nama</th>
                <th class="text-center" width="50">Aktif</th>
              </tr>
            </thead>
            <tbody>
              <?php if ($ruang != null): ?>
                <?php $i=1;foreach ($ruang as $row): ?>
                  <tr>
                    <td class="text-center"><?= $this->uri->segment(3)+($i++);?></td>
                    <td class="text-center">
                      <?php if($access->_update):?>
                        <a class="btn btn-xs btn-warning btn-flat" href="<?=base_url()?>ms_ruang/form/<?=$row->id_ruang?>"><i class="fa fa-pencil"></i></a> 
                      <?php endif; ?>
                      <?php if($access->_delete):?>
                        <a class="btn btn-xs btn-danger btn-flat" href="#" onclick="del('<?=$row->id_ruang?>')"><i class="fa fa-trash"></i></a>
                      <?php endif; ?>
                    </td>
                    <td><?=$row->ruang?></td>
                    <td class="text-center">
                      <?php if ($row->is_active == 1): ?>
                        <i class="fa fa-check text-green"></i>
                      <?php else: ?>
                        <i class="fa fa-close text-red"></i>
                      <?php endif; ?>
                    </td>
                  </tr>
                <?php endforeach; ?>
              <?php else: ?>
                <tr>
                  <td class="text-center" colspan="99">Tidak ada data!</td>
                </tr>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <span class="badge bg-purple" style="margin-top:6px;">Jumlah Data : <?=$num_rows?></span>
        <?php echo $this->pagination->create_links(); ?>
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- Modal Delete -->
<div id="modal_delete" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="<?=base_url()?>ms_ruang/delete" method="post" autocomplete="off">
        <input type="hidden" name="id_ruang" id="delete_id_ruang">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Data</h4>
        </div>
        <div class="modal-body">
          <p>Anda yakin ingin menghapus data ini?</p>
          <b class="cl-danger">Peringatan!</b>
          <p>Data ini mungkin digunakan atau terhubung dengan data lain.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
          <button type="submit" class="btn btn-danger btn-flat"><i class="fa fa-trash"></i> Hapus</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
  function del(id) {
    $("#modal_delete").modal('show');
    $("#delete_id_ruang").val(id);
  }
</script>