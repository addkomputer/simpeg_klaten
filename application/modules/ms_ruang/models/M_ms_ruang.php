<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ms_ruang extends CI_Model {

	public function get_list($number,$offset,$search = null)
  {
		if ($search != null) {
      if ($search['ruang'] != '') {
        $this->db->like('ruang',$search['ruang']);
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('ruang')
      ->get('ms_ruang',$number,$offset)
      ->result();
  }

  function num_rows($search = null){
		if ($search != null) {
      if ($search['ruang'] != '') {
        $this->db->like('ruang',$search['ruang']);
      }
    }
    
    return $this->db
      ->where('is_deleted','0')
      ->order_by('ruang')
      ->get('ms_ruang')
      ->num_rows();
  }
  
  function num_rows_total(){
    return $this->db
      ->where('is_deleted','0')
      ->order_by('ruang')
      ->get('ms_ruang')
      ->num_rows();
	}

	public function get_all()
	{
		return $this->db
			->where('is_deleted','0')
			->where('is_active','1')
			->get('ms_ruang')->result();
	}

  public function get_by_id($id)
  {
    return $this->db->where('id_ruang',$id)->get('ms_ruang')->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id_ruang','asc')->get('ms_ruang')->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id_ruang','desc')->get('ms_ruang')->row();
  }

  public function insert($data)
  {
    $this->db->insert('ms_ruang',$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id_ruang',$id)->update('ms_ruang',$data);
  }

  public function delete_temp($id)
  {
    $this->db->where('id_ruang',$id)->update('ms_ruang',array('is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id_ruang',$id)->delete('ms_ruang');
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE ms_ruang");
  }

}
