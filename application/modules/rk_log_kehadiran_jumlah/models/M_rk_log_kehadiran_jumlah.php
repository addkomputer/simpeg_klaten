<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_rk_log_kehadiran_jumlah extends CI_Model {

  private $tanggal, $suffix, $bulan, $tahun;

  function __construct(){
		parent::__construct();
    
    if ($this->session->userdata('search')) {
      $this->bulan = $this->session->userdata('search')['bulan'];
      $this->tahun = $this->session->userdata('search')['tahun'];
      $this->suffix = $this->tahun;
    }else{
      $this->bulan = date('m');
      $this->tahun = date('Y');
      $this->suffix = date('Y');
    }
  }
  
	public function get_list($number,$offset,$search = null)
  {
    $suffix = $search['tahun'];
    if ($this->db->table_exists('tb_log_kehadiran_'.$suffix))
    {
      $where = "WHERE ";
      $where .= "a.bulan = '".$search['bulan']."'";
      if ($search != null) {
        if ($search['id_jabatan'] != '') {
          $where .= "AND b.id_jabatan = '".$search['id_jabatan']."'";
        }
        if ($search['id_instansi'] != '') {
          $where .= "AND b.id_instansi = '".$search['id_instansi']."'";
        }
      }
      
      $query = $this->db->query(
        "SELECT 
          SUM(CASE WHEN a.id_tipe LIKE '21' THEN 1 ELSE 0 END) AS dinas_dalam,
          SUM(CASE WHEN a.id_tipe LIKE '22' THEN 1 ELSE 0 END) AS dinas_luar,
          SUM(CASE WHEN a.id_tipe = '3' THEN 1 ELSE 0 END) AS tugas_belajar,
          SUM(CASE WHEN a.id_tipe = '4' THEN 1 ELSE 0 END) AS sakit,
          SUM(CASE WHEN a.id_tipe LIKE '5%' THEN 1 ELSE 0 END) AS cuti,
          SUM(CASE WHEN a.id_tipe = '6' THEN 1 ELSE 0 END) AS bencana,
          SUM(CASE WHEN a.id_tipe = '7' THEN 1 ELSE 0 END) AS ganti,
          SUM(CASE WHEN a.id_tipe = '8' THEN 1 ELSE 0 END) AS apel,
          COUNT(id_tipe) AS total,
          b.id_pegawai, b.nomor_induk, b.gelar_depan, b.nama, b.gelar_belakang,
          c.instansi, d.jabatan, e.status_pegawai
        FROM tb_log_kehadiran_$suffix a
        RIGHT JOIN dt_pegawai b ON a.id_pegawai = b.id_pegawai
        LEFT JOIN ms_instansi c ON b.id_instansi = c.id_instansi
        LEFT JOIN ms_jabatan d ON b.id_jabatan = d.id_jabatan
        LEFT JOIN ms_status_pegawai e ON b.id_status_pegawai = e.id_status_pegawai
        $where
        GROUP BY a.id_pegawai
        LIMIT $offset,$number"
      );

      return $query->result();
    }else{
      return null;
    }
  }

  function num_rows($search = null){
    $suffix = $search['tahun'];
    if ($this->db->table_exists('tb_log_kehadiran_'.$suffix))
    {
      $where = "WHERE ";
      $where .= "a.bulan = '".$search['bulan']."'";
      if ($search != null) {
        if ($search['id_jabatan'] != '') {
          $where .= "AND b.id_jabatan = '".$search['id_jabatan']."'";
        }
        if ($search['id_instansi'] != '') {
          $where .= "AND b.id_instansi = '".$search['id_instansi']."'";
        }
      }
      
      $query = $this->db->query(
        "SELECT 
          SUM(CASE WHEN a.id_tipe LIKE '21' THEN 1 ELSE 0 END) AS dinas_dalam,
          SUM(CASE WHEN a.id_tipe LIKE '22' THEN 1 ELSE 0 END) AS dinas_luar,
          SUM(CASE WHEN a.id_tipe = '3' THEN 1 ELSE 0 END) AS tugas_belajar,
          SUM(CASE WHEN a.id_tipe = '4' THEN 1 ELSE 0 END) AS sakit,
          SUM(CASE WHEN a.id_tipe LIKE '5%' THEN 1 ELSE 0 END) AS cuti,
          SUM(CASE WHEN a.id_tipe = '6' THEN 1 ELSE 0 END) AS bencana,
          SUM(CASE WHEN a.id_tipe = '7' THEN 1 ELSE 0 END) AS ganti,
          SUM(CASE WHEN a.id_tipe = '8' THEN 1 ELSE 0 END) AS apel,
          COUNT(id_tipe) AS total,
          b.id_pegawai, b.nomor_induk, b.gelar_depan, b.nama, b.gelar_belakang,
          c.instansi, d.jabatan, e.status_pegawai
        FROM tb_log_kehadiran_$suffix a
        RIGHT JOIN dt_pegawai b ON a.id_pegawai = b.id_pegawai
        LEFT JOIN ms_instansi c ON b.id_instansi = c.id_instansi
        LEFT JOIN ms_jabatan d ON b.id_jabatan = d.id_jabatan
        LEFT JOIN ms_status_pegawai e ON b.id_status_pegawai = e.id_status_pegawai
        $where
        GROUP BY a.id_pegawai"
      );

      return $query->num_rows();
    }else{
      return null;
    }
  }
  
  function num_rows_total(){
    $suffix = $this->suffix;
    if ($this->db->table_exists('tb_log_kehadiran_'.$suffix))
    {
      $query = $this->db->query(
        "SELECT 
          SUM(CASE WHEN a.id_tipe LIKE '21' THEN 1 ELSE 0 END) AS dinas_dalam,
          SUM(CASE WHEN a.id_tipe LIKE '22' THEN 1 ELSE 0 END) AS dinas_luar,
          SUM(CASE WHEN a.id_tipe = '3' THEN 1 ELSE 0 END) AS tugas_belajar,
          SUM(CASE WHEN a.id_tipe = '4' THEN 1 ELSE 0 END) AS sakit,
          SUM(CASE WHEN a.id_tipe LIKE '5%' THEN 1 ELSE 0 END) AS cuti,
          SUM(CASE WHEN a.id_tipe = '6' THEN 1 ELSE 0 END) AS bencana,
          SUM(CASE WHEN a.id_tipe = '7' THEN 1 ELSE 0 END) AS ganti,
          SUM(CASE WHEN a.id_tipe = '8' THEN 1 ELSE 0 END) AS apel,
          COUNT(id_tipe) AS total,
          b.id_pegawai, b.nomor_induk, b.gelar_depan, b.nama, b.gelar_belakang,
          c.instansi, d.jabatan, e.status_pegawai
        FROM tb_log_kehadiran_$suffix a
        RIGHT JOIN dt_pegawai b ON a.id_pegawai = b.id_pegawai
        LEFT JOIN ms_instansi c ON b.id_instansi = c.id_instansi
        LEFT JOIN ms_jabatan d ON b.id_jabatan = d.id_jabatan
        LEFT JOIN ms_status_pegawai e ON b.id_status_pegawai = e.id_status_pegawai
        WHERE a.bulan = '$this->bulan'
        GROUP BY a.id_pegawai"
      );

      return $query->num_rows();
    }else{
      return null;
    }
  }

  public function get_print($search = null)
  {
    $suffix = $search['tahun'];
    if ($this->db->table_exists('tb_log_kehadiran_'.$suffix))
    {
      $where = "WHERE ";
      $where .= "a.bulan = '".$search['bulan']."'";
      if ($search != null) {
        if ($search['id_jabatan'] != '') {
          $where .= "AND b.id_jabatan = '".$search['id_jabatan']."'";
        }
        if ($search['id_instansi'] != '') {
          $where .= "AND b.id_instansi = '".$search['id_instansi']."'";
        }
      }
      
      $query = $this->db->query(
        "SELECT 
          SUM(CASE WHEN a.id_tipe LIKE '21' THEN 1 ELSE 0 END) AS dinas_dalam,
          SUM(CASE WHEN a.id_tipe LIKE '22' THEN 1 ELSE 0 END) AS dinas_luar,
          SUM(CASE WHEN a.id_tipe = '3' THEN 1 ELSE 0 END) AS tugas_belajar,
          SUM(CASE WHEN a.id_tipe = '4' THEN 1 ELSE 0 END) AS sakit,
          SUM(CASE WHEN a.id_tipe LIKE '5%' THEN 1 ELSE 0 END) AS cuti,
          SUM(CASE WHEN a.id_tipe = '6' THEN 1 ELSE 0 END) AS bencana,
          SUM(CASE WHEN a.id_tipe = '7' THEN 1 ELSE 0 END) AS ganti,
          SUM(CASE WHEN a.id_tipe = '8' THEN 1 ELSE 0 END) AS apel,
          COUNT(id_tipe) AS total,
          b.id_pegawai, b.nomor_induk, b.gelar_depan, b.nama, b.gelar_belakang,
          c.instansi, d.jabatan, e.status_pegawai
        FROM tb_log_kehadiran_$suffix a
        RIGHT JOIN dt_pegawai b ON a.id_pegawai = b.id_pegawai
        LEFT JOIN ms_instansi c ON b.id_instansi = c.id_instansi
        LEFT JOIN ms_jabatan d ON b.id_jabatan = d.id_jabatan
        LEFT JOIN ms_status_pegawai e ON b.id_status_pegawai = e.id_status_pegawai
        $where
        GROUP BY a.id_pegawai"
      );
      return $query->result();
    }else{
      return null;
    }
  }
  
  public function count_category($search)
  {
    $where = "WHERE ";
    if ($search != null) {
      $where .= "b.tanggal = '".date_to_id($search['tanggal'])."'";
      if ($search['id_jabatan'] != '') {
        $where .= "AND c.id_jabatan = '".$search['id_jabatan']."'";
      }
    }else{
      $where .= "a.tanggal = '".$this->tanggal."'";
    }
    
    $query = $this->db->query(
      "SELECT a.id_tipe,a.tipe, count(d.id_tipe) AS total
      FROM tb_log_tipe a 
      LEFT JOIN (
          SELECT b.id_tipe FROM tb_log_kehadiran_$this->suffix b
          LEFT JOIN dt_pegawai c ON b.nomor_induk = c.nomor_induk 
          $where
        ) d on d.id_tipe = a.id_tipe
      GROUP BY 
        a.id_tipe"
    );

    return $query->result();
  }

  public function count_print($search)
  {
    $suffix = $this->suffix;
    if ($this->db->table_exists('tb_log_kehadiran_'.$suffix)){
      return null;
    }else{
      return null;
    }
  }

	public function get_all()
	{
		return $this->db
			->where('is_active','1')
			->get("tb_log_kehadiran_$this->suffix")->result();
  }
  
  public function detail($tanggal,$nomor_induk)
  {
    $data = $this->db->where('nomor_induk',$nomor_induk)->get('dt_pegawai')->row();
    $data->detail = $this->db
      ->join('tb_device b','a.SN = b.device_sn')
      ->like('a.ScanDate',$tanggal)
      ->where('a.nomor_induk',$nomor_induk)
      ->get('tb_scanlog_'.$this->suffix.' a')->result();
    return $data;
  }

  public function get_by_id($id)
  {
    return $this->db->where('id',$id)->get("tb_log_kehadiran_$this->suffix")->row();
  }

  public function get_first()
  {
    return $this->db->order_by('id','asc')->get("tb_log_kehadiran_$this->suffix")->row();
  }

  public function get_last()
  {
    return $this->db->order_by('id','desc')->get("tb_log_kehadiran_$this->suffix")->row();
  }

  public function insert($data)
  {
    $this->db->insert("tb_log_kehadiran_$this->suffix",$data);
  }

  public function update($id,$data)
  {
    $this->db->where('id',$id)->update("tb_log_kehadiran_$this->suffix",$data);
  }

  public function delete_temp($id)
  {
    $updated_by = $this->session->userdata('user_fullname');
    $this->db->where('id',$id)->update("tb_log_kehadiran_$this->suffix",array('updated_by'=>$updated_by, 'is_deleted' => '1'));
  }

  public function delete_permanent($id)
  {
    $this->db->where('id',$id)->delete("tb_log_kehadiran_$this->suffix");
  }

  public function empty_table()
  {
    $this->db->query("TRUNCATE tb_log_kehadiran_$this->suffix");
  }

}
