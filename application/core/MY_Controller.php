<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends MX_Controller{

  function __construct(){
    parent::__construct();
		if($this->session->userdata('logged') == false){
      redirect(base_url().'ap_otentikasi/masuk');
    }else{
      $this->load->model('ap_init/m_ap_init');
    }
  }

  function view($content, $data = NULL){
    // Pagination config
    $config_pagination["full_tag_open"] = '<ul class="pagination pagination-sm no-margin pull-right">';
    $config_pagination["full_tag_close"] = '</ul>';	

    $config_pagination["first_link"] = "&laquo;";
    $config_pagination["first_tag_open"] = "<li>";
    $config_pagination["first_tag_close"] = "</li>";

    $config_pagination["last_link"] = "&raquo;";
    $config_pagination["last_tag_open"] = "<li>";
    $config_pagination["last_tag_close"] = "</li>";

    $config_pagination['next_link'] = '&gt;';
    $config_pagination['next_tag_open'] = '<li>';
    $config_pagination['next_tag_close'] = '</li>';

    $config_pagination['prev_link'] = '&lt;';
    $config_pagination['prev_tag_open'] = '<li>';
    $config_pagination['prev_tag_close'] = '<li>';
    $config_pagination['cur_tag_open'] = '<li class="active"><a href="#">';
    $config_pagination['cur_tag_close'] = '</a></li>';
    $config_pagination['num_tag_open'] = '<li>';
    $config_pagination['num_tag_close'] = '</li>';
    $config_pagination['num_links'] = 5;
    $this->pagination->initialize($config_pagination);
    // Load menu
    $this->load->model('ap_konfigurasi/m_ap_konfigurasi');
    $data['sidenav'] = $this->m_ap_konfigurasi->get_menu();
    // Load profil
    $this->load->model('ap_profil/m_ap_profil');
    $data['profil'] = $this->m_ap_profil->get_first();
    // Templating
    $data['header'] = $this->load->view('ap_template/header', $data, TRUE);
    $data['sidebar'] = $this->load->view('ap_template/sidebar', $data, TRUE);
    $data['topbar'] = $this->load->view('ap_template/topbar', $data, TRUE);
    $data['footer'] = $this->load->view('ap_template/footer', $data, TRUE);
    $data['content'] = $this->load->view($content, $data, TRUE);

    $this->load->view('ap_template/index', $data);
  }

}

class MY_Error extends MX_Controller{

  function render($content, $data = NULL){
    $data['header'] = $this->load->view('ap_template/error/header', $data, TRUE);
    $data['footer'] = $this->load->view('ap_template/error/footer', $data, TRUE);
    $data['content'] = $this->load->view($content, $data, TRUE);

    $this->load->view('ap_template/error/index', $data);
  }

}
